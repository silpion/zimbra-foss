/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2011, 2013, 2014 Zimbra, Inc.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */

package com.zimbra.common.jetty;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.rewrite.handler.Rule;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

public class PortRule extends Rule {

    private static final Logger LOG = Log.getLogger(PortRule.class);
    private int _port;
    protected Pattern _regex;

    private Integer _httpErrorStatusRegexNotmatched;
    private String _httpErrorReasonRegexNotMatched;  // a value on L10nUtil.MsgKey
    private String _resourceBundlePath;
    private ClassLoader _resourceBundleClassloader;

    /* ------------------------------------------------------------ */
    public PortRule()
    {
        _handling = true;
        _terminating = true;
    }

    /* ------------------------------------------------------------ */
    /**
     * Sets the port.
     *
     * @param port the port
     */
    public void setPort(int port)
    {
        _port = port;
    }

    /* ------------------------------------------------------------ */
    /**
     * Sets the regular expression string used to match with string URI.
     *
     * @param regex the regular expression.
     */
    public void setRegex(String regex)
    {
        _regex=Pattern.compile(regex);
    }

    /* ------------------------------------------------------------ */
    /**
     * If the regex is not matched, handle the request with the
     * http status and reason.
     *
     * @param value the replacement string.
     */
    public void setHttpErrorStatusRegexNotMatched(int status)
    {
        _httpErrorStatusRegexNotmatched = status;
    }

    /* ------------------------------------------------------------ */
    /**
     * If the regex is not matched, handle the request with the
     * http status and reason.
     *
     * @param value the replacement string.
     */
    public void setHttpErrorReasonRegexNotMatched(String reason)
    {
        _httpErrorReasonRegexNotMatched = reason;
    }

    public void setResourceBundlePath(String resourceBundlePath) {
        this._resourceBundlePath = resourceBundlePath;
    }

    private ClassLoader getResourceBundleClassloader() throws MalformedURLException {
        if (_resourceBundleClassloader == null) {
            File rbFile = new File(_resourceBundlePath == null ? "/opt/zimbra/conf/msgs" : _resourceBundlePath);
            URL[] urls = new URL[]{rbFile.toURI().toURL()};
            _resourceBundleClassloader = new URLClassLoader(urls);
        }
        return _resourceBundleClassloader;
    }

    @Override
    public String matchAndApply(String target, HttpServletRequest request, HttpServletResponse response)
    throws IOException {
        int port = request.getLocalPort();

        if (port == _port) {
            Matcher matcher=_regex.matcher(target);
            if (!matcher.matches()) {
                return apply(target, request, response);
            }
        }
        return null;
    }

    private String apply(String target, HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        String reason = null;
        if (_httpErrorReasonRegexNotMatched != null) {
            reason = getMessage(_httpErrorReasonRegexNotMatched);
        }

        if (reason == null) {
            response.sendError(_httpErrorStatusRegexNotmatched);
        } else {
            response.sendError(_httpErrorStatusRegexNotmatched, reason);
        }
        return target;
    }

    private String getMessage(String key) {
        ClassLoader loader = null;
        try {
            loader = getResourceBundleClassloader();
        } catch (MalformedURLException e) {
            LOG.warn("malformed url", e);
            return null;
        }
        try {
            ResourceBundle rb = ResourceBundle.getBundle("ZsMsg", Locale.getDefault(), loader);
            return rb.getString(key);
        } catch (MissingResourceException mre) {
            LOG.warn("missing resource", mre);
            return null;
        }
    }

    /**
     * Returns the rule pattern.
     */
    @Override
    public String toString()
    {
        return super.toString()+"["+_port+"]"+"["+ ((_regex != null) ? _regex.toString() : "") +"]"+"["+_httpErrorStatusRegexNotmatched+"]";
    }
}
