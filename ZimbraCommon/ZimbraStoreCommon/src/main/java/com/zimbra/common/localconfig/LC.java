/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015 Zimbra, Inc.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */

package com.zimbra.common.localconfig;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.dom4j.DocumentException;

import com.google.common.base.Strings;
import com.zimbra.common.util.Constants;
import com.zimbra.common.util.ZimbraLog;

/**
 * Provides convenient means to get at local configuration - stuff that we do
 * not want in LDAP.
 * <p>
 * NOTE: When adding a new KnownKey, you do not need to call setDoc. The
 * documentation string will come from the ZsMsg properties file, using the same
 * key as the KnownKey. You can still use setDoc but it is NOT recommended
 * because it will not be able to be translated.
 */
public final class LC {

    public static String get(String key) {
        try {
            return Strings.nullToEmpty(LocalConfig.getInstance().get(key));
        } catch (ConfigException never) {
            assert false : never;
            return "";
        }
    }

    public static String[] getAllKeys() {
        return LocalConfig.getInstance().allKeys();
    }

    /**
     * Reloads the local config file.
     *
     * @throws DocumentException if the config file was syntactically invalid
     * @throws ConfigException if the config file was semantically invalid
     */
    public static void reload() throws DocumentException, ConfigException {
        LocalConfig.load(null);
    }

    static void init() {
        // This method is there to guarantee static initializer of this
        // class is run.
    }

    @Supported
    public static final KnownKey zimbra_home = KnownKey.newKey("/opt/zimbra").protect();
    public static final KnownKey zimbra_java_path = KnownKey.newKey("java");
    @Supported
    public static final KnownKey zimbra_java_home = KnownKey.newKey("${zimbra_home}/${zimbra_java_path}");

    @Supported
    public static final KnownKey zimbra_log_directory = KnownKey.newKey("${zimbra_home}/log");

    @Supported
    public static final KnownKey zimbra_index_directory = KnownKey.newKey("${zimbra_home}/index");

    @Supported
    public static final KnownKey zimbra_store_directory = KnownKey.newKey("${zimbra_home}/store");

    @Supported
    public static final KnownKey zimbra_db_directory = KnownKey.newKey("${zimbra_home}/db");

    @Supported
    public static final KnownKey zimbra_tmp_directory = KnownKey.newKey("${zimbra_home}/data/tmp");

    @Supported
    public static final KnownKey zimbra_extension_directory = KnownKey.newKey("${zimbra_home}/lib/ext");

    @Supported
    public static final KnownKey zimbra_extension_common_directory = KnownKey.newKey("${zimbra_home}/lib/ext-common");

    @Supported
    public static final KnownKey zimbra_libexec_directory = KnownKey.newKey("${zimbra_home}/libexec");

    @Supported
    public static final KnownKey zimbra_mysql_user = KnownKey.newKey("zimbra");

    @Supported
    public static final KnownKey zimbra_mysql_password = KnownKey.newKey("zimbra").protect();

    @Supported
    public static final KnownKey zimbra_mysql_shutdown_timeout = KnownKey.newKey(60);

    @Supported
    public static final KnownKey zimbra_ldap_userdn = KnownKey.newKey("uid=zimbra,cn=admins,cn=zimbra");

    @Supported
    public static final KnownKey zimbra_ldap_user = KnownKey.newKey("zimbra");

    @Supported
    public static final KnownKey zimbra_ldap_password = KnownKey.newKey("zimbra").protect();

    @Supported
    public static final KnownKey zimbra_server_hostname = KnownKey.newKey("localhost");

    @Supported
    public static final KnownKey zimbra_attrs_directory = KnownKey.newKey("${zimbra_home}/conf/attrs");
    public static final KnownKey zimbra_rights_directory = KnownKey.newKey("${zimbra_home}/conf/rights");

    @Supported
    public static final KnownKey zimbra_user = KnownKey.newKey("zimbra");

    @Supported
    public static final KnownKey zimbra_uid = KnownKey.newKey(-1);

    @Supported
    public static final KnownKey zimbra_gid = KnownKey.newKey(-1);

    @Supported
    public static final KnownKey zimbra_log4j_properties = KnownKey.newKey("${zimbra_home}/conf/log4j.properties");

    @Supported
    public static final KnownKey zimbra_admin_service_port = KnownKey.newKey(7071);

    @Supported
    public static final KnownKey zimbra_mail_service_port = KnownKey.newKey(80);

    @Supported
    public static final KnownKey zimbra_zmprov_default_to_ldap = KnownKey.newKey(false);

    public static final KnownKey zmprov_safeguarded_attrs = KnownKey.newKey("zimbraServiceEnabled,zimbraServiceInstalled");

    public static final KnownKey zimbra_require_interprocess_security = KnownKey.newKey(1);

    @Supported
    public static final KnownKey localized_msgs_directory = KnownKey.newKey("${zimbra_home}/conf/msgs");

    @Supported
    public static final KnownKey localized_client_msgs_directory =
        KnownKey.newKey("${mailboxd_directory}/webapps/zimbra/WEB-INF/classes/messages");

    @Supported
    public static final KnownKey skins_directory = KnownKey.newKey("${mailboxd_directory}/webapps/zimbra/skins");

    @Supported
    public static final KnownKey zimbra_store_copy_buffer_size_kb = KnownKey.newKey(16); // KB
    public static final KnownKey zimbra_nio_file_copy_chunk_size_kb = KnownKey.newKey(512); // KB

    @Supported
    public static final KnownKey zimbra_index_max_readers = KnownKey.newKey(35);

    @Supported
    public static final KnownKey zimbra_index_max_writers = KnownKey.newKey(100);

    @Supported
    public static final KnownKey zimbra_index_lucene_merge_policy = KnownKey.newKey(true);

    @Supported
    public static final KnownKey zimbra_index_lucene_min_merge = KnownKey.newKey(1000);

    @Supported
    public static final KnownKey zimbra_index_lucene_max_merge = KnownKey.newKey(Integer.MAX_VALUE);

    @Supported
    public static final KnownKey zimbra_index_lucene_use_compound_file = KnownKey.newKey(true);

    @Supported
    public static final KnownKey zimbra_index_lucene_max_buffered_docs = KnownKey.newKey(200);

    @Supported
    public static final KnownKey zimbra_index_lucene_ram_buffer_size_kb = KnownKey.newKey(10240);

    @Supported
    public static final KnownKey zimbra_index_lucene_term_index_divisor = KnownKey.newKey(1);

    @Supported
    public static final KnownKey zimbra_index_lucene_max_terms_per_query = KnownKey.newKey(50000);

    @Supported
    public static final KnownKey zimbra_index_commit_wait = KnownKey.newKey(1000);

    public static final KnownKey zimbra_index_rfc822address_max_token_length = KnownKey.newKey(256);
    public static final KnownKey zimbra_index_rfc822address_max_token_count = KnownKey.newKey(512);

    @Supported
    public static final KnownKey zimbra_im_chat_flush_time = KnownKey.newKey(300);

    @Supported
    public static final KnownKey zimbra_im_chat_close_time = KnownKey.newKey(3600);

    @Supported
    public static final KnownKey zimbra_http_originating_ip_header = KnownKey.newKey("X-Forwarded-For");

    @Supported
    public static final KnownKey stats_img_folder = KnownKey.newKey("${zimbra_home}/logger/db/work");

    @Reloadable
    public static final KnownKey soap_fault_include_stack_trace = KnownKey.newKey(false);

    @Supported
    public static final KnownKey soap_response_buffer_size = KnownKey.newKey("");

    @Supported
    @Reloadable
    public static final KnownKey soap_response_chunked_transfer_encoding_enabled = KnownKey.newKey(true);
    public static final KnownKey zimbra_servlet_output_stream_buffer_size = KnownKey.newKey(10);


    @Supported
    public static final KnownKey ldap_host = KnownKey.newKey("");

    @Supported
    public static final KnownKey ldap_port = KnownKey.newKey("");

    @Supported
    public static final KnownKey ldap_url = KnownKey.newKey("");

    @Supported
    public static final KnownKey ldap_ldapi_socket_file = KnownKey.newKey("${zimbra_home}/data/ldap/state/run/ldapi");

    @Supported
    public static final KnownKey ldap_master_url = KnownKey.newKey("");
    public static final KnownKey ldap_bind_url = KnownKey.newKey("");;

    @Supported
    public static final KnownKey ldap_is_master = KnownKey.newKey(false);

    @Supported
    public static final KnownKey ldap_root_password = KnownKey.newKey("zimbra").protect();

    @Supported
    public static final KnownKey ldap_connect_timeout = KnownKey.newKey(30000);

    @Supported
    public static final KnownKey ldap_read_timeout = KnownKey.newKey(300000);

    @Supported
    public static final KnownKey ldap_deref_aliases = KnownKey.newKey("always");

    @Supported
    public static final KnownKey ldap_connect_pool_master = KnownKey.newKey(false);

    @Supported
    public static final KnownKey ldap_connect_pool_debug = KnownKey.newKey(false);

    @Supported
    public static final KnownKey ldap_connect_pool_initsize = KnownKey.newKey(1);

    @Supported
    public static final KnownKey ldap_connect_pool_maxsize = KnownKey.newKey(50);

    @Supported
    public static final KnownKey ldap_connect_pool_prefsize = KnownKey.newKey(0);

    @Supported
    public static final KnownKey ldap_connect_pool_timeout = KnownKey.newKey(120000);

    @Supported
    public static final KnownKey ldap_connect_pool_health_check_on_checkout_enabled = KnownKey.newKey(false);

    @Supported
    public static final KnownKey ldap_connect_pool_health_check_background_interval_millis = KnownKey.newKey(30000);

    @Supported
    public static final KnownKey ldap_connect_pool_health_check_max_response_time_millis = KnownKey.newKey(30000);

    @Supported
    public static final KnownKey ldap_replication_password = KnownKey.newKey("zmreplica");

    @Supported
    public static final KnownKey ldap_postfix_password = KnownKey.newKey("zmpostfix");

    @Supported
    public static final KnownKey ldap_amavis_password = KnownKey.newKey("zmamavis");
    public static final KnownKey ldap_nginx_password = KnownKey.newKey("zmnginx");

    @Supported
    public static final KnownKey ldap_starttls_supported = KnownKey.newKey(0);

    @Supported
    public static final KnownKey ldap_starttls_required = KnownKey.newKey(true);

    @Supported
    public static final KnownKey zimbra_directory_max_search_result = KnownKey.newKey(5000);

    public static final KnownKey ldap_common_loglevel = KnownKey.newKey(49152);
    public static final KnownKey ldap_common_require_tls = KnownKey.newKey(0);
    public static final KnownKey ldap_common_threads = KnownKey.newKey(8);
    public static final KnownKey ldap_common_toolthreads = KnownKey.newKey(2);
    public static final KnownKey ldap_common_writetimeout = KnownKey.newKey(360);
    public static final KnownKey ldap_common_tlsprotocolmin = KnownKey.newKey("3.1");
    public static final KnownKey ldap_common_tlsciphersuite = KnownKey.newKey("MEDIUM:HIGH");
    public static final KnownKey ldap_db_maxsize = KnownKey.newKey(85899345920L);
    public static final KnownKey ldap_db_envflags = KnownKey.newKey("writemap nometasync");
    public static final KnownKey ldap_accesslog_maxsize = KnownKey.newKey(85899345920L);
    public static final KnownKey ldap_accesslog_envflags = KnownKey.newKey("writemap nometasync");
    public static final KnownKey ldap_overlay_syncprov_checkpoint = KnownKey.newKey("20 10");
    public static final KnownKey ldap_overlay_syncprov_sessionlog = KnownKey.newKey(10000000L);
    public static final KnownKey ldap_overlay_accesslog_logpurge = KnownKey.newKey("01+00:00  00+04:00");
    public static final KnownKey ldap_monitor_mdb = KnownKey.newKey("true");
    public static final KnownKey ldap_monitor_alert_only = KnownKey.newKey("true");
    public static final KnownKey ldap_monitor_warning = KnownKey.newKey(80);
    public static final KnownKey ldap_monitor_critical = KnownKey.newKey(90);
    public static final KnownKey ldap_monitor_growth = KnownKey.newKey(25);

    public static final KnownKey postjournal_enabled = KnownKey.newKey("false");
    public static final KnownKey postjournal_helo_name = KnownKey.newKey("localhost");
    public static final KnownKey postjournal_reinject_host = KnownKey.newKey(null);
    public static final KnownKey postjournal_archive_host = KnownKey.newKey(null);
    public static final KnownKey postjournal_archive_rcpt_to = KnownKey.newKey("<>");
    public static final KnownKey postjournal_archive_bounce_to = KnownKey.newKey("<>");
    public static final KnownKey postjournal_strip_postfix_proxy = KnownKey.newKey(1);
    public static final KnownKey postjournal_per_user_journaling = KnownKey.newKey(1);
    public static final KnownKey postjournal_smtp_read_timeout = KnownKey.newKey(60);

    @Supported
    public static final KnownKey mysql_directory = KnownKey.newKey("/opt/zimbra/mariadb");

    @Supported
    public static final KnownKey mysql_data_directory = KnownKey.newKey("${zimbra_db_directory}/data");

    @Supported
    public static final KnownKey mysql_socket = KnownKey.newKey("/opt/zimbra/data/tmp/mysql/mysql.sock");

    @Supported
    public static final KnownKey mysql_pidfile = KnownKey.newKey("/opt/zimbra/log/mysql.pid");

    @Supported
    public static final KnownKey mysql_mycnf = KnownKey.newKey("/opt/zimbra/conf/my.cnf");

    @Supported
    public static final KnownKey mysql_errlogfile = KnownKey.newKey("${zimbra_home}/log/mysql_error.log");

    @Supported
    public static final KnownKey mysql_bind_address = KnownKey.newKey(null);

    @Supported
    public static final KnownKey mysql_port = KnownKey.newKey(7306);

    @Supported
    public static final KnownKey mysql_root_password = KnownKey.newKey("zimbra").protect();

    @Supported
    public static final KnownKey mysql_memory_percent = KnownKey.newKey(30);
    public static final KnownKey mysql_innodb_log_buffer_size = KnownKey.newKey(null);
    public static final KnownKey mysql_innodb_log_file_size = KnownKey.newKey(null);
    public static final KnownKey mysql_sort_buffer_size = KnownKey.newKey(null);
    public static final KnownKey mysql_read_buffer_size = KnownKey.newKey(null);

    @Supported
    public static final KnownKey mysql_backup_retention = KnownKey.newKey(0);

    public final static KnownKey logger_data_directory = KnownKey.newKey("${zimbra_home}/logger/db/data");
    public final static KnownKey logger_zmrrdfetch_port = KnownKey.newKey(10663);

    public static final KnownKey cbpolicyd_pid_file = KnownKey.newKey("${zimbra_log_directory}/cbpolicyd.pid");
    public static final KnownKey cbpolicyd_log_file = KnownKey.newKey("${zimbra_log_directory}/cbpolicyd.log");
    public static final KnownKey cbpolicyd_db_file = KnownKey.newKey("${zimbra_home}/data/cbpolicyd/db/cbpolicyd.sqlitedb");
    public static final KnownKey cbpolicyd_cache_file = KnownKey.newKey("${zimbra_home}/data/cache");
    public static final KnownKey cbpolicyd_log_mail = KnownKey.newKey("main");
    public static final KnownKey cbpolicyd_log_detail = KnownKey.newKey("modules");

    public static final KnownKey sqlite_shared_cache_enabled = KnownKey.newKey(false);
    public static final KnownKey sqlite_cache_size = KnownKey.newKey(500);
    public static final KnownKey sqlite_journal_mode = KnownKey.newKey("PERSIST");
    public static final KnownKey sqlite_page_size = KnownKey.newKey(4096);
    public static final KnownKey sqlite_sync_mode = KnownKey.newKey("NORMAL");

    @Supported
    public static final KnownKey mailboxd_directory = KnownKey.newKey("${zimbra_home}/mailboxd");

    @Supported
    public static final KnownKey mailboxd_java_heap_size = KnownKey.newKey(null);

    @Supported
    public static final KnownKey mailboxd_java_heap_new_size_percent = KnownKey.newKey(25);

    @Supported
    public static final KnownKey mailboxd_thread_stack_size = KnownKey.newKey("256k");

    @Supported
    public static final KnownKey mailboxd_java_options = KnownKey.newKey("-server" +
            " -Dhttps.protocols=TLSv1,TLSv1.1,TLSv1.2" +
            " -Djdk.tls.client.protocols=TLSv1,TLSv1.1,TLSv1.2" +
            " -Djava.awt.headless=true" +
            " -Dsun.net.inetaddr.ttl=${networkaddress_cache_ttl}" +
            " -Dorg.apache.jasper.compiler.disablejsr199=true" +
            " -XX:+UseConcMarkSweepGC" +
            " -XX:SoftRefLRUPolicyMSPerMB=1" +
            " -verbose:gc" +
            " -XX:+PrintGCDetails" +
            " -XX:+PrintGCDateStamps" +
            " -XX:+PrintGCApplicationStoppedTime" +
            " -XX:-OmitStackTraceInFastThrow" +
            " -Xloggc:/opt/zimbra/log/gc.log" +
            " -XX:-UseGCLogFileRotation" +
            " -XX:NumberOfGCLogFiles=20" +
            " -XX:GCLogFileSize=4096K");
    @Supported
    public static final KnownKey mailboxd_pidfile = KnownKey.newKey("${zimbra_log_directory}/mailboxd.pid");

    @Supported
    public static final KnownKey mailboxd_keystore = KnownKey.newKey("${mailboxd_directory}/etc/keystore");

    @Supported
    public static final KnownKey mailboxd_keystore_password = KnownKey.newKey("zimbra");

    public static final KnownKey mailboxd_keystore_base = KnownKey.newKey("${zimbra_home}/conf/keystore.base");
    public static final KnownKey mailboxd_keystore_base_password = KnownKey.newKey("zimbra");

    @Supported
    public static final KnownKey mailboxd_truststore = KnownKey.newKey("${zimbra_java_home}/lib/security/cacerts");

    @Supported
    public static final KnownKey mailboxd_truststore_password = KnownKey.newKey("changeit");

    public static final KnownKey mailboxd_output_filename = KnownKey.newKey("zmmailboxd.out");

    @Supported
    public static final KnownKey mailboxd_output_file = KnownKey.newKey("${zimbra_log_directory}/${mailboxd_output_filename}");

    @Supported
    public static final KnownKey mailboxd_output_rotate_interval = KnownKey.newKey(86400);

    @Supported
    public static final KnownKey client_ssl_truststore = KnownKey.newKey("${mailboxd_truststore}");

    @Supported
    public static final KnownKey client_ssl_truststore_password = KnownKey.newKey("${mailboxd_truststore_password}");

    @Supported
    public static final KnownKey ssl_allow_untrusted_certs = KnownKey.newKey(false);

    public static final KnownKey ssl_allow_mismatched_certs = KnownKey.newKey(true);

    public static final KnownKey ssl_allow_accept_untrusted_certs = KnownKey.newKey(true);

    public static final KnownKey ssl_disable_dh_cipher_suite = KnownKey.newKey(true);

    public static final KnownKey ssl_default_digest = KnownKey.newKey("sha256");

    @Supported
    public static final KnownKey zimlet_directory = KnownKey.newKey("${zimbra_home}/zimlets-deployed");

    @Supported
    public static final KnownKey calendar_outlook_compatible_allday_events = KnownKey.newKey(false);

    @Supported
    public static final KnownKey calendar_entourage_compatible_timezones = KnownKey.newKey(true);

    @Supported
    public static final KnownKey exchange_free_busy_interval_min = KnownKey.newKey(15);

    public static final KnownKey calendar_cache_directory = KnownKey.newKey("${zimbra_tmp_directory}/calcache");

    public static final KnownKey spnego_java_options =  KnownKey.newKey(
            "-Djava.security.krb5.conf=${mailboxd_directory}/etc/krb5.ini " +
            "-Djava.security.auth.login.config=${mailboxd_directory}/etc/spnego.conf " +
            "-Djavax.security.auth.useSubjectCredsOnly=false");

    public static final KnownKey milter_bind_port = KnownKey.newKey(0);
    public static final KnownKey milter_bind_address = KnownKey.newKey(null);

    //used only in dev
    public static final KnownKey milter_in_process_mode = KnownKey.newKey(false);

    @Supported
    public static final KnownKey krb5_keytab = KnownKey.newKey("${zimbra_home}/conf/krb5.keytab");

    @Supported
    public static final KnownKey zimbra_mtareport_max_users = KnownKey.newKey(50);

    @Supported
    public static final KnownKey zimbra_mtareport_max_hosts = KnownKey.newKey(50);

    public static final KnownKey zmconfigd_enable_config_restarts = KnownKey.newKey("true");
    public static final KnownKey zmconfigd_interval = KnownKey.newKey(60);
    public static final KnownKey zmconfigd_log_level = KnownKey.newKey(3);
    public static final KnownKey zmconfigd_listen_port = KnownKey.newKey(7171);
    public static final KnownKey zmconfigd_startup_pause = KnownKey.newKey(60);

    public static final KnownKey zimbra_configrewrite_timeout = KnownKey.newKey(120);

    @Supported
    public static final KnownKey zimbra_mailbox_groups = KnownKey.newKey(100);

    /*
    public static final KnownKey zimbra_class_ldap_client = KnownKey.newKey("com.zimbra.cs.ldap.jndi.JNDILdapClient");
    public static final KnownKey zimbra_class_provisioning = KnownKey.newKey("com.zimbra.cs.account.ldap.legacy.LegacyLdapProvisioning");
    */

    public static final KnownKey zimbra_class_ldap_client = KnownKey.newKey("com.zimbra.cs.ldap.unboundid.UBIDLdapClient");
    public static final KnownKey zimbra_class_provisioning = KnownKey.newKey("com.zimbra.cs.account.ldap.LdapProvisioning");


    public static final KnownKey zimbra_class_accessmanager = KnownKey.newKey("com.zimbra.cs.account.accesscontrol.ACLAccessManager");
    public static final KnownKey zimbra_class_mboxmanager = KnownKey.newKey("com.zimbra.cs.mailbox.MailboxManager");
    public static final KnownKey zimbra_class_shareddeliverycoordinator = KnownKey.newKey("");
    public static final KnownKey zimbra_class_database = KnownKey.newKey("com.zimbra.cs.db.MySQL");
    public static final KnownKey zimbra_class_store = KnownKey.newKey("com.zimbra.cs.store.nfs.NFSAwarePosixStoreManager");
    public static final KnownKey zimbra_class_application = KnownKey.newKey("com.zimbra.cs.util.ZimbraApplication");
    public static final KnownKey zimbra_class_rulerewriterfactory = KnownKey.newKey("com.zimbra.cs.filter.RuleRewriterFactory");
    public static final KnownKey zimbra_class_datasourcemanager = KnownKey.newKey("com.zimbra.cs.datasource.DataSourceManager");
    public static final KnownKey zimbra_class_attrmanager = KnownKey.newKey("com.zimbra.cs.account.AttributeManager");
    public static final KnownKey zimbra_class_soapsessionfactory = KnownKey.newKey("com.zimbra.soap.DefaultSoapSessionFactory");
    public static final KnownKey zimbra_class_dbconnfactory = KnownKey.newKey("com.zimbra.cs.db.ZimbraConnectionFactory");
    public static final KnownKey zimbra_class_customproxyselector = KnownKey.newKey(""); //intentionally has no value; set one if u want to use a custom proxy selector
    public static final KnownKey zimbra_class_galgroupinfoprovider = KnownKey.newKey("com.zimbra.cs.gal.GalGroupInfoProvider");
    public static final KnownKey zimbra_class_jsieve_comparators_ascii_casemap = KnownKey.newKey("org.apache.jsieve.comparators.AsciiCasemap");
    public static final KnownKey zimbra_class_two_factor_auth_factory = KnownKey.newKey("com.zimbra.cs.account.auth.twofactor.TwoFactorAuth$DefaultFactory");

    // XXX REMOVE AND RELEASE NOTE
    public static final KnownKey data_source_trust_self_signed_certs = KnownKey.newKey(false);
    public static final KnownKey data_source_fetch_size = KnownKey.newKey(5);
    public static final KnownKey data_source_max_message_memory_size = KnownKey.newKey(2097152); // 2 MB
    public static final KnownKey data_source_new_sync_enabled = KnownKey.newKey(false);
    public static final KnownKey data_source_xsync_class = KnownKey.newKey("");
    public static final KnownKey data_source_xsync_factory_class = KnownKey.newKey("");
    public static final KnownKey data_source_config = KnownKey.newKey("${zimbra_home}/conf/datasource.xml");
    public static final KnownKey data_source_ioexception_handler_class = KnownKey.newKey("com.zimbra.cs.datasource.IOExceptionHandler");

    @Supported
    public static final KnownKey timezone_file = KnownKey.newKey("${zimbra_home}/conf/timezones.ics");
    public static final KnownKey zmstat_interval = KnownKey.newKey(30);
    public static final KnownKey zmstat_disk_interval = KnownKey.newKey(600);
    public static final KnownKey zmstat_max_retention = KnownKey.newKey(0);

    public static final KnownKey zmstat_df_excludes = KnownKey.newKey("");

    public static final KnownKey zmdisklog_warn_threshold = KnownKey.newKey(85);
    public static final KnownKey zmdisklog_critical_threshold = KnownKey.newKey(95);

    public static final KnownKey zimbra_csv_mapping_file = KnownKey.newKey("${zimbra_home}/conf/zimbra-contact-fields.xml");

    public static final KnownKey zimbra_authtoken_cookie_domain = KnownKey.newKey("");
    public static final KnownKey zimbra_zmjava_options = KnownKey.newKey("-Xmx256m" +
            " -Dhttps.protocols=TLSv1,TLSv1.1,TLSv1.2" +
            " -Djdk.tls.client.protocols=TLSv1,TLSv1.1,TLSv1.2");
    public static final KnownKey zimbra_zmjava_java_library_path = KnownKey.newKey("");
    public static final KnownKey zimbra_zmjava_java_ext_dirs = KnownKey.newKey("");
    public static final KnownKey debug_xmpp_disable_client_tls = KnownKey.newKey(0);
    public static final KnownKey im_dnsutil_dnsoverride = KnownKey.newKey("");

    /**
     * {@code true} to use Zimbra's MIME parser implementation
     * ({@code com.zimbra.common.mime.shim.JavaMailMimeMessage}),
     * otherwise use JavaMail's default implementation
     * ({@code javax.mail.internet.MimemMessage}).
     */
    public static final KnownKey javamail_zparser = KnownKey.newKey(true);

	public static final KnownKey mime_max_recursion = KnownKey.newKey(20);
	public static final KnownKey mime_promote_empty_multipart = KnownKey.newKey(true);
    public static final KnownKey mime_handle_nonprintable_subject = KnownKey.newKey(true);
    public static final KnownKey mime_encode_compound_xwiniso2022jp_as_iso2022jp = KnownKey.newKey(true);
    public static final KnownKey mime_split_address_at_semicolon = KnownKey.newKey(true);

    public static final KnownKey yauth_baseuri = KnownKey.newKey("https://login.yahoo.com/WSLogin/V1");

    @Supported
    public static final KnownKey httpclient_internal_connmgr_max_host_connections = KnownKey.newKey(100);
    @Supported
    public static final KnownKey httpclient_external_connmgr_max_host_connections = KnownKey.newKey(100);

    @Supported
    public static final KnownKey httpclient_internal_connmgr_max_total_connections = KnownKey.newKey(300);
    @Supported
    public static final KnownKey httpclient_external_connmgr_max_total_connections = KnownKey.newKey(300);

    public static final KnownKey httpclient_internal_connmgr_stale_connection_check = KnownKey.newKey(true);
    public static final KnownKey httpclient_external_connmgr_stale_connection_check = KnownKey.newKey(true);

    public static final KnownKey httpclient_internal_connmgr_tcp_nodelay = KnownKey.newKey(false);
    public static final KnownKey httpclient_external_connmgr_tcp_nodelay = KnownKey.newKey(false);

    public static final KnownKey httpclient_internal_connmgr_connection_timeout = KnownKey.newKey(25 * Constants.MILLIS_PER_SECOND);
    public static final KnownKey httpclient_external_connmgr_connection_timeout = KnownKey.newKey(25 * Constants.MILLIS_PER_SECOND);

    public static final KnownKey httpclient_internal_connmgr_so_timeout = KnownKey.newKey(60 * Constants.MILLIS_PER_SECOND);
    public static final KnownKey httpclient_external_connmgr_so_timeout = KnownKey.newKey(45 * Constants.MILLIS_PER_SECOND);

    public static final KnownKey httpclient_internal_client_connection_timeout = KnownKey.newKey(30 * Constants.MILLIS_PER_SECOND);
    public static final KnownKey httpclient_external_client_connection_timeout = KnownKey.newKey(30 * Constants.MILLIS_PER_SECOND);

    public static final KnownKey httpclient_internal_connmgr_idle_reaper_sleep_interval = KnownKey.newKey(5 * Constants.MILLIS_PER_MINUTE);
    public static final KnownKey httpclient_external_connmgr_idle_reaper_sleep_interval = KnownKey.newKey(5 * Constants.MILLIS_PER_MINUTE);

    public static final KnownKey httpclient_internal_connmgr_idle_reaper_connection_timeout = KnownKey.newKey(5 * Constants.MILLIS_PER_MINUTE);
    public static final KnownKey httpclient_external_connmgr_idle_reaper_connection_timeout = KnownKey.newKey(5 * Constants.MILLIS_PER_MINUTE);

    public static final KnownKey httpclient_soaphttptransport_retry_count = KnownKey.newKey(2);
    public static final KnownKey httpclient_soaphttptransport_so_timeout = KnownKey.newKey(300 * Constants.MILLIS_PER_SECOND);
    public static final KnownKey httpclient_soaphttptransport_keepalive_connections = KnownKey.newKey(true);

    /**
     * Bug: 47051 Known key for the CLI utilities SOAP HTTP transport timeout.
     * The default value is set to 0 i.e. no timeout.
     */
    public static final KnownKey cli_httpclient_soaphttptransport_so_timeout  = KnownKey.newKey(0);


    public static final KnownKey httpclient_convertd_so_timeout = KnownKey.newKey(-1);
    public static final KnownKey httpclient_convertd_keepalive_connections = KnownKey.newKey(true);

    @Supported
    public static final KnownKey client_use_system_proxy = KnownKey.newKey(false);

    @Supported
    public static final KnownKey client_use_native_proxy_selector = KnownKey.newKey(false);

    public static final KnownKey shared_mime_info_globs = KnownKey.newKey("${zimbra_home}/conf/globs2");
    public static final KnownKey shared_mime_info_magic = KnownKey.newKey("${zimbra_home}/conf/magic");

    public static final KnownKey xmpp_server_tls_enabled = KnownKey.newKey(true);

    public static final KnownKey xmpp_server_dialback_enabled = KnownKey.newKey(true);

    public static final KnownKey xmpp_server_session_allowmultiple = KnownKey.newKey(true);

    public static final KnownKey xmpp_server_session_idle = KnownKey.newKey(20 * 60 * 1000);

    public static final KnownKey xmpp_server_session_idle_check_time = KnownKey.newKey(5 * 60 * 1000);

    public static final KnownKey xmpp_server_processing_core_threads = KnownKey.newKey(2);

    public static final KnownKey xmpp_server_processing_max_threads = KnownKey.newKey(50);

    public static final KnownKey xmpp_server_processing_queue = KnownKey.newKey(50);

    public static final KnownKey xmpp_server_outgoing_max_threads = KnownKey.newKey(20);

    public static final KnownKey xmpp_server_outgoing_queue = KnownKey.newKey(50);

    public static final KnownKey xmpp_server_read_timeout = KnownKey.newKey(3 * 60 * 1000);

    public static final KnownKey xmpp_server_socket_remoteport = KnownKey.newKey(5269);

    public static final KnownKey xmpp_server_compression_policy = KnownKey.newKey("disabled");

    public static final KnownKey xmpp_server_certificate_verify = KnownKey.newKey(false);

    public static final KnownKey xmpp_server_certificate_verify_chain = KnownKey.newKey(true);

    public static final KnownKey xmpp_server_certificate_verify_root = KnownKey.newKey(true);

    public static final KnownKey xmpp_server_certificate_verify_validity = KnownKey.newKey(true);

    public static final KnownKey xmpp_server_certificate_accept_selfsigned = KnownKey.newKey(true);

    public static final KnownKey xmpp_muc_enabled = KnownKey.newKey(true);

    public static final KnownKey xmpp_muc_service_name = KnownKey.newKey("conference");

    public static final KnownKey xmpp_muc_discover_locked = KnownKey.newKey(true);

    public static final KnownKey xmpp_muc_restrict_room_creation = KnownKey.newKey(false);

    public static final KnownKey xmpp_muc_room_create_jid_list = KnownKey.newKey("");

    public static final KnownKey xmpp_muc_unload_empty_hours = KnownKey.newKey(5);

    public static final KnownKey xmpp_muc_sysadmin_jid_list = KnownKey.newKey("");

    public static final KnownKey xmpp_muc_idle_user_sweep_ms = KnownKey.newKey(5 * Constants.MILLIS_PER_MINUTE);

    public static final KnownKey xmpp_muc_idle_user_timeout_ms = KnownKey.newKey(0);

    public static final KnownKey xmpp_muc_log_sweep_time_ms = KnownKey.newKey(5 * Constants.MILLIS_PER_MINUTE);

    public static final KnownKey xmpp_muc_log_batch_size = KnownKey.newKey(50);

    public static final KnownKey xmpp_muc_default_history_type = KnownKey.newKey("number");

    public static final KnownKey xmpp_muc_history_number = KnownKey.newKey(25);

    public static final KnownKey xmpp_private_storage_enabled = KnownKey.newKey(true);

    public static final KnownKey xmpp_client_compression_policy = KnownKey.newKey("optional");

    public static final KnownKey xmpp_client_write_timeout = KnownKey.newKey(60 * Constants.MILLIS_PER_SECOND);

    public static final KnownKey xmpp_session_conflict_limit = KnownKey.newKey(0);

    public static final KnownKey xmpp_client_idle_timeout = KnownKey.newKey(10 * 60 * 1000);

    public static final KnownKey xmpp_cloudrouting_idle_timeout = KnownKey.newKey(5 * Constants.MILLIS_PER_MINUTE);

    public static final KnownKey xmpp_offline_type = KnownKey.newKey("store_and_drop");

    public static final KnownKey xmpp_offline_quota = KnownKey.newKey(100 * 1024);

    public static final KnownKey xmpp_dns_override = KnownKey.newKey("");

    public static final KnownKey zmailbox_message_cachesize = KnownKey.newKey(1);

    public static final KnownKey freebusy_queue_directory = KnownKey.newKey("${zimbra_home}/fbqueue/");
    public static final KnownKey freebusy_exchange_cn1 = KnownKey.newKey(null);
    public static final KnownKey freebusy_exchange_cn2 = KnownKey.newKey(null);
    public static final KnownKey freebusy_exchange_cn3 = KnownKey.newKey(null);

    public static final KnownKey data_source_scheduling_enabled = KnownKey.newKey(true);
    public static final KnownKey data_source_eas_sync_email = KnownKey.newKey(true);
    public static final KnownKey data_source_eas_sync_contacts = KnownKey.newKey(true);
    public static final KnownKey data_source_eas_sync_calendar = KnownKey.newKey(true);
    public static final KnownKey data_source_eas_sync_tasks = KnownKey.newKey(true);
    public static final KnownKey data_source_eas_window_size = KnownKey.newKey(50);
    public static final KnownKey data_source_eas_mime_truncation = KnownKey.newKey(4);

    @Supported
    public static final KnownKey socks_enabled = KnownKey.newKey(false);

    public static final KnownKey socket_connect_timeout = KnownKey.newKey(30000);

    @Supported
    public static final KnownKey socket_so_timeout = KnownKey.newKey(30000);

    public static final KnownKey networkaddress_cache_ttl = KnownKey.newKey(60);

    public static final KnownKey zdesktop_local_account_id = KnownKey.newKey(null);

    @Supported
    public static final KnownKey out_of_disk_error_unix = KnownKey.newKey("No space left on device");

    @Supported
    public static final KnownKey out_of_disk_error_windows = KnownKey.newKey("There is not enough space on the disk");

    @Supported
    public static final KnownKey antispam_mysql_enabled = KnownKey.newKey(false);
    public static final KnownKey antispam_mysql_directory = KnownKey.newKey("${zimbra_home}/mta/mariadb");
    public static final KnownKey antispam_mysql_data_directory = KnownKey.newKey("${zimbra_home}/data/amavisd/mysql/data");
    public static final KnownKey antispam_mysql_errlogfile = KnownKey.newKey("${zimbra_home}/log/antispam-mysqld.log");
    public static final KnownKey antispam_mysql_mycnf = KnownKey.newKey("${zimbra_home}/conf/antispam-my.cnf");
    public static final KnownKey antispam_mysql_pidfile = KnownKey.newKey("${zimbra_home}/data/amavisd/mysql/mysql.pid");
    public static final KnownKey antispam_mysql_host = KnownKey.newKey(null);
    public static final KnownKey antispam_mysql_port = KnownKey.newKey(7308);
    public static final KnownKey antispam_mysql_socket = KnownKey.newKey("${zimbra_home}/data/amavisd/mysql/mysql.sock");
    public static final KnownKey antispam_mysql_user = KnownKey.newKey("zimbra");
    public static final KnownKey antispam_mysql_root_password = KnownKey.newKey("");
    public static final KnownKey antispam_mysql_password = KnownKey.newKey("");

    public static final KnownKey av_notify_domain = KnownKey.newKey("");

    @Supported
    public static final KnownKey av_notify_user = KnownKey.newKey("");
    public static final KnownKey postfix_mail_owner = KnownKey.newKey("postfix");
    public static final KnownKey postfix_setgid_group = KnownKey.newKey("postdrop");

    // LDAP Custom DIT base DN for LDAP app admin entries
    public static final KnownKey ldap_dit_base_dn_appadmin      = KnownKey.newKey("");
    // LDAP Custom DIT base DN for config branch
    public static final KnownKey ldap_dit_base_dn_config        = KnownKey.newKey("");
    //LDAP Custom DIT base DN for cos entries
    public static final KnownKey ldap_dit_base_dn_cos           = KnownKey.newKey("");
    //LDAP Custom DIT base DN for global dynamicgroup entries
    public static final KnownKey ldap_dit_base_dn_global_dynamicgroup = KnownKey.newKey("");
    //LDAP Custom DIT base DN for domain entries
    public static final KnownKey ldap_dit_base_dn_domain        = KnownKey.newKey("");
    // LDAP Custom DIT base DN for mail(accounts, aliases, DLs, resources) entries
    public static final KnownKey ldap_dit_base_dn_mail          = KnownKey.newKey("");
    // LDAP Custom DIT base DN for mime entries"
    public static final KnownKey ldap_dit_base_dn_mime          = KnownKey.newKey("");
    // LDAP Custom DIT base DN for server entries
    public static final KnownKey ldap_dit_base_dn_server        = KnownKey.newKey("");
    // LDAP Custom DIT base DN for alwaysOnCluster entries
    public static final KnownKey ldap_dit_base_dn_alwaysoncluster        = KnownKey.newKey("");
    // LDAP Custom DIT base DN for uncservice entries
    public static final KnownKey ldap_dit_base_dn_ucservice     = KnownKey.newKey("");
    // LDAP Custom DIT base DN for share locator entries
    public static final KnownKey ldap_dit_base_dn_share_locator = KnownKey.newKey("");
    // LDAP Custom DIT base DN for xmpp component entries
    public static final KnownKey ldap_dit_base_dn_xmppcomponent = KnownKey.newKey("");
    // LDAP Custom DIT base DN for zimlet entries
    public static final KnownKey ldap_dit_base_dn_zimlet        = KnownKey.newKey("");
    // LDAP Custom DIT RDN attr for cos entries
    public static final KnownKey ldap_dit_naming_rdn_attr_cos          = KnownKey.newKey("");
    // LDAP Custom DIT RDN attr for dynamicgroup entries
    public static final KnownKey ldap_dit_naming_rdn_attr_dynamicgroup = KnownKey.newKey("");
    // LDAP Custom DIT RDN attr for globalconfig entry
    public static final KnownKey ldap_dit_naming_rdn_attr_globalconfig = KnownKey.newKey("");
    // LDAP Custom DIT RDN attr for globalgrant entry
    public static final KnownKey ldap_dit_naming_rdn_attr_globalgrant  = KnownKey.newKey("");
    // LDAP Custom DIT RDN attr for mime entries
    public static final KnownKey ldap_dit_naming_rdn_attr_mime         = KnownKey.newKey("");
    // LDAP Custom DIT RDN attr for server entries
    public static final KnownKey ldap_dit_naming_rdn_attr_server       = KnownKey.newKey("");
    // LDAP Custom DIT RDN attr for ucservice entries
    public static final KnownKey ldap_dit_naming_rdn_attr_ucservice    = KnownKey.newKey("");
    public static final KnownKey ldap_dit_naming_rdn_attr_user         = KnownKey.newKey("");
    public static final KnownKey ldap_dit_naming_rdn_attr_share_locator= KnownKey.newKey("");
    // LDAP Custom DIT RDN attr for xmpp component entries
    public static final KnownKey ldap_dit_naming_rdn_attr_xmppcomponent= KnownKey.newKey("");
    // LDAP Custom DIT RDN attr for zimlet entries
    public static final KnownKey ldap_dit_naming_rdn_attr_zimlet       = KnownKey.newKey("");
    // LDAP Custom DIT base DN for LDAP admin entries
    public static final KnownKey ldap_dit_base_dn_admin         = KnownKey.newKey("");

    @Supported
    public static final KnownKey zmprov_tmp_directory = KnownKey.newKey("${zimbra_tmp_directory}/zmprov");

    public static final KnownKey command_line_editing_enabled = KnownKey.newKey(true);

    public static final KnownKey robots_txt = KnownKey.newKey("${zimbra_home}/conf/robots.txt");

    //appliance
    public static final KnownKey zimbra_vami_user = KnownKey.newKey("vmware");
    public static final KnownKey zimbra_vami_password = KnownKey.newKey("vmware").protect();
    public static final KnownKey zimbra_vami_installmode = KnownKey.newKey("single");

    public enum PUBLIC_SHARE_VISIBILITY { samePrimaryDomain, all, none };

    /**
     * Added for Bug 99825 which relates to the security implications of public shares being visible to users
     * in different domains.  If nothing else, it provided a means for harvesting details of accounts in other
     * domains which happen to have shared folders publicly.
     * This setting affects the behavior of the account namespace SOAP GetShareInfoRequest
     * Possible values:
     * "samePrimaryDomain" - Accounts can only see public shares advertised by accounts whose primary domain
     *                       name matches the primary domain name of the requesting account.
     * "all"               - Accounts with this set can see public shares advertised by any accounts.
     * "none"              - Accounts with this set cannot get details of any public shares.
     *  Note that this doesn't prevent mounting of shares which are discovered out of band (e.g. via invitation
     *  messages).  Shares which are already in use will also be reported.
     */
    @Supported
    public static final KnownKey public_share_advertising_scope =
        KnownKey.newKey(PUBLIC_SHARE_VISIBILITY.samePrimaryDomain.toString());

    public static PUBLIC_SHARE_VISIBILITY getPublicShareAdvertisingScope() {
        String value = LC.public_share_advertising_scope.value();
        try {
            return PUBLIC_SHARE_VISIBILITY.valueOf(value);
        } catch (Exception ex) {
            ZimbraLog.misc.warn("Bad LC setting %s=%s causing exception '%s'.  Using '%s' as value.",
                    LC.public_share_advertising_scope.key(), LC.public_share_advertising_scope.value(),
                    ex.getMessage(), PUBLIC_SHARE_VISIBILITY.none);
            return PUBLIC_SHARE_VISIBILITY.none;
        }
    }

 //Triton integration
    public static final KnownKey triton_store_url = KnownKey.newKey("");
    public static final KnownKey triton_hash_type = KnownKey.newKey("SHA0");
    public static final KnownKey triton_upload_buffer_size = KnownKey.newKey(25000);

    public static final KnownKey octopus_public_static_folder = KnownKey.newKey("${zimbra_home}/jetty/static");

     // See LdapServerSetFactory.ServerSetType
    public static final KnownKey ldap_client_server_set_type = KnownKey.newKey("FailoverServerSet");

    static {
        // Automatically set the key name with the variable name.
        for (Field field : LC.class.getFields()) {
            if (field.getType() == KnownKey.class) {
                assert(Modifier.isPublic(field.getModifiers()));
                assert(Modifier.isStatic(field.getModifiers()));
                assert(Modifier.isFinal(field.getModifiers()));
                try {
                    KnownKey key = (KnownKey) field.get(null);
                    // Automatically set the key name with the variable name.
                    key.setKey(field.getName());

                    // process annotations
                    if(field.isAnnotationPresent(Supported.class)) {
                        key.setSupported(true);
                    }
                    if(field.isAnnotationPresent(Reloadable.class)) {
                        key.setReloadable(true);
                    }

                } catch (Throwable never) {
                    assert false : never;
                }
            }
        }
    }

    /**
     * Used to apply the reloadable flag to a KnownKey in LC
     * @author jpowers
     *
     */
    @Target({ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Reloadable {
    }

    /**
     * This annotation represents a supported local config setting. To make a new
     * setting show up in the zmlocalconfig -i command, use this annotation
     *
     * @author jpowers
     *
     */
    @Target({ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Supported {

    }
}
