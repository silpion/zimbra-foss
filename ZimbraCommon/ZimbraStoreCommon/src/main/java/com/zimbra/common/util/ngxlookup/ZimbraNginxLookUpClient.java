/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2013, 2014 Zimbra, Inc.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */

package com.zimbra.common.util.ngxlookup;

import java.io.IOException;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpVersion;
import org.apache.commons.httpclient.methods.GetMethod;

import com.zimbra.common.httpclient.HttpClientUtil;
import com.zimbra.common.service.ServiceException;
import com.zimbra.common.servicelocator.RoundRobinSelector;
import com.zimbra.common.servicelocator.Selector;
import com.zimbra.common.servicelocator.ServiceLocator;
import com.zimbra.common.servicelocator.ZimbraServiceNames;
import com.zimbra.common.util.ZimbraHttpConnectionManager;
import com.zimbra.common.util.ZimbraLog;

public class ZimbraNginxLookUpClient {
    public static Selector<ServiceLocator.Entry> mailServerSelector = new RoundRobinSelector<>();
    public static Selector<ServiceLocator.Entry> nginxLookupSelector = new RoundRobinSelector<>();
    private static final String urlExtension = "/service/extension/nginx-lookup";
    private static final String ngxPassword = "_password_";
    private ServiceLocator serviceLocator;

    public ZimbraNginxLookUpClient(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public ServiceLocator.Entry getUpstreamMailServer() throws ServiceException {
        try {
            ServiceLocator.Entry entry = serviceLocator.findOne(ZimbraServiceNames.MAILSTORE, mailServerSelector, null, true);
            if (entry == null) {
                throw ServiceException.FAILURE("All Upstream Mail Servers are unavailable", null);
            }
            return entry;
        } catch (IOException e) {
            throw ServiceException.FAILURE("Failed contacting service locator to find an upstream mailstore", e);
        }
    }

    private ServiceLocator.Entry getNginxRouteHandler() throws ServiceException {
        try {
            String serviceName = ZimbraServiceNames.getNameForMailstoreExtension("nginx-lookup");
            ServiceLocator.Entry entry = serviceLocator.findOne(serviceName, nginxLookupSelector, null, true);
            if (entry == null) {
                throw ServiceException.FAILURE("All nginx lookup handlers are unavailable", null);
            }
            return entry;
        } catch (IOException e) {
            throw ServiceException.FAILURE("Failed contacting service locator to find an upstream mailstore", e);
        }
    }

    /**
     * @return a non-null NginxAuthServer
     * @throws ServiceException if the nginx lookup servlet could not be reached, or if it returned an error
     */
    public NginxAuthServer getRouteforAccount(String userName, String authMethod, String authProtocol, String clientIP,
            String proxyIP, String virtualHost) throws ServiceException {
        ServiceLocator.Entry nginxLookUpHandler = getNginxRouteHandler();
        ZimbraLog.misc.debug("getting route for account %s with handler %s", userName, nginxLookUpHandler);

        String scheme = nginxLookUpHandler.tags.contains("ssl") ? "https" : "http";
        String url = (new StringBuilder(scheme).append("://")
                .append(nginxLookUpHandler.hostName)
                .append(":").append(nginxLookUpHandler.servicePort)
                .append(urlExtension)).toString();
        GetMethod method = new GetMethod(url);

        method.setRequestHeader("Auth-Method", authMethod);
        method.setRequestHeader("Auth-User", userName);
        method.setRequestHeader("Auth-Pass", ngxPassword);
        method.setRequestHeader("Auth-Protocol", authProtocol);
        method.setRequestHeader("Auth-Login-Attempt", "0"); // for web requests, login attempts is always 0
        method.setRequestHeader("X-Proxy-IP", proxyIP);
        method.setRequestHeader("Client-IP", clientIP);
        method.setRequestHeader("X-Proxy-Host", virtualHost);
        HttpClient client = ZimbraHttpConnectionManager.getInternalHttpConnMgr().newHttpClient();
        // currently we use default httpclient_internal_connmgr_connection_timeout instead of ngxConnectTimeout
        client.getParams().setParameter("http.protocol.version", HttpVersion.HTTP_1_0);

        try {
            int statusCode = HttpClientUtil.executeMethod(client, method);
            Header authStatusHeader = method.getResponseHeader("Auth-Status");
            if (authStatusHeader == null) {
                String message = method.getResponseBodyAsString();
                ZimbraLog.misc.error("Response missing Auth-Status header; url %s is probably not a mailstore with the nginx-lookup extension\r\n(%s)", statusCode, url, message);
                throw ServiceException.FAILURE(message, null);
            }
            String authStatus = authStatusHeader.getValue();
            if (statusCode == 200 && "OK".equals(authStatus)) {
                return new NginxAuthServer(method.getResponseHeader("Auth-Server").getValue(), method.getResponseHeader("Auth-Port").getValue(),
                        method.getResponseHeader("Auth-User").getValue());
            } else {
                String message = method.getResponseBodyAsString();
            	ZimbraLog.misc.warn("unexpected return %d Auth-Status=%s\r\n%s", statusCode, authStatus, message);
            	throw ServiceException.FAILURE(message, null);
            }
        } catch (IOException e) {
            String message = "Failed contacting nginx lookup service";
            ZimbraLog.misc.warn(message, e);
            throw ServiceException.FAILURE(message, e);
        } finally {
            method.releaseConnection();
        }
    }
}
