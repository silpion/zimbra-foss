/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2015 Zimbra Software, LLC.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * ***** END LICENSE BLOCK *****
 */
package com.zimbra.common.servicelocator;

import java.io.IOException;

import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Test;

import com.zimbra.common.service.ServiceException;

/**
 * Unit test for {@link ChainedServiceLocator}.
 */
public final class ChainedServiceLocatorTest {

    /** Regression test bug 98788 - ServiceException(code=NOT_FOUND) is authoritative, don't try next in chain */
    @Test
    public void testBug98788() throws IOException, ServiceException {
        // Setup expectations
        ServiceLocator first = EasyMock.createMock(ServiceLocator.class);
        ServiceLocator second = EasyMock.createMock(ServiceLocator.class);
        EasyMock.expect(first.isHealthy("foo", "bar")).andThrow(ServiceException.NOT_FOUND("No such thing"));
        EasyMock.replay(first, second);

        // Test
        ChainedServiceLocator serviceLocator = new ChainedServiceLocator(first, second);
        try {
            serviceLocator.isHealthy("foo", "bar");
            Assert.fail("Expected ServiceException");
        } catch (ServiceException e) {
            Assert.assertEquals(ServiceException.NOT_FOUND, e.getCode());
        }
    }
}
