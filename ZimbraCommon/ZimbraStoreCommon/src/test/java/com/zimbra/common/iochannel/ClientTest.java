/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2015 Zimbra, Inc.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
package com.zimbra.common.iochannel;

import java.util.ArrayList;
import java.util.Collection;

import junit.framework.Assert;

import org.junit.Test;

import com.zimbra.common.iochannel.Client.PeerServer;

public class ClientTest {

    @Test
    public void testGetPeerServers() throws Exception {
        Config config = new Config() {
            final int PORT = 1234;

            @Override
            public ServerConfig getLocalConfig() {
                return new ServerConfig("localhost", "localhost", PORT);
            }

            @Override
            public Collection<ServerConfig> getPeerServers() {
                ArrayList<ServerConfig> peers = new ArrayList<ServerConfig>();
                peers.add(new ServerConfig("peer1", "localhost", PORT));
                peers.add(new ServerConfig("peer2", "localhost", PORT));
                peers.add(new ServerConfig("peer3", "localhost", PORT));
                return peers;
            }
        };
        Client client = Client.start(config);
        Collection<PeerServer> peerServers = client.getPeerServers();
        Assert.assertEquals(3, peerServers.size());
    }
}
