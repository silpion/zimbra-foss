/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2015 Zimbra Software, LLC.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * ***** END LICENSE BLOCK *****
 */
package com.zimbra.common.servicelocator;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Unit test for {@link RoundRobinSelector}.
 */
public final class RoundRobinSelectorTest {

    @Test
    public void testBasic() throws Exception {
        // Setup test case
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");

        // Test
        Selector<String> selector = new RoundRobinSelector<>();
        Assert.assertEquals("one", selector.selectOne(list));
        Assert.assertEquals("two", selector.selectOne(list));
        Assert.assertEquals("three", selector.selectOne(list));
        Assert.assertEquals("one", selector.selectOne(list));
    }

    @Test
    public void testDeleteLastBeforeSelectLast() throws Exception {
        // Setup test case
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");

        // Test
        Selector<String> selector = new RoundRobinSelector<>();
        Assert.assertEquals("one", selector.selectOne(list));
        Assert.assertEquals("two", selector.selectOne(list));
        list.remove("three");
        Assert.assertEquals("one", selector.selectOne(list));
    }

    @Test
    public void testAddBeforeSelectLast() throws Exception {
        // Setup test case
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");

        // Test
        Selector<String> selector = new RoundRobinSelector<>();
        Assert.assertEquals("one", selector.selectOne(list));
        Assert.assertEquals("two", selector.selectOne(list));
        list.add("three");
        Assert.assertEquals("three", selector.selectOne(list));
        Assert.assertEquals("one", selector.selectOne(list));
    }

    @Test
    public void testDeleteNonLast() throws Exception {
        // Setup test case
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");

        // Test
        Selector<String> selector = new RoundRobinSelector<>();
        Assert.assertEquals("one", selector.selectOne(list));
        list.remove("two");
        Assert.assertEquals("three", selector.selectOne(list));
        Assert.assertEquals("one", selector.selectOne(list));
    }

    /**
     * Selector needs to work without getting tied to a particular collection. It needs to track
     * values, and work across multiple calls to a service locator returning a new collection each time.
     */
    @Test
    public void testClonedList() throws Exception {
        // Setup test case
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        Selector<String> selector = new RoundRobinSelector<>();
        Assert.assertEquals("one", selector.selectOne(list));
        Assert.assertEquals("two", selector.selectOne(list));

        // Perform test
        List<String> newList = new ArrayList<>(list);
        Assert.assertEquals("three", selector.selectOne(newList));
        Assert.assertEquals("one", selector.selectOne(newList));
    }

    @Test
    public void testClonedListWithNewEntry() throws Exception {
        // Setup test case
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");

        Selector<String> selector = new RoundRobinSelector<>();
        Assert.assertEquals("one", selector.selectOne(list));
        Assert.assertEquals("two", selector.selectOne(list));

        List<String> newList = new ArrayList<>(list);
        newList.add("four");

        // Perform test
        Assert.assertEquals("three", selector.selectOne(newList));
        Assert.assertEquals("four", selector.selectOne(newList));
        Assert.assertEquals("one", selector.selectOne(newList));
    }

    @Test
    public void testClonedListWithNewAndRemovedEntries() throws Exception {
        // Setup test case
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");

        Selector<String> selector = new RoundRobinSelector<>();
        Assert.assertEquals("one", selector.selectOne(list));
        Assert.assertEquals("two", selector.selectOne(list));

        List<String> newList = new ArrayList<>(list);
        newList.add("four");
        newList.remove("two");

        // Perform test
        Assert.assertEquals("three", selector.selectOne(newList));
        Assert.assertEquals("four", selector.selectOne(newList));
        Assert.assertEquals("one", selector.selectOne(newList));
        Assert.assertEquals("three", selector.selectOne(newList));
    }
}
