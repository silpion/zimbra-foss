package com.zimbra.common.iochannel;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import com.zimbra.common.consul.CatalogRegistration.Service;
import com.zimbra.common.service.ServiceException;
import com.zimbra.common.servicelocator.Selector;
import com.zimbra.common.servicelocator.ServiceLocator;

public class MyServiceLocator implements ServiceLocator {
    Config testConfig;

    public MyServiceLocator(Config testConfig) {
        this.testConfig = testConfig;
    }

    @Override
    public void deregister(String serviceID) throws IOException, ServiceException {

    }

    @Override
    public void deregisterSilent(String serviceID) {
    }

    @Override
    public List<Entry> find(String serviceName, String tag, boolean healthyOnly) throws IOException, ServiceException {
        ServiceLocator.Entry entry = new ServiceLocator.Entry(testConfig.getLocalConfig().host, InetAddress.getByName(testConfig.getLocalConfig().host).getHostAddress(), testConfig.getLocalConfig().port);
        List<Entry> list = new ArrayList<>();
        list.add(entry);
        return list;
    }

    @Override
    public Entry findOne(String serviceName, Selector<Entry> selector, String tag, boolean healthyOnly) throws IOException, ServiceException {
        List<Entry> list = find(serviceName, tag, true);
        if (list.isEmpty() && !healthyOnly) {
            list = find(serviceName, tag, false);
        }
        if (list.isEmpty()) {
            throw ServiceException.NOT_FOUND("Failed locating an instance of " + serviceName);
        }

        return selector.selectOne(list);
    }

    @Override
    public boolean isHealthy(String serviceID, String hostName) throws IOException, ServiceException {
        return false;
    }

    @Override
    public void ping() throws IOException {
    }

    @Override
    public void register(Service service) throws IOException, ServiceException {
    }

    @Override
    public void registerSilent(Service service) {
    }

}
