Summary:            Zimbra's Mod Security build
Name:               zimbra-modsecurity
Version:            2.9.0
Release:            1zimbra9.0b1
License:            Apache-2.0
Source:             %{name}-%{version}.tar.gz
BuildRequires:      zimbra-httpd-devel >= 2.4.12-1zimbra9.0b1
BuildRequires:      zimbra-apr-devel >= 1.5.1-1zimbra9.0b1
BuildRequires:      zimbra-apr-util-devel >= 1.5.4-1zimbra9.0b1
BuildRequires:      zimbra-libxml2-devel >= 2.9.2-1zimbra9.0b1
BuildRequires:      expat-devel, pcre-devel, zlib-devel
Requires:           expat, pcre, zlib
Requires:           zimbra-apr-libs >= 1.5.1-1zimbra9.0b1
Requires:           zimbra-apr-util-libs >= 1.5.4-1zimbra9.0b1
Requires:           zimbra-libxml2-libs >= 2.9.2-1zimbra9.0b1
AutoReqProv:        no
URL:                https://www.modsecurity.org/

%description
The Zimbra Mod Security build

%prep
%setup -n modsecurity-%{version}

%build
LDFLAGS="-Wl,-rpath,/opt/zimbra/common/lib"; export LDFLAGS; \
CFLAGS="-O0 -g"; export CFLAGS; \
./configure --prefix=/opt/zimbra/common \
--enable-standalone-module \
--disable-mlogc \
--with-apxs=/opt/zimbra/common/bin/apxs \
--with-apr=/opt/zimbra/common/bin/apr-1-config \
--with-apu=/opt/zimbra/common/bin/apu-1-config \
--with-libxml=/opt/zimbra/common/bin/xml2-config
make

%install
make install DESTDIR=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT/opt/zimbra/common/lib/mod_security2.so
rm -rf $RPM_BUILD_ROOT/opt/zimbra/common/lib/apache2

%package devel
Summary:        Modsecurity Development
Requires: zimbra-modsecurity= %{version}-%{release}
AutoReqProv:        no

%description devel
The zimbra-modsecurity-devel package contains the linking libraries

%files
%defattr(-,root,root)
%exclude /opt/zimbra/common/bin
/opt/zimbra/common/lib/*.so

%files devel
/opt/zimbra/common/lib/*.a
/opt/zimbra/common/lib/*.la
