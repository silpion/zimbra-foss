Summary:            Zimbra's Apache HTTPD build
Name:               zimbra-httpd
Version:            VERSION
Release:            ITERATIONZAPPEND
License:            Apache-2.0
Source:             %{name}-%{version}.tar.bz2
BuildRequires:      zimbra-apr-devel
BuildRequires:      zimbra-apr-util-devel
BuildRequires:      zlib-devel
BuildRequires:      pcre-devel
Requires:           zlib, pcre
Requires:           zimbra-apr-libs
Requires:           zimbra-apr-util-libs
AutoReqProv:        no
URL:                http://httpd.apache.org/

%description
The Zimbra Apache HTTPD build

%prep
%setup -n httpd-%{version}

%build
LDFLAGS="-Wl,-rpath,OZCL"; export LDFLAGS; \
CFLAGS="-O2 -g"; export CFLAGS; \
./configure --prefix=OZC \
  --with-apr=OZCB/apr-1-config \
  --with-apr-util=OZCB/apu-1-config \
  --mandir=OZCS/man \
  --libexecdir=OZCL/apache2/modules \
  --datadir=/opt/zimbra/data/httpd
make

%install
make install DESTDIR=${RPM_BUILD_ROOT}

%package devel
Summary:        Apache HTTPD Development
Requires: zimbra-httpd = %{version}-%{release}
Requires: zimbra-apr-devel
Requires: zimbra-apr-util-devel
AutoReqProv:        no

%description devel
The zimbra-httpd-devel package contains the linking libraries and include files

%files
%defattr(-,root,root)
OZCB
OZCS
OZCL/apache2/modules
/opt/zimbra/data
%exclude /opt/zimbra/data/httpd/build
%exclude OZCB/apxs

%files devel
%defattr(-,root,root)
OZCI
/opt/zimbra/data/httpd/build
OZC/conf
OZCB/apxs
