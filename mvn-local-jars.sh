#!/bin/sh

cd ZimbraCommon/jars-bootstrap

#Zimbra patched jars
mvn install:install-file -Dfile=tnef-1.8.0.jar -DgroupId=net.freeutils.jtnef -DartifactId=tnef -Dversion=1.8.0 -Dpackaging=jar
mvn install:install-file -Dfile=ical4j-0.9.16-patched.jar -DgroupId=ical4j -DartifactId=ical4j -Dversion=0.9.16-patched -Dpackaging=jar
mvn install:install-file -Dfile=ant-1.7.0-ziputil-patched.jar -DgroupId=com.zimbra -DartifactId=ant-1.7.0-ziputil-patched -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=ant-tar-patched.jar -DgroupId=com.zimbra -DartifactId=ant-tar-patched -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=yuicompressor-2.4.2-zimbra.jar -DgroupId=com.yahoo.platform.yui -DartifactId=yuicompressor -Dversion=2.4.2-zimbra -Dpackaging=jar
mvn install:install-file -Dfile=nekohtml-1.9.13.1z.jar -DgroupId=net.sourceforge.nekohtml -DartifactId=nekohtml -Dversion=1.9.13.1z -Dpackaging=jar

#not patched, but not in Maven central
mvn install:install-file -Dfile=junixsocket-1.3.jar -DgroupId=org.newsclub -DartifactId=junixsocket -Dversion=1.3 -Dpackaging=jar
mvn install:install-file -Dfile=gifencoder.jar -DgroupId=net.jmge -DartifactId=gifencoder -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=jcharset.jar -DgroupId=net.freeutils.charset -DartifactId=jcharset -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=libidn-1.24.jar -DgroupId=org.gnu.inet -DartifactId=libidn -Dversion=1.24 -Dpackaging=jar
mvn install:install-file -Dfile=syslog4j-0.9.46-bin.jar -DgroupId=org.syslog4j -DartifactId=syslog4j -Dversion=0.9.46 -Dpackaging=jar

#this is our jar, but source is currently not in Perforce so we manually install jar
mvn install:install-file -Dfile=qless-0.2.1.jar -DgroupId=com.zimbra.qless -DartifactId=qless-java -Dversion=0.2.1 -Dpackaging=jar

#we generate this from WSDL, could be part of build process
mvn install:install-file -Dfile=ews_2010.jar -DgroupId=com.zimbra -DartifactId=ews -Dversion=2010 -Dpackaging=jar

