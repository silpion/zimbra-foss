/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Web Client
 * Copyright (C) 2014 Zimbra, Inc.
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://www.zimbra.com/license
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
 * have been added to cover use of software over a computer network and provide for limited attribution 
 * for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 * See the License for the specific language governing rights and limitations under the License. 
 * The Original Code is Zimbra Open Source Web Client. 
 * The Initial Developer of the Original Code is Zimbra, Inc. 
 * All portions of the code are Copyright (C) 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK *****
 */
tinymce.addI18n('lt',{
"Cut": "I\u0161kirpti",
"Header 2": "Antra\u0161t\u0117 2",
"Your browser doesn't support direct access to the clipboard. Please use the Ctrl+X\/C\/V keyboard shortcuts instead.": "Nar\u0161ykl\u0117s nustatymai neleid\u017eia redaktoriui tiesiogiai pasiekti laikinosios atminties. Pra\u0161ome naudoti klaviat\u016bros klavi\u0161us Ctrl+X\/C\/V.",
"Div": "Div",
"Paste": "\u012ed\u0117ti",
"Close": "U\u017edaryti",
"Pre": "Pre",
"Align right": "Lygiuoti de\u0161in\u0117je",
"New document": "Naujas dokumentas",
"Blockquote": "Citata",
"Numbered list": "Skaitmeninis s\u0105ra\u0161as",
"Increase indent": "Didinti \u012ftrauk\u0105",
"Formats": "Formatai",
"Headers": "Antra\u0161t\u0117s",
"Select all": "Pa\u017eym\u0117ti visk\u0105",
"Header 3": "Antra\u0161t\u0117 3",
"Blocks": "Blokai",
"Undo": "Atstatyti",
"Strikethrough": "Perbrauktas",
"Bullet list": "\u017denklinimo s\u0105ra\u0161as",
"Header 1": "Antra\u0161t\u0117 1",
"Superscript": "Vir\u0161utinis indeksas",
"Clear formatting": "Naikinti formatavim\u0105",
"Subscript": "Apatinis indeksas",
"Header 6": "Antra\u0161t\u0117 6",
"Redo": "Gr\u0105\u017einti",
"Paragraph": "Paragrafas",
"Ok": "Gerai",
"Bold": "Pary\u0161kintas",
"Code": "Kodas",
"Italic": "Kursyvinis",
"Align center": "Centruoti",
"Header 5": "Antra\u0161t\u0117 5",
"Decrease indent": "Ma\u017einti \u012ftrauk\u0105",
"Header 4": "Antra\u0161t\u0117 4",
"Paste is now in plain text mode. Contents will now be pasted as plain text until you toggle this option off.": "Dabar \u012fterpiama paprastojo teksto re\u017eimu. Kol \u0161i parinktis \u012fjungta, turinys bus \u012fterptas kaip paprastas tekstas.",
"Underline": "Pabrauktas",
"Cancel": "Atsisakyti",
"Justify": "I\u0161d\u0117styti per vis\u0105 plot\u012f",
"Inline": "Inline",
"Copy": "Kopijuoti",
"Align left": "Lygiuoti kair\u0117je",
"Visual aids": "Vaizdin\u0117s priemon\u0117s",
"Lower Greek": "Ma\u017eosios graik\u0173",
"Square": "Kvadratas",
"Default": "Pagrindinis",
"Lower Alpha": "Ma\u017eosios raid\u0117s",
"Circle": "Apskritimas",
"Disc": "Diskas",
"Upper Alpha": "Did\u017eiosios raid\u0117s",
"Upper Roman": "Did\u017eiosios rom\u0117n\u0173",
"Lower Roman": "Ma\u017eosios rom\u0117n\u0173",
"Emoticons": "Jaustukai",
"Horizontal space": "Horizontalus tarpas",
"Insert\/edit image": "\u012eterpti|Tvarkyti paveiksl\u0117l\u012f",
"General": "Pagrindinis",
"Advanced": "I\u0161pl\u0117stas",
"Source": "Pirmin\u0117 nuoroda",
"Border": "R\u0117melis",
"Constrain proportions": "Taikyti proporcijas",
"Vertical space": "Vertikalus tarpas",
"Image description": "Paveiksl\u0117lio apra\u0161as",
"Style": "Stilius",
"Dimensions": "Matmenys",
"Insert image": "\u012eterpti paveiksl\u0117l\u012f",
"Remove link": "\u0160alinti nuorod\u0105",
"Url": "Nuoroda",
"Text to display": "Rodomas tekstas",
"Insert link": "\u012eterpti nuorod\u0105",
"New window": "Naujas langas",
"None": "Niekas",
"Target": "Tikslin\u0117 nuoroda",
"Insert\/edit link": "\u012eterpti\/taisyti nuorod\u0105",
"Show invisible characters": "Rodyti nematomus simbolius"
});