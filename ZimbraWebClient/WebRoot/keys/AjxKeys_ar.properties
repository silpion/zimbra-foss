# 
# ***** BEGIN LICENSE BLOCK *****
# Zimbra Collaboration Suite Web Client
# Copyright (C) 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014 Zimbra, Inc.
# 
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software Foundation,
# version 2 of the License.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
# ***** END LICENSE BLOCK *****
#

# Keyboard Shortcuts for the Zimbra Ajax Toolkit
#
# Below is a list of properties that define shortcuts. Each shortcut belongs
# to a "map", which is the context in which the shortcut applies. For these
# shortcuts, that is typically determined by which widget (a button or a list,
# for example) currently has focus.
#
# The property key consists of several parts joined by dots. The first part
# is always the name of a map. The second part is either the name of an action,
# or the name of a field. Field names are lowercase, and action names are mixed
# case. The final part of the key may be a platform.
#
# Maps
# ----
#
# The map names in this properties file refer to widgets (basic UI pieces)
# in the toolkit. The map name is the first part of the property name and
# comes before the period. The following are valid map names:
#
#	dialog					a dialog box (typically has OK/Cancel buttons)
#	button					a pressable button, which may have a dropdown submenu
#	list					a list view of a set of items
#	menu					a menu of possible choices, may have submenus
#	toolbarHorizontal		a horizontal set of widgets (usually buttons)
#	toolbarVertical			a vertical set of widgets (usually buttons)
#
# There is a special map called "keys" that provides information about the
# keyboard being used. It does not define any actual shortcuts.
#
# Fields
# ------
#
#	display			What the user must type to run the shortcut
#	keycode			Keyboard codes for the shortcut
#	description		Explanatory text
#
# Actions
# -------
#
# An action is an event triggered by a shortcut. It is what the shortcut
# does. Most of the time, the action invoked by a toolkit shortcut emulates
# something that could have been done using the mouse. Note that an action
# may be implemented by more than one widget; exactly what happens depends
# on the widget. To see what actions are available and what each action does,
# check the documentation within the shortcut definitions below.
#
# The action "INHERIT" has special meaning. When it is used, the name of a
# map is given rather than a key sequence. The current map will copy all the
# shortcuts of the given map, and then may add or override those shortcuts.
# In general, you probably do not want to change those properties.
#    
# To define a shortcut that applies only on a particular platform (Windows,
# Macintosh, or Linux), add a platform identifier to the action. The platform
# identifier can be one of:
#
#    win mac linux
#
# For example:
#
#    list.SubMenu.display.mac = ,; Shift+,; Ctrl+M
#    list.SubMenu.keycode.mac = 188; Shift+188; Ctrl+77
#    
# Key Sequences (shortcuts)
# -------------
#
# A key sequence is a set of one or more keys that triggers an action. Each
# key in the sequence may have a modifier (such as the Control or Shift key).
# Most key sequences consist of just one key. The keys in multiple-key
# sequences are separated with a comma. The next key in a sequence must be
# struck within a short time for the sequence to continue.
#
# The following are valid modifiers:
#
#    Ctrl Alt Shift Meta
#
# To add a modifier to a key, specify the modifier, then a plus sign, then the
# key. For example: Ctrl+C. If you want to add more than one modifier, use
# another plus sign. For example: Ctrl+Alt+Del.
#
# When using a modifier as part of the "display" property (which is what the user
# sees as how to enter the shortcut), put the modifier in lower case in curly
# brackets (but leave it as Shift, Ctrl, Alt, or Meta in the "keycode" property).
# For example:
#
#   list.SelectAll.display = {ctrl}+A
#   list.SelectAll.keycode = Ctrl+65
#
# That will accomplish two things:
#
#   1. Translators will need to translate the modifier only once. (For example,
#      "Ctrl" on a German keyboard is "Strg".
#
#   2. If the modifier varies by platform, the appropriate one will be shown. For
#      example, the "Meta" key on a PC is "Windows" and on a Mac it's "Cmd".
#
# If you want to have more than one shortcut for the same action, use a 
# semicolon (and optional space) to separate the shortcuts. For example, to
# specify three different shortcuts for the list action Foo:
#
#    list.Foo.display 		= A; B; C 
#    list.Foo.keycode		= 65; 66; 67
# 
# Each key sequence must be defined in two ways. The 'display' version is used
# on the Shortcuts page to tell the user how to run the shortcut:
#
#    dialog.Cancel.display = Esc
#
# The 'keycode' version is used to match the keystroke to the shortcut by looking
# at the numeric keycode sent by the keyboard:
#
#    dialog.Cancel.keycode = 27
#
# Key sequences have no notion of upper case or lower case. They map to what
# you see on your keyboard (for example, a "T"), rather than the character it
# produces when you strike it (a "t"). To specify a keystroke that requires the
# Shift key, you must use the Shift modifier. For example, to specify the "@"
# key, you'd use: Shift+2.
#
# Each letter, number, and non-shifted printable character represents itself:
#
#    A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 0 1 2 3 4 5 6 7 8 9
#    ` - = [ ] ; ' , . /
#
# Non-printable keys are specified with special names. The following special
# keys are available:
#
#    Home End Esc Del Backspace Enter ArrowUp ArrowDown ArrowLeft ArrowRight Space
#
#
# Documentation
# -------------
#
# Maps and actions can be documented by appending ".description" to the map
# name or the action and using that as a property name. The descriptive
# text is the property's value. The descriptions below show up as content in
# the Shortcuts tab on the Options page.
#
# Since properties are unordered, we need to provide a hint about the order in
# which we want them to appear. They will appear in low-to-high order of the value
# for the ".sort" version of the property. That applies to both maps and
# actions. By default, the sort values increment by ten so that it's easy
# to insert a new item between two others without a ripple effect.

# Map: keys

keys.shift.display = Shift
keys.shift.keycode = 16
keys.ctrl.display = Ctrl
keys.ctrl.keycode = 17
keys.alt.display = Alt
keys.alt.keycode = 18
keys.meta.display = Meta
keys.meta.display.win = Windows
keys.meta.display.mac = Cmd
keys.meta.keycode.win = 91
keys.meta.keycode.mac = 224


# Map: dialog

dialog.description = \u0627\u0644\u0627\u062e\u062a\u0635\u0627\u0631\u0627\u062a \u0641\u064a \u0645\u0631\u0628\u0639\u0627\u062a \u0627\u0644\u062d\u0648\u0627\u0631
dialog.sort = 40000

dialog.Cancel.display = Esc
dialog.Cancel.keycode = 27
dialog.Cancel.description = \u0625\u0644\u063a\u0627\u0621 \u0627\u0644\u062a\u063a\u064a\u064a\u0631\u0627\u062a \u0641\u064a \u0645\u0631\u0628\u0639 \u0627\u0644\u062d\u0648\u0627\u0631 \u200e(\u0646\u0641\u0633 \u0648\u0638\u064a\u0641\u0629 \u0627\u0644\u0632\u0631 "\u0625\u0644\u063a\u0627\u0621")
dialog.Cancel.sort = 40020

dialog.Enter.display = \u0625\u062f\u062e\u0627\u0644; \u0645\u0633\u0627\u0641\u0629
dialog.Enter.keycode = 13; 32
dialog.Enter.description = \u062d\u0641\u0638 \u0627\u0644\u062a\u063a\u064a\u064a\u0631\u0627\u062a \u0641\u064a \u0645\u0631\u0628\u0639 \u0627\u0644\u062d\u0648\u0627\u0631 \u200e(\u0646\u0641\u0633 \u0648\u0638\u064a\u0641\u0629 \u0627\u0644\u0632\u0631 "\u0645\u0648\u0627\u0641\u0642")
dialog.Enter.sort = 40010

dialog.Yes.display = \u0646
dialog.Yes.keycode = 89
dialog.Yes.description = \u0627\u0644\u0631\u062f "\u0646\u0639\u0645"
dialog.Yes.sort = 40030

dialog.No.display = \u0644
dialog.No.keycode = 78
dialog.No.description = \u0627\u0644\u0631\u062f "\u0644\u0627"
dialog.No.sort = 40040

# Map: button

button.description = \u0627\u062e\u062a\u0635\u0627\u0631\u0627\u062a \u0627\u0644\u0623\u0632\u0631\u0627\u0631
button.sort = 10000

button.Select.display = \u0625\u062f\u062e\u0627\u0644\u061b \u0645\u0633\u0627\u0641\u0629
button.Select.keycode = 13; 32
button.Select.description = \u0627\u0636\u063a\u0637 \u0639\u0644\u0649 \u0627\u0644\u0632\u0631
button.Select.sort = 10010

button.SubMenu.display = \u060c\u061b {ctrl}+Enter\u061b {ctrl}+Space\u061b {shift}+F10\u061b \u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0633\u0641\u0644
button.SubMenu.keycode = 188\u061b Ctrl+13\u061b Ctrl+32\u061b Shift+121\u061b 40
button.SubMenu.display.mac = \u060c\u061b {ctrl}+Enter\u061b {ctrl}+Space\u061b \u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0633\u0641\u0644
button.SubMenu.keycode.mac = 188\u061b Ctrl+13\u061b Ctrl+32\u061b 40
button.SubMenu.description = \u0639\u0631\u0636 \u0627\u0644\u0642\u0627\u0626\u0645\u0629 \u0627\u0644\u0645\u0646\u0628\u062b\u0642\u0629 \u0644\u0644\u0632\u0631
button.SubMenu.sort = 10020;

# Map: list

list.description = \u0627\u0644\u0627\u062e\u062a\u0635\u0627\u0631\u0627\u062a \u0641\u064a \u0627\u0644\u0642\u0648\u0627\u0626\u0645
list.sort = 30000

list.AddNext.display = {shift}+\u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0633\u0641\u0644
list.AddNext.keycode = Shift+40
list.AddNext.description = \u0625\u0636\u0627\u0641\u0629 \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u062a\u0627\u0644\u064a \u0625\u0644\u0649 \u0627\u0644\u062a\u062d\u062f\u064a\u062f
list.AddNext.sort = 30090

list.AddPrevious.display = {shift}+\u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0639\u0644\u0649
list.AddPrevious.keycode = Shift+38
list.AddPrevious.description = \u0625\u0636\u0627\u0641\u0629 \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u0633\u0627\u0628\u0642 \u0625\u0644\u0649 \u0627\u0644\u062a\u062d\u062f\u064a\u062f
list.AddPrevious.sort = 30100

list.DoubleClick.display = \u0625\u062f\u062e\u0627\u0644
list.DoubleClick.keycode = 13
list.DoubleClick.description = \u0627\u0646\u0642\u0631 \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u0645\u062d\u062f\u062f \u0646\u0642\u0631\u064b\u0627 \u0645\u0632\u062f\u0648\u062c\u064b\u0627
list.DoubleClick.sort = 30070

list.Next.display = {ctrl}+\u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0633\u0641\u0644
list.Next.keycode = Ctrl+40
list.Next.description = \u0627\u0644\u062a\u0631\u0643\u064a\u0632 \u0639\u0644\u0649 \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u062a\u0627\u0644\u064a \u0628\u062f\u0648\u0646 \u062a\u062d\u062f\u064a\u062f
list.Next.sort = 30110

list.Previous.display = {ctrl}+\u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0639\u0644\u0649
list.Previous.keycode = Ctrl+38
list.Previous.description = \u0627\u0644\u062a\u0631\u0643\u064a\u0632 \u0639\u0644\u0649 \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u0633\u0627\u0628\u0642 \u0628\u062f\u0648\u0646 \u062a\u062d\u062f\u064a\u062f
list.Previous.sort = 30120

list.SelectAll.display = {ctrl}+A
list.SelectAll.keycode = Ctrl+65
list.SelectAll.description = \u062a\u062d\u062f\u064a\u062f \u0643\u0627\u0641\u0629 \u0627\u0644\u0639\u0646\u0627\u0635\u0631 \u0627\u0644\u0645\u0631\u0626\u064a\u0629
list.SelectAll.sort = 30030

list.SelectCurrent.display = {ctrl}+`
list.SelectCurrent.keycode = Ctrl+192
list.SelectCurrent.description = \u062a\u062d\u062f\u064a\u062f/\u0625\u0644\u063a\u0627\u0621 \u062a\u062d\u062f\u064a\u062f \u0627\u0644\u0639\u0646\u0635\u0631 \u0645\u062d\u0644 \u0627\u0644\u062a\u0631\u0643\u064a\u0632
list.SelectCurrent.sort = 30060

list.SelectFirst.display = \u0627\u0644\u0635\u0641\u062d\u0629 \u0627\u0644\u0631\u0626\u064a\u0633\u064a\u0629
list.SelectFirst.display.mac = \u0627\u0644\u0635\u0641\u062d\u0629 \u0627\u0644\u0631\u0626\u064a\u0633\u064a\u0629; {meta}+\u0627\u0644\u0633\u0647\u0645 \u0644\u0644\u064a\u0633\u0627\u0631
list.SelectFirst.keycode = 36
list.SelectFirst.keycode.mac = 36; Meta+37
list.SelectFirst.description = \u062a\u062d\u062f\u064a\u062f \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u0623\u0648\u0644
list.SelectFirst.sort = 30040

list.SelectLast.display = \u0627\u0644\u0646\u0647\u0627\u064a\u0629
list.SelectLast.display.mac = \u0627\u0644\u0635\u0641\u062d\u0629 \u0627\u0644\u0631\u0626\u064a\u0633\u064a\u0629; {meta}+\u0627\u0644\u0633\u0647\u0645 \u0644\u0644\u064a\u0645\u064a\u0646
list.SelectLast.keycode = 35
list.SelectLast.keycode.mac = 36; Meta+39
list.SelectLast.description = \u062a\u062d\u062f\u064a\u062f \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u0623\u062e\u064a\u0631
list.SelectLast.sort = 30050

list.SelectNext.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0633\u0641\u0644; \u0645\u0633\u0627\u0641\u0629; J
list.SelectNext.keycode = 40; 32; 74
list.SelectNext.description = \u062a\u062d\u062f\u064a\u062f \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u062a\u0627\u0644\u064a
list.SelectNext.sort = 30010

list.SelectPrevious.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0639\u0644\u0649; K
list.SelectPrevious.keycode = 38; 75
list.SelectPrevious.description = \u062a\u062d\u062f\u064a\u062f \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u0633\u0627\u0628\u0642
list.SelectPrevious.sort = 30020

list.PageUp.display = PageUp
list.PageUp.display.mac = \u0635\u0641\u062d\u0629 \u0644\u0623\u0639\u0644\u0649\u061b {alt}+\u0645\u0641\u062a\u0627\u062d \u0627\u0644\u0633\u0647\u0645 \u0644\u0644\u0623\u0639\u0644\u0649
list.PageUp.keycode = 33
list.PageUp.keycode.mac = 33\u061b Alt+38
list.PageUp.description = \u0627\u0644\u062a\u0645\u0631\u064a\u0631 \u0635\u0641\u062d\u0629 \u0648\u0627\u062d\u062f\u0629 \u0644\u0623\u0639\u0644\u0649
list.PageUp.sort = 30023

list.PageDown.display = PageDown
list.PageDown.display.mac = \u0635\u0641\u062d\u0629 \u0644\u0623\u0633\u0641\u0644\u061b {alt}+\u0645\u0641\u062a\u0627\u062d \u0627\u0644\u0633\u0647\u0645 \u0644\u0644\u0623\u0633\u0641\u0644
list.PageDown.keycode = 34
list.PageDown.keycode.mac = 34\u061b Alt+40
list.PageDown.description = \u0627\u0644\u062a\u0645\u0631\u064a\u0631 \u0635\u0641\u062d\u0629 \u0648\u0627\u062d\u062f\u0629 \u0644\u0623\u0633\u0641\u0644
list.PageDown.sort = 30027

list.SubMenu.display = ,; {ctrl}+Enter; {ctrl}+Space; {shift}+F10
list.SubMenu.keycode = 188; Ctrl+13; Ctrl+32; Shift+121
list.SubMenu.display.mac = ,; {ctrl}+Enter; {ctrl}+Space
list.SubMenu.keycode.mac = 188; Ctrl+13; Ctrl+32
list.SubMenu.description = \u0639\u0631\u0636 \u0627\u0644\u0642\u0627\u0626\u0645\u0629 \u0627\u0644\u0645\u0646\u0628\u062b\u0642\u0629 \u0644\u0644\u0632\u0631
list.SubMenu.sort = 30065;

# Map: tree

tree.description = \u0623\u0634\u062c\u0627\u0631
tree.sort = 30500

tree.Next.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0633\u0641\u0644
tree.Next.keycode = 40
tree.Next.description = \u0627\u0644\u0627\u0646\u062a\u0642\u0627\u0644 \u0625\u0644\u0649 \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u062a\u0627\u0644\u064a
tree.Next.sort = 30510

tree.Enter.display = \u0625\u062f\u062e\u0627\u0644
tree.Enter.keycode = 13; 32
tree.Enter.description = \u062a\u062d\u062f\u064a\u062f \u200e(\u0641\u064a \u0627\u0644\u0645\u0646\u0633\u062f\u0644\u0629)
tree.Enter.sort = 30580

tree.Previous.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0639\u0644\u0649
tree.Previous.keycode = 38
tree.Previous.description = \u0627\u0644\u0627\u0646\u062a\u0642\u0627\u0644 \u0625\u0644\u0649 \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u0633\u0627\u0628\u0642
tree.Previous.sort = 30520

tree.Expand.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0644\u064a\u0645\u064a\u0646
tree.Expand.keycode = 39
tree.Expand.description = \u062a\u0648\u0633\u064a\u0639
tree.Expand.sort = 30530

tree.Collapse.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0644\u064a\u0633\u0627\u0631
tree.Collapse.keycode = 37
tree.Collapse.description = \u0637\u064a
tree.Collapse.sort = 30540

tree.SelectFirst.display = \u0627\u0644\u0635\u0641\u062d\u0629 \u0627\u0644\u0631\u0626\u064a\u0633\u064a\u0629
tree.SelectFirst.display.mac = \u0627\u0644\u0635\u0641\u062d\u0629 \u0627\u0644\u0631\u0626\u064a\u0633\u064a\u0629\u061b {meta}+\u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0639\u0644\u0649
tree.SelectFirst.keycode = 36
tree.SelectFirst.keycode.mac = 36\u061b Meta+38
tree.SelectFirst.description = \u062a\u062d\u062f\u064a\u062f \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u0623\u0648\u0644
tree.SelectFirst.sort = 30560

tree.SelectLast.display = \u0627\u0644\u0627\u0646\u062a\u0647\u0627\u0621
tree.SelectLast.display.mac = \u0627\u0644\u0635\u0641\u062d\u0629 \u0627\u0644\u0631\u0626\u064a\u0633\u064a\u0629\u061b {meta}+\u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0633\u0641\u0644
tree.SelectLast.keycode = 35
tree.SelectLast.keycode.mac = 36\u061b Meta+40
tree.SelectLast.description = \u062a\u062d\u062f\u064a\u062f \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u0623\u062e\u064a\u0631
tree.SelectLast.sort = 30570

tree.SubMenu.display = ,; {ctrl}+Enter; {ctrl}+Space; {shift}+F10
tree.SubMenu.keycode = 188; Ctrl+13; Ctrl+32; Shift+121
tree.SubMenu.display.mac = ,; {ctrl}+Enter; {ctrl}+Space
tree.SubMenu.keycode.mac = 188; Ctrl+13; Ctrl+32
tree.SubMenu.description = \u0639\u0631\u0636 \u0627\u0644\u0642\u0627\u0626\u0645\u0629 \u0627\u0644\u0645\u0646\u0628\u062b\u0642\u0629 \u0644\u0644\u0632\u0631
tree.SubMenu.sort = 30550;

# Map: menu

menu.description = \u0627\u062e\u062a\u0635\u0627\u0631\u0627\u062a \u0627\u0644\u0642\u0648\u0627\u0626\u0645 \u0627\u0644\u0645\u0646\u0628\u062b\u0642\u0629
menu.sort = 20000

menu.Cancel.display = Esc
menu.Cancel.keycode = 27
menu.Cancel.description = \u0627\u0633\u062a\u0628\u0639\u0627\u062f \u0627\u0644\u0642\u0627\u0626\u0645\u0629
menu.Cancel.sort = 20040

menu.ParentMenu.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0644\u064a\u0633\u0627\u0631
menu.ParentMenu.keycode = 37
menu.ParentMenu.description = \u0625\u062e\u0641\u0627\u0621 \u0627\u0644\u0642\u0627\u0626\u0645\u0629 \u0627\u0644\u0641\u0631\u0639\u064a\u0629
menu.ParentMenu.sort = 20060

menu.Select.display = \u0625\u062f\u062e\u0627\u0644\u061b \u0645\u0633\u0627\u0641\u0629
menu.Select.keycode = 13; 32
menu.Select.description = \u062a\u062d\u062f\u064a\u062f \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u062d\u0627\u0644\u064a
menu.Select.sort = 20030

menu.SelectNext.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0633\u0641\u0644
menu.SelectNext.keycode = 40
menu.SelectNext.description = \u0627\u0644\u0627\u0646\u062a\u0642\u0627\u0644 \u0625\u0644\u0649 \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u062a\u0627\u0644\u064a
menu.SelectNext.sort = 20010

menu.SelectPrevious.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0639\u0644\u0649
menu.SelectPrevious.keycode = 38
menu.SelectPrevious.description = \u0627\u0644\u0627\u0646\u062a\u0642\u0627\u0644 \u0625\u0644\u0649 \u0627\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u0633\u0627\u0628\u0642
menu.SelectPrevious.sort = 20020

menu.PageUp.display = PageUp
menu.PageUp.display.mac = {alt}+\u0633\u0647\u0645 \u0644\u0623\u0639\u0644\u0649
menu.PageUp.keycode = 33
menu.PageUp.keycode.mac = Alt+38
menu.PageUp.description = \u0627\u0644\u062a\u0645\u0631\u064a\u0631 \u0635\u0641\u062d\u0629 \u0648\u0627\u062d\u062f\u0629 \u0644\u0623\u0639\u0644\u0649
menu.PageUp.sort = 20023

menu.PageDown.display = PageDown
menu.PageDown.display.mac = {alt}+\u0633\u0647\u0645 \u0644\u0623\u0633\u0641\u0644
menu.PageDown.keycode = 34
menu.PageDown.keycode.mac = Alt+40
menu.PageDown.description = \u0627\u0644\u062a\u0645\u0631\u064a\u0631 \u0635\u0641\u062d\u0629 \u0648\u0627\u062d\u062f\u0629 \u0644\u0623\u0633\u0641\u0644
menu.PageDown.sort = 20027

menu.SubMenu.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0644\u064a\u0645\u064a\u0646
menu.SubMenu.keycode = 39
menu.SubMenu.description = \u0625\u0638\u0647\u0627\u0631 \u0627\u0644\u0642\u0627\u0626\u0645\u0629 \u0627\u0644\u0641\u0631\u0639\u064a\u0629 \u0644\u0644\u0639\u0646\u0635\u0631 \u0627\u0644\u062d\u0627\u0644\u064a
menu.SubMenu.sort = 20050

# Map: toolbarHorizontal

#L10N_IGNORE_BLOCK_BEGIN
toolbarHorizontal.INHERIT = button
#L10N_IGNORE_BLOCK_END
toolbarHorizontal.description = \u0627\u062e\u062a\u0635\u0627\u0631\u0627\u062a \u0623\u0634\u0631\u0637\u0629 \u0627\u0644\u0623\u062f\u0648\u0627\u062a \u0627\u0644\u0623\u0641\u0642\u064a\u0629
toolbarHorizontal.sort = 50000

toolbarHorizontal.Next.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0644\u064a\u0645\u064a\u0646
toolbarHorizontal.Next.keycode = 39
toolbarHorizontal.Next.description = \u0627\u0644\u062a\u0631\u0643\u064a\u0632 \u0639\u0644\u0649 \u0627\u0644\u0632\u0631 \u0627\u0644\u062a\u0627\u0644\u064a
toolbarHorizontal.Next.sort = 50010

toolbarHorizontal.Previous.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0644\u064a\u0633\u0627\u0631
toolbarHorizontal.Previous.keycode = 37
toolbarHorizontal.Previous.description = \u0627\u0644\u062a\u0631\u0643\u064a\u0632 \u0639\u0644\u0649 \u0627\u0644\u0632\u0631 \u0627\u0644\u0633\u0627\u0628\u0642
toolbarHorizontal.Previous.sort = 50020

# Map: toolbarVertical

#L10N_IGNORE_BLOCK_BEGIN
toolbarVertical.INHERIT = button
#L10N_IGNORE_BLOCK_END
toolbarVertical.description = \u0627\u062e\u062a\u0635\u0627\u0631\u0627\u062a \u0623\u0634\u0631\u0637\u0629 \u0627\u0644\u0623\u062f\u0648\u0627\u062a \u0627\u0644\u0631\u0623\u0633\u064a\u0629
toolbarVertical.sort = 51000

toolbarVertical.Next.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0633\u0641\u0644
toolbarVertical.Next.keycode = 40
toolbarVertical.Next.description = \u0627\u0644\u062a\u0631\u0643\u064a\u0632 \u0639\u0644\u0649 \u0627\u0644\u0632\u0631 \u0627\u0644\u062a\u0627\u0644\u064a
toolbarVertical.Next.sort = 51010

toolbarVertical.Previous.display = \u0627\u0644\u0633\u0647\u0645 \u0644\u0623\u0639\u0644\u0649
toolbarVertical.Previous.keycode = 38
toolbarVertical.Previous.description = \u0627\u0644\u062a\u0631\u0643\u064a\u0632 \u0639\u0644\u0649 \u0627\u0644\u0632\u0631 \u0627\u0644\u0633\u0627\u0628\u0642
toolbarVertical.Previous.sort = 51020

# Map: editor

editor.description = \u0627\u0644\u0627\u062e\u062a\u0635\u0627\u0631\u0627\u062a \u0636\u0645\u0646 \u0645\u062d\u0631\u0631 HTML\u200f
editor.sort = 60000

editor.Bold.display = {ctrl}+B
editor.Bold.keycode = Ctrl+66
editor.Bold.description = \u062c\u0639\u0644 \u0627\u0644\u0646\u0635 \u063a\u0627\u0645\u0642\u064b\u0627
editor.Bold.sort = 60010

editor.CenterJustify.display = {ctrl}+E
editor.CenterJustify.keycode = Ctrl+69
editor.CenterJustify.description = \u0645\u062d\u0627\u0630\u0627\u0629 \u0627\u0644\u0646\u0635 \u0625\u0644\u0649 \u0627\u0644\u0648\u0633\u0637
editor.CenterJustify.sort = 60070

editor.Header1.display = {ctrl}+\u0661
editor.Header1.keycode = Ctrl+49
editor.Header1.description = \u0627\u062c\u0639\u0644 \u0627\u0644\u0646\u0635 \u0639\u0646\u0648\u0627\u0646\u0627\u064b \u0645\u0646 \u0627\u0644\u0645\u0633\u062a\u0648\u0649 \u0661
editor.Header1.sort = 60090

editor.Header2.display = {ctrl}+\u0662
editor.Header2.keycode = Ctrl+50
editor.Header2.description = \u0627\u062c\u0639\u0644 \u0627\u0644\u0646\u0635 \u0639\u0646\u0648\u0627\u0646\u0627\u064b \u0645\u0646 \u0627\u0644\u0645\u0633\u062a\u0648\u0649 \u0662
editor.Header2.sort = 60100

editor.Header3.display = {ctrl}+\u0663
editor.Header3.keycode = Ctrl+51
editor.Header3.description = \u0627\u062c\u0639\u0644 \u0627\u0644\u0646\u0635 \u0639\u0646\u0648\u0627\u0646\u0627\u064b \u0645\u0646 \u0627\u0644\u0645\u0633\u062a\u0648\u0649 \u0663
editor.Header3.sort = 60110

editor.Header4.display = {ctrl}+\u0664
editor.Header4.keycode = Ctrl+52
editor.Header4.description = \u0627\u062c\u0639\u0644 \u0627\u0644\u0646\u0635 \u0639\u0646\u0648\u0627\u0646\u0627\u064b \u0645\u0646 \u0627\u0644\u0645\u0633\u062a\u0648\u0649 \u0664
editor.Header4.sort = 60120

editor.Header5.display = {ctrl}+\u0665
editor.Header5.keycode = Ctrl+53
editor.Header5.description = \u0627\u062c\u0639\u0644 \u0627\u0644\u0646\u0635 \u0639\u0646\u0648\u0627\u0646\u0627\u064b \u0645\u0646 \u0627\u0644\u0645\u0633\u062a\u0648\u0649 \u0665
editor.Header5.sort = 60130

editor.Header6.display = {ctrl}+\u0666
editor.Header6.keycode = Ctrl+54
editor.Header6.description = \u0627\u062c\u0639\u0644 \u0627\u0644\u0646\u0635 \u0639\u0646\u0648\u0627\u0646\u0627\u064b \u0645\u0646 \u0627\u0644\u0645\u0633\u062a\u0648\u0649 \u0666
editor.Header6.sort = 60140

editor.Italic.display = {ctrl}+I
editor.Italic.keycode = Ctrl+73
editor.Italic.description = \u0627\u062c\u0639\u0644 \u0627\u0644\u0646\u0635 \u0628\u062e\u0637 \u0645\u0627\u0626\u0644
editor.Italic.sort = 60020

editor.LeftJustify.display = {ctrl}+L
editor.LeftJustify.keycode = Ctrl+76
editor.LeftJustify.description = \u0645\u062d\u0627\u0630\u0627\u0629 \u0627\u0644\u0646\u0635 \u0625\u0644\u0649 \u0627\u0644\u064a\u0633\u0627\u0631
editor.LeftJustify.sort = 60050

editor.RightJustify.display = {ctrl}+R
editor.RightJustify.keycode = Ctrl+82
editor.RightJustify.description = \u0645\u062d\u0627\u0630\u0627\u0629 \u0627\u0644\u0646\u0635 \u0625\u0644\u0649 \u0627\u0644\u064a\u0645\u064a\u0646
editor.RightJustify.sort = 60060

editor.Strikethru.display = {ctrl}+{shift}+K
editor.Strikethru.keycode = Ctrl+Shift+75
editor.Strikethru.description = \u0625\u0636\u0627\u0641\u0629 \u062e\u0637 \u0639\u0628\u0631 \u0645\u0646\u062a\u0635\u0641 \u0627\u0644\u0646\u0635
editor.Strikethru.sort = 60030

editor.Underline.display = {ctrl}+U
editor.Underline.keycode = Ctrl+85
editor.Underline.description = \u062a\u0633\u0637\u064a\u0631 \u0627\u0644\u0646\u0635
editor.Underline.sort = 60040

editor.InsertLink.display = {ctrl}+K
editor.InsertLink.keycode = Ctrl+75
editor.InsertLink.description = \u0645\u0631\u0628\u0639 \u062d\u0648\u0627\u0631 \u0625\u062f\u0631\u0627\u062c \u0627\u0644\u0631\u0627\u0628\u0637
editor.InsertLink.sort = 60045
# Map: tabView

tabView.description = \u0639\u0631\u0648\u0636 \u0639\u0644\u0627\u0645\u0627\u062a \u0627\u0644\u062a\u0628\u0648\u064a\u0628
tabView.sort = 110000

tabView.GoToTab.display = {ctrl}+NNN
tabView.GoToTab.keycode = Ctrl+NNN
tabView.GoToTab.description = \u0627\u0644\u0627\u0646\u062a\u0642\u0627\u0644 \u0625\u0644\u0649 \u0639\u0644\u0627\u0645\u0629 \u0627\u0644\u062a\u0628\u0648\u064a\u0628 [n]
tabView.GoToTab.sort = 110030

tabView.NextTab.display = {ctrl}+J
tabView.NextTab.keycode = Ctrl+74
tabView.NextTab.description = \u0639\u0644\u0627\u0645\u0629 \u0627\u0644\u062a\u0628\u0648\u064a\u0628 \u0627\u0644\u062a\u0627\u0644\u064a\u0629
tabView.NextTab.sort = 110010

tabView.PreviousTab.display = {ctrl}+S
tabView.PreviousTab.keycode = Ctrl+83
tabView.PreviousTab.description = \u0639\u0644\u0627\u0645\u0629 \u0627\u0644\u062a\u0628\u0648\u064a\u0628 \u0627\u0644\u0633\u0627\u0628\u0642\u0629
tabView.PreviousTab.sort = 110020
