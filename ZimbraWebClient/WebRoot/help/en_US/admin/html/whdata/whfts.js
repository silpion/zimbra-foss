/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Web Client
 * Copyright (C) 2008, 2009, 2010, 2014 Zimbra, Inc.
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://www.zimbra.com/license
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
 * have been added to cover use of software over a computer network and provide for limited attribution 
 * for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 * See the License for the specific language governing rights and limitations under the License. 
 * The Original Code is Zimbra Open Source Web Client. 
 * The Initial Developer of the Original Code is Zimbra, Inc. 
 * All portions of the code are Copyright (C) 2008, 2009, 2010, 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK *****
 */
﻿//	WebHelp 5.10.001
var gaFileMapping = new Array();
var gaFileTopicMapping = new Array();

function fileMapping(sStartKey, sEndKey, sFileName)
{
	this.sStartKey = sStartKey;
	this.sEndKey = sEndKey;
	this.sFileName = sFileName;
	this.aFtsKeys = null;
}

function fileTopicMapping(nIdBegin, nIdEnd, sFileName)
{
	this.nBegin = nIdBegin;
	this.nEnd = nIdEnd;
	this.sFileName = sFileName;
	this.aTopics = null;
}


function iWM(sStartKey, sEndKey, sFileName)
{
	gaFileMapping[gaFileMapping.length] = new fileMapping(sStartKey, sEndKey, sFileName);	
}

function window_OnLoad()
{
	if (parent && parent != this && parent.ftsReady)
	{
		parent.ftsReady(gaFileMapping, gaFileTopicMapping);
	}		
}

function iTM(nIdBegin, nIdEnd, sFileName)
{
	gaFileTopicMapping[gaFileTopicMapping.length] = new fileTopicMapping(nIdBegin, nIdEnd, sFileName);	
}

window.onload = window_OnLoad;
