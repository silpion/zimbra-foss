/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2015 Zimbra, Inc.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
package com.zimbra.cs.redolog.servlet;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zimbra.common.consul.CatalogRegistration;
import com.zimbra.common.consul.CatalogRegistration.Service;
import com.zimbra.common.consul.ConsulClient;
import com.zimbra.common.consul.LeaderResponse;
import com.zimbra.common.consul.ServiceHealthResponse;
import com.zimbra.common.consul.SessionResponse;
import com.zimbra.common.localconfig.DebugConfig;
import com.zimbra.common.localconfig.LC;
import com.zimbra.common.service.ServiceException;
import com.zimbra.common.servicelocator.ServiceLocator;
import com.zimbra.common.util.ByteUtil;
import com.zimbra.common.util.StringUtil;
import com.zimbra.cs.account.Provisioning;
import com.zimbra.cs.account.Server;
import com.zimbra.cs.redolog.FilesystemRedoLogFile;
import com.zimbra.cs.redolog.HttpRedoLogFile;
import com.zimbra.cs.redolog.LeaderAwareRedoLogManager;
import com.zimbra.cs.redolog.LeaderAwareRedoLogProvider;
import com.zimbra.cs.redolog.LeaderChangeListener;
import com.zimbra.cs.redolog.RedoLogFile;
import com.zimbra.cs.redolog.RedoLogInput;
import com.zimbra.cs.redolog.RedoLogManager;
import com.zimbra.cs.redolog.RedoLogOutput;
import com.zimbra.cs.redolog.RedoLogProvider;
import com.zimbra.cs.redolog.RedologLeaderListener;
import com.zimbra.cs.redolog.TransactionId;
import com.zimbra.cs.redolog.logger.FileLogReader;
import com.zimbra.cs.redolog.op.RedoableOp;
import com.zimbra.cs.redolog.util.RedoLogVerify;
import com.zimbra.cs.redolog.util.RedoLogVerify.Params;
import com.zimbra.cs.util.BuildInfo;
import com.zimbra.cs.util.Zimbra;

public class RedologServlet extends HttpServlet {

    private static final long serialVersionUID = -2910139141606373055L;
    private static final Log LOG = LogFactory.getLog(RedologServlet.class);

    private static final String PARAM_FORMAT = "fmt";
    private static final String PARAM_MBOX_ID = "mboxId";
    private static final String PARAM_SERVER_ID = "serverId";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_COMMAND = "cmd";
    private static final String PARAM_COUNT = "count";
    private static final String PARAM_CUTOFF = "cutoff";
    private static final String PARAM_SEQ = "seq";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_FORCE_PEERS = "forcePeers";
    private static final String PARAM_FORCE = "force";

    private static final String FORMAT_DUMP = "dump";
    private static final String FORMAT_FILEREF = "fileref";
    private static final String FORMAT_FILE = "file";
    private static final String FORMAT_RAW = "raw";
    private static final String FORMAT_CURRENT_SEQ = "seq";

    private static final String TYPE_CURRENT = "current";
    private static final String TYPE_ARCHIVE = "archive";

    private static final String COMMAND_ROLLOVER = "rollover";
    private static final String COMMAND_APPEND = "append";
    private static final String COMMAND_GETIDS = "getIds";
    private static final String COMMAND_DELETE = "delete";

    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_REDOLOG = "application/x-zimbra-redolog";
    private static final String CONTENT_TYPE_REDOLOG_FILEREF = "application/x-zimbra-redolog-fileref";
    private static final String CONTENT_TYPE_REDOLOG_FILE = "application/x-zimbra-redolog-file";
    private static final String CONTENT_TYPE_REDODUMP = "application/x-zimbra-redodump";

    private static final String REDOLOG_SERVICE_NAME = "zimbra-redolog";

    protected RedoLogManager logManager;
    protected ConsulClient consulClient;
    protected ServiceLocator serviceLocator;

    private Service consulService;
    private String consulSessionId;

    private RedologLeaderListener leaderListener;

    private boolean stopping = false;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            Zimbra.startupMinimal(RedoLogConfig.class);
            consulClient = Zimbra.getAppContext().getBean(ConsulClient.class);
            serviceLocator = Zimbra.getAppContext().getBean(ServiceLocator.class);
            registerWithServiceLocator();
            leaderListener = new RedologLeaderListener(consulSessionId, consulService);
            leaderListener.init();

            LeaderChangeListener leaderObtainer = new LeaderChangeListener() {
                @Override
                public void onLeaderChange(String newLeaderSessionId, LeaderStateChange stateChange) {
                    Thread t = new Thread("redolog-leader-obtain") {
                        @Override
                        public void run() {
                            try {
                                obtainLeadership(true);
                            } catch (ServletException e) {
                                LOG.error("unable to obtain leadership due to exception", e);
                            }
                        }
                    };
                    t.setDaemon(true);
                    t.start();
                }
            };
            leaderListener.addListener(leaderObtainer);
            //trigger initial obtain attempt in background; can't complete until servlet healthy
            leaderObtainer.onLeaderChange(null, null);

            RedoLogProvider redoLog = RedoLogProvider.getInstance();
            if (redoLog instanceof LeaderAwareRedoLogProvider) {
                ((LeaderAwareRedoLogProvider) redoLog).setLeaderListener(leaderListener);
            }
            redoLog.startup(false);
            logManager = redoLog.getRedoLogManager();
        } catch (ServiceException e) {
            throw new ServletException("redologging startup failed", e);
        }
        LOG.info("initialized redolog servlet");
    }

    @Override
    public void destroy() {
        stopping = true;
        super.destroy();
        logManager.stop();
        leaderListener.stop();
        if (leaderListener.isLeader()) {
            try {
                consulClient.releaseLeadership(consulService.name, consulSessionId);
            } catch (IOException e) {
                LOG.error("unable to release leadership for redolog service", e);
            }
        }
        if (consulSessionId != null) {
            consulClient.deleteSessionSilent(consulSessionId);
        }
        if (consulService != null) {
            serviceLocator.deregisterSilent(consulService.id);
        }
        try {
            Zimbra.shutdown();
        } catch (ServiceException e) {
            LOG.error("exception while destorying servlet", e);
        }
    }

    protected void registerWithServiceLocator() throws ServletException {
        try {
            // Read protocol and port configuration
            Server localServer = Provisioning.getInstance().getLocalServer();
            int port = localServer.getRedologBindPort();

            //use ssl except in dev/debug cases
            if (DebugConfig.redologSSLRequired) {
                consulService = registerWithServiceLocator(REDOLOG_SERVICE_NAME, port, "https");
            } else {
                consulService = registerWithServiceLocator(REDOLOG_SERVICE_NAME, port, "http");
            }

        } catch (ServiceException e) {
            throw new ServletException("Failed reading provisioning before registering mailstore with service locator", e);
        }
    }

    protected Service registerWithServiceLocator(String serviceName, int port, String checkScheme) {
        String serviceID = serviceName + ":" + port;
        CatalogRegistration.Service service = new CatalogRegistration.Service(serviceID, serviceName, port);
        if (checkScheme.startsWith("https")) {
            service.tags.add("ssl");
        }
        String url = checkScheme + "://localhost:" + port + "/redolog/health";
        CatalogRegistration.Check check = new CatalogRegistration.Check(serviceID + ":health", serviceName);
        check.script = new File(LC.zimbra_libexec_directory.value()).getAbsolutePath() + "/zmhealthcheck-redolog " + url;
        check.interval = "1s";
        service.check = check;
        serviceLocator.registerSilent(service);
        return service;
    }

    private boolean obtainLeadership(boolean blockWhenNoLeader) throws ServletException {
        LeaderResponse leader;
        try {
            //wait until service is healthy. if we just shutdown and restarted it can still be in failed state here
            int attempts = 0;
            int maxAttempts = 20;
            long sleep = 5000;
            while (!stopping && attempts < maxAttempts) {
                leader = consulClient.findLeader(consulService);
                if (leader != null && leader.sessionId != null) {
                    return StringUtil.equal(consulSessionId, leader.sessionId);
                } else {
                    if (attempts > 0) {
                        LOG.debug("service not healthy or unable to elect leader; waiting");
                        //no healthy matching service found; wait a moment
                        try {
                            Thread.sleep(sleep);
                        } catch (InterruptedException e) {
                        }
                    }
                    List<ServiceHealthResponse> healthyServices = consulClient.health(consulService.name, true);
                    if (healthyServices != null && healthyServices.size() > 0) {
                        for (ServiceHealthResponse healthyService : healthyServices) {
                            if (StringUtil.equalIgnoreCase(consulService.id, healthyService.service.id)) {
                                if (consulSessionId == null || consulClient.getSessionInfo(consulSessionId) == null) {
                                    consulSessionId = createSession();
                                }
                                boolean obtained = consulClient.acquireLeadership(consulService.name, consulSessionId, null);
                                if (obtained || !blockWhenNoLeader) {
                                    return obtained;
                                }
                                break;
                            }
                        }
                    }
                    attempts++;
                }
            }
            LOG.debug("never able to obtain healthy session?");
            return false;
        }
        catch (IOException e) {
            throw new ServletException("unable to obtain leadership due to IOException", e);
        }
    }

    private String createSession() throws IOException {
        String checkId = "service:" + consulService.id;
        List<String> checks = new ArrayList<String>();
        checks.add(checkId);
        //TODO: using the serviceId as session name isn't ideal
        SessionResponse session = consulClient.createSession(consulService.id, 0, checks);
        leaderListener.setConsulSessionId(session.id);
        return session.id;
    }

    private void logHeaders(HttpServletRequest req) {
        if (LOG.isDebugEnabled()) {
            Enumeration<String> headerNames = req.getHeaderNames();
            while (headerNames != null && headerNames.hasMoreElements()) {
                String headerName = headerNames.nextElement();
                Enumeration<String> values = req.getHeaders(headerName);
                while (values != null && values.hasMoreElements()) {
                    LOG.trace("##Header## [" + headerName + "] = ["+ values.nextElement() + "]");
                }
            }
        }
    }

    protected void sendError(HttpServletResponse resp, int code, String msg) throws ServletException {
        try {
            resp.sendError(code, msg);
        } catch (IOException e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException {
        LOG.trace("redolog servlet - POST");
        if (req.getServletPath() != null) {
            if (!req.getServletPath().equals("/data")) {
                sendError(resp, HttpServletResponse.SC_NOT_FOUND, "not found");
                return;
            }
        }
        if (!leaderListener.isLeader()) {
            LOG.debug("not the leader");
            if (leaderListener.getLeaderSessionId() == null) {
                LOG.debug("attempting to obtain leadership");
                boolean leader = obtainLeadership(false);
                if (LOG.isDebugEnabled()) {
                    LOG.debug("obtained?" + leader);
                }
                if (leader) {
                    //we obtained but listener hasn't caught up yet; wait up to 1 second for it to do so
                    int tries = 0;
                    while (!leaderListener.isLeader() && tries < 10) {
                        tries++;
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                        }
                    }
                }
            }
        }
        logHeaders(req);

        String command = req.getParameter(PARAM_COMMAND);

        if (command == null || command.equalsIgnoreCase(COMMAND_APPEND)) {
            String contentType = req.getHeader(HEADER_CONTENT_TYPE);
            if (contentType != null && !contentType.equalsIgnoreCase(CONTENT_TYPE_REDOLOG)) {
                sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "unexpected content type " + contentType);
                return;
            }
            RedoLogInput input;
            RedoLogOutput output;
            try {
                input = new RedoLogInput(req.getInputStream());
                output = new RedoLogOutput(resp.getOutputStream());
            } catch (IOException e) {
                throw new ServletException("unable to construct i/o from stream", e);
            }
            RedoableOp op = null;
            try {
                while((op = RedoableOp.deserializeOp(input)) != null) {
                    if (!op.getTransactionId().isInitialized()) {
                        if (op.isEndMarker()) {
                            sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "end marker specified without transactionId");
                            return;
                        }
                        op.setTransactionId(logManager.getNewTxnId());
                    }
                    try {
                        logManager.log(op, true);
                    } catch (ServiceException e) {
                        if (e.getCode().equalsIgnoreCase(ServiceException.TEMPORARILY_UNAVAILABLE)) {
                            sendError(resp, HttpServletResponse.SC_SERVICE_UNAVAILABLE, "redolog service unavailable; try again in a moment");
                            return;
                        } else {
                            throw new ServletException(e);
                        }
                    }
                    op.getTransactionId().serialize(output);
                }
            } catch (EOFException eof) {
                //expected end of input
                LOG.debug("eof reached");
            } catch (IOException e) {
                throw new ServletException("exception deserializing ops", e);
            }
        } else if (command.equalsIgnoreCase(COMMAND_ROLLOVER)) {
            boolean forcePeers = Boolean.valueOf(req.getParameter(PARAM_FORCE_PEERS));
            try {
                if (!forcePeers) {
                    if (!(leaderListener.isLeader() || Boolean.valueOf(req.getParameter(PARAM_FORCE)))) {
                        sendError(resp, HttpServletResponse.SC_CONFLICT, "not the redolog leader, see " + leaderListener.getLeaderSessionId());
                        return;
                    } else {
                        logManager.forceRollover();
                        return;
                    }
                } else if (forcePeers) {
                    //force rollover manually
                    //intentionally unsafe; should only be used in edge recovery cases when a server is completely gone
                    if (!leaderListener.isLeader()) {
                        sendError(resp, HttpServletResponse.SC_CONFLICT, "not the redolog leader, see " + leaderListener.getLeaderSessionId());
                        return;
                    }
                    if (logManager instanceof LeaderAwareRedoLogManager) {
                        ((LeaderAwareRedoLogManager) logManager).forceRolloverPeers(req.getParameter(PARAM_SERVER_ID));
                        return;
                    }
                }
                //implicit else - bad rollover request
                sendError(resp, HttpServletResponse.SC_BAD_GATEWAY, "invalid rollover request");
                return;
            } catch (ServiceException | IOException e) {
                throw new ServletException(e);
            }
        } else if (command.equalsIgnoreCase(COMMAND_GETIDS)) {
            int numToReserve = 1;
            try {
                String count = req.getParameter(PARAM_COUNT);
                if (count != null) {
                    numToReserve = Math.min(10000, Integer.valueOf(count));
                }
            } catch (NumberFormatException nfe) {
                sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "unexpected non-integer count");
                return;
            }

            try {
                RedoLogOutput output = new RedoLogOutput(resp.getOutputStream());
                for (int i = 0; i < numToReserve; i++) {
                    TransactionId id = logManager.getNewTxnId();
                    id.serialize(output);
                }
            } catch (IOException e) {
                throw new ServletException("unable to construct i/o from stream", e);
            }
        } else if (command.equalsIgnoreCase(COMMAND_DELETE)) {
            if (!leaderListener.isLeader()) {
                sendError(resp, HttpServletResponse.SC_CONFLICT, "not the redolog leader, see " + leaderListener.getLeaderSessionId());
                return;
            }
            String cutoff = req.getParameter(PARAM_CUTOFF);
            if (cutoff == null) {
                sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "cutoff not specified");
                return;
            }
            try {
                long oldestTimestamp = Long.valueOf(cutoff);
                RedoLogProvider.getInstance().getRedoLogManager().deleteArchivedLogFiles(oldestTimestamp);
            } catch (NumberFormatException nfe) {
                sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "unexpected non-long cutoff");
                return;
            } catch (IOException e) {
                throw new ServletException("unable to delete archived log files", e);
            }
        } else {
            sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "unknown command");
            return;
        }
        LOG.debug("redolog servlet - post done");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        LOG.trace("redolog servlet - GET");
        logHeaders(req);
        String format = req.getParameter(PARAM_FORMAT);
        String[] mboxIds = req.getParameterValues(PARAM_MBOX_ID);
        String[] serverIds = req.getParameterValues(PARAM_SERVER_ID);
        String type = req.getParameter(PARAM_TYPE);

        if (req.getServletPath() != null) {
            if (req.getServletPath().equalsIgnoreCase("/health")) {
                //TODO: logManager.isHealthy()?
                resp.getOutputStream().write(("Zimbra Redolog Service " + BuildInfo.FULL_VERSION + " OK").getBytes("UTF-8"));
                //for now, just return OK if we're up
                return;
            } else if (!req.getServletPath().equals("/data")) {
                sendError(resp, HttpServletResponse.SC_NOT_FOUND, "not found");
                return;
            }
        }

        if (format == null || format.equalsIgnoreCase(FORMAT_DUMP)) {
            Params params = new RedoLogVerify.Params();
            params.snippet = true;
            if (mboxIds != null && mboxIds.length > 0) {
                for (String mboxId : mboxIds) {
                    try {
                        params.mboxIds.add(Integer.valueOf(mboxId));
                    } catch (NumberFormatException nfe) {
                        sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "unexpected non-integer mboxId");
                        return;
                    }
                }
            }
            if (serverIds != null && serverIds.length > 0) {
                for (String serverId : serverIds) {
                    params.serverIds.add(serverId);
                }
            }
            RedoLogVerify verify = new RedoLogVerify(params, new PrintStream(resp.getOutputStream()));
            resp.addHeader(HEADER_CONTENT_TYPE, CONTENT_TYPE_REDODUMP);
            File file = null;
            if (type == null || type.equalsIgnoreCase(TYPE_CURRENT)) {
                file = logManager.getLogFile();
                verify.verifyFile(file);
            } else if (type.equals(TYPE_ARCHIVE)) {
                RedoLogFile[] archives = logManager.getArchivedLogs();
                if (archives != null && archives.length > 0) {
                    for (RedoLogFile archive : archives) {
                        //TODO: verify accept stream/redologfile.
                        //ok for now if we assume redolog service always has file access
                        verify.verifyFile(archive.getFile());
                    }
                }
            }
        } else if (format.equalsIgnoreCase(FORMAT_RAW)) {
            resp.addHeader(HEADER_CONTENT_TYPE, CONTENT_TYPE_REDOLOG);
            RedoLogFile[] files = null;
            if (type == null || type.equalsIgnoreCase(TYPE_CURRENT)) {
                files = new RedoLogFile[] {new FilesystemRedoLogFile(logManager.getLogFile())};
            } else if (type.equals(TYPE_ARCHIVE)) {
                files = logManager.getArchivedLogs();
            }
            if (files == null) {
                sendError(resp, HttpServletResponse.SC_NOT_FOUND, "no archived redologs found");
                return;
            }
            for (RedoLogFile file : files) {
                //TODO: refactor further away from file
                //ok for now if we assume redolog service always has file access
                FileLogReader reader = new FileLogReader(file.getFile(), false);
                reader.open();
                try {
                    RedoableOp op = null;
                    while ((op = reader.getNextOp()) != null) {
                        ByteUtil.copy(op.getInputStream(), true, resp.getOutputStream(), false);
                    }
                } finally {
                    reader.close();
                }
            }
        } else if (format.equalsIgnoreCase(FORMAT_CURRENT_SEQ)) {
            resp.getOutputStream().write((logManager.getCurrentLogSequence() + "").getBytes("UTF-8"));
        } else if (format.equalsIgnoreCase(FORMAT_FILEREF)) {
            resp.addHeader(HEADER_CONTENT_TYPE, CONTENT_TYPE_REDOLOG_FILEREF);
            RedoLogFile[] files = null;
            if (type == null || type.equalsIgnoreCase(TYPE_CURRENT)) {
                //TODO: refactor further from file, ok for this iteration tho
                //ok for now if we assume redolog service always has file access
                HttpRedoLogFile httpLogFile = new HttpRedoLogFile(new FilesystemRedoLogFile(logManager.getLogFile()));
                files = new RedoLogFile[]{httpLogFile};
            } else if (type.equals(TYPE_ARCHIVE)) {
                String seq = req.getParameter(PARAM_SEQ);
                if (seq == null) {
                    files = logManager.getArchivedLogs();
                } else {
                    long seqNum = -1;
                    try {
                        seqNum = Long.parseLong(seq);
                    } catch(NumberFormatException e) {
                        sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "invalid sequence number");
                        return;
                    }
                    files = logManager.getArchivedLogsFromSequence(seqNum);
                }
            }
            if (files == null || files.length < 1) {
                sendError(resp, HttpServletResponse.SC_NOT_FOUND, "no archived redologs found");
                return;
            }
            for (RedoLogFile file : files) {
                HttpRedoLogFile httpLogFile = null;
                if (file instanceof HttpRedoLogFile) {
                    httpLogFile = (HttpRedoLogFile) file;
                } else {
                    httpLogFile = new HttpRedoLogFile(file);
                }
                resp.getOutputStream().write((httpLogFile.encodeToString() + "\r\n").getBytes("UTF-8"));
            }
        } else if (format.equalsIgnoreCase(FORMAT_FILE)) {
            resp.addHeader(HEADER_CONTENT_TYPE, CONTENT_TYPE_REDOLOG_FILE);
            //we should not need type current/arch here; let the logRef imply that?
            //or maybe seq < 0 == current?
            String seq = req.getParameter(PARAM_SEQ);
            if (seq == null) {
                sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "must specify seq");
                return;
            }
            long seqNum;
            try {
                seqNum = Long.valueOf(seq);
            } catch (NumberFormatException nfe) {
                sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "invalid sequence number; not a long value");
                return;
            }
            //TODO: refactor 'current' log file
            RedoLogFile file = null;
            if (seqNum == -1) {
                file = new FilesystemRedoLogFile(logManager.getLogFile());
            } else {
                file = logManager.getArchivedLog(seqNum);
            }
            if (file != null) {
                String name = req.getParameter(PARAM_NAME);
                if (name != null && !name.equals(file.getName())) {
                    sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "name/seq mismatch");
                    return;
                }
            } else {
                sendError(resp, HttpServletResponse.SC_NOT_FOUND, "sequence number not found");
                return;
            }
            ByteUtil.copy(file.getInputStream(), true, resp.getOutputStream(), false);

        } else {
            sendError(resp, HttpServletResponse.SC_BAD_REQUEST, "unknown format");
            return;
        }
    }
}
