/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2010, 2011, 2012, 2013, 2014 Zimbra, Inc.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
package com.zimbra.cs.account;

import com.zimbra.common.service.ServiceException;
import com.zimbra.common.util.ZimbraLog;


/**
 * Implementation of {@link Provisioning} for redolog servlet integration tests
 *
 */
public final class StubRedologProvisioning extends StubLocalhostProvisioning {

    public StubRedologProvisioning() {
        super();
        try {
            getLocalServer().setRedoLogLogPath(System.getProperty("java.io.tmpdir") + "/redolog/redo.log");
            getLocalServer().setRedoLogArchiveDir(System.getProperty("java.io.tmpdir") + "/redolog/archive");
            getLocalServer().setRedoLogDeleteOnRollover(false);
            String zimbraJettyMavenPort = System.getProperty("jetty.port");
            if (zimbraJettyMavenPort == null || zimbraJettyMavenPort.trim().length() < 1) {
                zimbraJettyMavenPort = "8080";
            }
            getLocalServer().setRedologBindPort(Integer.valueOf(zimbraJettyMavenPort));
            getLocalServer().setRedisUrl(new String[] {"redis://127.0.0.1:6379"});
            String zimbraId = System.getProperty("zimbra.test.serverid");
            if (zimbraId != null) {
                getLocalServer().setId(zimbraId);
            }
        } catch (ServiceException e) {
            ZimbraLog.redolog.warn("unable to setup redolog prov stub",e);
        }
    }

}
