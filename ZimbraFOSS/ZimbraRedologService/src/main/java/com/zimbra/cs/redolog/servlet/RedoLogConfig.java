/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2015 Zimbra Software, LLC.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
package com.zimbra.cs.redolog.servlet;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Lazy;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.util.Pool;

import com.zimbra.common.consul.ConsulClient;
import com.zimbra.common.consul.ConsulServiceLocator;
import com.zimbra.common.service.ServiceException;
import com.zimbra.common.servicelocator.ServiceLocator;
import com.zimbra.common.util.ZimbraLog;
import com.zimbra.cs.account.Provisioning;
import com.zimbra.cs.account.Server;
import com.zimbra.cs.redolog.LeaderAwareRedoLogProvider;
import com.zimbra.cs.redolog.RedoLogProvider;
import com.zimbra.cs.redolog.seq.LocalSequenceNumberGenerator;
import com.zimbra.cs.redolog.seq.RedisSequenceNumberGenerator;
import com.zimbra.cs.redolog.seq.SequenceNumberGenerator;
import com.zimbra.cs.redolog.txn.LocalTxnIdGenerator;
import com.zimbra.cs.redolog.txn.RedisTxnIdGenerator;
import com.zimbra.cs.redolog.txn.TxnIdGenerator;
import com.zimbra.cs.util.Zimbra;

/**
 * Redolog servlet spring configuration
 *
 */
@Configuration
@EnableAspectJAutoProxy
@Lazy
public class RedoLogConfig {

    @Bean
    public RedoLogProvider redologProvider() throws Exception {
        RedoLogProvider instance = null;
        Class<?> klass = null;
        Server config = Provisioning.getInstance().getLocalServer();
        //
        String className = null;
        //TODO: for now this is hard-coded; but since we can have /redolog and /service on the same host
        //need to create a separate attribute for the redologservlet provider class

        //className = config.getAttr(Provisioning.A_zimbraRedoLogProvider);
        if (className != null) {
            klass = Class.forName(className);
        } else {
//            klass = DefaultRedoLogProvider.class;
            klass = LeaderAwareRedoLogProvider.class;
            ZimbraLog.misc.debug("Redolog provider name not specified.  Using default " +
                                 klass.getName());
        }
        instance = (RedoLogProvider) klass.newInstance();
        return instance;
    }

    @Bean
    public ConsulClient consulClient() throws IOException, ServiceException {
        Server server = Provisioning.getInstance().getLocalServer();
        String url = server.getConsulURL();
        return new ConsulClient(url);
    }

    @Bean
    public ServiceLocator serviceLocator() throws Exception {
        return new ConsulServiceLocator(consulClient());
    }

    public Set<HostAndPort> redisUris() throws ServiceException {
        String[] uris = Provisioning.getInstance().getLocalServer().getRedisUrl();
        if (uris.length == 0) {
            return Collections.emptySet();
        }
        Set<HostAndPort> result = new HashSet<>();
        try {
            for (String uriStr: uris) {
                URI uri = new URI(uriStr);
                result.add(new HostAndPort(uri.getHost(), uri.getPort() == -1 ? 6379 : uri.getPort()));
            }
            return result;
        } catch (URISyntaxException e) {
            throw ServiceException.PARSE_ERROR("Invalid Redis URI", e);
        }
    }

    public Set<String> redisSentinelUris() throws ServiceException {
        String[] uris = Provisioning.getInstance().getLocalServer().getRedisSentinelUrl();
        if (uris.length == 0) {
            return Collections.emptySet();
        }
        Set<String> result = new HashSet<>();
        try {
            for (String uriStr: uris) {
                URI uri = new URI(uriStr);
                result.add(uri.getHost() + ":" + (uri.getPort() == -1 ? 26379 : uri.getPort()));
            }
            return result;
        } catch (URISyntaxException e) {
            throw ServiceException.PARSE_ERROR("Invalid Redis Sentinel URI", e);
        }
    }

    /** Returns whether Redis services are available, whether cluster or master/slave/stand-alone */
    public boolean isRedisAvailable() throws ServiceException {
        Set<HostAndPort> uris = redisUris();
        if (uris.isEmpty()) {
            return false;
        }
        try (Jedis jedis = jedisPool().getResource()) {
            jedis.get("");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isRedisClusterAvailable() throws ServiceException {
        try {
            return jedisCluster() != null;
        } catch (Exception e) {
            ZimbraLog.misc.warn("Failed connecting to a Redis Cluster; defaulting to non-cluster mode Redis access (%s)", e.getLocalizedMessage());
            return false;
        }
    }

    /** Returns a JedisCluster client if possible, or null */
    @Bean
    public JedisCluster jedisCluster() throws ServiceException {
        Set<HostAndPort> uris = redisUris();
        if (uris.isEmpty()) {
            return null;
        }
        try {
            return new JedisCluster(uris);
        } catch (Exception e) {
            ZimbraLog.misc.warn("Failed connecting to a Redis Cluster; defaulting to non-cluster mode Redis access (%s)", e.getLocalizedMessage());
            return null;
        }
    }


    /** Returns a Jedis Pool if possible, or null */
    @Bean
    public Pool<Jedis> jedisPool() throws ServiceException {
        Set<HostAndPort> uris = redisUris();
        if (uris.isEmpty()) {
            return null;
        }

        String redisMaster = Provisioning.getInstance().getLocalServer().getRedisSentinelMaster();
        Set<String> sentinelUris = redisSentinelUris();

        // Perform unmanaged non-HA config
        if (redisMaster == null || sentinelUris == null || sentinelUris.isEmpty()) {
            HostAndPort hostAndPort = uris.iterator().next();
            return new JedisPool(hostAndPort.getHost(), hostAndPort.getPort());
        }

        // Perform Sentinel-based HA config
        ZimbraLog.misc.info("Using Redis Sentinels %s for Redis master %s", sentinelUris.toString(), redisMaster);
        return new JedisSentinelPool(redisMaster, sentinelUris);
    }

    @Bean
    public TxnIdGenerator redologTxnIdGenerator() throws Exception
    {
        TxnIdGenerator idGenerator = null;
        if (isRedisAvailable()) {
            idGenerator = new RedisTxnIdGenerator(jedisPool());
        }

        if (idGenerator == null) {
          if (Zimbra.isAlwaysOn()) {
              throw new Exception("Redis is required in always on environment");
          }
          idGenerator = new LocalTxnIdGenerator();
        }
        return idGenerator;
    }

    @Bean
    public SequenceNumberGenerator redologSeqNumGenerator() throws Exception
    {
        SequenceNumberGenerator generator = null;
        if (isRedisAvailable()) {
            generator = new RedisSequenceNumberGenerator(jedisPool());
        }

        if (generator == null) {
          if (Zimbra.isAlwaysOn()) {
              throw new Exception("Redis is required in always on environment");
          }
          generator = new LocalSequenceNumberGenerator();
        }
        return generator;
    }
}
