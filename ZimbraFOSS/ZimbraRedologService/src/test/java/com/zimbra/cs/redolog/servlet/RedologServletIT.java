/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2015 Zimbra, Inc.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */

package com.zimbra.cs.redolog.servlet;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ibm.icu.util.StringTokenizer;
import com.zimbra.common.consul.CatalogRegistration;
import com.zimbra.common.consul.ConsulClient;
import com.zimbra.common.consul.LeaderResponse;
import com.zimbra.common.service.ServiceException;
import com.zimbra.common.servicelocator.ServiceLocator;
import com.zimbra.common.servicelocator.ServiceLocator.Entry;
import com.zimbra.common.util.ByteUtil;
import com.zimbra.common.util.DevNullOutputStream;
import com.zimbra.common.util.ZimbraLog;
import com.zimbra.cs.mailbox.Folder.FolderOptions;
import com.zimbra.cs.mailbox.Mailbox;
import com.zimbra.cs.mailbox.MailboxTestUtil;
import com.zimbra.cs.redolog.HttpRedoLogFile;
import com.zimbra.cs.redolog.HttpRedoLogManager;
import com.zimbra.cs.redolog.RedoLogInput;
import com.zimbra.cs.redolog.RedoLogOutput;
import com.zimbra.cs.redolog.TransactionId;
import com.zimbra.cs.redolog.logger.FileLogReader;
import com.zimbra.cs.redolog.op.AbortTxn;
import com.zimbra.cs.redolog.op.CommitTxn;
import com.zimbra.cs.redolog.op.CreateFolder;
import com.zimbra.cs.redolog.op.MockDeleteMailbox;
import com.zimbra.cs.redolog.op.MockRedoableOp;
import com.zimbra.cs.redolog.op.RedoableOp;
import com.zimbra.cs.redolog.util.RedoLogVerify;
import com.zimbra.cs.util.Zimbra;
import com.zimbra.cs.util.ZimbraConfig;

public class RedologServletIT {

    private static String URL = "http://localhost:" + System.getProperty("jetty.port", "8080") + "/redolog/data";

    @BeforeClass
    public static void init() throws Exception {
        MailboxTestUtil.initProvisioning("../../ZimbraServer/");

        Zimbra.startupMinimal(ZimbraConfig.class);
        ConsulClient client = null;
        try {
            client = Zimbra.getAppContext().getBean(ConsulClient.class);
            client.ping();
        } catch(Exception e) {
            Assume.assumeNoException(e);
        }

        //wait up to 20 seconds for leader to initialize - avoid races during test startup

        int tries = 0;
        LeaderResponse leader = null;
        while (tries < 40 && (leader == null || leader.sessionId == null)) {
            Thread.sleep(500);
            leader = client.findLeader(new CatalogRegistration.Service("zimbra-redolog"));
            tries++;
        }
        Assert.assertTrue(leader != null && leader.sessionId != null);
    }

    @Before
    public void setup() throws Exception {
    }

    @Test
    public void testPostRedoOp() throws IOException {
        MockRedoableOp op = new MockRedoableOp(1, "testOp", Mailbox.ID_FOLDER_USER_ROOT, new FolderOptions());
        op.setTransactionId(new TransactionId());
        op.setTimestamp(System.currentTimeMillis());
        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(URL);
        post.setRequestEntity(new InputStreamRequestEntity(op.getInputStream()));
        int code = client.executeMethod(post);
        Assert.assertEquals(HttpStatus.SC_OK, code);
        RedoLogInput in = new RedoLogInput(post.getResponseBodyAsStream());
        TransactionId txnId = new TransactionId();
        txnId.deserialize(in);
        Assert.assertTrue(txnId.getCounter() > 0);
        Assert.assertTrue(txnId.getTime() > 0);
    }

    @Test
    public void testDumpRedoOp() throws IOException {
        MockRedoableOp op = new MockRedoableOp(1, "testOp", Mailbox.ID_FOLDER_USER_ROOT, new FolderOptions());
        op.setTransactionId(new TransactionId());
        op.setTimestamp(System.currentTimeMillis());
        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(URL);
        post.setRequestEntity(new InputStreamRequestEntity(op.getInputStream()));
        int code = client.executeMethod(post);

        Assert.assertEquals(HttpStatus.SC_OK, code);
        RedoLogInput in = new RedoLogInput(post.getResponseBodyAsStream());
        TransactionId txnId = new TransactionId();
        txnId.deserialize(in);
        Assert.assertTrue(txnId.getCounter() > 0);
        Assert.assertTrue(txnId.getTime() > 0);

        GetMethod get = new GetMethod(URL);
        get.setQueryString("fmt=dump");
        code = client.executeMethod(get);

        Assert.assertEquals(HttpStatus.SC_OK, code);

        BufferedReader reader = new BufferedReader(new InputStreamReader(get.getResponseBodyAsStream()));
        String line = null;
        String lastLine = "";
        while ((line = reader.readLine()) != null) {
            if (line.trim().length() > 0) {
                lastLine = line;
            }
        }
        Assert.assertTrue(lastLine.contains(txnId.toString()));
    }

    @Test
    public void testGetRawRedoOp() throws IOException {
        MockRedoableOp op = new MockRedoableOp(1, "testOp", Mailbox.ID_FOLDER_USER_ROOT, new FolderOptions());
        op.setTransactionId(new TransactionId());
        op.setTimestamp(System.currentTimeMillis());
        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(URL);
        post.setRequestEntity(new InputStreamRequestEntity(op.getInputStream()));
        int code = client.executeMethod(post);

        Assert.assertEquals(HttpStatus.SC_OK, code);
        RedoLogInput in = new RedoLogInput(post.getResponseBodyAsStream());
        TransactionId txnId = new TransactionId();
        txnId.deserialize(in);
        Assert.assertTrue(txnId.getCounter() > 0);
        Assert.assertTrue(txnId.getTime() > 0);

        GetMethod get = new GetMethod(URL);
        get.setQueryString("fmt=raw");
        code = client.executeMethod(get);

        Assert.assertEquals(HttpStatus.SC_OK, code);

        RedoLogInput input = new RedoLogInput(get.getResponseBodyAsStream());

        RedoableOp rawOp = null;
        RedoableOp lastOp = null;

        try {
            while((rawOp = RedoableOp.deserializeOp(input)) != null) {
                ZimbraLog.test.debug("deserialized op %s", rawOp);
                lastOp = rawOp;
            }

            Assert.assertEquals(txnId, lastOp.getTransactionId());
        } catch (EOFException eof) {
            //expected end of input
        }

    }

    @Test
    public void testPostWrongCtype() throws IOException {
        MockRedoableOp op = new MockRedoableOp(1, "testOp", Mailbox.ID_FOLDER_USER_ROOT, new FolderOptions());
        op.setTransactionId(new TransactionId());
        op.setTimestamp(System.currentTimeMillis());
        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(URL);
        post.setRequestHeader(new Header("Content-Type", "application/x-zimbra-wrong"));
        post.setRequestEntity(new InputStreamRequestEntity(op.getInputStream()));
        int code = client.executeMethod(post);
        Assert.assertEquals(HttpStatus.SC_BAD_REQUEST, code);
    }

    @Test
    public void testPostCorrectCtype() throws IOException {
        MockRedoableOp op = new MockRedoableOp(1, "testOp", Mailbox.ID_FOLDER_USER_ROOT, new FolderOptions());
        op.setTransactionId(new TransactionId());
        op.setTimestamp(System.currentTimeMillis());
        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(URL);
        post.setRequestEntity(new InputStreamRequestEntity(op.getInputStream()));
        post.setRequestHeader(new Header("Content-Type", "application/x-zimbra-redolog"));
        int code = client.executeMethod(post);

        Assert.assertEquals(HttpStatus.SC_OK, code);
        RedoLogInput in = new RedoLogInput(post.getResponseBodyAsStream());
        TransactionId txnId = new TransactionId();
        txnId.deserialize(in);
        Assert.assertTrue(txnId.getCounter() > 0);
        Assert.assertTrue(txnId.getTime() > 0);
    }

    @Test
    public void testPostMultipleRedoOp() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        RedoLogOutput out = new RedoLogOutput(baos);

        MockRedoableOp op = new MockRedoableOp(1, "testOp1", Mailbox.ID_FOLDER_USER_ROOT, new FolderOptions());
        op.setTransactionId(new TransactionId());
        op.setTimestamp(System.currentTimeMillis());
        op.serializeOperation(out);

        op = new MockRedoableOp(1, "testOp2", -1, new FolderOptions());
        op.setTransactionId(new TransactionId());
        op.setTimestamp(System.currentTimeMillis());
        op.serializeOperation(out);

        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(URL);
        post.setRequestEntity(new InputStreamRequestEntity(new ByteArrayInputStream(baos.toByteArray())));
        int code = client.executeMethod(post);

        Assert.assertEquals(HttpStatus.SC_OK, code);
        RedoLogInput in = new RedoLogInput(post.getResponseBodyAsStream());
        TransactionId txnId1 = new TransactionId();
        txnId1.deserialize(in);
        Assert.assertTrue(txnId1.getCounter() > 0);
        Assert.assertTrue(txnId1.getTime() > 0);

        TransactionId txnId2 = new TransactionId();
        txnId2.deserialize(in);
        Assert.assertTrue(txnId2.getCounter() > 0);
        Assert.assertTrue(txnId2.getTime() > 0);

        Assert.assertTrue(txnId2.compareTo(txnId1) > 0);
    }

    @Test
    public void testGetIds() throws IOException {
        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(URL);
        post.addParameter("cmd", "getids");
        int count = 10;
        post.addParameter("count", count+"");
        int code = client.executeMethod(post);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        RedoLogInput in = new RedoLogInput(post.getResponseBodyAsStream());
        TransactionId txnIdLast = null;
        for (int i = 0; i < count; i++) {
            TransactionId txnId = new TransactionId();
            txnId.deserialize(in);
            Assert.assertTrue(txnIdLast == null || txnId.compareTo(txnIdLast) > 0);
            txnIdLast = txnId;
        }
        try {
            //should be no more
            TransactionId txnId = new TransactionId();
            txnId.deserialize(in);
            Assert.fail("more transactionIds than expected");
        } catch (EOFException expected) {
        }
    }

    @Test
    public void testEndOpNoTxnId() throws IOException {
        MockRedoableOp op = new MockRedoableOp(1, "testOp", Mailbox.ID_FOLDER_USER_ROOT, new FolderOptions());
        op.setTransactionId(new TransactionId());
        op.setTimestamp(System.currentTimeMillis());
        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(URL);
        post.setRequestEntity(new InputStreamRequestEntity(op.getInputStream()));
        int code = client.executeMethod(post);

        Assert.assertEquals(HttpStatus.SC_OK, code);

        AbortTxn abort = new AbortTxn(op);
        post = new PostMethod(URL);
        post.setRequestEntity(new InputStreamRequestEntity(abort.getInputStream()));
        code = client.executeMethod(post);

        Assert.assertEquals(HttpStatus.SC_BAD_REQUEST, code);
    }

    @Test
    public void testPreserveOpId() throws IOException {
        MockRedoableOp op = new MockRedoableOp(1, "testOp", Mailbox.ID_FOLDER_USER_ROOT, new FolderOptions());
        int timestamp = 1234;
        int counter = 5678;
        TransactionId origTxnId = new TransactionId(timestamp, counter);
        op.setTransactionId(origTxnId);
        op.setTimestamp(timestamp);

        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(URL);
        post.setRequestEntity(new InputStreamRequestEntity(op.getInputStream()));
        int code = client.executeMethod(post);

        Assert.assertEquals(HttpStatus.SC_OK, code);

        RedoLogInput in = new RedoLogInput(post.getResponseBodyAsStream());
        TransactionId txnId = new TransactionId();
        txnId.deserialize(in);

        Assert.assertEquals(origTxnId, txnId);
    }

    @Test
    public void testPreserveDeleteId() throws IOException {
        MockDeleteMailbox op = new MockDeleteMailbox(9999);
        int timestamp = 1234;
        int counter = 5678;
        TransactionId origTxnId = new TransactionId(timestamp, counter);
        op.setTransactionId(origTxnId);
        op.setTimestamp(timestamp);

        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(URL);
        post.setRequestEntity(new InputStreamRequestEntity(op.getInputStream()));
        int code = client.executeMethod(post);

        Assert.assertEquals(HttpStatus.SC_OK, code);

        RedoLogInput in = new RedoLogInput(post.getResponseBodyAsStream());
        TransactionId txnId = new TransactionId();
        txnId.deserialize(in);

        Assert.assertEquals(origTxnId, txnId);
    }

    @Test
    public void testGetCurrentSeqNum() throws IOException {
        HttpClient client = new HttpClient();
        GetMethod get = new GetMethod(URL);
        get.setQueryString("fmt=seq");
        int code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        long seqNum = Long.parseLong(get.getResponseBodyAsString().trim());
        Assert.assertTrue(seqNum >= 0);

        //do a rollover; have to change this if we drop rollover from api
        PostMethod post = new PostMethod(URL);
        post.setParameter("cmd", "rollover");
        code = client.executeMethod(post);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        get = new GetMethod(URL);
        get.setQueryString("fmt=seq");
        code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        long nextSeqNum = Long.parseLong(get.getResponseBodyAsString().trim());
        //in theory a rollover could increment by more than one, so just make sure it's >
        Assert.assertTrue(nextSeqNum > seqNum);
    }


    @Test
    public void testArchiveRedoLog() throws IOException {
        createValidatedArchivedOp();
    }


    private void findTransactionIdInResp(HttpMethodBase method, TransactionId txnId, boolean expectToFind) throws IOException {
        RedoLogInput input = new RedoLogInput(method.getResponseBodyAsStream());
        RedoableOp rawOp = null;
        boolean found = true;
        try {
            while((rawOp = RedoableOp.deserializeOp(input)) != null) {
                ZimbraLog.test.debug("deserialized op %s", rawOp);
                if (!expectToFind) {
                    Assert.assertFalse(txnId.equals(rawOp.getTransactionId()));
                } else {
                    if (txnId.equals(rawOp.getTransactionId())) {
                        found = true;
                        break;
                    }
                }
            }
        } catch (EOFException eof) {
            //expected end of input
        }
        Assert.assertTrue(found || !expectToFind);
    }

    private MockRedoableOp createValidatedOp() throws IOException {
        MockRedoableOp op = new MockRedoableOp(1, "testOp", Mailbox.ID_FOLDER_USER_ROOT, new FolderOptions());
        op.setTransactionId(new TransactionId());
        long op1Timestamp = System.currentTimeMillis();
        op.setTimestamp(op1Timestamp);
        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(URL);
        post.setRequestEntity(new InputStreamRequestEntity(op.getInputStream()));
        int code = client.executeMethod(post);
        Assert.assertEquals(HttpStatus.SC_OK, code);
        RedoLogInput in = new RedoLogInput(post.getResponseBodyAsStream());
        TransactionId txnId = new TransactionId();
        txnId.deserialize(in);
        Assert.assertTrue(txnId.getCounter() > 0);
        Assert.assertTrue(txnId.getTime() > 0);
        op.setTransactionId(txnId);

        //commit the transaction
        CommitTxn commit = new CommitTxn(op);
        post = new PostMethod(URL);
        post.setRequestEntity(new InputStreamRequestEntity(commit.getInputStream()));
        code = client.executeMethod(post);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        //and make sure it's in the active log
        GetMethod get = new GetMethod(URL);
        get.setQueryString("fmt=raw&type=current");
        code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);
        findTransactionIdInResp(get, txnId, true);

        return op;
    }

    private MockRedoableOp createValidatedArchivedOp() throws IOException {
        //1. post an op to use as a tracer
        MockRedoableOp op = createValidatedOp();
        TransactionId txnId = op.getTransactionId();

        //2. make sure the txnId is not in archive...
        //maybe small chance of spurious failure here if rollover happens in the background...but it shouldn't in isolated test
        GetMethod get = new GetMethod(URL);
        HttpClient client = new HttpClient();
        get.setQueryString("fmt=raw&type=archive");
        int code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);
        findTransactionIdInResp(get, txnId, false);

        //3. do a rollover so it's in archive now
        rollover();

        //4. now txn should be in the archive
        ZimbraLog.test.debug("step 4 not in archive");
        get = new GetMethod(URL);
        get.setQueryString("fmt=raw&type=archive");
        code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);
        findTransactionIdInResp(get, txnId, true);

        //and not in active
        get = new GetMethod(URL);
        get.setQueryString("fmt=raw&type=current");
        code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);
        findTransactionIdInResp(get, txnId, false);

        return op;
    }

    private void rollover() throws HttpException, IOException {
        HttpClient client = new HttpClient();
        //have to change this if we drop rollover from api
        PostMethod post = new PostMethod(URL);
        post.setParameter("cmd", "rollover");
        int code = client.executeMethod(post);
        Assert.assertEquals(HttpStatus.SC_OK, code);
    }

    @Test
    public void testDeleteRedologArchive() throws IOException {
        MockRedoableOp op = createValidatedArchivedOp();

        rollover();

        //txn still in archive
        ZimbraLog.test.debug("delete still in archive");
        GetMethod get = new GetMethod(URL);
        get.setQueryString("fmt=raw&type=archive");
        HttpClient client = new HttpClient();
        int code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);
        TransactionId txnId = op.getTransactionId();
        findTransactionIdInResp(get, txnId, true);

        //now delete
        PostMethod post = new PostMethod(URL);
        post.setParameter("cmd", "delete");
        post.setParameter("cutoff", System.currentTimeMillis()+"");
        code = client.executeMethod(post);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        //and make sure no longer in archive
        get = new GetMethod(URL);
        get.setQueryString("fmt=raw&type=archive");
        code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);
        findTransactionIdInResp(get, txnId, false);
    }

    @Test
    public void testGetFileRef() throws IOException {
        //make sure we have an archive
        MockRedoableOp op = createValidatedOp();

        GetMethod get = new GetMethod(URL);
        findOpInFileRefs("fmt=fileref&type=current", op);
    }

    @Test
    public void testGetArchiveFileRef() throws IOException {
        //make sure we have an archive
        MockRedoableOp op = createValidatedArchivedOp();
        rollover();

        findOpInFileRefs("fmt=fileref&type=archive&seq=-1", op);
    }

    private void findOpInFileRefs(String query, MockRedoableOp op) throws HttpException, IOException {
        GetMethod get = new GetMethod(URL);
        get.setQueryString(query);
        HttpClient client = new HttpClient();
        int code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        String body = get.getResponseBodyAsString();
        StringTokenizer stoke = new StringTokenizer(body, "\r\n");
        HttpRedoLogFile logRef = null;
        boolean found = false;

        while (stoke.hasMoreTokens()) {
            String line = stoke.nextToken();
            logRef = HttpRedoLogFile.decodeFromString(line);
            ZimbraLog.test.info("got logRef %s", logRef.encodeToString());
            Assert.assertNotNull(logRef.getName());
            Assert.assertTrue(logRef.getLength() > -1);
            //TODO: hmm better way to indicate 'iscurrent?'
            Assert.assertTrue(logRef.getSeq() > -1 || logRef.getName().endsWith("redo.log"));

            Assert.assertNotNull(logRef);
            //now get the log file
            get = new GetMethod(URL);
            get.setQueryString("fmt=file&name="+logRef.getName()+"&seq="+logRef.getSeq());
            client = new HttpClient();
            code = client.executeMethod(get);
            Assert.assertEquals(HttpStatus.SC_OK, code);

            //make sure response is valid redolog file
            RedoLogVerify.Params params = new RedoLogVerify.Params();
            RedoLogVerify verify = new RedoLogVerify(params,new PrintStream(new DevNullOutputStream()));

            File logFile = File.createTempFile("redologverify", ".out");
            FileOutputStream fos = new FileOutputStream(logFile);
            ByteUtil.copy(get.getResponseBodyAsStream(), true, fos, true);

            verify.scanLog(logFile);

            FileLogReader logReader = new FileLogReader(logFile, false);
            logReader.open();

            //matching seq/length
            //TODO: 'current', sizes/seq num aren't sent/exact...
            if (logRef.getSeq() != -1) {
                Assert.assertEquals(logRef.getSeq(), logReader.getHeader().getSequence());
                Assert.assertEquals(logRef.getLength(), logReader.getHeader().getFileSize());
            }
            //the op should _generally_ be in the highest seq number long file; which is the last one returned from getrefs
            //not guaranteed since logging order is somewhat non-deterministic
            RedoableOp fileOp = null;
            ZimbraLog.test.debug("looking in file %s for op %s", logRef.encodeToString(), op);
            while (!found && (fileOp = logReader.getNextOp()) != null) {
                ZimbraLog.test.debug("fileop %s", fileOp);
                if (fileOp.getTransactionId().equals(op.getTransactionId()) && fileOp.getClass().isAssignableFrom(op.getClass())) {
                    //looks right; mismatch after here is bad data, not just found=false
                    CreateFolder createFolder = (CreateFolder) fileOp;
                    Assert.assertEquals(op.getChangeId(), createFolder.getChangeId());
                    Assert.assertEquals(op.getMailboxId(), createFolder.getMailboxId());
                    Assert.assertEquals(op.getFolderUuid(),createFolder.getFolderUuid());
                    Assert.assertEquals(op.getServerId(), createFolder.getServerId());
                    Assert.assertEquals(op.getTimestamp(), createFolder.getTimestamp());
                    Assert.assertEquals(op.getTxnOpCode(), createFolder.getTxnOpCode());
                    found = true;
                    ZimbraLog.test.debug("found op in file %s", logRef.encodeToString());
                    break;
                }

            }
        }
        Assert.assertTrue(found);
    }

    @Test
    public void testGetInvalidSequence() throws IOException {
        //make sure we have an archive
        createValidatedArchivedOp();
        rollover();

        GetMethod get = new GetMethod(URL);
        get.setQueryString("fmt=fileref&type=archive&seq=-1");
        HttpClient client = new HttpClient();
        int code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        String body = get.getResponseBodyAsString();
        StringTokenizer stoke = new StringTokenizer(body, "\r\n");
        HttpRedoLogFile logRef = null;
        long maxSequence = -1;
        while (stoke.hasMoreTokens()) {
            String line = stoke.nextToken();
            logRef = HttpRedoLogFile.decodeFromString(line);
            if (logRef.getSeq() > maxSequence) {
                maxSequence = logRef.getSeq();
            }
        }

        get = new GetMethod(URL);
        get.setQueryString("fmt=fileref&type=archive&seq="+maxSequence+1);
        code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_NOT_FOUND, code);
    }

    @Test
    public void testServiceRegistration() throws IOException, ServiceException {
        ServiceLocator locator = Zimbra.getAppContext().getBean(ServiceLocator.class);
        try {
            locator.ping();
        } catch (IOException ioe) {
            Assume.assumeNoException(ioe);
        }
        List<Entry> services = locator.find("zimbra-redolog", null, false);
        Assert.assertNotNull(services);

        Assert.assertTrue(services.size() > 0);
        boolean found = false;
        int expectedPort = Integer.valueOf(System.getProperty("jetty.port", "8080"));
        for (Entry service : services) {
            if (service.servicePort.intValue() == expectedPort) {
                found = true;
                //not checking IP/hostname since consul doesn't handle laptop IP change well
                //just checking that the servlet registered itself
//              java.net.InetAddress localhost = java.net.InetAddress.getLocalHost();
//              Assert.assertEquals(localhost.getHostName(), service.getFirst());
//              Assert.assertEquals(localhost.getHostAddress(), service.getSecond());
                break;
            }
        }
        Assert.assertTrue(found);
    }

    @Test
    @Ignore //inconsistent Consul behavior on laptops with dynamic IPs; old IP returned after change
    public void testRecordContiguity() throws Exception {

        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod(HttpRedoLogManager.getUrl(false));
        post.setParameter("cmd", "rollover");
        post.setParameter("forcePeers", "true");
        int code = client.executeMethod(post);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        post = new PostMethod(HttpRedoLogManager.getUrl(false));
        post.setParameter("cmd", "delete");
        post.setParameter("cutoff", System.currentTimeMillis()+"");
        code = client.executeMethod(post);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        //test for exercising failover/rollover logic
        //increase tries and start/stop/crash/invalidate nodes for testing

        int tries = 5000;
        int sleep = 1000;
        int retries = 0;
        LinkedHashMap<TransactionId, MockRedoableOp> ops = new LinkedHashMap<TransactionId, MockRedoableOp>(tries);
        MockRedoableOp op = null;
        for (int i = 0; i < tries; i++) {
            if (retries > tries) {
                break;
            }
            if (op == null) {
                op = new MockRedoableOp(1, "testOp-"+ i, Mailbox.ID_FOLDER_USER_ROOT, new FolderOptions());
                op.setTransactionId(new TransactionId());
                op.setTimestamp(System.currentTimeMillis());
            } else {
                retries++;
                i--;
            }
            String url = null;
            try {
                url = HttpRedoLogManager.getUrl(false);//switch true/false for no leader fallback testing
                Assert.assertNotNull(url);
            } catch (IOException ioe) {
                //no leader at this moment; pause and keep trying
                ZimbraLog.test.debug("no leader, waiting? ", ioe);
                Thread.sleep(sleep);
                continue;
            }
            client = new HttpClient();
            post = new PostMethod(url);
            post.setRequestEntity(new InputStreamRequestEntity(op.getInputStream()));
            code = -1;
            try {
                code = client.executeMethod(post);
                ZimbraLog.test.info(i + "- resp: " + code);
                Assert.assertTrue(code == HttpStatus.SC_OK || code == HttpStatus.SC_CONFLICT || code == HttpStatus.SC_SERVICE_UNAVAILABLE);
            } catch (IOException ioe) {
                ZimbraLog.test.info("IOException", ioe);
            }
            if (code != HttpStatus.SC_OK) {
                Thread.sleep(sleep);
                continue;
            } else {
                RedoLogInput in = new RedoLogInput(post.getResponseBodyAsStream());
                TransactionId txnId = new TransactionId();
                txnId.deserialize(in);
                Assert.assertTrue(txnId.getCounter() > 0);
                Assert.assertTrue(txnId.getTime() > 0);
                op.setTransactionId(txnId);
                ops.put(txnId, op);
                op = null;
            }
        }

        ZimbraLog.test.info("finished %d ops with %d retries", tries, retries);

        //now grab them back - should be in transaction order
        //this part of test is not setup to be resilient to failover, so stop disruptions before here

        client = new HttpClient();
        post = new PostMethod(HttpRedoLogManager.getUrl(false));
        post.setParameter("cmd", "rollover");
        post.setParameter("forcePeers", "true");
        code = client.executeMethod(post);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        GetMethod get = new GetMethod(HttpRedoLogManager.getUrl(false));
        client = new HttpClient();
        get.setQueryString("fmt=raw&type=archive");
        code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        RedoableOp rawOp = null;
        RedoLogInput input = new RedoLogInput(get.getResponseBodyAsStream());

        try {
            while((rawOp = RedoableOp.deserializeOp(input)) != null) {
                ZimbraLog.test.debug("deserialized op %s", rawOp);
                if (ops.keySet().contains(rawOp.getTransactionId())) {
                    //better be the first one
                    Assert.assertEquals(ops.keySet().iterator().next(), rawOp.getTransactionId());
                    op = ops.remove(rawOp.getTransactionId());
                    Assert.assertNotNull(op);
                    Assert.assertTrue(rawOp instanceof CreateFolder);
                    Assert.assertEquals(op.getName(), ((CreateFolder) rawOp).getName());
                }
            }
        } catch (EOFException eof) {
            //expected end of input
        }

        Assert.assertTrue("all expected ops found in archive", ops.isEmpty());

        //also make sure the file seq numbers are contiguous
        get = new GetMethod(URL);
        get.setQueryString("fmt=fileref&type=archive");
        client = new HttpClient();
        code = client.executeMethod(get);
        Assert.assertEquals(HttpStatus.SC_OK, code);

        String body = get.getResponseBodyAsString();
        StringTokenizer stoke = new StringTokenizer(body, "\r\n");
        HttpRedoLogFile logRef = null;

        long lastSeq = -1;
        while (stoke.hasMoreTokens()) {
            String line = stoke.nextToken();
            logRef = HttpRedoLogFile.decodeFromString(line);
            ZimbraLog.test.info("got logRef %s", logRef.encodeToString());
            Assert.assertNotNull(logRef.getName());
            Assert.assertTrue(lastSeq == -1 || logRef.getSeq() == lastSeq + 1);
            lastSeq = logRef.getSeq();
        }
    }

}
