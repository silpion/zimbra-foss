# Shared Redolog Support


## Overview

The Zimbra redolog system keeps a binary log of each significant mailbox transaction. This log is used for recovery after a crash during transaction commit, as well as for incremental backup and restore. Since requests for a given mailbox may be serviced by different mailstores at different periods of time, it is necessary to maintain a consistent shared log. 

## Storage

In this iteration we plan to support storage of redolog to a shared mountpoint supporting NFSv4. Technically and functionally we should be able to write to any standard Linux mountpoint, however pending testing we believe NFSv4 can provide the throughput necessary to support the transaction levels for modest size mailstore clusters. For example, we may determine that a given NFS configuration may be shared between 4-5 mailstores in a cluster, or 8-10 mailstores for another NFS configuration with higher performance characteristics.

## Servlet

A REST servlet has been created to encapsulate the business of reading and writing redolog records to a persistent store. This allows sequencing, rollover, locking, and other internals to be handled independently from the mailstore, so that future changes to those internals do not directly affect the mailstore. Using REST also provides a reasonably straightforward way for other clients such as curl to access the data, for debugging or future development purposes.

The servlet will be deployed under the /redolog context in a war file named zimbraredologservice. This war file must be deployed on at least one Jetty node, and should be deployed on 2 or more per cluster to provide for failover.

[Bug 97435](https://bugzilla.zimbra.com/show_bug.cgi?id=97435)

## Leader Election

Currently the process of writing to physical disk will be handled by a single node at any given time. This node will be the leader, established via a [Consul leader election](https://www.consul.io/docs/guides/leader-election.html). If the leader fails, steps down, or is demoted a new leader will be elected in its place. 

[Bug 98073](https://bugzilla.zimbra.com/show_bug.cgi?id=98073)

Since there may be brief periods of time when no leader is elected (e.g. during a redolog service failover), and since the redolog write phase is a critical step in each mailbox transaction; the redolog servlet will accept write operations when there is no leader. These operations will be queued locally and flushed to the leader as soon as one is elected. This mitigates the system-wide bottleneck that would otherwise exist during a failover; however it presents a small risk of redolog records being lost if a second node fails before a leader has been elected. A bug has been raised to consider addressing this in a future iteration.

[Bug 98672](https://bugzilla.zimbra.com/show_bug.cgi?id=98672)

## Mailstore Writer

The mailstore will write to the REST service using HTTP. This client will attempt to find the leader via Consul, but will support falling back to a healthy node if no leader is present. All accesses from the mailstore will go through the REST servlet; even if the mailstore happens to have the storage mounted locally.

[Bug 97610](https://bugzilla.zimbra.com/show_bug.cgi?id=97610)

## Basic Configuration

**Developers: Note that step 1 and 2 are performed automatically as part of the server 'reset-all' build. This allows use of the service for a single node dev environment even if you choose not to connect it to NFS.**

1. Install redolog service. This will be a menu option in the installer; or installed automatically with other services during development iterations. This will cause the webapp to be placed in the /jetty/webapps/zimbraredologservice directory, and will cause the value 'redolog' to be added to the zimbraServiceEnabled attribute. [Bug 98671](https://bugzilla.zimbra.com/show_bug.cgi?id=98671)

2. Configure mailstore for redolog service usage. This will likely become the default option once installation work is complete. Set the zimbraRedoLogProvider attribute to 'com.zimbra.cs.redolog.HttpRedoLogProvider' on all mailstore servers (or in cluster/global config)

3. Mount an NFSv4 share somewhere on the filesystem; for example in /opt/zimbra-data/mnt/redolog Setup varies for each operating system version. [Ubuntu NFSv4 How To](https://help.ubuntu.com/community/NFSv4Howto). Mount the same share on each redolog service instance in the cluster

4. Set the zimbraRedoLogLogPath and zimbraRedoLogArchiveDir to point to the path mounted in step 3. The LogPath attribute is a full filename (e.g. /opt/zimbra-data/mnt/redolog/redo.log) while the ArchiveDir attribute is a directory (e.g. /opt/zimbra-data/mnt/redolog/archive)

5. Restart all redolog and mailstore services

## Security

The redolog service does not require authentication at this time. Since it exposes the raw data for all email received by the system this will be have to be considered before putting it into production. The service listens on a separate port (7994), so for most sites it will not be accessible from outside the data center unless the site specifically configures a firewall exception for the redolog port.  Additional protections such as only allowing access from known internal IP ranges, or requiring a system administrator auth token could be added by extending existing filters and authentication modules used by other webapps. 

## Other Considerations

The default configuration provides complete support for server crash recovery, mailbox failover, and backup/restore. In certain situations it may be desirable to disable, reduce, or limit these features for performance benefit or other similar reasons. This section discusses some of the tradeoffs. These are not intended as recommendations or supported configurations, but rather to outline some of the possibilities for reducing overhead in extreme circumstances or for future iteration.

- Individual server crash recovery only. Redologs could be maintained on an individual per-server basis, in the same manner that they are in 8.x releases; by using FileRedoLogManager directly on each mailstore; bypassing the new service entirely. This would greatly reduce the chance of a bottleneck since there would be no need to maintain sequencing of operations across mailstores. In this mode, if a server fails and mailboxes are routed to a new server there is a small chance of inconsistent data, which would need manual replay to resolve. This option would render incremental backups useless since no consistent sequential log of individual user actions would exist.

- Asynchronous logging. Some parts of the redolog write process are already asynchronous, however others could be made asynchronous. The tradeoff here would be a chance of lost redolog data; for example if mailstore proceeds as if an op record is persisted but it is really in a transient state waiting for eventual write, that op could become lost in a crash. Worth exploring if the system bottlenecks on write activity.

- Stage ops to queue. Currently we assume that the write to disk will be reasonably fast so that we can sequence the writes directly by the order of HTTP requests. If the disk write throughput cannot keep up, it may be possible to write temporarily to a queue backed by Redis or another queuing service. The leader would then drain from the queue. This would keep ordering and persistence while minimizing the effect of a slow disk on mailstore request throughput.

## Debugging Commands

Here is a collection of useful commands for debugging. Where 7994 appears; this is the Jetty redolog port (zimbraRedologBindPort). Where 8500 appears, this is the Consul port.

### Cause a redolog rollover


rollover a single node. fails if the node is not the leader:
```sh
$ curl -X POST 'https://localhost:7994/redolog/data?cmd=rollover'
```

force rollover a single node, even if it is not the leader:
```sh
$ curl -X POST 'https://localhost:7994/redolog/data?cmd=rollover&force=true'
```

force rollover peer nodes if they exist
```sh
$ curl -X POST 'https://localhost:7994/redolog/data?cmd=rollover&forcePeers=true'
```

### Get redolog data


current file in text format similar to zmredodump
```sh
$ curl 'https://localhost:7994/redolog/data?type=current'
```

archive file
```sh
$ curl 'https://localhost:7994/redolog/data?type=archive'
```

raw binary format
```sh
$ curl 'https://localhost:7994/redolog/data?fmt=raw' -o test.redolog
```

### Send example redolog transaction

First, save the data to example.redolog. The format is the same as the on-disk format; minus headers
```sh
$ curl -X POST 'https://localhost:7994/redolog/data' --data-binary @example.redolog --verbose -H "Content-Type:application/x-zimbra-redolog" -o txnids.out
```

### Find the Redolog Leader (Consul)

First find the sessionId
```sh
$ curl http://localhost:8500/v1/kv/service/zimbra-redolog/leader
[{"CreateIndex":57719,"ModifyIndex":146719,"LockIndex":20,"Key":"service/zimbra-redolog/leader","Flags":0,"Value":null,"Session":"9ce2d2e0-81ea-6045-e391-7cadc10e1938"}]
```

Then find the node
```sh
$ curl http://localhost:8500/v1/session/info/9ce2d2e0-81ea-6045-e391-7cadc10e1938
[{"CreateIndex":103,"ID":"9ce2d2e0-81ea-6045-e391-7cadc10e1938","Name":"zimbra-redolog:7994","Node":"example001.zimbra.com","Checks":["service:zimbra-redolog:7994"],"LockDelay":15000000000,"Behavior":"release","TTL":""}]
```

### List all known Redolog service instances (Consul)

```sh
$ curl http://localhost:8500/v1/catalog/service/zimbra-redolog
[{"Node":"zdev-vm014.eng.zimbra.com","Address":"10.137.242.164","ServiceID":"zimbra-redolog:7994","ServiceName":"zimbra-redolog","ServiceTags":["ssl"],"ServiceAddress":"","ServicePort":7994},
{"Node":"zdev-vm017.eng.zimbra.com","Address":"10.137.242.167","ServiceID":"zimbra-redolog:7994","ServiceName":"zimbra-redolog","ServiceTags":["ssl"],"ServiceAddress":"","ServicePort":7994}]
```
