package com.zimbra.cs.im;

import org.dom4j.QName;

import com.zimbra.common.service.ServiceException;
import com.zimbra.common.soap.MailConstants;
import com.zimbra.cs.extension.ExtensionException;
import com.zimbra.cs.extension.ZimbraExtension;
import com.zimbra.cs.service.im.ZimbraIMService;
import com.zimbra.soap.SoapServlet;

/**
 * @author Greg Solovyev
 *
 */
public class ZimbraIMExtension implements ZimbraExtension {
    public static final String EXTENSION_NAME = "zimbraim";
    public static final String E_GET_BOSH_SESSION_REQUEST = "GetBOSHSessionRequest";
    public static final String E_GET_BOSH_SESSION_RESPONSE = "GetBOSHSessionResponse";
    public static final QName GET_BOSH_SESSION_REQUEST = QName.get(E_GET_BOSH_SESSION_REQUEST, MailConstants.NAMESPACE);
    public static final QName GET_BOSH_SESSION_RESPONSE = QName.get(E_GET_BOSH_SESSION_RESPONSE, MailConstants.NAMESPACE);
    public ZimbraIMExtension() {
        // TODO Auto-generated constructor stub
    }

    /* (non-Javadoc)
     * @see com.zimbra.cs.extension.ZimbraExtension#getName()
     */
    @Override
    public String getName() {
        return EXTENSION_NAME;
    }

    @Override
    public void init() throws ExtensionException, ServiceException {
        SoapServlet.addService("SoapServlet", new ZimbraIMService());

    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }

}
