package com.zimbra.cs.service.im;

import com.zimbra.cs.im.ZimbraIMExtension;
import com.zimbra.soap.DocumentDispatcher;
import com.zimbra.soap.DocumentService;

public class ZimbraIMService implements DocumentService {

    public ZimbraIMService() {
        // TODO Auto-generated constructor stub
    }

    public void registerHandlers(DocumentDispatcher dispatcher) {
        dispatcher.registerHandler(ZimbraIMExtension.GET_BOSH_SESSION_REQUEST, new GetBOSHSession());
    }

}
