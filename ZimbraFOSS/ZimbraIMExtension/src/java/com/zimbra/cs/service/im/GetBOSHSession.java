package com.zimbra.cs.service.im;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import javax.security.auth.login.LoginException;

import rocks.xmpp.core.session.XmppSession;
import rocks.xmpp.core.session.XmppSessionConfiguration;
import rocks.xmpp.extensions.httpbind.BoshConnection;
import rocks.xmpp.extensions.httpbind.BoshConnectionConfiguration;

import com.zimbra.common.service.ServiceException;
import com.zimbra.common.soap.Element;
import com.zimbra.cs.account.AuthTokenException;
import com.zimbra.cs.account.Provisioning;
import com.zimbra.cs.account.Server;
import com.zimbra.cs.im.ZimbraIMExtension;
import com.zimbra.cs.service.account.AccountDocumentHandler;
import com.zimbra.soap.ZimbraSoapContext;

public class GetBOSHSession extends AccountDocumentHandler {

    @Override
    public Element handle(Element request, Map<String, Object> context)
            throws ServiceException {
        ZimbraSoapContext zsc = getZimbraSoapContext(context);
        Element response = zsc.createElement(ZimbraIMExtension.GET_BOSH_SESSION_RESPONSE);
        // Connect
        XmppSession xmppSession = null;
        try {
        	Provisioning prov = Provisioning.getInstance();
        	Server localServer = prov.getLocalServer();
            BoshConnectionConfiguration boshConnectionConfiguration = BoshConnectionConfiguration.builder()
                    .hostname(localServer.getReverseProxyXmppBoshHostname())
                    .port(localServer.getReverseProxyXmppBoshPort())
                    .secure(false)
                    .file(localServer.getReverseProxyXmppBoshRemoteHttpBindURL())
                    .build();

            Class<?>[] extensions = new Class<?>[0];
            Arrays.asList(extensions, XmppSession.class);
            XmppSessionConfiguration configuration = XmppSessionConfiguration.builder()
                    .defaultResponseTimeout(5000).build();
            xmppSession = new XmppSession(zsc.getAuthToken().getAccount().getDomainName(), configuration, boshConnectionConfiguration);
            xmppSession.connect();
            // Login
            xmppSession.login(zsc.getAuthToken().getAccount().getName(), String.format("__zmauth__%s",zsc.getAuthToken().getEncoded()));
            BoshConnection connection = (BoshConnection)(xmppSession.getActiveConnection());
            String sid = connection.getSessionId();
            String jid = xmppSession.getConnectedResource().toString();
            long rid = connection.detach();
            response.addUniqueElement("XMPPSession").addAttribute("sid",sid).addAttribute("rid", rid).addAttribute("jid", jid).addAttribute("url", localServer.getReverseProxyXmppBoshLocalHttpBindURL());
        } catch (IOException | LoginException | AuthTokenException e) {
            throw ServiceException.FAILURE("Caught an exception trying to authenticate to XMPP server", e);
        } finally {
            if(xmppSession != null) {
                try {
                    xmppSession.close();
                } catch (IOException e) {
                    throw ServiceException.FAILURE("Caught an exception trying to close to XMPP session", e);
                }
            }
        }
        return response;
    }

}
