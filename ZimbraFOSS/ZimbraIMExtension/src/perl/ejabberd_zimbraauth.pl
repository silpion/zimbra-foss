#!/usr/bin/perl -w

use FindBin;                 # locate this script
use lib "$FindBin::Bin/";

use LWP::UserAgent;

use XmlElement;
use XmlDoc;
use Soap;

use lib '.';
use Log::Log4perl qw(:easy);


my $ZIMBRANS = "urn:zimbra";
my $ACCTNS = "urn:zimbraAccount";
my $MAILNS = "urn:zimbraMail";
my $SOAP = $Soap::Soap12;
my $url = "http://localhost:7070/service/soap/";
my $adminToken;
Log::Log4perl->easy_init({level => $INFO, file => ">> /opt/zimbra/log/zimbraauth.log", });

sub LOG() {
    my ($str) = @_;
    INFO($$." ".$str);
}

sub get_token() {
    my ($username,$host,$password) = @_;
    my $d = new XmlDoc;
    my $name = &concat_username($username, $host);
    $d->start('AuthRequest', $ACCTNS);
    $d->add('account', undef, { by => "name"}, $name);
    $d->add('password', undef, undef, $password);
    $d->end();
    my $authResponse = $SOAP->invoke($url, $d->root());
    my $tNode = $authResponse && $authResponse->find_child('authToken');
    return ($tNode && $tNode->content) || 0;
}

sub get_admin_token() {
    return &get_token("jabberadmin", "gregs-mbp.local", "test123");
}

sub concat_username() {
    my ($username, $host) = @_;
    return ($username =~ /@/) ? $username : ($username . "@" . $host);
}

sub auth_password() {
    my ($username,$host,$password) = @_;
    my $token = &get_token($username,$host,$password);
    return ($token)? 1:0;
}

sub auth_token() {
    my ($username,$host,$token) = @_;
    my $name = &concat_username($username, $host);
    my $context = $SOAP->zimbraContext($token);
    my $d = new XmlDoc;
    $d->start('GetInfoRequest', $ACCTNS, {sections=>'mbox'});
    $d->end();
    my $response = $SOAP->invoke($url, $d->root(), $context);
    my $nNode = $response && $response->find_child('name');
    my $tName = $nNode && $nNode->content;
    return ($tName && ($tName eq $name));
}

sub is_user() {
    my ($username, $host, $renewToken) = @_;
    if (!$adminToken || $renewToken) {
	$adminToken = &get_admin_token();
    }
    my $name = &concat_username($username, $host);
    my $context = $SOAP->zimbraContext($adminToken);
    my $d = new XmlDoc;
    $d->start('GetAccountInfoRequest', $ACCTNS);
    $d->start('account', undef, {by=>'name'}, $name);
    $d->end();
    $d->end();
    my $response = $SOAP->invoke($url, $d->root(), $context);
    if ($response->name eq 'Fault' && !$renewToken) {
	my $code = $response->find_descendant('Detail','Error','Code');
	if ($code && $code->content eq 'service.AUTH_REQUIRED') { # Token is invalid, get a new one and try again
	    return &is_user($username, $host, 1);
	}
    }
    return $response->name eq 'GetAccountInfoResponse';
}

sub read_input() {
    while(!sysread(STDIN, $b, 2)) { # run until input
	sleep(1);
    }
    $length = unpack("n", $b); # Decode as unsigned short (big endian)
    #&LOG("Reading length field from ejabberd; input length is ".($length||"ZERO")." bytes");
    if ($length) {
	sysread(STDIN, $buffer, $length);
	$buffer =~ s/^\s+//; # trim the string
	$buffer =~ s/\s+$//;
	#&LOG("Read ".$length." bytes of data from ejabberd: ".$buffer);
	return split(/:/, $buffer);
    }
}

sub write_output() {
    my ($answer) = @_;
    syswrite(STDOUT, pack("nn", 2, $answer ? 1 : 0)); # Encode as unsigned short (big endian)
}

$SIG{TERM} = sub {
    &LOG("SHUTTING DOWN (SIGTERM) ...");
    exit(0);
};

$SIG{INT} = sub {
    &LOG("SHUTTING DOWN (SIGINT) ...");
    exit(0);
};

&LOG("STARTING UP...");
while (1) {
    my ($op,$username,$host,$password) = read_input();
    $answer = 0;
    
    if ($op eq "auth") {
        if(length($password)<100) {
            $answer = &auth_password($username, $host, $password);
        } else {
            my $pwdPrefx = substr ($password,0,10);
            if($pwdPrefx eq "__zmauth__") {
                $answer = &auth_token($username, $host, substr ($password,10));
            }
        }
    }

    if ($op eq "isuser") {
	   $answer = &is_user($username, $host);
    }
    &LOG("{op: ".($op||"NULL").", username: ".($username||"NULL").", host: ".($host||"NULL").($password?", password: <HIDDEN>":"")."} => ".($answer?"OK":"FAIL"));
    &write_output($answer);
}