/* The following code was generated by JFlex 1.6.0 */

/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2010, 2011, 2013 Zimbra Software, LLC.
 * 
 * The contents of this file are subject to the Zimbra Public License
 * Version 1.4 ("License"); you may not use this file except in
 * compliance with the License.  You may obtain a copy of the License at
 * http://www.zimbra.com/license.
 * 
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * ***** END LICENSE BLOCK *****
 */
package com.zimbra.solr;

import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;


/**
 * This class is a scanner generated by 
 * <a href="http://www.jflex.de/">JFlex</a> 1.6.0
 * from the specification file <tt>/Users/iraykin/p4/main/depot/zimbra/users/gregs/SOLR/SolrPlugins/src/com/zimbra/solr/UniversalLexer.jflex</tt>
 */
final class ZimbraLexer {

  /** This character denotes the end of file */
  public static final int YYEOF = -1;

  /** initial size of the lookahead buffer */
  private static final int ZZ_BUFFERSIZE = 16384;

  /** lexical states */
  public static final int YYINITIAL = 0;

  /**
   * ZZ_LEXSTATE[l] is the state in the DFA for the lexical state l
   * ZZ_LEXSTATE[l+1] is the state in the DFA for the lexical state l
   *                  at the beginning of a line
   * l is of the form l = 2*k, k a non negative integer
   */
  private static final int ZZ_LEXSTATE[] = { 
     0, 0
  };

  /** 
   * Translates characters to character classes
   */
  private static final String ZZ_CMAP_PACKED = 
    "\11\0\1\0\1\14\1\0\1\0\1\15\22\0\1\0\1\11\1\0"+
    "\1\11\1\11\1\11\1\12\1\22\1\11\1\11\1\0\1\5\1\10"+
    "\1\4\1\7\1\0\12\16\1\0\1\11\1\5\1\0\1\5\1\11"+
    "\1\23\32\13\1\11\1\0\1\11\1\11\1\6\1\0\32\13\1\11"+
    "\1\0\1\11\1\11\6\0\1\0\44\0\1\13\12\0\1\13\4\0"+
    "\1\13\5\0\27\13\1\0\37\13\1\0\u01ca\13\4\0\14\13\16\0"+
    "\5\13\7\0\1\13\1\0\1\13\201\0\5\13\1\0\2\13\2\0"+
    "\4\13\1\0\1\13\6\0\1\13\1\0\3\13\1\0\1\13\1\0"+
    "\24\13\1\0\123\13\1\0\213\13\10\0\246\13\1\0\46\13\2\0"+
    "\1\13\7\0\47\13\110\0\33\13\5\0\3\13\55\0\53\13\25\0"+
    "\12\16\4\0\2\13\1\0\143\13\1\0\1\13\17\0\2\13\7\0"+
    "\2\13\12\16\3\13\2\0\1\13\20\0\1\13\1\0\36\13\35\0"+
    "\131\13\13\0\1\13\16\0\12\16\41\13\11\0\2\13\4\0\1\13"+
    "\5\0\26\13\4\0\1\13\11\0\1\13\3\0\1\13\27\0\31\13"+
    "\107\0\23\13\121\0\66\13\3\0\1\13\22\0\1\13\7\0\12\13"+
    "\4\0\12\16\1\0\20\13\4\0\10\13\2\0\2\13\2\0\26\13"+
    "\1\0\7\13\1\0\1\13\3\0\4\13\3\0\1\13\20\0\1\13"+
    "\15\0\2\13\1\0\3\13\4\0\12\16\2\13\23\0\6\13\4\0"+
    "\2\13\2\0\26\13\1\0\7\13\1\0\2\13\1\0\2\13\1\0"+
    "\2\13\37\0\4\13\1\0\1\13\7\0\12\16\2\0\3\13\20\0"+
    "\11\13\1\0\3\13\1\0\26\13\1\0\7\13\1\0\2\13\1\0"+
    "\5\13\3\0\1\13\22\0\1\13\17\0\2\13\4\0\12\16\25\0"+
    "\10\13\2\0\2\13\2\0\26\13\1\0\7\13\1\0\2\13\1\0"+
    "\5\13\3\0\1\13\36\0\2\13\1\0\3\13\4\0\12\16\1\0"+
    "\1\13\21\0\1\13\1\0\6\13\3\0\3\13\1\0\4\13\3\0"+
    "\2\13\1\0\1\13\1\0\2\13\3\0\2\13\3\0\3\13\3\0"+
    "\14\13\26\0\1\13\25\0\12\16\25\0\10\13\1\0\3\13\1\0"+
    "\27\13\1\0\20\13\3\0\1\13\32\0\2\13\6\0\2\13\4\0"+
    "\12\16\25\0\10\13\1\0\3\13\1\0\27\13\1\0\12\13\1\0"+
    "\5\13\3\0\1\13\40\0\1\13\1\0\2\13\4\0\12\16\1\0"+
    "\2\13\22\0\10\13\1\0\3\13\1\0\51\13\2\0\1\13\20\0"+
    "\1\13\21\0\2\13\4\0\12\16\12\0\6\13\5\0\22\13\3\0"+
    "\30\13\1\0\11\13\1\0\1\13\2\0\7\13\37\0\12\16\20\0"+
    "\1\3\60\3\1\3\2\3\14\3\7\3\11\3\12\21\46\3\1\2"+
    "\2\2\1\2\1\2\2\2\2\2\1\2\1\2\2\2\1\2\6\2"+
    "\4\2\1\2\7\2\1\2\3\2\1\2\1\2\1\2\1\2\2\2"+
    "\2\2\1\2\4\2\1\2\2\2\11\2\1\2\2\2\5\2\1\2"+
    "\1\2\11\2\12\20\2\2\4\2\40\2\1\13\37\0\12\16\26\0"+
    "\10\13\1\0\44\13\33\0\5\13\163\0\53\2\24\2\1\2\12\20"+
    "\6\2\6\2\4\2\4\2\3\2\1\2\3\2\2\2\7\2\3\2"+
    "\4\2\15\2\14\2\1\2\1\2\12\20\6\2\46\13\1\0\1\13"+
    "\5\0\1\13\2\0\53\13\1\0\u014d\13\1\0\4\13\2\0\7\13"+
    "\1\0\1\13\1\0\4\13\2\0\51\13\1\0\4\13\2\0\41\13"+
    "\1\0\4\13\2\0\7\13\1\0\1\13\1\0\4\13\2\0\17\13"+
    "\1\0\71\13\1\0\4\13\2\0\103\13\45\0\20\13\20\0\125\13"+
    "\14\0\u026c\13\2\0\21\13\1\0\32\13\5\0\113\13\6\0\10\13"+
    "\7\0\15\13\1\0\4\13\16\0\22\13\16\0\22\13\16\0\15\13"+
    "\1\0\3\13\17\0\64\2\43\2\1\2\4\2\1\2\3\2\12\20"+
    "\26\2\20\0\12\16\6\0\130\13\10\0\51\13\1\0\1\13\5\0"+
    "\106\13\12\0\37\13\47\0\12\16\36\13\2\0\5\13\13\0\54\13"+
    "\25\0\7\13\10\0\12\16\46\0\27\13\11\0\65\13\53\0\12\16"+
    "\6\0\12\16\15\0\1\13\135\0\57\13\21\0\7\13\4\0\12\16"+
    "\51\0\36\13\15\0\2\13\12\16\54\13\32\0\44\13\34\0\12\16"+
    "\3\0\3\13\12\16\44\13\153\0\4\13\1\0\4\13\3\0\2\13"+
    "\11\0\300\13\100\0\u0116\13\2\0\6\13\2\0\46\13\2\0\6\13"+
    "\2\0\10\13\1\0\1\13\1\0\1\13\1\0\1\13\1\0\37\13"+
    "\2\0\65\13\1\0\7\13\1\0\1\13\3\0\3\13\1\0\7\13"+
    "\3\0\4\13\2\0\6\13\4\0\15\13\5\0\3\13\1\0\7\13"+
    "\53\0\1\0\1\0\107\0\1\13\15\0\1\13\20\0\15\13\145\0"+
    "\1\13\4\0\1\13\2\0\12\13\1\0\1\13\3\0\5\13\6\0"+
    "\1\13\1\0\1\13\1\0\1\13\1\0\4\13\1\0\13\13\2\0"+
    "\4\13\5\0\5\13\4\0\1\13\64\0\2\13\u0a7b\0\57\13\1\0"+
    "\57\13\1\0\205\13\6\0\4\13\3\0\2\13\14\0\46\13\1\0"+
    "\1\13\5\0\1\13\2\0\70\13\7\0\1\13\20\0\27\13\11\0"+
    "\7\13\1\0\7\13\1\0\7\13\1\0\7\13\1\0\7\13\1\0"+
    "\7\13\1\0\7\13\1\0\7\13\120\0\1\13\120\0\u0180\1\5\0"+
    "\2\13\52\0\5\13\5\0\2\13\3\0\1\1\126\1\6\1\3\1"+
    "\1\1\132\1\1\1\4\1\5\1\51\1\3\1\136\1\21\1\33\1"+
    "\65\1\20\1\u0200\1\u19b6\1\112\1\u51cd\1\63\1\u048d\13\103\0\56\13"+
    "\2\0\u010d\13\3\0\20\13\12\16\2\13\24\0\57\13\20\0\37\13"+
    "\2\0\106\13\61\0\11\13\2\0\147\13\2\0\4\13\1\0\36\13"+
    "\2\0\2\13\105\0\13\13\1\0\3\13\1\0\4\13\1\0\27\13"+
    "\35\0\64\13\16\0\62\13\34\0\12\16\30\0\6\13\3\0\1\13"+
    "\4\0\12\16\34\13\12\0\27\13\31\0\35\13\7\0\57\13\34\0"+
    "\1\13\12\16\6\0\5\13\1\0\12\13\12\16\5\13\1\0\51\13"+
    "\27\0\3\13\1\0\10\13\4\0\12\16\6\0\27\13\3\0\1\13"+
    "\3\0\62\13\1\0\1\13\3\0\2\13\2\0\5\13\2\0\1\13"+
    "\1\0\1\13\30\0\3\13\2\0\13\13\7\0\3\13\14\0\6\13"+
    "\2\0\6\13\2\0\6\13\11\0\7\13\1\0\7\13\1\0\53\13"+
    "\1\0\4\13\4\0\2\13\132\0\43\13\15\0\12\16\6\0\u2ba4\1"+
    "\14\1\27\1\4\1\61\1\4\1\u2100\0\u016e\13\2\0\152\13\46\0"+
    "\7\13\14\0\5\13\5\0\1\13\1\0\12\13\1\0\15\13\1\0"+
    "\5\13\1\0\1\13\1\0\2\13\1\0\2\13\1\0\154\13\41\0"+
    "\u016b\13\22\0\100\13\2\0\66\13\50\0\14\13\164\0\5\13\1\0"+
    "\207\13\3\0\20\1\12\17\7\1\32\1\6\1\32\1\13\1\131\1"+
    "\3\1\6\1\2\1\6\1\2\1\6\1\2\1\3\1\23\1\20\0"+
    "\14\13\1\0\32\13\1\0\23\13\1\0\2\13\1\0\17\13\2\0"+
    "\16\13\42\0\173\13\u0185\0\35\13\3\0\61\13\57\0\40\13\20\0"+
    "\21\13\1\0\10\13\6\0\46\13\12\0\36\13\2\0\44\13\4\0"+
    "\10\13\60\0\236\13\2\0\12\16\126\0\50\13\10\0\64\13\234\0"+
    "\u0137\13\11\0\26\13\12\0\10\13\230\0\6\13\2\0\1\13\1\0"+
    "\54\13\1\0\2\13\3\0\1\13\2\0\27\13\12\0\27\13\11\0"+
    "\37\13\141\0\26\13\12\0\32\13\106\0\70\13\6\0\2\13\100\0"+
    "\1\13\17\0\4\13\1\0\3\13\1\0\33\13\54\0\35\13\3\0"+
    "\35\13\43\0\10\13\1\0\34\13\33\0\66\13\12\0\26\13\12\0"+
    "\23\13\15\0\22\13\156\0\111\13\u03ba\0\65\13\56\0\12\16\23\0"+
    "\55\13\40\0\31\13\7\0\12\16\11\0\44\13\17\0\12\16\20\0"+
    "\43\13\3\0\1\13\14\0\60\13\16\0\4\13\13\0\12\16\1\13"+
    "\45\0\22\13\1\0\31\13\204\0\57\13\21\0\12\16\13\0\10\13"+
    "\2\0\2\13\2\0\26\13\1\0\7\13\1\0\2\13\1\0\5\13"+
    "\3\0\1\13\37\0\5\13\u011e\0\60\13\24\0\2\13\1\0\1\13"+
    "\10\0\12\16\246\0\57\13\121\0\60\13\24\0\1\13\13\0\12\16"+
    "\46\0\53\13\25\0\12\16\u01d6\0\100\13\12\16\25\0\1\13\u01c0\0"+
    "\71\13\u0507\0\u0399\13\u0c67\0\u042f\13\u33d1\0\u0239\13\7\0\37\13\1\0"+
    "\12\16\146\0\36\13\22\0\60\13\20\0\4\13\14\0\12\16\11\0"+
    "\25\13\5\0\23\13\u0370\0\105\13\13\0\1\13\102\0\15\13\u4060\0"+
    "\2\13\u0bfe\0\153\13\5\0\15\13\3\0\11\13\7\0\12\13\u1766\0"+
    "\125\13\1\0\107\13\1\0\2\13\2\0\1\13\2\0\2\13\2\0"+
    "\4\13\1\0\14\13\1\0\1\13\1\0\7\13\1\0\101\13\1\0"+
    "\4\13\2\0\10\13\1\0\7\13\1\0\34\13\1\0\4\13\1\0"+
    "\5\13\1\0\1\13\3\0\7\13\1\0\u0154\13\2\0\31\13\1\0"+
    "\31\13\1\0\37\13\1\0\31\13\1\0\37\13\1\0\31\13\1\0"+
    "\37\13\1\0\31\13\1\0\37\13\1\0\31\13\1\0\10\13\2\0"+
    "\62\16\u1000\0\305\13\u053b\0\4\13\1\0\33\13\1\0\2\13\1\0"+
    "\1\13\2\0\1\13\1\0\12\13\1\0\4\13\1\0\1\13\1\0"+
    "\1\13\6\0\1\13\4\0\1\13\1\0\1\13\1\0\1\13\1\0"+
    "\3\13\1\0\2\13\1\0\1\13\2\0\1\13\1\0\1\13\1\0"+
    "\1\13\1\0\1\13\1\0\1\13\1\0\2\13\1\0\1\13\2\0"+
    "\4\13\1\0\7\13\1\0\4\13\1\0\4\13\1\0\1\13\1\0"+
    "\12\13\1\0\21\13\5\0\3\13\1\0\5\13\1\0\21\13\u1144\0"+
    "\ua6d7\13\51\0\u1035\13\13\0\336\13\u3fe2\0\u021e\13\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\u05f0\0";

  /** 
   * Translates characters to character classes
   */
  private static final char [] ZZ_CMAP = zzUnpackCMap(ZZ_CMAP_PACKED);

  /** 
   * Translates DFA states to action switch labels.
   */
  private static final int [] ZZ_ACTION = zzUnpackAction();

  private static final String ZZ_ACTION_PACKED_0 =
    "\1\0\1\1\1\2\1\3\1\4\1\1\1\5\1\6"+
    "\1\1\4\6\1\0\5\6\3\0\1\6\2\0\2\6"+
    "\3\0\5\6\1\7\1\6\1\7\2\10\1\7\1\11"+
    "\1\0\1\12\1\11\1\0\4\7\2\10\6\7\2\6"+
    "\1\0\2\6\4\0\1\13\1\10\3\0\2\6\1\10"+
    "\2\7\1\10\1\7\1\0\1\7\1\10\1\14\1\0"+
    "\3\7";

  private static int [] zzUnpackAction() {
    int [] result = new int[87];
    int offset = 0;
    offset = zzUnpackAction(ZZ_ACTION_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAction(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /** 
   * Translates a state to a row index in the transition table
   */
  private static final int [] ZZ_ROWMAP = zzUnpackRowMap();

  private static final String ZZ_ROWMAP_PACKED_0 =
    "\0\0\0\24\0\50\0\74\0\120\0\144\0\170\0\214"+
    "\0\240\0\264\0\310\0\334\0\360\0\144\0\u0104\0\u0118"+
    "\0\u012c\0\u0140\0\u0154\0\u0168\0\u017c\0\u0190\0\u01a4\0\u01b8"+
    "\0\u01cc\0\u01e0\0\u01f4\0\u0208\0\u021c\0\u0230\0\u0244\0\u0258"+
    "\0\u026c\0\u0280\0\u0294\0\u02a8\0\u02bc\0\u02d0\0\u02e4\0\u02f8"+
    "\0\u030c\0\u0190\0\u0320\0\u0334\0\u0348\0\u035c\0\u0370\0\u0384"+
    "\0\u0398\0\u03ac\0\u03c0\0\u03d4\0\u03e8\0\u03fc\0\u0410\0\u0424"+
    "\0\u0438\0\u044c\0\u0460\0\u0474\0\u0488\0\u049c\0\u04b0\0\u04c4"+
    "\0\u04d8\0\u04ec\0\u0500\0\u0514\0\u0528\0\u053c\0\u0550\0\u0564"+
    "\0\u0578\0\u058c\0\u05a0\0\u05b4\0\u05c8\0\u05dc\0\u05f0\0\u0604"+
    "\0\u0618\0\u062c\0\u0640\0\u0654\0\u0668\0\u067c\0\u0690";

  private static int [] zzUnpackRowMap() {
    int [] result = new int[87];
    int offset = 0;
    offset = zzUnpackRowMap(ZZ_ROWMAP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackRowMap(String packed, int offset, int [] result) {
    int i = 0;  /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int high = packed.charAt(i++) << 16;
      result[j++] = high | packed.charAt(i++);
    }
    return j;
  }

  /** 
   * The transition table of the DFA
   */
  private static final int [] ZZ_TRANS = zzUnpackTrans();

  private static final String ZZ_TRANS_PACKED_0 =
    "\1\2\1\3\1\4\1\5\3\6\2\2\2\7\1\10"+
    "\1\2\1\11\1\12\1\13\1\14\1\15\2\2\25\0"+
    "\1\3\2\0\3\3\10\0\1\3\6\0\1\4\1\0"+
    "\3\4\11\0\1\4\6\0\1\5\15\0\1\5\3\0"+
    "\1\3\1\4\1\0\3\16\4\0\1\17\2\0\1\17"+
    "\1\20\1\21\1\17\13\0\2\7\15\0\1\22\1\17"+
    "\1\23\1\24\1\25\1\0\1\26\1\27\2\0\4\12"+
    "\1\30\1\31\14\0\1\2\13\0\1\32\1\17\1\33"+
    "\1\34\1\35\2\0\1\12\2\0\4\12\1\0\1\36"+
    "\1\0\1\3\2\0\1\37\1\20\1\40\1\34\1\35"+
    "\2\0\1\12\2\0\1\12\1\13\2\12\1\0\1\36"+
    "\2\0\1\4\1\0\1\41\1\21\1\42\1\34\1\35"+
    "\2\0\1\12\2\0\2\12\1\14\1\12\1\0\1\36"+
    "\3\0\1\5\1\32\1\17\1\33\1\34\1\35\2\0"+
    "\1\12\2\0\3\12\1\15\1\0\1\36\4\0\3\17"+
    "\4\0\1\17\2\0\4\17\3\0\1\3\2\0\3\20"+
    "\4\0\1\17\2\0\1\17\1\20\2\17\4\0\1\4"+
    "\1\0\3\21\4\0\1\17\2\0\2\17\1\21\1\17"+
    "\6\0\3\17\4\0\1\43\2\0\4\44\6\0\3\17"+
    "\4\0\1\45\2\0\4\46\15\0\1\47\2\0\4\50"+
    "\15\0\1\25\2\0\4\51\15\0\1\52\14\0\1\22"+
    "\1\17\1\23\1\53\1\25\1\0\1\26\1\27\2\0"+
    "\4\12\1\30\1\31\13\0\1\54\23\0\1\55\2\0"+
    "\4\56\6\0\3\17\4\0\1\57\2\0\4\60\6\0"+
    "\3\17\4\0\1\61\2\0\4\62\15\0\1\63\2\0"+
    "\4\64\15\0\1\65\2\0\4\66\15\0\1\56\2\0"+
    "\4\56\3\0\1\3\2\0\3\20\4\0\1\57\2\0"+
    "\1\60\1\67\2\60\3\0\1\3\2\0\3\20\4\0"+
    "\1\61\2\0\1\62\1\70\2\62\4\0\1\4\1\0"+
    "\3\21\4\0\1\57\2\0\2\60\1\71\1\60\4\0"+
    "\1\4\1\0\3\21\4\0\1\61\2\0\2\62\1\72"+
    "\1\62\6\0\1\73\1\17\1\74\1\75\3\0\1\43"+
    "\2\0\4\44\1\0\1\36\4\0\1\76\1\17\1\77"+
    "\1\100\1\101\2\0\1\44\2\0\4\44\1\0\1\36"+
    "\4\0\1\74\1\17\1\74\1\102\3\0\1\45\2\0"+
    "\4\46\1\0\1\36\4\0\1\77\1\17\1\77\1\103"+
    "\1\101\2\0\1\46\2\0\4\46\1\0\1\36\4\0"+
    "\1\75\1\0\1\102\1\104\3\0\1\105\2\0\4\50"+
    "\1\0\1\36\4\0\1\100\1\0\1\103\1\100\1\101"+
    "\2\0\1\50\2\0\4\50\1\0\1\36\4\0\1\101"+
    "\1\0\3\101\2\0\1\51\2\0\4\51\15\0\1\105"+
    "\2\0\4\50\15\0\1\54\6\0\1\30\5\0\1\106"+
    "\2\0\1\106\3\0\1\55\2\0\4\56\6\0\1\106"+
    "\2\0\1\106\3\0\1\56\2\0\4\56\6\0\1\22"+
    "\1\17\1\23\1\53\1\25\2\0\1\57\2\0\4\60"+
    "\1\0\1\36\4\0\1\32\1\17\1\33\1\34\1\35"+
    "\2\0\1\60\2\0\4\60\1\0\1\36\4\0\1\23"+
    "\1\17\1\23\1\107\1\25\2\0\1\61\2\0\4\62"+
    "\1\0\1\36\4\0\1\33\1\17\1\33\1\110\1\35"+
    "\2\0\1\62\2\0\4\62\1\0\1\36\4\0\1\53"+
    "\1\0\1\107\1\53\1\25\2\0\1\63\2\0\4\64"+
    "\1\0\1\36\4\0\1\34\1\0\1\110\1\34\1\35"+
    "\2\0\1\64\2\0\4\64\1\0\1\36\4\0\1\25"+
    "\1\0\3\25\2\0\1\65\2\0\4\66\6\0\1\35"+
    "\1\0\3\35\2\0\1\66\2\0\4\66\3\0\1\3"+
    "\2\0\1\37\1\20\1\40\1\34\1\35\2\0\1\60"+
    "\2\0\1\60\1\67\2\60\1\0\1\36\1\0\1\3"+
    "\2\0\1\40\1\20\1\40\1\110\1\35\2\0\1\62"+
    "\2\0\1\62\1\70\2\62\1\0\1\36\2\0\1\4"+
    "\1\0\1\41\1\21\1\42\1\34\1\35\2\0\1\60"+
    "\2\0\2\60\1\71\1\60\1\0\1\36\2\0\1\4"+
    "\1\0\1\42\1\21\1\42\1\110\1\35\2\0\1\62"+
    "\2\0\2\62\1\72\1\62\1\0\1\36\4\0\3\17"+
    "\4\0\1\111\2\0\4\111\6\0\3\17\4\0\1\112"+
    "\2\0\4\112\15\0\1\113\2\0\4\113\6\0\3\17"+
    "\4\0\1\114\2\0\4\114\6\0\3\17\4\0\1\115"+
    "\2\0\4\115\15\0\1\116\2\0\4\116\15\0\1\117"+
    "\2\0\4\117\15\0\1\120\2\0\4\120\15\0\1\121"+
    "\2\0\4\121\15\0\1\122\2\0\4\113\6\0\1\75"+
    "\1\0\1\102\1\75\3\0\1\105\2\0\4\50\1\0"+
    "\1\36\13\0\1\123\2\0\4\123\15\0\1\124\2\0"+
    "\4\125\15\0\1\126\2\0\4\127\6\0\1\73\1\17"+
    "\1\74\1\75\3\0\1\111\2\0\4\111\1\0\1\36"+
    "\4\0\1\74\1\17\1\74\1\102\3\0\1\112\2\0"+
    "\4\112\1\0\1\36\4\0\1\75\1\0\1\102\1\75"+
    "\3\0\1\113\2\0\4\113\1\0\1\36\4\0\1\22"+
    "\1\17\1\23\1\53\1\25\2\0\1\114\2\0\4\114"+
    "\1\0\1\36\4\0\1\23\1\17\1\23\1\107\1\25"+
    "\2\0\1\115\2\0\4\115\1\0\1\36\4\0\1\53"+
    "\1\0\1\107\1\53\1\25\2\0\1\116\2\0\4\116"+
    "\1\0\1\36\4\0\1\25\1\0\3\25\2\0\1\117"+
    "\2\0\4\117\6\0\1\102\1\0\2\102\3\0\1\120"+
    "\2\0\4\120\1\0\1\36\4\0\1\107\1\0\2\107"+
    "\1\25\2\0\1\121\2\0\4\121\1\0\1\36\4\0"+
    "\1\75\1\0\1\102\1\104\3\0\1\113\2\0\4\113"+
    "\1\0\1\36\4\0\1\106\2\0\1\106\3\0\1\123"+
    "\2\0\4\123\6\0\1\102\1\0\2\102\3\0\1\124"+
    "\2\0\4\125\1\0\1\36\4\0\1\103\1\0\2\103"+
    "\1\101\2\0\1\125\2\0\4\125\1\0\1\36\4\0"+
    "\1\107\1\0\2\107\1\25\2\0\1\126\2\0\4\127"+
    "\1\0\1\36\4\0\1\110\1\0\2\110\1\35\2\0"+
    "\1\127\2\0\4\127\1\0\1\36";

  private static int [] zzUnpackTrans() {
    int [] result = new int[1700];
    int offset = 0;
    offset = zzUnpackTrans(ZZ_TRANS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackTrans(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      value--;
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /* error codes */
  private static final int ZZ_UNKNOWN_ERROR = 0;
  private static final int ZZ_NO_MATCH = 1;
  private static final int ZZ_PUSHBACK_2BIG = 2;

  /* error messages for the codes above */
  private static final String ZZ_ERROR_MSG[] = {
    "Unkown internal scanner error",
    "Error: could not match input",
    "Error: pushback value was too large"
  };

  /**
   * ZZ_ATTRIBUTE[aState] contains the attributes of state <code>aState</code>
   */
  private static final int [] ZZ_ATTRIBUTE = zzUnpackAttribute();

  private static final String ZZ_ATTRIBUTE_PACKED_0 =
    "\1\0\1\11\13\1\1\0\5\1\3\0\1\1\2\0"+
    "\2\1\3\0\14\1\1\0\2\1\1\0\16\1\1\0"+
    "\2\1\4\0\2\1\3\0\7\1\1\0\3\1\1\0"+
    "\3\1";

  private static int [] zzUnpackAttribute() {
    int [] result = new int[87];
    int offset = 0;
    offset = zzUnpackAttribute(ZZ_ATTRIBUTE_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAttribute(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /** the input device */
  private java.io.Reader zzReader;

  /** the current state of the DFA */
  private int zzState;

  /** the current lexical state */
  private int zzLexicalState = YYINITIAL;

  /** this buffer contains the current text to be matched and is
      the source of the yytext() string */
  private char zzBuffer[] = new char[ZZ_BUFFERSIZE];

  /** the textposition at the last accepting state */
  private int zzMarkedPos;

  /** the current text position in the buffer */
  private int zzCurrentPos;

  /** startRead marks the beginning of the yytext() string in the buffer */
  private int zzStartRead;

  /** endRead marks the last character in the buffer, that has been read
      from input */
  private int zzEndRead;

  /** number of newlines encountered up to the start of the matched text */
  private int yyline;

  /** the number of characters up to the start of the matched text */
  private int yychar;

  /**
   * the number of characters from the last newline up to the start of the 
   * matched text
   */
  private int yycolumn;

  /** 
   * zzAtBOL == true <=> the scanner is currently at the beginning of a line
   */
  private boolean zzAtBOL = true;

  /** zzAtEOF == true <=> the scanner is at the EOF */
  private boolean zzAtEOF;

  /** denotes if the user-EOF-code has already been executed */
  private boolean zzEOFDone;
  
  /** 
   * The number of occupied positions in zzBuffer beyond zzEndRead.
   * When a lead/high surrogate has been read from the input stream
   * into the final zzBuffer position, this will have a value of 1;
   * otherwise, it will have a value of 0.
   */
  private int zzFinalHighSurrogate = 0;

  /* user code: */

int yychar() { return yychar; }

void getTerm(CharTermAttribute t) {
    t.copyBuffer(zzBuffer, zzStartRead, zzMarkedPos - zzStartRead);
}

void getTerm(CharTermAttribute t, int offset, int len) {
    t.copyBuffer(zzBuffer, zzStartRead + offset, len);
}



  /**
   * Creates a new scanner
   */
  ZimbraLexer() {
  }


  /** 
   * Unpacks the compressed character translation table.
   *
   * @param packed   the packed character translation table
   * @return         the unpacked character translation table
   */
  private static char [] zzUnpackCMap(String packed) {
    char [] map = new char[0x110000];
    int i = 0;  /* index in packed string  */
    int j = 0;  /* index in unpacked array */
    while (i < 2444) {
      int  count = packed.charAt(i++);
      char value = packed.charAt(i++);
      do map[j++] = value; while (--count > 0);
    }
    return map;
  }


  /**
   * Refills the input buffer.
   *
   * @return      <code>false</code>, iff there was new input.
   * 
   * @exception   java.io.IOException  if any I/O-Error occurs
   */
  private boolean zzRefill() throws java.io.IOException {

    /* first: make room (if you can) */
    if (zzStartRead > 0) {
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
      System.arraycopy(zzBuffer, zzStartRead,
                       zzBuffer, 0,
                       zzEndRead-zzStartRead);

      /* translate stored positions */
      zzEndRead-= zzStartRead;
      zzCurrentPos-= zzStartRead;
      zzMarkedPos-= zzStartRead;
      zzStartRead = 0;
    }

    /* is the buffer big enough? */
    if (zzCurrentPos >= zzBuffer.length - zzFinalHighSurrogate) {
      /* if not: blow it up */
      char newBuffer[] = new char[zzBuffer.length*2];
      System.arraycopy(zzBuffer, 0, newBuffer, 0, zzBuffer.length);
      zzBuffer = newBuffer;
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
    }

    /* fill the buffer with new input */
    int requested = zzBuffer.length - zzEndRead;           
    int totalRead = 0;
    while (totalRead < requested) {
      int numRead = zzReader.read(zzBuffer, zzEndRead + totalRead, requested - totalRead);
      if (numRead == -1) {
        break;
      }
      totalRead += numRead;
    }

    if (totalRead > 0) {
      zzEndRead += totalRead;
      if (totalRead == requested) { /* possibly more input available */
        if (Character.isHighSurrogate(zzBuffer[zzEndRead - 1])) {
          --zzEndRead;
          zzFinalHighSurrogate = 1;
        }
      }
      return false;
    }

    // totalRead = 0: End of stream
    return true;
  }

    
  /**
   * Closes the input stream.
   */
  public final void yyclose() throws java.io.IOException {
    zzAtEOF = true;            /* indicate end of file */
    zzEndRead = zzStartRead;  /* invalidate buffer    */

    if (zzReader != null)
      zzReader.close();
  }


  /**
   * Resets the scanner to read from a new input stream.
   * Does not close the old reader.
   *
   * All internal variables are reset, the old input stream 
   * <b>cannot</b> be reused (internal buffer is discarded and lost).
   * Lexical state is set to <tt>ZZ_INITIAL</tt>.
   *
   * Internal scan buffer is resized down to its initial length, if it has grown.
   *
   * @param reader   the new input stream 
   */
  public final void yyreset(java.io.Reader reader) {
    zzReader = reader;
    zzAtBOL  = true;
    zzAtEOF  = false;
    zzEOFDone = false;
    zzEndRead = zzStartRead = 0;
    zzCurrentPos = zzMarkedPos = 0;
    zzFinalHighSurrogate = 0;
    yyline = yychar = yycolumn = 0;
    zzLexicalState = YYINITIAL;
    if (zzBuffer.length > ZZ_BUFFERSIZE)
      zzBuffer = new char[ZZ_BUFFERSIZE];
  }


  /**
   * Returns the current lexical state.
   */
  public final int yystate() {
    return zzLexicalState;
  }


  /**
   * Enters a new lexical state
   *
   * @param newState the new lexical state
   */
  public final void yybegin(int newState) {
    zzLexicalState = newState;
  }


  /**
   * Returns the text matched by the current regular expression.
   */
  public final String yytext() {
    return new String( zzBuffer, zzStartRead, zzMarkedPos-zzStartRead );
  }


  /**
   * Returns the character at position <tt>pos</tt> from the 
   * matched text. 
   * 
   * It is equivalent to yytext().charAt(pos), but faster
   *
   * @param pos the position of the character to fetch. 
   *            A value from 0 to yylength()-1.
   *
   * @return the character at position pos
   */
  public final char yycharat(int pos) {
    return zzBuffer[zzStartRead+pos];
  }


  /**
   * Returns the length of the matched text region.
   */
  public final int yylength() {
    return zzMarkedPos-zzStartRead;
  }


  /**
   * Reports an error that occured while scanning.
   *
   * In a wellformed scanner (no or only correct usage of 
   * yypushback(int) and a match-all fallback rule) this method 
   * will only be called with things that "Can't Possibly Happen".
   * If this method is called, something is seriously wrong
   * (e.g. a JFlex bug producing a faulty scanner etc.).
   *
   * Usual syntax/scanner level error handling should be done
   * in error fallback rules.
   *
   * @param   errorCode  the code of the errormessage to display
   */
  private void zzScanError(int errorCode) {
    String message;
    try {
      message = ZZ_ERROR_MSG[errorCode];
    }
    catch (ArrayIndexOutOfBoundsException e) {
      message = ZZ_ERROR_MSG[ZZ_UNKNOWN_ERROR];
    }

    throw new Error(message);
  } 


  /**
   * Pushes the specified amount of characters back into the input stream.
   *
   * They will be read again by then next call of the scanning method
   *
   * @param number  the number of characters to be read again.
   *                This number must not be greater than yylength()!
   */
  public void yypushback(int number)  {
    if ( number > yylength() )
      zzScanError(ZZ_PUSHBACK_2BIG);

    zzMarkedPos -= number;
  }


  /**
   * Resumes scanning until the next regular expression is matched,
   * the end of input is encountered or an I/O-Error occurs.
   *
   * @return      the next token
   * @exception   java.io.IOException  if any I/O-Error occurs
   */
  public ZimbraTokenizer.TokenType next() throws java.io.IOException {
    int zzInput;
    int zzAction;

    // cached fields:
    int zzCurrentPosL;
    int zzMarkedPosL;
    int zzEndReadL = zzEndRead;
    char [] zzBufferL = zzBuffer;
    char [] zzCMapL = ZZ_CMAP;

    int [] zzTransL = ZZ_TRANS;
    int [] zzRowMapL = ZZ_ROWMAP;
    int [] zzAttrL = ZZ_ATTRIBUTE;

    while (true) {
      zzMarkedPosL = zzMarkedPos;

      yychar+= zzMarkedPosL-zzStartRead;

      zzAction = -1;

      zzCurrentPosL = zzCurrentPos = zzStartRead = zzMarkedPosL;
  
      zzState = ZZ_LEXSTATE[zzLexicalState];

      // set up zzAction for empty match case:
      int zzAttributes = zzAttrL[zzState];
      if ( (zzAttributes & 1) == 1 ) {
        zzAction = zzState;
      }


      zzForAction: {
        while (true) {
    
          if (zzCurrentPosL < zzEndReadL) {
            zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
            zzCurrentPosL += Character.charCount(zzInput);
          }
          else if (zzAtEOF) {
            zzInput = YYEOF;
            break zzForAction;
          }
          else {
            // store back cached positions
            zzCurrentPos  = zzCurrentPosL;
            zzMarkedPos   = zzMarkedPosL;
            boolean eof = zzRefill();
            // get translated positions and possibly new buffer
            zzCurrentPosL  = zzCurrentPos;
            zzMarkedPosL   = zzMarkedPos;
            zzBufferL      = zzBuffer;
            zzEndReadL     = zzEndRead;
            if (eof) {
              zzInput = YYEOF;
              break zzForAction;
            }
            else {
              zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
              zzCurrentPosL += Character.charCount(zzInput);
            }
          }
          int zzNext = zzTransL[ zzRowMapL[zzState] + zzCMapL[zzInput] ];
          if (zzNext == -1) break zzForAction;
          zzState = zzNext;

          zzAttributes = zzAttrL[zzState];
          if ( (zzAttributes & 1) == 1 ) {
            zzAction = zzState;
            zzMarkedPosL = zzCurrentPosL;
            if ( (zzAttributes & 8) == 8 ) break zzForAction;
          }

        }
      }

      // store back cached position
      zzMarkedPos = zzMarkedPosL;

      switch (zzAction < 0 ? zzAction : ZZ_ACTION[zzAction]) {
        case 1: 
          { /* ignore */
          }
        case 13: break;
        case 2: 
          { return ZimbraTokenizer.TokenType.CJK;
          }
        case 14: break;
        case 3: 
          { return ZimbraTokenizer.TokenType.SOUTHEAST_ASIAN;
          }
        case 15: break;
        case 4: 
          { return ZimbraTokenizer.TokenType.THAI;
          }
        case 16: break;
        case 5: 
          { return ZimbraTokenizer.TokenType.PUNC;
          }
        case 17: break;
        case 6: 
          { return ZimbraTokenizer.TokenType.ALNUM;
          }
        case 18: break;
        case 7: 
          { return ZimbraTokenizer.TokenType.NUM;
          }
        case 19: break;
        case 8: 
          { return ZimbraTokenizer.TokenType.HOST;
          }
        case 20: break;
        case 9: 
          { return ZimbraTokenizer.TokenType.COMPANY;
          }
        case 21: break;
        case 10: 
          { return ZimbraTokenizer.TokenType.APOSTROPHE;
          }
        case 22: break;
        case 11: 
          { return ZimbraTokenizer.TokenType.ACRONYM;
          }
        case 23: break;
        case 12: 
          { return ZimbraTokenizer.TokenType.EMAIL;
          }
        case 24: break;
        default: 
          if (zzInput == YYEOF && zzStartRead == zzCurrentPos) {
            zzAtEOF = true;
            return null;
          } 
          else {
            zzScanError(ZZ_NO_MATCH);
          }
      }
    }
  }
}
