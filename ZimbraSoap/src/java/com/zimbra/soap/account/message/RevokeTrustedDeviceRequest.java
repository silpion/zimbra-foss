package com.zimbra.soap.account.message;

import javax.xml.bind.annotation.XmlRootElement;

import com.zimbra.common.soap.AccountConstants;

@XmlRootElement(name=AccountConstants.E_REVOKE_TRUSTED_DEVICE_REQUEST)
public class RevokeTrustedDeviceRequest {

    public RevokeTrustedDeviceRequest() {}

}
