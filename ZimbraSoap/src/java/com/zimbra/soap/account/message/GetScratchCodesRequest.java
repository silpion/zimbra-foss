package com.zimbra.soap.account.message;

import javax.xml.bind.annotation.XmlRootElement;

import com.zimbra.common.soap.AccountConstants;

@XmlRootElement(name=AccountConstants.E_GET_SCRATCH_CODES_REQUEST)
public class GetScratchCodesRequest {

}
