package com.zimbra.soap.account.message;

import javax.xml.bind.annotation.XmlRootElement;

import com.zimbra.common.soap.AccountConstants;

@XmlRootElement(name=AccountConstants.E_GENERATE_SCRATCH_CODES_REQUEST)
public class GenerateScratchCodesRequest {

}
