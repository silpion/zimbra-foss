package com.zimbra.soap.mail.message;

import javax.xml.bind.annotation.XmlRootElement;

import com.zimbra.common.soap.MailConstants;

@XmlRootElement(name=MailConstants.E_GET_DATA_SOURCE_USAGE_REQUEST)
public class GetDataSourceUsageRequest {
}
