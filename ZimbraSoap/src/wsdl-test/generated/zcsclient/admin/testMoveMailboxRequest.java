
package generated.zcsclient.admin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import generated.zcsclient.zm.testClusterSelector;


/**
 * <p>Java class for moveMailboxRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="moveMailboxRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="account" type="{urn:zimbraAdmin}moveMailboxInfo"/>
 *         &lt;element name="cluster" type="{urn:zimbra}clusterSelector" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "moveMailboxRequest", propOrder = {
    "account",
    "cluster"
})
public class testMoveMailboxRequest {

    @XmlElement(required = true)
    protected testMoveMailboxInfo account;
    protected testClusterSelector cluster;

    /**
     * Gets the value of the account property.
     * 
     * @return
     *     possible object is
     *     {@link testMoveMailboxInfo }
     *     
     */
    public testMoveMailboxInfo getAccount() {
        return account;
    }

    /**
     * Sets the value of the account property.
     * 
     * @param value
     *     allowed object is
     *     {@link testMoveMailboxInfo }
     *     
     */
    public void setAccount(testMoveMailboxInfo value) {
        this.account = value;
    }

    /**
     * Gets the value of the cluster property.
     * 
     * @return
     *     possible object is
     *     {@link testClusterSelector }
     *     
     */
    public testClusterSelector getCluster() {
        return cluster;
    }

    /**
     * Sets the value of the cluster property.
     * 
     * @param value
     *     allowed object is
     *     {@link testClusterSelector }
     *     
     */
    public void setCluster(testClusterSelector value) {
        this.cluster = value;
    }

}
