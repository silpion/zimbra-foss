
package generated.zcsclient.admin;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for countObjectsType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="countObjectsType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="account"/>
 *     &lt;enumeration value="domain"/>
 *     &lt;enumeration value="accountOnUCService"/>
 *     &lt;enumeration value="userAccount"/>
 *     &lt;enumeration value="cos"/>
 *     &lt;enumeration value="domainOnUCService"/>
 *     &lt;enumeration value="dl"/>
 *     &lt;enumeration value="server"/>
 *     &lt;enumeration value="internalUserAccount"/>
 *     &lt;enumeration value="cosOnUCService"/>
 *     &lt;enumeration value="alias"/>
 *     &lt;enumeration value="calresource"/>
 *     &lt;enumeration value="internalArchivingAccount"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "countObjectsType")
@XmlEnum
public enum testCountObjectsType {

    @XmlEnumValue("account")
    ACCOUNT("account"),
    @XmlEnumValue("domain")
    DOMAIN("domain"),
    @XmlEnumValue("accountOnUCService")
    ACCOUNT_ON_UC_SERVICE("accountOnUCService"),
    @XmlEnumValue("userAccount")
    USER_ACCOUNT("userAccount"),
    @XmlEnumValue("cos")
    COS("cos"),
    @XmlEnumValue("domainOnUCService")
    DOMAIN_ON_UC_SERVICE("domainOnUCService"),
    @XmlEnumValue("dl")
    DL("dl"),
    @XmlEnumValue("server")
    SERVER("server"),
    @XmlEnumValue("internalUserAccount")
    INTERNAL_USER_ACCOUNT("internalUserAccount"),
    @XmlEnumValue("cosOnUCService")
    COS_ON_UC_SERVICE("cosOnUCService"),
    @XmlEnumValue("alias")
    ALIAS("alias"),
    @XmlEnumValue("calresource")
    CALRESOURCE("calresource"),
    @XmlEnumValue("internalArchivingAccount")
    INTERNAL_ARCHIVING_ACCOUNT("internalArchivingAccount");
    private final String value;

    testCountObjectsType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static testCountObjectsType fromValue(String v) {
        for (testCountObjectsType c: testCountObjectsType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
