
package generated.zcsclient.mail;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for shareNotifAction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="shareNotifAction">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="expire"/>
 *     &lt;enumeration value="revoke"/>
 *     &lt;enumeration value="edit"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "shareNotifAction")
@XmlEnum
public enum testShareNotifAction {

    @XmlEnumValue("expire")
    EXPIRE("expire"),
    @XmlEnumValue("revoke")
    REVOKE("revoke"),
    @XmlEnumValue("edit")
    EDIT("edit");
    private final String value;

    testShareNotifAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static testShareNotifAction fromValue(String v) {
        for (testShareNotifAction c: testShareNotifAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
