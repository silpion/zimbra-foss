
package generated.zcsclient.zm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for grantGranteeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="grantGranteeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="guest"/>
 *     &lt;enumeration value="pub"/>
 *     &lt;enumeration value="dom"/>
 *     &lt;enumeration value="key"/>
 *     &lt;enumeration value="cos"/>
 *     &lt;enumeration value="grp"/>
 *     &lt;enumeration value="usr"/>
 *     &lt;enumeration value="all"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "grantGranteeType")
@XmlEnum
public enum testGrantGranteeType {

    @XmlEnumValue("guest")
    GUEST("guest"),
    @XmlEnumValue("pub")
    PUB("pub"),
    @XmlEnumValue("dom")
    DOM("dom"),
    @XmlEnumValue("key")
    KEY("key"),
    @XmlEnumValue("cos")
    COS("cos"),
    @XmlEnumValue("grp")
    GRP("grp"),
    @XmlEnumValue("usr")
    USR("usr"),
    @XmlEnumValue("all")
    ALL("all");
    private final String value;

    testGrantGranteeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static testGrantGranteeType fromValue(String v) {
        for (testGrantGranteeType c: testGrantGranteeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
