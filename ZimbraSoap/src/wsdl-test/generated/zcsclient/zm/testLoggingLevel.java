
package generated.zcsclient.zm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loggingLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="loggingLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="trace"/>
 *     &lt;enumeration value="info"/>
 *     &lt;enumeration value="warn"/>
 *     &lt;enumeration value="error"/>
 *     &lt;enumeration value="debug"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "loggingLevel")
@XmlEnum
public enum testLoggingLevel {

    @XmlEnumValue("trace")
    TRACE("trace"),
    @XmlEnumValue("info")
    INFO("info"),
    @XmlEnumValue("warn")
    WARN("warn"),
    @XmlEnumValue("error")
    ERROR("error"),
    @XmlEnumValue("debug")
    DEBUG("debug");
    private final String value;

    testLoggingLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static testLoggingLevel fromValue(String v) {
        for (testLoggingLevel c: testLoggingLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
