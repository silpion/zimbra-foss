
package generated.zcsclient.zm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for targetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="targetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="server"/>
 *     &lt;enumeration value="global"/>
 *     &lt;enumeration value="ucservice"/>
 *     &lt;enumeration value="alwaysoncluster"/>
 *     &lt;enumeration value="dl"/>
 *     &lt;enumeration value="xmppcomponent"/>
 *     &lt;enumeration value="cos"/>
 *     &lt;enumeration value="zimlet"/>
 *     &lt;enumeration value="domain"/>
 *     &lt;enumeration value="account"/>
 *     &lt;enumeration value="calresource"/>
 *     &lt;enumeration value="group"/>
 *     &lt;enumeration value="config"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "targetType")
@XmlEnum
public enum testTargetType {

    @XmlEnumValue("server")
    SERVER("server"),
    @XmlEnumValue("global")
    GLOBAL("global"),
    @XmlEnumValue("ucservice")
    UCSERVICE("ucservice"),
    @XmlEnumValue("alwaysoncluster")
    ALWAYSONCLUSTER("alwaysoncluster"),
    @XmlEnumValue("dl")
    DL("dl"),
    @XmlEnumValue("xmppcomponent")
    XMPPCOMPONENT("xmppcomponent"),
    @XmlEnumValue("cos")
    COS("cos"),
    @XmlEnumValue("zimlet")
    ZIMLET("zimlet"),
    @XmlEnumValue("domain")
    DOMAIN("domain"),
    @XmlEnumValue("account")
    ACCOUNT("account"),
    @XmlEnumValue("calresource")
    CALRESOURCE("calresource"),
    @XmlEnumValue("group")
    GROUP("group"),
    @XmlEnumValue("config")
    CONFIG("config");
    private final String value;

    testTargetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static testTargetType fromValue(String v) {
        for (testTargetType c: testTargetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
