
package generated.zcsclient.zm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for granteeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="granteeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="email"/>
 *     &lt;enumeration value="dom"/>
 *     &lt;enumeration value="grp"/>
 *     &lt;enumeration value="key"/>
 *     &lt;enumeration value="pub"/>
 *     &lt;enumeration value="all"/>
 *     &lt;enumeration value="edom"/>
 *     &lt;enumeration value="usr"/>
 *     &lt;enumeration value="egp"/>
 *     &lt;enumeration value="gst"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "granteeType")
@XmlEnum
public enum testGranteeType {

    @XmlEnumValue("email")
    EMAIL("email"),
    @XmlEnumValue("dom")
    DOM("dom"),
    @XmlEnumValue("grp")
    GRP("grp"),
    @XmlEnumValue("key")
    KEY("key"),
    @XmlEnumValue("pub")
    PUB("pub"),
    @XmlEnumValue("all")
    ALL("all"),
    @XmlEnumValue("edom")
    EDOM("edom"),
    @XmlEnumValue("usr")
    USR("usr"),
    @XmlEnumValue("egp")
    EGP("egp"),
    @XmlEnumValue("gst")
    GST("gst");
    private final String value;

    testGranteeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static testGranteeType fromValue(String v) {
        for (testGranteeType c: testGranteeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
