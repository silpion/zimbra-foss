
package generated.zcsclient.zm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for clusterBy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="clusterBy">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="name"/>
 *     &lt;enumeration value="id"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "clusterBy")
@XmlEnum
public enum testClusterBy {

    @XmlEnumValue("name")
    NAME("name"),
    @XmlEnumValue("id")
    ID("id");
    private final String value;

    testClusterBy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static testClusterBy fromValue(String v) {
        for (testClusterBy c: testClusterBy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
