
package generated.zcsclient.zm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchSortBy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="searchSortBy">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="dateDesc"/>
 *     &lt;enumeration value="none"/>
 *     &lt;enumeration value="rcptAsc"/>
 *     &lt;enumeration value="rcptDesc"/>
 *     &lt;enumeration value="taskPercCompletedDesc"/>
 *     &lt;enumeration value="taskStatusDesc"/>
 *     &lt;enumeration value="nameDesc"/>
 *     &lt;enumeration value="durDesc"/>
 *     &lt;enumeration value="taskDueAsc"/>
 *     &lt;enumeration value="taskPercCompletedAsc"/>
 *     &lt;enumeration value="subjDesc"/>
 *     &lt;enumeration value="subjAsc"/>
 *     &lt;enumeration value="taskDueDesc"/>
 *     &lt;enumeration value="durAsc"/>
 *     &lt;enumeration value="dateAsc"/>
 *     &lt;enumeration value="taskStatusAsc"/>
 *     &lt;enumeration value="nameAsc"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "searchSortBy")
@XmlEnum
public enum testSearchSortBy {

    @XmlEnumValue("dateDesc")
    DATE_DESC("dateDesc"),
    @XmlEnumValue("none")
    NONE("none"),
    @XmlEnumValue("rcptAsc")
    RCPT_ASC("rcptAsc"),
    @XmlEnumValue("rcptDesc")
    RCPT_DESC("rcptDesc"),
    @XmlEnumValue("taskPercCompletedDesc")
    TASK_PERC_COMPLETED_DESC("taskPercCompletedDesc"),
    @XmlEnumValue("taskStatusDesc")
    TASK_STATUS_DESC("taskStatusDesc"),
    @XmlEnumValue("nameDesc")
    NAME_DESC("nameDesc"),
    @XmlEnumValue("durDesc")
    DUR_DESC("durDesc"),
    @XmlEnumValue("taskDueAsc")
    TASK_DUE_ASC("taskDueAsc"),
    @XmlEnumValue("taskPercCompletedAsc")
    TASK_PERC_COMPLETED_ASC("taskPercCompletedAsc"),
    @XmlEnumValue("subjDesc")
    SUBJ_DESC("subjDesc"),
    @XmlEnumValue("subjAsc")
    SUBJ_ASC("subjAsc"),
    @XmlEnumValue("taskDueDesc")
    TASK_DUE_DESC("taskDueDesc"),
    @XmlEnumValue("durAsc")
    DUR_ASC("durAsc"),
    @XmlEnumValue("dateAsc")
    DATE_ASC("dateAsc"),
    @XmlEnumValue("taskStatusAsc")
    TASK_STATUS_ASC("taskStatusAsc"),
    @XmlEnumValue("nameAsc")
    NAME_ASC("nameAsc");
    private final String value;

    testSearchSortBy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static testSearchSortBy fromValue(String v) {
        for (testSearchSortBy c: testSearchSortBy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
