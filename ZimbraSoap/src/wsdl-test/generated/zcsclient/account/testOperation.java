
package generated.zcsclient.account;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for operation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="operation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="setOwners"/>
 *     &lt;enumeration value="addOwners"/>
 *     &lt;enumeration value="acceptSubsReq"/>
 *     &lt;enumeration value="removeOwners"/>
 *     &lt;enumeration value="rejectSubsReq"/>
 *     &lt;enumeration value="rename"/>
 *     &lt;enumeration value="delete"/>
 *     &lt;enumeration value="grantRights"/>
 *     &lt;enumeration value="addMembers"/>
 *     &lt;enumeration value="setRights"/>
 *     &lt;enumeration value="revokeRights"/>
 *     &lt;enumeration value="removeMembers"/>
 *     &lt;enumeration value="modify"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "operation")
@XmlEnum
public enum testOperation {

    @XmlEnumValue("setOwners")
    SET_OWNERS("setOwners"),
    @XmlEnumValue("addOwners")
    ADD_OWNERS("addOwners"),
    @XmlEnumValue("acceptSubsReq")
    ACCEPT_SUBS_REQ("acceptSubsReq"),
    @XmlEnumValue("removeOwners")
    REMOVE_OWNERS("removeOwners"),
    @XmlEnumValue("rejectSubsReq")
    REJECT_SUBS_REQ("rejectSubsReq"),
    @XmlEnumValue("rename")
    RENAME("rename"),
    @XmlEnumValue("delete")
    DELETE("delete"),
    @XmlEnumValue("grantRights")
    GRANT_RIGHTS("grantRights"),
    @XmlEnumValue("addMembers")
    ADD_MEMBERS("addMembers"),
    @XmlEnumValue("setRights")
    SET_RIGHTS("setRights"),
    @XmlEnumValue("revokeRights")
    REVOKE_RIGHTS("revokeRights"),
    @XmlEnumValue("removeMembers")
    REMOVE_MEMBERS("removeMembers"),
    @XmlEnumValue("modify")
    MODIFY("modify");
    private final String value;

    testOperation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static testOperation fromValue(String v) {
        for (testOperation c: testOperation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
