/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2009, 2010, 2011, 2013, 2014 Zimbra, Inc.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
package com.zimbra.cs.index;

import org.apache.solr.client.solrj.util.ClientUtils;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;

import com.google.common.base.Strings;

/**
 * Helper for Lucene {@link Document}.
 *
 * @author ysasaki
 */
public final class IndexDocument {
	private final SolrInputDocument inputDocument;

	public IndexDocument() {
		inputDocument = new SolrInputDocument();
	}

	public IndexDocument(SolrInputDocument doc) {
		inputDocument = doc;
	}

	public SolrInputDocument toInputDocument() {
		return inputDocument;
	}

	public SolrDocument toDocument() {
		return ClientUtils.toSolrDocument(inputDocument);
	}

	public void addMimeType(String value) {
		inputDocument.addField(LuceneFields.L_MIMETYPE, value);
	}

	public void addPartName(String value) {
		inputDocument.addField(LuceneFields.L_PARTNAME, value);
	}

	public void addFilename(String value) {
		inputDocument.addField(LuceneFields.L_FILENAME, value);
	}

	public void addSortSize(long value) {
		inputDocument.addField(LuceneFields.L_SORT_SIZE, String.valueOf(value));
	}

	public void removeSortSize() {
		inputDocument.removeField(LuceneFields.L_SORT_SIZE);
	}

	public void addSortAttachment(boolean value) {
		inputDocument.addField(LuceneFields.L_SORT_ATTACH,
				LuceneFields.valueForBooleanField(value));
	}

	public void removeSortAttachment() {
		inputDocument.removeField(LuceneFields.L_SORT_ATTACH);
	}

	public void addSortFlag(boolean value) {
		inputDocument.addField(LuceneFields.L_SORT_FLAG,
				 LuceneFields.valueForBooleanField(value));
	}

	public void removeSortFlag() {
		inputDocument.removeField(LuceneFields.L_SORT_FLAG);
	}

	public void addSortPriority(int value) {
		inputDocument.addField(LuceneFields.L_SORT_PRIORITY,
				LuceneFields.valueForPriority(value));
	}

	public void removeSortPriority() {
		inputDocument.removeField(LuceneFields.L_SORT_PRIORITY);
	}

	public void addFrom(String value) {
		inputDocument.addField(LuceneFields.L_H_FROM, value);
	}

	public void removeFrom() {
		inputDocument.removeField(LuceneFields.L_H_FROM);
	}

	public void addTo(String value) {
		inputDocument.addField(LuceneFields.L_H_TO, value);
	}

	public void removeTo() {
		inputDocument.removeField(LuceneFields.L_H_TO);
	}

	public void addCc(String value) {
		inputDocument.addField(LuceneFields.L_H_CC, value);
	}

	public void removeCc() {
		inputDocument.removeField(LuceneFields.L_H_CC);
	}

    public void addSender(String value) {
        inputDocument.addField(LuceneFields.L_H_SENDER, value);
    }

    public void removeSender() {
        inputDocument.removeField(LuceneFields.L_H_SENDER);
    }

	public void addEnvFrom(String value) {
		inputDocument.addField(LuceneFields.L_H_X_ENV_FROM, value);
	}

	public void addEnvTo(String value) {
		inputDocument.addField(LuceneFields.L_H_X_ENV_TO, value);
	}

	public void addMessageId(String value) {
		inputDocument.addField(LuceneFields.L_H_MESSAGE_ID, value);
	}

	public void addField(String value) {
		inputDocument.addField(LuceneFields.L_FIELD, value);
	}

	public void addSortName(String value) {
		if (Strings.isNullOrEmpty(value)) {
			return;
		}
		inputDocument.addField(LuceneFields.L_SORT_NAME, value.toLowerCase());
	}

	public void removeSortName() {
		inputDocument.removeField(LuceneFields.L_SORT_NAME);
	}

	public void addSubject(String value) {
		inputDocument.addField(LuceneFields.L_H_SUBJECT, value);
	}

	public void removeSubject() {
		inputDocument.removeField(LuceneFields.L_H_SUBJECT);
	}

	public void addSortSubject(String value) {
		if (Strings.isNullOrEmpty(value)) {
			return;
		}
		inputDocument.addField(LuceneFields.L_SORT_SUBJECT, value.toUpperCase());
	}

	public void removeSortSubject() {
		inputDocument.removeField(LuceneFields.L_SORT_SUBJECT);
	}

	public void addContent(String value) {
		inputDocument.addField(LuceneFields.L_CONTENT, value);
	}

	public void addAttachments(String value) {
		inputDocument.addField(LuceneFields.L_ATTACHMENTS, value);
	}

	public void addMailboxBlobId(int value) {
		inputDocument.addField(LuceneFields.L_MAILBOX_BLOB_ID, String.valueOf(value));
	}

	public void removeMailboxBlobId() {
		inputDocument.removeField(LuceneFields.L_MAILBOX_BLOB_ID);
	}

	public void addSortDate(long value) {
		inputDocument.addField(LuceneFields.L_SORT_DATE, String.valueOf(value));
	}

	public void removeSortDate() {
		inputDocument.removeField(LuceneFields.L_SORT_DATE);
	}

	public void addContactData(String value) {
		inputDocument.addField(LuceneFields.L_CONTACT_DATA, value);
	}

	public void addObjects(String value) {
		inputDocument.addField(LuceneFields.L_OBJECTS, value);
	}

	public void addVersion(int value) {
		inputDocument.addField(LuceneFields.L_VERSION, String.valueOf(value));
	}
}
