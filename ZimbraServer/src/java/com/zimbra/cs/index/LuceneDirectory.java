/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2008, 2009, 2010, 2011, 2012, 2013, 2014 Zimbra, Inc.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
package com.zimbra.cs.index;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.IOContext;
import org.apache.lucene.store.IndexInput;
import org.apache.lucene.store.IndexOutput;
import org.apache.lucene.store.Lock;
import org.apache.lucene.store.LockFactory;
import org.apache.lucene.store.MMapDirectory;
import org.apache.lucene.store.NIOFSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.store.SingleInstanceLockFactory;

import com.zimbra.common.account.ZAttrProvisioning;
import com.zimbra.common.localconfig.LC;
import com.zimbra.common.util.ZimbraLog;
import com.zimbra.cs.account.ldap.LdapProvisioning;
import com.zimbra.cs.stats.ZimbraPerf;
import com.zimbra.cs.util.ProvisioningUtil;

/**
 * Lucene {@link FSDirectory} wrapper to count I/O bytes.
 * <p>
 * This forwards all its method calls to the underlying {@link FSDirectory}.
 *
 * @see FSDirectory
 * @author ysasaki
 */
public final class LuceneDirectory extends Directory {
    private final FSDirectory directory;

    private LuceneDirectory(FSDirectory dir) {
        directory = dir;
    }

    /**
     * Creates a new {@link LuceneDirectory} with {@code SingleInstanceLockFactory}.
     * <p>
     * You can switch Lucene's {@link FSDirectory} implementation by {@link LC#zimbra_index_lucene_io_impl}.
     * <ul>
     *  <li>{@code null} -Lucene will try to pick the best {@link FSDirectory} implementation given the current
     *      environment. Currently this returns {@link MMapDirectory} for most Solaris and Windows 64-bit JREs,
     *      {@link NIOFSDirectory} for other non-Windows JREs, and {@link SimpleFSDirectory} for other JREs on Windows.
     *  <li>{@code simple} - straightforward implementation using java.io.RandomAccessFile. However, it has poor
     *      concurrent performance (multiple threads will bottleneck) as it synchronizes when multiple threads read from
     *      the same file.
     *  <li>{@code nio} - uses java.nio's FileChannel's positional io when reading to avoid synchronization when reading
     *      from the same file. Unfortunately, due to a Windows-only Sun JRE bug this is a poor choice for Windows, but
     *      on all other platforms this is the preferred choice.
     *  <li>{@code mmap} - uses memory-mapped IO when reading. This is a good choice if you have plenty of virtual
     *      memory relative to your index size, eg if you are running on a 64 bit JRE, or you are running on a 32 bit
     *      JRE but your index sizes are small enough to fit into the virtual memory space. Java has currently the
     *      limitation of not being able to unmap files from user code. The files are unmapped, when GC releases the
     *      byte buffers. Due to this bug in Sun's JRE, MMapDirectory's IndexInput.close() is unable to close the
     *      underlying OS file handle. Only when GC finally collects the underlying objects, which could be quite some
     *      time later, will the file handle be closed. This will consume additional transient disk usage: on Windows,
     *      attempts to delete or overwrite the files will result in an exception; on other platforms, which typically
     *      have a "delete on last close" semantics, while such operations will succeed, the bytes are still consuming
     *      space on disk. For many applications this limitation is not a problem (e.g. if you have plenty of disk
     *      space, and you don't rely on overwriting files on Windows) but it's still an important limitation to be
     *      aware of. This class supplies a (possibly dangerous) workaround mentioned in the bug report, which may fail
     *      on non-Sun JVMs.
     * </ul>
     *
     * @param path directory path
     */
    public static LuceneDirectory open(File path) throws IOException {
    	String impl  = ProvisioningUtil.getServerAttribute(ZAttrProvisioning.A_zimbraIndexLuceneIoImpl, "nio");
        FSDirectory dir;
        if ("nio".equals(impl)) {
            dir = new NIOFSDirectory(path.toPath(), new SingleInstanceLockFactory());
        } else if ("mmap".equals(impl)) {
            dir = new MMapDirectory(path.toPath(), new SingleInstanceLockFactory());
        } else if ("simple".equals(impl)) {
            dir = new SimpleFSDirectory(path.toPath(), new SingleInstanceLockFactory());
        } else {
            dir = FSDirectory.open(path.toPath(), new SingleInstanceLockFactory());
        }
        ZimbraLog.index.info("OpenLuceneIndex impl=%s,dir=%s", dir.getClass().getSimpleName(), path);
        return new LuceneDirectory(dir);
    }

    public Path getDirectory() {
        return directory.getDirectory();
    }

    @Override
    public String[] listAll() throws IOException {
        return directory.listAll();
    }


    @Override
    public void deleteFile(String name) throws IOException {
        directory.deleteFile(name);
    }

    @Override
    public long fileLength(String name) throws IOException {
        return directory.fileLength(name);
    }

	@Override
	public IndexOutput createOutput(String name, IOContext context)
			throws IOException {
		return new LuceneIndexOutput(directory.createOutput(name, context));
	}

    @Override
    public void sync(Collection<String> names) throws IOException {
        directory.sync(names);
    }

	@Override
	public IndexInput openInput(String name, IOContext context) throws IOException {
		return new LuceneIndexInput(directory.openInput(name, context));
	}

    @Override
    public Lock makeLock(String name) {
        return directory.makeLock(name);
    }



    @Override
    public void close() throws IOException {
        directory.close();
    }


    @Override
    public String toString() {
        return directory.toString();
    }

    private static final class LuceneIndexInput extends IndexInput {

		private final IndexInput input;
        private boolean disableCounters = ProvisioningUtil.getServerAttribute(ZAttrProvisioning.A_zimbraIndexDisablePerfCounters, false);

        LuceneIndexInput(IndexInput in) {
        	super("");
            input = in;
        }

        @Override
        public byte readByte() throws IOException {
            if (!disableCounters) {
                ZimbraPerf.COUNTER_IDX_BYTES_READ.increment(1);
            }
            return input.readByte();
        }

        @Override
        public void readBytes(byte[] b, int offset, int len) throws IOException {
            if (!disableCounters) {
                ZimbraPerf.COUNTER_IDX_BYTES_READ.increment(len);
            }
            input.readBytes(b, offset, len);
        }

        @Override
        public void readBytes(byte[] b, int offset, int len, boolean useBuffer)
            throws IOException {
            if (!disableCounters) {
                ZimbraPerf.COUNTER_IDX_BYTES_READ.increment(len);
            }
            input.readBytes(b, offset, len, useBuffer);
        }

        @Override
        public void close() throws IOException {
            input.close();
        }

        @Override
        public long getFilePointer() {
            return input.getFilePointer();
        }

        @Override
        public void seek(long pos) throws IOException {
            input.seek(pos);
        }

        @Override
        public long length() {
            return input.length();
        }

        @Override
        public IndexInput clone() {
            return new LuceneIndexInput(input.clone());
        }

		@Override
		public IndexInput slice(String arg0, long arg1, long arg2)
				throws IOException {
			return input.slice(arg0, arg1, arg2);
		}
    }

    private static final class LuceneIndexOutput extends IndexOutput {
        private final IndexOutput output;
        private boolean disableCounters = ProvisioningUtil.getServerAttribute(ZAttrProvisioning.A_zimbraIndexDisablePerfCounters, false);

        LuceneIndexOutput(IndexOutput out) {
        	super("");
            output = out;
        }

        @Override
        public void writeByte(byte b) throws IOException {
            if (!disableCounters) {
                ZimbraPerf.COUNTER_IDX_BYTES_WRITTEN.increment(1);
            }
            output.writeByte(b);
        }

        @Override
        public void writeBytes(byte[] b, int len) throws IOException {
            if (!disableCounters) {
                ZimbraPerf.COUNTER_IDX_BYTES_WRITTEN.increment(len);
            }
            output.writeBytes(b, len);
        }

        @Override
        public void writeBytes(byte[] b, int offset, int len) throws IOException {
            if (!disableCounters) {
                ZimbraPerf.COUNTER_IDX_BYTES_WRITTEN.increment(len);
            }
            output.writeBytes(b, offset, len);
        }


        @Override
        public void close() throws IOException {
            output.close();
        }

        @Override
        public long getFilePointer() {
            return output.getFilePointer();
        }

		@Override
		public long getChecksum() throws IOException {
			return output.getChecksum();
		}
    }

	@Override
	public void renameFile(String source, String dest) throws IOException {
		// TODO Auto-generated method stub
		
	}
}
