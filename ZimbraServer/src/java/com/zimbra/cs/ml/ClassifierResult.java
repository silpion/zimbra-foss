package com.zimbra.cs.ml;
/**
 *
 * @author iraykin
 *
 */
public interface ClassifierResult {

	public Label getLabel();

	public String getReason();
}
