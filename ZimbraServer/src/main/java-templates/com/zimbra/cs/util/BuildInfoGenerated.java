package com.zimbra.cs.util;

class BuildInfoGenerated {
    public static final String MAJORVERSION = "${zimbra.buildinfo.majorversion}";
    public static final String MINORVERSION = "${zimbra.buildinfo.minorversion}";
    public static final String MICROVERSION = "${zimbra.buildinfo.microversion}";
    public static final String RELCLASS = "${zimbra.buildinfo.relclass}";
    public static final String RELNUM = "${zimbra.buildinfo.relnum}";
    public static final String BUILDNUM = "${zimbra.buildinfo.buildnum}";
    public static final String VERSION = "${zimbra.buildinfo.version}";
    public static final String TYPE = "${zimbra.buildinfo.type}";
    public static final String RELEASE = "${zimbra.buildinfo.release}";
    public static final String DATE = "${zimbra.buildinfo.timestamp}";
    public static final String HOST = "${zimbra.buildinfo.host}";
}