/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2011, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
package sample.oauth.provider;

import java.net.URLEncoder;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import sample.oauth.provider.core.SampleZmOAuthProvider;

import com.zimbra.common.service.ServiceException;
import com.zimbra.common.util.ZimbraLog;
import com.zimbra.common.util.memcached.MemcachedSerializer;
import com.zimbra.cs.account.AuthToken;
import com.zimbra.cs.account.Provisioning;
import com.zimbra.cs.account.ZimbraAuthToken;

/**
 */
public class OAuthAccessorSerializer implements MemcachedSerializer<OAuthAccessor> {

    @Override
    public Object serialize(OAuthAccessor value) {
        String consumer_key = (String) value.consumer.getProperty("key");
        String approved_on = (String) value.consumer.getProperty("approved_on");
        String device = (String) value.consumer.getProperty("device");
        String token_secret = value.tokenSecret;
        String callback = (String) value.getProperty(OAuth.OAUTH_CALLBACK);
        String user = (String) value.getProperty("user");
        String authorized;
        if (value.getProperty("authorized") != null) {
            authorized = value.getProperty("authorized").toString();
        } else {
            authorized = null;
        }
        String zauthtoken = (String) value.getProperty("ZM_AUTH_TOKEN");
        String verifier = (String) value.getProperty(OAuth.OAUTH_VERIFIER);

        String result = "consumer_key:" + consumer_key + ",token_secret:" + token_secret + //
                ",callback:" + callback + ",user:" + user + ",authorized:" + authorized + //
                ",zauthtoken:" + zauthtoken + ",verifier:" + verifier +",approved_on:" + approved_on +",device="+device;
        //return value.encode().toString();

        ZimbraLog.extensions.debug("put value: " + result + "  into memcache.");

        return result;
    }

    @Override
    public OAuthAccessor deserialize(Object obj) throws ServiceException {

        String value = (String) obj;
        ZimbraLog.extensions.debug("get value: " + value);
        String consumer_key = value.substring(0, value.indexOf(",token_secret")).substring(13);
        String token_secret = value.substring(value.indexOf(",token_secret"), value.indexOf(",callback")).substring(14);
        String callback = value.substring(value.indexOf(",callback"), value.indexOf(",user")).substring(10);
        String user = value.substring(value.indexOf(",user"), value.indexOf(",authorized")).substring(6);
        String authorized = value.substring(value.indexOf(",authorized"), value.indexOf(",zauthtoken")).substring(12);
        String zauthtoken = value.substring(value.indexOf(",zauthtoken"), value.indexOf(",verifier")).substring(12);
        String verifier = value.substring(value.indexOf(",verifier"), value.indexOf(",approved_on")).substring(10);
        String approved_on = value.substring(value.indexOf(",approved_on"), value.indexOf(",device")).substring(13);
        String device = value.substring(value.indexOf(",device")).substring(8);

        ZimbraLog.extensions.debug("consumer_key:" + consumer_key);
        ZimbraLog.extensions.debug("callback:" + callback);
        ZimbraLog.extensions.debug("user:" + user);
        ZimbraLog.extensions.debug("authorized:" + authorized);
        ZimbraLog.extensions.debug("zauthtoken:" + zauthtoken);
        ZimbraLog.extensions.debug("verifier:" + verifier);
        ZimbraLog.extensions.debug("approved_on:" + approved_on);
        ZimbraLog.extensions.debug("device:" + device);

        try {
            OAuthConsumer consumer = SampleZmOAuthProvider.getConsumer(consumer_key);
            OAuthAccessor accessor = new OAuthAccessor(consumer);
            accessor.tokenSecret = token_secret;
            accessor.setProperty(OAuth.OAUTH_CALLBACK, callback);

            if (!user.equals("null")) {
                accessor.setProperty("user", user);
            }

            if (authorized.equalsIgnoreCase(Boolean.FALSE.toString())) {
                accessor.setProperty("authorized", Boolean.FALSE);
            } else if (authorized.equalsIgnoreCase(Boolean.TRUE.toString())) {
                accessor.setProperty("authorized", Boolean.TRUE);
            }

            if (!zauthtoken.equals("null")) {
                accessor.setProperty("ZM_AUTH_TOKEN", zauthtoken);
                AuthToken zimbraAuthToken = ZimbraAuthToken.getAuthToken(zauthtoken);
                accessor.setProperty("ZM_ACC_DISPLAYNAME", zimbraAuthToken.getAccount().getAttr(Provisioning.A_displayName) == null ? "" : URLEncoder.encode(zimbraAuthToken.getAccount().getAttr(Provisioning.A_displayName),"UTF-8"));
                accessor.setProperty("ZM_ACC_CN", zimbraAuthToken.getAccount().getName()==null ? "" : URLEncoder.encode(zimbraAuthToken.getAccount().getName(),"UTF-8"));
                accessor.setProperty("ZM_ACC_GIVENNAME", zimbraAuthToken.getAccount().getAttr(Provisioning.A_givenName) == null ? "" : URLEncoder.encode(zimbraAuthToken.getAccount().getAttr(Provisioning.A_givenName),"UTF-8"));
                accessor.setProperty("ZM_ACC_SN",zimbraAuthToken.getAccount().getAttr(Provisioning.A_sn) == null ? "" : URLEncoder.encode(zimbraAuthToken.getAccount().getAttr(Provisioning.A_sn),"UTF-8"));
                accessor.setProperty("ZM_ACC_EMAIL",zimbraAuthToken.getAccount().getMail() == null ? "" :  URLEncoder.encode(zimbraAuthToken.getAccount().getMail(),"UTF-8"));
            }

            if (!verifier.equals("null")) {
                accessor.setProperty(OAuth.OAUTH_VERIFIER, verifier);
            }

            if (null != approved_on) {
                accessor.consumer.setProperty("approved_on", approved_on);
            }

            if (null != device) {
                accessor.consumer.setProperty("device", device);
            }
            return accessor;
        } catch (Exception e) {
            //need more hack here for hadnling IOException properly
            throw ServiceException.FAILURE("IOException", e);
        }
    }
}