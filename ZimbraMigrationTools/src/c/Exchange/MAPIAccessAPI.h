/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
#pragma once
#include "logger.h"
#include "Util.h"
#include "MAPIRules.h"

namespace Zimbra
{
namespace MAPI
{

typedef struct _Folder_Data
{
    wstring         name;
    SBinary         sbin;
    wstring         folderpath;
    wstring         containerclass;
    long            zimbraspecialfolderid;
    unsigned long   itemcount;
} Folder_Data;

typedef struct _Item_Data
{
    SBinary         sbMessageID;
    long            lItemType;
    __int64         i64SubmitDate;
    string          strMsgEntryId;
    wstring         strSubject;
} Item_Data;

typedef struct _MessagePart
{
    wstring contentType;
    wstring content;
} MessagePart;

// base item data
typedef struct _BaseItemData
{
    __int64 MessageDate;
} BaseItemData;

// folders to skip
enum
{
    TS_JOURNAL = 0, TS_OUTBOX, TS_SYNC_CONFLICTS, TS_SYNC_ISSUES, TS_SYNC_LOCAL_FAILURES, TS_SYNC_SERVER_FAILURES, TS_FOLDERS_MAX
};


// contact item data
typedef struct _ContactItemData: BaseItemData
{
    wstring AssistantPhone;
    wstring CallbackPhone;
    wstring CarPhone;
    wstring Company;
    wstring CompanyPhone;
    wstring Department;
    wstring Email1;
    wstring Email2;
    wstring Email3;
    wstring FileAs;
    wstring FirstName;
    wstring HomeCity;
    wstring HomeCountry;
    wstring HomeFax;
    wstring HomePhone;
    wstring HomePhone2;
    wstring HomePostalCode;
    wstring HomeState;
    wstring HomeStreet;
    wstring HomeURL;
    wstring JobTitle;
    wstring LastName;
    wstring MiddleName;
    wstring MobilePhone;
    wstring NamePrefix;
    wstring NameSuffix;
    wstring Notes;
    wstring OtherCity;
    wstring OtherCountry;
    wstring OtherFax;
    wstring OtherPhone;
    wstring OtherPostalCode;
    wstring OtherState;
    wstring OtherStreet;
    wstring OtherURL;
    wstring Pager;
    wstring PrimaryPhone;
    wstring WorkCity;
    wstring WorkCountry;
    wstring WorkFax;
    wstring WorkPhone;
    wstring WorkPhone2;
    wstring WorkPostalCode;
    wstring WorkState;
    wstring WorkStreet;
    wstring WorkURL;
    wstring Birthday;
    wstring UserField1;
    wstring UserField2;
    wstring UserField3;
    wstring UserField4;
    wstring NickName;
    wstring pDList;
    wstring Type;
    wstring PictureID;
    wstring IMAddress1;
    wstring Anniversary;
    wstring ContactImagePath;
    wstring ImageContenttype;
    wstring ImageContentdisp;
    vector<ContactUDFields> UserDefinedFields;
    vector<LPWSTR>* vTags;
} ContactItemData;

typedef struct
{
    LPTSTR buffer;
    unsigned long size;
} data_buffer;

typedef struct _MessageItemData: BaseItemData
{
    wstring Subject;
    bool IsFlagged;
    wstring Urlname;
    bool IsDraft;
    bool IsFromMe;
    bool IsUnread;
    bool IsForwarded;
    bool RepliedTo;
    bool HasAttachments;
    bool IsUnsent;
    bool HasHtml;
    bool HasText;
    __int64 deliveryDate;
    wstring DeliveryDateString;
    wstring DeliveryUnixString;
    __int64 Date;
    wstring DateUnixString;
    wstring DateString;
    vector<LPWSTR>* vTags;
    data_buffer textbody;
    data_buffer htmlbody;

    DWORD dwMimeSize;
    wstring MimeFile;
    LPWSTR pwsMimeBuffer;
} MessageItemData;

typedef struct _ApptItemData: BaseItemData
{
    wstring Subject;
    wstring Name;
    wstring Location;
    wstring Uid;
    wstring PartStat;
    wstring CurrStat;
    wstring FreeBusy;
    wstring Transparency;
    wstring AllDay;
    wstring StartDate;
    wstring CalFilterDate;
    wstring EndDate;
    wstring ApptClass;
    wstring AlarmTrigger;
    wstring RSVP;
    Organizer organizer;

    TzStrings tzLegacy;
    TzStrings tzStart;
    TzStrings tzEnd;

    vector<Attendee*> vAttendees;
    vector<AttachmentInfo*> vAttachments;
    vector<MessagePart> vMessageParts;
    vector<LPWSTR>* vTags;

    // recurrence stuff
    wstring recurPattern;
    wstring recurInterval;
    wstring recurWkday;
    wstring recurEndType;
    wstring recurCount;
    wstring recurEndDate;
    wstring recurDayOfMonth;
    wstring recurMonthOccurrence;
    wstring recurMonthOfYear;
    vector<MAPIAppointment*> vExceptions;

    // exception
    wstring ExceptionType;

    //data_buffer textbody;
    //data_buffer htmlbody;
} ApptItemData;

typedef struct _TaskItemData: BaseItemData
{
    wstring Subject;
    wstring Importance;
    wstring TaskStart;
    wstring TaskFilterDate;
    wstring TaskDue;
    wstring Status;
    wstring PercentComplete;
    wstring TotalWork;
    wstring ActualWork;
    wstring Companies;
    wstring Mileage;
    wstring BillingInfo;
    wstring TaskFlagDueBy;
    wstring ApptClass;
    vector<AttachmentInfo*> vAttachments;
    vector<MessagePart> vMessageParts;
    vector<LPWSTR>* vTags;

    // DCB Note that tasks lack Tz member - I think they need it!

    // recurrence stuff
    wstring recurPattern;
    wstring recurInterval;
    wstring recurWkday;
    wstring recurEndType;
    wstring recurCount;
    wstring recurEndDate;
    wstring recurDayOfMonth;
    wstring recurMonthOccurrence;
    wstring recurMonthOfYear;
} TaskItemData;

#define FL_NONE				0
#define FL_PUBLIC_FOLDER	1
typedef enum MIG_TYPE {MAILBOX_MIG, PST_MIG, PROFILE_MIG} MIG_TYPE;



// ====================================================================================
// MAPIAccess API
// ====================================================================================
//
// Provides C# layer with access to MAPI items. C# access this via CMapiWrapper
//
// Typical calls:
// - MAPIAccessAPI ctor
// - InitGlobalSessionAndStore()
// - UserInit() <- logs onto store and gets root folder
//
// - GetRootFolderHierarchy() <- returns vector<Folder_Data> (name, EID, folderpath, zimID, itemcount)
//
//

class MAPIAccessAPI
{
public:
    MAPIAccessAPI(wstring strUserName, wstring strUserAccount);
    ~MAPIAccessAPI();

    // static methods to be used by all mailboxes/profile/PST
    // lpcwstrMigTarget -> Exchange Admin Profile for Exchange mailboxes migration
    // lpcwstrMigTarget -> Local Exchange profile migration
    // lpcwstrMigTarget -> PST file path for PST migration
    static LPCWSTR InitGlobalSessionAndStore(LPCWSTR lpcwstrMigTarget, LPCWSTR userAccount=L"", ULONG flag=FL_NONE);
    static void UnInitGlobalSessionAndStore();

    // Per mailbox methods.
    LPCWSTR InitializeUser();
    LPCWSTR GetRootFolderHierarchy(vector<Folder_Data> &vfolderlist);
    LPCWSTR GetFolderItemsList(SBinary sbFolderEID, vector<Item_Data> &ItemList);
    LPCWSTR GetItem(SBinary sbItemEID, BaseItemData &itemData);

    // OOO
    LPWSTR  GetOOOStateAndMsg();
    LPCWSTR GetExchangeRules(vector<CRule> &vRuleList);

    // Rules
    LPCWSTR InitializePublicFolders();
    HRESULT EnumeratePublicFolders(std::vector<std::string> &pubFldrList);

    // Public folders
    static MIG_TYPE GetMigrationType(){return m_MigType;}

private:
    void InitFoldersToSkip();

    static LPCWSTR _InitGlobalSessionAndStore(LPCWSTR lpcwstrMigTarget, ULONG flag);	
    static void internalInit();

    LPCWSTR OpenUserStore();
    LPCWSTR OpenPublicStore();

    LPCWSTR _GetRootFolderHierarchy(vector<Folder_Data> &vfolderlist);
    LPCWSTR _InitializeUser();
    LPCWSTR _GetFolderItemsList(SBinary sbFolderEID, vector<Item_Data> &ItemList);
    LPCWSTR _GetItem(SBinary sbItemEID, BaseItemData &itemData);

    HRESULT CacheFolderDataAndIterateChildFoldersDepthFirst(Zimbra::MAPI::MAPIFolder &folder, vector<Folder_Data> &fd);
    bool SkipFolder(ExchangeSpecialFolderId exfid);
    void MAPIFolder2FolderData(Zimbra::MAPI::MAPIFolder *folder, Folder_Data &flderdata);

    static LONG WINAPI UnhandledExceptionFilter(LPEXCEPTION_POINTERS pExPtrs);

    static void SetOOMRegistry();
    static void ResetOOMRegistry();

    HRESULT GetInternalFolder(SBinary sbFolderEID, MAPIFolder &folder);

    /*
    // OBSOLETE?
    // void traverse_folder(Zimbra::MAPI::MAPIFolder &folder);
    */

private:
    static MIG_TYPE m_MigType;
    static std::wstring m_strTargetProfileName;
    static std::wstring m_strExchangeHostName;

    static bool m_bSingleMailBoxMigration;
    static bool m_bHasJoinedDomain;
    std::wstring m_strUserName;
    std::wstring m_strUserAccount;
    ExchangeSpecialFolderId m_aFoldersToSkip[TS_FOLDERS_MAX];

    // MAPI stuff
    static Zimbra::MAPI::MAPISession *m_zmmapisession;
    static Zimbra::MAPI::MAPIStore   *m_defaultStore;
           Zimbra::MAPI::MAPIStore   *m_userStore;
           Zimbra::MAPI::MAPIFolder  *m_rootFolder;
           Zimbra::MAPI::MAPIStore   *m_publicStore;

};
}
}
