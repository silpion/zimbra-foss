/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
// MapiWrapper.cpp : Implementation of CMapiWrapper

#include "common.h"
#include "Exchange.h"
#include "ExchangeAdmin.h"
#include "MapiWrapper.h"
#include <ATLComTime.h>

// C5E4267B-AE6C-4E31-956A-06D8094D0CBE
const IID UDTVariable_IID = {0xC5E4267B, 0xAE6C, 0x4E31, {0x95, 0x6A, 0x06, 0xD8, 0x09, 0x4D, 0x0C, 0xBE}};
const IID UDTItem_IID     = {0xC5E4267A, 0xAE6C, 0x4E31, {0x95, 0x6A, 0x06, 0xD8, 0x09, 0x4D, 0x0C, 0xBE}};

std::string format_error(unsigned __int32 hr)
{
    std::stringstream ss;
    ss << "COM call Failed. Error code = 0x" << std::hex << hr << std::endl;
    return ss.str();
}

STDMETHODIMP CMapiWrapper::InterfaceSupportsErrorInfo(REFIID riid)
{
    static const IID *const arr[] = {&IID_IMapiWrapper};

    for (int i = 0; i < sizeof (arr) / sizeof (arr[0]); i++)
    {
        if (InlineIsEqualGUID(*arr[i], riid))
            return S_OK;
    }
    return S_FALSE;
}

STDMETHODIMP CMapiWrapper::ConnectToServer(BSTR /*ServerHostName*/, BSTR /*Port*/, BSTR AdminID)
{
    LOGFN_TRACE_NO;

    LPCWSTR status = MAPIAccessAPI::InitGlobalSessionAndStore( /*ServerHostName, */ AdminID, FL_NONE);
    if(status != NULL)
    {
        LOG_ERROR(_T("InitGlobalSessionAndStore failed '%s'"), status);
        return S_FALSE;
    }

    return S_OK;
}

STDMETHODIMP CMapiWrapper::GlobalInit(BSTR pMAPITarget, BSTR pAdminUser, BSTR pAdminPassword, BSTR *pErrorText)
{
    LOGFN_TRACE_NO;

    LPCWSTR lpszErrorText = ExchangeOps::InitExchangeOps((LPCWSTR)pMAPITarget, (LPCWSTR)pAdminUser, (LPCWSTR)pAdminPassword);
    
    *pErrorText = (lpszErrorText) ? CComBSTR(lpszErrorText) : CComBSTR("");

    return S_OK;
}

STDMETHODIMP CMapiWrapper::ImportMailOptions(BSTR /*OptionsTag*/)
{
    LOGFN_TRACE_NO;
    return S_OK;
}

STDMETHODIMP CMapiWrapper::GetProfilelist(VARIANT* Profiles, BSTR* statusmessage)
//
// NB Returns only Exchange profiles
//
{
    LOGFN_TRACE_NO;

    if (statusmessage == NULL)
    {
        _ASSERT(FALSE);
        return E_POINTER;
    }

    HRESULT hr = MAPIInitialize(NULL);

    #ifdef DEBUG
        // DCB Step onto E_FAIL to test error handling
        bool Fail = false;
        if (Fail)
            hr = E_FAIL;
    #endif

    CComBSTR status = L"";
    if (hr != S_OK)
    {
        status.Append(L" MapiInitialize error ");
        status.Append(format_error(hr).c_str());
        LOG_ERROR(_T("%s"), status.m_str);
        *statusmessage = status.Detach();
        return hr;
    }

    Zimbra::Mapi::Memory::SetMemAllocRoutines(NULL, MAPIAllocateBuffer, MAPIAllocateMore, MAPIFreeBuffer);

    vector<string> vProfileList;
    hr = m_pExchAdmin->GetAllProfiles(vProfileList);
    if (hr != S_OK)
    {
        status.Append(" GetAllProfiles error ");
        status.Append(format_error(hr).c_str());
        LOG_ERROR(_T("%s"), status.m_str);

        *statusmessage = status.Detach();
        return hr;
    }

    if (vProfileList.size() == 0)
    {
        LOG_ERROR(_T("No profiles returned for GetAllProfiles"));
        status = L"No profiles";
        *statusmessage = status.Detach();
        return S_OK;
    }

    vector<CComBSTR> tempvectors;
    std::vector<string>::iterator its;
    for (its = (vProfileList.begin()); its != vProfileList.end(); its++)
    {
        string str = (*its).c_str();
        CComBSTR temp = SysAllocString(str_to_wstr(str).c_str());
        tempvectors.push_back(temp);
    }

    // Copy tempvectors into out param
    VariantInit(Profiles);
    Profiles->vt = VT_ARRAY | VT_BSTR;
    SAFEARRAY *psa;
    SAFEARRAYBOUND bounds = { (ULONG)vProfileList.size(), 0 };
    psa = SafeArrayCreate(VT_BSTR, 1, &bounds);
    BSTR *bstrArray;
    SafeArrayAccessData(psa, (void **)&bstrArray);
    std::vector<CComBSTR>::iterator it;
    int i = 0;
    for (it = (tempvectors.begin()); it != tempvectors.end(); it++, i++)
        bstrArray[i] = SysAllocString((*it).m_str);

    SafeArrayUnaccessData(psa);
    Profiles->parray = psa;

    *statusmessage =  status;
    status.Detach();

    MAPIUninitialize();

    return hr;
}

std::wstring CMapiWrapper::str_to_wstr(const std::string &str)
{
    std::wstring wstr(str.length() + 1, 0);
    MultiByteToWideChar(CP_ACP, 0, str.c_str(), (int)str.length(), &wstr[0], (int)str.length());
    return wstr;
}

STDMETHODIMP CMapiWrapper::GlobalUninit(BSTR *pErrorText)
{
    LOGFN_TRACE_NO;

    LPCWSTR lpszErrorText = ExchangeOps::UninitExchangeOps();

    *pErrorText = (lpszErrorText) ? CComBSTR(lpszErrorText) : CComBSTR("");
    return S_OK;
}

STDMETHODIMP CMapiWrapper::AvoidInternalErrors(BSTR lpToCmp, LONG *lRetval)
{
    *lRetval=(LONG)ExchangeOps::AvoidInternalErrors((LPCWSTR)lpToCmp);
    return S_OK;
}

STDMETHODIMP CMapiWrapper::SelectExchangeUsers(VARIANT *Users, BSTR *pErrorText)
{
    LOGFN_TRACE_NO;

    vector<ObjectPickerData> vUserList;
    LPCWSTR lpszErrorText = ExchangeOps::SelectExchangeUsers(vUserList);
    if(lpszErrorText != NULL)
    {
        LOG_ERROR(_T("SelectExchangeUsers failed '%s'"), lpszErrorText);
        return S_FALSE;
    }

    if(vUserList.size() == 0)
    {
        LOG_ERROR(_T("SelectExchangeUsers returned no users"));
        return S_OK;
    }

    wstring str = L"", strDisplayName = L"", strGivenName = L"", strSN = L"", strZFP = L"";
    vector<CComBSTR> tempvectors;
    std::vector<ObjectPickerData>::iterator its;
    for (its = (vUserList.begin()); its != vUserList.end(); its++)
    {
        ObjectPickerData obj = (*its);

        // FBS bug 71646 -- 3/26/12
        str = (*its).wstrUsername;
        for (size_t j = 0; j < (*its).pAttributeList.size(); j++)
        {
            if (its->pAttributeList[j].first == L"displayName")
                strDisplayName = its->pAttributeList[j].second;

            if (its->pAttributeList[j].first == L"givenName")
                strGivenName = its->pAttributeList[j].second;

            if (its->pAttributeList[j].first == L"sn")
                strSN = its->pAttributeList[j].second;

            if (its->pAttributeList[j].first == L"zimbraForeignPrincipal")
                strZFP = its->pAttributeList[j].second;

        }

        str += L"~";
        str += strDisplayName;
        str += L"~";
        str += strGivenName;
        str += L"~";
        str += strSN;
        str += L"~";
        str += strZFP;
        //
        CComBSTR temp = SysAllocString(str.c_str());
        tempvectors.push_back(temp);
    }

    VariantInit(Users);
    Users->vt = VT_ARRAY | VT_BSTR;

    SAFEARRAYBOUND bounds = { (ULONG)vUserList.size(), 0 };
    SAFEARRAY *psa;
    psa = SafeArrayCreate(VT_BSTR, 1, &bounds);

    BSTR *bstrArray;
    SafeArrayAccessData(psa, (void **)&bstrArray);

    std::vector<CComBSTR>::iterator it;
    int i = 0;
    for (it = (tempvectors.begin()); it != tempvectors.end(); it++, i++)
        bstrArray[i] = SysAllocString((*it).m_str);

    SafeArrayUnaccessData(psa);

    Users->parray = psa;
    *pErrorText = (lpszErrorText) ? CComBSTR(lpszErrorText) : CComBSTR("");

    return S_OK;
}

