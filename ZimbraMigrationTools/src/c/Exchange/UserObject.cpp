/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
// UserObject.cpp : Implementation of CUserObject

#include "common.h"
#include "Logger.h"
#include "UserObject.h"

STDMETHODIMP CUserObject::InterfaceSupportsErrorInfo(REFIID riid)
{
    static const IID* const arr[] = {&IID_IUserObject};

    for (int i = 0; i < sizeof (arr) / sizeof (arr[0]); i++)
    {
        if (InlineIsEqualGUID(*arr[i], riid))
            return S_OK;
    }
    return S_FALSE;
}

wstring BSTR2Str(const BSTR& b)
{
    UINT l = SysStringLen(b);
    if (!l)
        return L"";
    return wstring(b, l);
}

STDMETHODIMP CUserObject::Init(BSTR host, BSTR location, BSTR account, long PublicFlag, BSTR *pErrorText)
{
    LOGFN_TRACE_NO;

    HRESULT hr = S_FALSE;

    LOG_TRACE(_T("host:     '%s'"), BSTR2Str(host).c_str());
    LOG_TRACE(_T("location: '%s'"), BSTR2Str(location).c_str());
    LOG_TRACE(_T("account:  '%s'"), BSTR2Str(account).c_str());

    if (PublicFlag)
    {
        LPCWSTR err = MAPIAccessAPI::InitGlobalSessionAndStore(location,L"EXCH_ADMIN",PublicFlag);
        if (err)
            *pErrorText = CComBSTR(err);
        else
            hr = mapiObj->UserInit(L"", L"", pErrorText);
        hr = mapiObj->InitializePublicFolders(pErrorText);
    }
    else
    {
        if (host && *host)
            hr = mapiObj->UserInit(location, account, pErrorText);
        else
        {
            LPCWSTR err = MAPIAccessAPI::InitGlobalSessionAndStore(location);
            if (err)
                *pErrorText = CComBSTR(err);
            else
                hr = mapiObj->UserInit(L"", account, pErrorText);
        }
    }
    if (FAILED(hr))
    {
        CComBSTR str = "Init error ";
        str.AppendBSTR(*pErrorText);
        LOG_ERROR(_T("%s"), str.m_str);
    }
    return hr;
}

STDMETHODIMP CUserObject::Uninit()
{
    LOGFN_TRACE_NO;

    HRESULT hr = mapiObj->UserUninit();
    return hr;
}

STDMETHODIMP CUserObject::GetFolders(VARIANT *vObjects)
{
    LOGFN_TRACE_NO;

    LOG_TRACE(_T("======================================================================================="));
    LOG_INFO( _T("Getting folder list from source store"));
    LOG_TRACE(_T("======================================================================================="));

    //dlog.dump(L"--------------", L"aaaaaaaa\r\n   bbbb\r\nccccccc");
    VariantInit(vObjects);
    HRESULT hr = mapiObj->GetFolderList(vObjects);    
    if (hr!=S_OK)
        LOG_ERROR(_T("GetFolderList failed %08X"), hr);

    return hr;
}

STDMETHODIMP CUserObject::GetItemsForFolder(IFolderObject *folderObj, VARIANT creationDate, VARIANT *vItems)
{
    LOGFN_TRACE_NO;

    BSTR b;
    HRESULT hr = folderObj->get_FolderPath(&b);
    LOG_TRACE(_T("---------------------------------------------------------------------------------------"));
    LOG_TRACE(_T("Getting all items from folder '%s'..."), b);
    LOG_TRACE(_T("---------------------------------------------------------------------------------------"));

    VariantInit(vItems);
    hr = mapiObj->GetItemsList(folderObj, creationDate, vItems);
    if (FAILED(hr))
         LOG_ERROR(_T("GetItemsList failed %08X"), hr);

    return hr;
}

STDMETHODIMP CUserObject::GetMapiAccessObject(BSTR /*userID*/, IMapiAccessWrap **pVal)
{
    LOGFN_TRACE_NO;

    *pVal = mapiObj;
    HRESULT hr = (*pVal)->AddRef();
    if (FAILED(hr))
         LOG_ERROR(_T("AddRef failed %08X"), hr);

    return hr;
}

STDMETHODIMP CUserObject::GetOOO(BSTR *pOOO)
{
    LOGFN_TRACE_NO;

    HRESULT hr = mapiObj->GetOOOInfo(pOOO);
    if (FAILED(hr))
        LOG_ERROR(_T("GetOOOInfo failed %08X"), hr);

    return hr;
}

STDMETHODIMP CUserObject::GetRules(VARIANT *vRules)
{
    LOGFN_TRACE_NO;

    VariantInit(vRules);
    HRESULT hr = mapiObj->GetRuleList(vRules);
    if (FAILED(hr))
        LOG_ERROR(_T("GetRuleList failed %08X"), hr);

    return hr;
}
