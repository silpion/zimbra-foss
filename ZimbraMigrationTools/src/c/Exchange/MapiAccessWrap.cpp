/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
// MapiAccessWrap.cpp : Implementation of CMapiAccessWrap

#include "common.h"
#include "MapiAccessWrap.h"

Zimbra::Util::CriticalSection  cs;
// CMapiAccessWrap

STDMETHODIMP CMapiAccessWrap::InterfaceSupportsErrorInfo(REFIID riid)
{
    static const IID* const arr[] = 
    {
        &IID_IMapiAccessWrap
    };

    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
    {
        if (InlineIsEqualGUID(*arr[i],riid))
            return S_OK;
    }
    return S_FALSE;
}

STDMETHODIMP CMapiAccessWrap::UserInit(BSTR UserName, BSTR userAccount, BSTR *StatusMsg)
{
    LOGFN_TRACE_NO;

    Zimbra::Util::AutoCriticalSection autocriticalsection(cs);

    maapi = new Zimbra::MAPI::MAPIAccessAPI(UserName, userAccount);

    // Init session and stores
    LPCWSTR lpStatus = maapi->InitializeUser();

    *StatusMsg = (lpStatus) ? CComBSTR(lpStatus) : SysAllocString(L"");

    return S_OK;
}

STDMETHODIMP CMapiAccessWrap::UserUninit()
{
    LOGFN_TRACE_NO;

    if (maapi)
    {
        delete maapi;
    }
    maapi = NULL;
    return S_OK;
}

STDMETHODIMP CMapiAccessWrap::GetFolderList(VARIANT *folders)
{
    LOGFN_TRACE_NO;

    HRESULT hr = S_OK;

    VariantInit(folders);
    folders->vt = VT_ARRAY | VT_DISPATCH;

    USES_CONVERSION;

    /*
    // DCB Test migration SUMMARY log
    LOG_SUMMARY(_T("Getting folder list"));
    int i = 0;
    int j = 1/i;
    (void)j;*/

    // ==========================================================
    // Get folders into vFolders
    // ==========================================================
    vector<Folder_Data> vFolders;
    LPCWSTR lpStatus= maapi->GetRootFolderHierarchy(vFolders);
    if (lpStatus != NULL )
    {
        LOG_ERROR(_T("GetRootFolderHierarchy failed '%s'"), lpStatus);
        folders->parray = NULL;
        hr = S_FALSE;
        return hr;
    }

    std::vector<Folder_Data>::iterator it;
    size_t size = vFolders.size();

    LOG_INFO(_T(" "));
    if(size == 0)
    {
        LOG_ERROR(_T("No folders found"));
        hr = S_OK;
        return hr;
    }
    else
        LOG_SUMMARY(_T("Found %d folders:"), size);

    // ==========================================================
    // Transfer vFolders to "folders" out param
    // ==========================================================

    // Create a safe array of folders
    SAFEARRAYBOUND bounds = { (ULONG)size, 0 };
    SAFEARRAY *psa = SafeArrayCreate(VT_DISPATCH, 1, &bounds);
    IFolderObject **pfolders;
    SafeArrayAccessData(psa, (void **)&pfolders);

    it = vFolders.begin();
    for (size_t i = 0; i < size; i++, it++)
    {
        // --------------------------------------
        // Log the folder details
        // --------------------------------------
        wstring sFoldName = it->name.c_str();
        wstring sPadFoldName;
        sPadFoldName.resize(max(60 - wcslen(it->name.c_str()), 0), L' ');
        sFoldName+=sPadFoldName;


        long sfid = it->zimbraspecialfolderid;
        if (!sfid)
        {
            // Not a special folder
            LOG_SUMMARY(_T(" %s [%5d item(s])"), sFoldName.c_str(), it->itemcount);
        }
        else
        {
            // It's a special folder
            wstring s;
            switch(sfid)
            {
                case ZM_ROOT:               s = _T("ROOT");                break;
                case ZM_INBOX:              s = _T("INBOX");               break;
                case ZM_TRASH:              s = _T("TRASH");               break; 
                case ZM_SPAM:               s = _T("JUNK");                break;
                case ZM_SENT_MAIL:          s = _T("SENT_MAIL");           break;
                case ZM_DRAFTS:             s = _T("DRAFTS");              break;
                case ZM_CONTACTS:           s = _T("CONTACTS");            break;
                case ZM_TAGS:               s = _T("TAGS");                break;
                case ZM_CONVERSATIONS:      s = _T("CONVERSATIONS");       break;
                case ZM_CALENDAR:           s = _T("CALENDAR");            break;
                case ZM_MAILBOX_ROOT:       s = _T("MAILBOX_ROOT");        break;
                case ZM_WIKI:               s = _T("WIKI");                break;
                case ZM_EMAILEDCONTACTS:    s = _T("EMAILEDCONTACTS");     break;
                case ZM_CHATS:              s = _T("CHATS");               break;
                case ZM_TASKS:              s = _T("TASKS");               break;
                default: _ASSERT(FALSE);
            }

            LOG_SUMMARY(_T(" %s [%5d item(s)] SF:%s %d"), sFoldName.c_str(), it->itemcount, s.c_str(), sfid);
        }

        // --------------------------------------
        // Create a IFolderObject for this folder
        // --------------------------------------
        CComPtr<IFolderObject> pIFolderObject;
        hr = CoCreateInstance(CLSID_FolderObject, NULL, CLSCTX_ALL, IID_IFolderObject, reinterpret_cast<void **>(&pIFolderObject));
        if (SUCCEEDED(hr))
        {
            // --------------------------------------
            // Set its attributes
            // --------------------------------------

            // Name
            CComBSTR bstrName(it->name.c_str());
            pIFolderObject->put_Name(SysAllocString(bstrName));

            // ZimbraSpecialFolderId
            pIFolderObject->put_ZimbraSpecialFolderId(it->zimbraspecialfolderid);

            // Folder Path
            CComBSTR bstrPath(it->folderpath.c_str());
            pIFolderObject->put_FolderPath(SysAllocString(bstrPath));

            // Container class
            CComBSTR bstrClass(it->containerclass.c_str());
            pIFolderObject->put_ContainerClass(SysAllocString(bstrClass));

            // Item count
            pIFolderObject->put_ItemCount(it->itemcount);

            // Folder EID
            VARIANT var;
            SBinary Folderid = it->sbin;
            VariantInit(&var);
            var.vt = VT_ARRAY | VT_UI1;
            SAFEARRAYBOUND rgsabound[1];
            rgsabound[0].cElements = Folderid.cb;
            rgsabound[0].lLbound = 0;
            var.parray = SafeArrayCreate(VT_UI1, 1, rgsabound);
            if (var.parray != NULL)
            {
                void *pArrayData = NULL;
                SafeArrayAccessData(var.parray, &pArrayData);
                memcpy(pArrayData, Folderid.lpb, Folderid.cb); // Copy data to it
                SafeArrayUnaccessData(var.parray);
            }
            else
                LOG_ERROR(_T("SafeArrayCreate failed"));

            pIFolderObject->put_EID(var);
        }

        if (FAILED(hr))
        {
            LOG_ERROR(_T("Failed %08X"), hr);
            return S_FALSE;
        }

        // --------------------------------------
        // Copy the folder obj into the array
        // --------------------------------------
        pIFolderObject.CopyTo(&pfolders[i]);

    } // for (walk folders)
    LOG_INFO(_T(" "));

    SafeArrayUnaccessData(psa);
    folders->parray = psa;

    return S_OK;
}

STDMETHODIMP CMapiAccessWrap::GetItemsList(IFolderObject *FolderObj, VARIANT vCreationDate, VARIANT *vItems)
{
    LOGFN_TRACE_NO;

    HRESULT hr = S_OK;
    VariantInit(vItems);
    vItems->vt = VT_ARRAY | VT_DISPATCH;
    SAFEARRAY *psa;

    // ==================================================================================
    // Get folder's items into vItemDataList
    // ==================================================================================
    vector<Item_Data> vItemDataList;

    SBinary folderEntryid;
    folderEntryid.cb = 0;
    folderEntryid.lpb = NULL;

    USES_CONVERSION;

    VARIANT vararg;
    VariantInit(&vararg);
    vararg.vt = (VT_ARRAY | VT_UI1);

    FolderObj->get_EID(&vararg);
    if (vararg.vt == (VT_ARRAY | VT_UI1))       // (OLE SAFEARRAY)
    {
        // Retrieve size of array
        folderEntryid.cb = vararg.parray->rgsabound[0].cElements;
        ULONG size = folderEntryid.cb;

        folderEntryid.lpb = new BYTE[size];     // Allocate a buffer to store the data
        if (folderEntryid.lpb != NULL)
        {
            void *pArrayData;
            SafeArrayAccessData(vararg.parray, &pArrayData);
            memcpy(folderEntryid.lpb, pArrayData, size);        
            SafeArrayUnaccessData(vararg.parray);

            LPCWSTR lpStatus = maapi->GetFolderItemsList(folderEntryid, vItemDataList);
            if (lpStatus != NULL)
            {
                LOG_ERROR(_T("GetFolderItemsList failed '%s'"), lpStatus);
                hr = S_FALSE;
                return hr;
           }
        }
        else
            LOG_ERROR(_T("GetFolderItemsList folderEntryid is null"));
    }

    size_t size = vItemDataList.size();
    if (size == 0)
    {
        LOG_ERROR(_T("GetFolderItemsList returned no folders"));
        hr = S_OK;
        return hr;
    }
    else
        LOG_INFO(_T("Returned %d item(s)"), size);


    // ==================================================================================
    // Transfer the folder items from vItemDataList to OUT param "vItems"
    // ==================================================================================
    SAFEARRAYBOUND bounds = { (ULONG)size, 0 };
    psa = SafeArrayCreate(VT_DISPATCH, 1, &bounds);
    IItemObject **pItems;
    SafeArrayAccessData(psa, (void **)&pItems);

    vector<Item_Data>::iterator it = vItemDataList.begin();
    for (size_t i = 0; i < size; i++, it++)
    {
        // -------------------------
        // Create pIItemObject
        // -------------------------
        CComPtr<IItemObject> pIItemObject;
        hr = CoCreateInstance(CLSID_ItemObject, NULL, CLSCTX_ALL, IID_IItemObject, reinterpret_cast<void **>(&pIItemObject));
        if (SUCCEEDED(hr))
        {
            // -----------------
            // Type
            // -----------------
            pIItemObject->put_Type((FolderType)(it->lItemType));

            // -----------------
            // ID
            // -----------------
            // pIItemObject->put_ID(it->sbMessageID))

            // -----------------
            // Parent folder
            // -----------------
            pIItemObject->put_Parentfolder(FolderObj);

            // -----------------
            // Creation date
            // -----------------
            vCreationDate.vt = VT_DATE;
            vCreationDate.date = (long)(it->i64SubmitDate);

            // -----------------
            // ItemID
            // -----------------
            SBinary Itemid = it->sbMessageID;

            VARIANT var;
            VariantInit(&var);
            var.vt = VT_ARRAY | VT_UI1;

            SAFEARRAYBOUND rgsabound[1];
            rgsabound[0].cElements = Itemid.cb;
            rgsabound[0].lLbound = 0;
            var.parray = SafeArrayCreate(VT_UI1, 1, rgsabound);
            if (var.parray != NULL)
            {
                void *pArrayData = NULL;
                SafeArrayAccessData(var.parray, &pArrayData);
                memcpy(pArrayData, Itemid.lpb, Itemid.cb);
                SafeArrayUnaccessData(var.parray);
            }
            else
                LOG_ERROR(_T("GetFolderItemsList SafeArrayCreate is null"));

            pIItemObject->put_ItemID(var);

            // -----------------
            // Subject
            // -----------------
            pIItemObject->put_Subject(SysAllocString(it->strSubject.c_str()));

            // -----------------
            // ID as string
            // -----------------
            /*
            Zimbra::Util::ScopedArray<CHAR> spUid(new CHAR[(Itemid.cb * 2) + 1]);
            if (spUid.get() != NULL)
            {
                Zimbra::Util::HexFromBin(Itemid.lpb, Itemid.cb, spUid.get());
                CComBSTR str = spUid.getref();
                pIItemObject->put_IDasString(str);
                SysFreeString(str);
            }
            */
        }

        if (FAILED(hr))
            return S_FALSE;

        // -------------------------------------------
        // Copy the the pIItemObject into the variant
        // -------------------------------------------
        pIItemObject.CopyTo(&pItems[i]);

    } // for

    SafeArrayUnaccessData(psa);
    vItems->parray = psa;

    if (folderEntryid.lpb != NULL)
        delete folderEntryid.lpb;

    return S_OK;
}

STDMETHODIMP CMapiAccessWrap::GetData(BSTR UserId, VARIANT ItemId, FolderType type, VARIANT *pRet)
//
// ItemId is the item to fetch
// type is the folder it resides in (this is used to decide what kind of item it is)
//
// Calls maapi->GetItem() to get the data as a C++ struct ContactItemData/MessageItemData etc
// and then builds a dictionary (name-value pairs) for each attibute and outputs in pVal
//
{
    LOGFN_TRACE_NO;

    HRESULT hr = S_OK;

    std::map<BSTR, BSTR> mapDict;

    SBinary ItemID;
    CComBSTR name = UserId;

    FolderType ft = type;
    if (ItemId.vt == (VT_ARRAY | VT_UI1))       // (OLE SAFEARRAY)
    {
        // Get the ItemID from the passed-in variant
        ItemID.cb = ItemId.parray->rgsabound[0].cElements;
        ItemID.lpb = new BYTE[ItemID.cb];
        if (ItemID.lpb != NULL)
        {
            void *pArrayData;
            SafeArrayAccessData(ItemId.parray, &pArrayData);
            memcpy(ItemID.lpb, pArrayData, ItemID.cb);
            SafeArrayUnaccessData(ItemId.parray);

            LPCWSTR ret = NULL;

            if (ft == 2)
            {
                // =======================================================================
                // Contact
                // =======================================================================
                LOG_TRACE(_T("Contact"));

                ContactItemData cd;
                ret = maapi->GetItem(ItemID, cd);
                if(ret != NULL)
                {
                    delete ItemID.lpb;
                    hr = S_FALSE;
                    return hr;
                }

                if (ret == NULL)	// FBS bug 71630 -- 3/22/12
                {
                    mapDict[L"assistantPhone"]      = SysAllocString((cd.AssistantPhone).c_str());
                    mapDict[L"birthday"]            = SysAllocString((cd.Birthday).c_str());
                    mapDict[L"anniversary"]         = SysAllocString((cd.Anniversary).c_str());
                    mapDict[L"callbackPhone"]       = SysAllocString((cd.CallbackPhone).c_str());
                    mapDict[L"carPhone"]            = SysAllocString((cd.CarPhone).c_str());
                    mapDict[L"company"]             = SysAllocString((cd.Company).c_str());
                    mapDict[L"companyPhone"]        = SysAllocString((cd.CompanyPhone).c_str());
                    mapDict[L"department"]          = SysAllocString((cd.Department).c_str());
                    mapDict[L"email"]               = SysAllocString((cd.Email1).c_str());
                    mapDict[L"email2"]              = SysAllocString((cd.Email2).c_str());
                    mapDict[L"email3"]              = SysAllocString((cd.Email3).c_str());
                    mapDict[L"fileAs"]              = SysAllocString((cd.FileAs).c_str());
                    mapDict[L"firstName"]           = SysAllocString((cd.FirstName).c_str());
                    mapDict[L"homeCity"]            = SysAllocString((cd.HomeCity).c_str());
                    mapDict[L"homeCountry"]         = SysAllocString((cd.HomeCountry).c_str());
                    mapDict[L"homeFax"]             = SysAllocString((cd.HomeFax).c_str());
                    mapDict[L"homePhone"]           = SysAllocString((cd.HomePhone).c_str());
                    mapDict[L"homePhone2"]          = SysAllocString((cd.HomePhone2).c_str());
                    mapDict[L"homePostalCode"]      = SysAllocString((cd.HomePostalCode).c_str());
                    mapDict[L"homeState"]           = SysAllocString((cd.HomeState).c_str());
                    mapDict[L"homeStreet"]          = SysAllocString((cd.HomeStreet).c_str());
                    mapDict[L"homeURL"]             = SysAllocString((cd.HomeURL).c_str());
                    mapDict[L"imAddress1"]          = SysAllocString((cd.IMAddress1).c_str());
                    mapDict[L"jobTitle"]            = SysAllocString((cd.JobTitle).c_str());
                    mapDict[L"lastName"]            = SysAllocString((cd.LastName).c_str());
                    mapDict[L"middleName"]          = SysAllocString((cd.MiddleName).c_str());
                    mapDict[L"mobilePhone"]         = SysAllocString((cd.MobilePhone).c_str());
                    mapDict[L"namePrefix"]          = SysAllocString((cd.NamePrefix).c_str());
                    mapDict[L"nameSuffix"]          = SysAllocString((cd.NameSuffix).c_str());
                    mapDict[L"nickname"]            = SysAllocString((cd.NickName).c_str());
                    mapDict[L"notes"]               = SysAllocString((cd.Notes).c_str());
                    mapDict[L"otherCity"]           = SysAllocString((cd.OtherCity).c_str());
                    mapDict[L"otherCountry"]        = SysAllocString((cd.OtherCountry).c_str());
                    mapDict[L"otherFax"]            = SysAllocString((cd.OtherFax).c_str());
                    mapDict[L"otherPhone"]          = SysAllocString((cd.OtherPhone).c_str());
                    mapDict[L"otherPostalCode"]     = SysAllocString((cd.OtherPostalCode).c_str());
                    mapDict[L"otherState"]          = SysAllocString((cd.OtherState).c_str());
                    mapDict[L"otherStreet"]         = SysAllocString((cd.OtherStreet).c_str());
                    mapDict[L"otherURL"]            = SysAllocString((cd.OtherURL).c_str());
                    mapDict[L"pager"]               = SysAllocString((cd.Pager).c_str());
                    mapDict[L"workCity"]            = SysAllocString((cd.WorkCity).c_str());
                    mapDict[L"workCountry"]         = SysAllocString((cd.WorkCountry).c_str());
                    mapDict[L"workFax"]             = SysAllocString((cd.WorkFax).c_str());
                    mapDict[L"workPhone"]           = SysAllocString((cd.WorkPhone).c_str());
                    mapDict[L"workPhone2"]          = SysAllocString((cd.WorkPhone2).c_str());
                    mapDict[L"workPhone3"]          = SysAllocString((cd.PrimaryPhone).c_str());
                    mapDict[L"workPostalCode"]      = SysAllocString((cd.WorkPostalCode).c_str());
                    mapDict[L"workState"]           = SysAllocString((cd.WorkState).c_str());
                    mapDict[L"workStreet"]          = SysAllocString((cd.WorkStreet).c_str());
                    mapDict[L"workURL"]             = SysAllocString((cd.WorkURL).c_str());
                    mapDict[L"outlookUserField1"]   = SysAllocString((cd.UserField1).c_str());
                    mapDict[L"outlookUserField2"]   = SysAllocString((cd.UserField2).c_str());
                    mapDict[L"outlookUserField3"]   = SysAllocString((cd.UserField3).c_str());
                    mapDict[L"outlookUserField4"]   = SysAllocString((cd.UserField4).c_str());
                    mapDict[L"image"]               = SysAllocString((cd.ContactImagePath).c_str());
                    mapDict[L"imageContentType"]    = SysAllocString((cd.ImageContenttype).c_str());
                    mapDict[L"imageContentDisp"]    = SysAllocString((cd.ImageContentdisp).c_str());

                    if (cd.Type.length() > 0)
                    {
                        if (wcsicmp(cd.Type.c_str(), L"group") == 0)
                        {
                            mapDict[L"type"] = SysAllocString(cd.Type.c_str());
                            mapDict[L"dlist"] = SysAllocString((cd.pDList).c_str());
                        }
                    }

                    if (cd.UserDefinedFields.size() > 0)
                    {
                        vector<ContactUDFields>::iterator it;
                        for (it = cd.UserDefinedFields.begin(); it != cd.UserDefinedFields.end(); it++)
                        {
                            BSTR bstrNam = SysAllocString(it->Name.c_str());
                            mapDict[bstrNam] = SysAllocString(it->value.c_str());
                        }
                    }

                    bool bHasTags = false;
                    if (cd.vTags)
                    {
                        wstring tagData;
                        int numTags = (int)cd.vTags->size();
                        if (numTags > 0)
                        {
                            for (int i = 0; i < numTags; i++)
                            {
                                tagData += (*cd.vTags)[i];
                                if (i < (numTags - 1))
                                    tagData += L",";
                            }
                            mapDict[L"tags"] = SysAllocString(tagData.c_str());
                            delete cd.vTags;
                            bHasTags = true;
                        }
                    }
                    if (!bHasTags)
                        mapDict[L"tags"] = SysAllocString(L"");
                }
                

            }
            else 
            if ((ft == 1) || (ft == 5))    
            {
                // =======================================================================
                // message or meeting request
                // =======================================================================
                LOG_TRACE(_T("Message/MeetingRqst"));

                // Get the raw MAPI data to msgdata
                MessageItemData msgdata;
                ret = maapi->GetItem(ItemID, msgdata);
                if (ret != NULL)
                {
                    delete ItemID.lpb;
                    hr = S_FALSE;
                    return hr;
                }

                // msgdata -> dictionary
                if (ret == NULL)	// 71630
                {
                    // Subject
                    mapDict[L"Subject"] = SysAllocString((msgdata.Subject).c_str());
                    LOG_INFO(_T("Subject: '%s'"), msgdata.Subject.c_str());

                    // Date
                    mapDict[L"Date"] = SysAllocString((msgdata.DateString).c_str());

                    // dwMimeSize
                    WCHAR pwszMimeSize[10];
                    _ltow(msgdata.dwMimeSize, pwszMimeSize, 10);
                    mapDict[L"MimeSize"] = SysAllocString(pwszMimeSize);

                    // MimeFile
                    mapDict[L"MimeFile"] = SysAllocString((msgdata.MimeFile).c_str());

                    // UrlName
                    mapDict[L"UrlName"] = SysAllocString((msgdata.Urlname).c_str());
                    LOG_TRACE(_T("urlname: '%s'"), msgdata.Urlname.c_str());

                    // rcvdDate
                    mapDict[L"rcvdDate"] = SysAllocString((msgdata.DeliveryUnixString.c_str()));

                    
                    
                    // wstrmimeBuffer
                    if (msgdata.pwsMimeBuffer)
                    {
                        BSTR bstrMimeBuffer = SysAllocString(msgdata.pwsMimeBuffer); // Allocates a new string + copies the passed-in string into it
                        Zimbra::MAPI::Util::PauseAfterLargeMemDelta();

                        mapDict[L"wstrmimeBuffer"] = bstrMimeBuffer; // Doesnt alloc further mem
                        Zimbra::MAPI::Util::PauseAfterLargeMemDelta();

                        delete[] msgdata.pwsMimeBuffer;
                        msgdata.pwsMimeBuffer = NULL;
                        Zimbra::MAPI::Util::PauseAfterLargeMemDelta();
                    }



                    // DateUnixString
                    mapDict[L"DateUnixString"] = SysAllocString(msgdata.DateUnixString.c_str());

                    // Tags
                    bool bHasTags = false;
                    if (msgdata.vTags)
                    {
                        wstring tagData;
                        int numTags = (int)msgdata.vTags->size();
                        if (numTags > 0)
                        {
                            for (int i = 0; i < numTags; i++)
                            {
                                tagData += (*msgdata.vTags)[i];
                                if (i < (numTags - 1))
                                    tagData += L",";
                            }
                            mapDict[L"tags"] = SysAllocString(tagData.c_str());
                            LOG_INFO(_T("tagData: '%s'"), tagData.c_str());

                            delete msgdata.vTags;
                            bHasTags = true;
                        }
                    }
                    if (!bHasTags)
                        mapDict[L"tags"] = SysAllocString(L"");

                    // flags
                    CComBSTR flags = L"";
                    if (msgdata.HasAttachments)
                        flags.Append(L"a");
                    if (msgdata.IsUnread)
                        flags.Append(L"u");
                    if (msgdata.IsFlagged)
                        flags.Append(L"f");
                    //if(msgdata.HasText)
                    //     flags.AppendBSTR(L"T");
                    //if(msgdata.HasHtml)
                    //     flags.AppendBSTR(L"H");
                    if (msgdata.IsDraft)
                        flags.Append(L"d");
                    if (msgdata.IsForwarded)
                        flags.Append(L"w");
                    if ((msgdata.IsUnsent) || (msgdata.Urlname.substr(0, 11) == L"/Sent Items"))
                        flags.Append(L"s");
                    if (msgdata.RepliedTo)
                        flags.Append(L"r");

                    /*
                    mapDict[L"Has Attachments"] = (msgdata.HasAttachments)? L"True":L"False";
                    mapDict[L"HasHTML"]         = (msgdata.HasHtml)?        L"True":L"False";
                    mapDict[L"HasText"]         = (msgdata.HasText)?        L"True":L"False";
                    mapDict[L"IsDraft"]         = (msgdata.IsDraft)?        L"True":L"False";
                    mapDict[L"IsFlagged"]       = (msgdata.IsFlagged)?      L"True":L"False";
                    mapDict[L"IsForwarded"]     = (msgdata.IsForwarded)?    L"True":L"False";
                    mapDict[L"IsFromMe"]        = (msgdata.IsFromMe)?       L"True":L"False";
                    mapDict[L"IsUnread"]        = (msgdata.IsUnread)?       L"True":L"False";
                    mapDict[L"IsUnsent"]        = (msgdata.IsUnsent)?       L"True":L"False";
                    mapDict[L"IsUnread"]        = (msgdata.IsUnread)?       L"True":L"False";
                    mapDict[L"RepliedTo"]       = (msgdata.IsUnread)?       L"True":L"False";
                    */

                    mapDict[L"flags"] = SysAllocString(flags);

                    /*printf("Subject: %S Date: %I64X DateString:%S		\
                     *      DeliveryDate: %I64X deliveryDateString: %S		\
                     *      Has Attachments: %d Has HTML:%d Has Text:%d	\
                     *      Is Draft:%d Is Flagged: %d Is Forwarded: %d	\
                     *      IsFromMe:%d IsUnread:%d IsUnsent:%d IsRepliedTo:%d	\
                     *      URLName: %S\n",
                     *      msgdata.Subject.c_str(), msgdata.Date, msgdata.DateString.c_str(),
                     *      msgdata.deliveryDate, msgdata.DeliveryDateString.c_str(),msgdata.HasAttachments,
                     *      msgdata.HasHtml, msgdata.HasText,msgdata.IsDraft,msgdata.IsFlagged,msgdata.IsForwarded,
                     *      msgdata.IsFromMe, msgdata.IsUnread, msgdata.IsUnsent,msgdata.RepliedTo,msgdata.Urlname.c_str()
                     *      );
                     *
                     * printf("MIME FILE PATH: %S\n\n\n\n", msgdata.MimeFile.c_str());*/
                }
            }
            else 
            if (ft == 3)
            {
                // =======================================================================
                // Appt
                // =======================================================================
                LOG_TRACE(_T("Appt"));

                // Get the raw MAPI data to apptData
                ApptItemData apptData;
                ret = maapi->GetItem(ItemID, apptData);
                if(ret != NULL)
                {
                    delete ItemID.lpb;
                    hr= S_FALSE;
                    return hr;
                }

                // apptData -> dictionary

                // ------------------------------------------------------------------
                // Series attributes
                // ------------------------------------------------------------------
                if (ret == NULL)	// 71630
                {
                    if (apptData.Uid.length() == 0)     // FBS bug 72893 -- 4/12/12
                        apptData.Uid = Zimbra::MAPI::Util::GetGUID();

                    mapDict[L"name"]            = SysAllocString(apptData.Name                          .c_str());
                    mapDict[L"s"]               = SysAllocString(apptData.StartDate                     .c_str());
                    mapDict[L"e"]               = SysAllocString(apptData.EndDate                       .c_str());
                    mapDict[L"sFilterDate"]     = SysAllocString(apptData.CalFilterDate                 .c_str());   // FBS bug 73982 -- 5/14/12
                    mapDict[L"rsvp"]            = SysAllocString(apptData.RSVP                          .c_str());
                    mapDict[L"ptst"]            = SysAllocString(apptData.PartStat                      .c_str());
                    mapDict[L"currst"]          = SysAllocString(apptData.CurrStat                      .c_str());
                    mapDict[L"fb"]              = SysAllocString(apptData.FreeBusy                      .c_str());
                    mapDict[L"allDay"]          = SysAllocString(apptData.AllDay                        .c_str());
                    mapDict[L"transp"]          = SysAllocString(apptData.Transparency                  .c_str());
                    mapDict[L"su"]              = SysAllocString(apptData.Subject                       .c_str());
                    mapDict[L"loc"]             = SysAllocString(apptData.Location                      .c_str());
                    mapDict[L"uid"]             = SysAllocString(apptData.Uid                           .c_str());
                    mapDict[L"m"]               = SysAllocString(apptData.AlarmTrigger                  .c_str());
                    mapDict[L"class"]           = SysAllocString(apptData.ApptClass                     .c_str());
                    mapDict[L"orAddr"]          = SysAllocString(apptData.organizer.addr                .c_str());
                    mapDict[L"orName"]          = SysAllocString(apptData.organizer.nam                 .c_str());
                    mapDict[L"contentType0"]    = SysAllocString(apptData.vMessageParts[0].contentType  .c_str());
                    mapDict[L"content0"]        = SysAllocString(apptData.vMessageParts[0].content      .c_str());
                    mapDict[L"contentType1"]    = SysAllocString(apptData.vMessageParts[1].contentType  .c_str());
                    mapDict[L"content1"]        = SysAllocString(apptData.vMessageParts[1].content      .c_str());

                    // attendees
                    wstring attendeeData = L"";
                    int numAttendees = (int)apptData.vAttendees.size();
                    if (numAttendees > 0)
                    {
                        for (int i = 0; i < numAttendees; i++)
                        {
                            Attendee* pAttendee = apptData.vAttendees[i];
                            attendeeData += pAttendee->nam;
                            attendeeData += L"~";
                            attendeeData += pAttendee->addr;
                            attendeeData += L"~";
                            attendeeData += pAttendee->role;
                            attendeeData += L"~";
                            attendeeData += pAttendee->partstat;
                            if (i < (numAttendees - 1))     // don't write comma after last attendee
                                attendeeData += L"~";
                        }
                        mapDict[L"attendees"] = SysAllocString(attendeeData.c_str());
                        for (int i = (numAttendees - 1); i >= 0; i--)
                            delete (apptData.vAttendees[i]);
                    }

                    // attachments
                    int numAttachments = (int)apptData.vAttachments.size();
                    if (numAttachments > 0)
                    {
                        BSTR attrs[NUM_ATTACHMENT_ATTRS];

                        WCHAR pwszNumAttachments[10];
                        _ltow(numAttachments, pwszNumAttachments, 10);
                        mapDict[L"numAttachments"] = SysAllocString(pwszNumAttachments);

                        for (int i = 0; i < numAttachments; i++)
                        {
                            AttachmentInfo* pAtt = apptData.vAttachments[i];
                            CreateAttachmentAttrs(attrs, i, -1);

                            LPTSTR pwszTmp = NULL;

                            LPSTR pszContentType = pAtt->pszContentType;
                            AtoW((LPSTR)pszContentType, pwszTmp);
                            mapDict[attrs[0]] =  SysAllocString(pwszTmp);
                            delete[] pwszTmp;

                            LPSTR pszTempFile = pAtt->pszTempFile;
                            AtoW((LPSTR)pszTempFile, pwszTmp);
                            mapDict[attrs[1]] =  SysAllocString(pwszTmp);
                            delete[] pwszTmp;

                            LPSTR pszRealName = pAtt->pszRealName;
                            AtoW((LPSTR)pszRealName, pwszTmp);
                            mapDict[attrs[2]] =  SysAllocString(pwszTmp);
                            delete[] pwszTmp;

                            LPSTR pszContentDisposition = pAtt->pszContentDisposition;
                            AtoW((LPSTR)pszContentDisposition, pwszTmp);
                            mapDict[attrs[3]] =  SysAllocString(pwszTmp);
                            delete[] pwszTmp;
                        }

                        // clean up any attachment
                        for (int i = (numAttachments - 1); i >= 0; i--)
                        {
                            AttachmentInfo* pAtt = apptData.vAttachments[i];
                            delete pAtt->pszContentType;
                            delete pAtt->pszTempFile;
                            delete pAtt->pszRealName;
                            delete pAtt->pszContentDisposition;
                            delete pAtt;
                        }                       
                    }

                    // Tags
                    bool bHasTags = false;
                    if (apptData.vTags)
                    {
                        wstring tagData;
                        int numTags = (int)apptData.vTags->size();
                        if (numTags > 0)
                        {
                            for (int i = 0; i < numTags; i++)
                            {
                                tagData += (*apptData.vTags)[i];
                                if (i < (numTags - 1))
                                    tagData += L",";
                            }
                            mapDict[L"tags"] = SysAllocString(tagData.c_str());
                            delete apptData.vTags;
                            bHasTags = true;
                        }
                    }
                    if (!bHasTags)
                        mapDict[L"tags"] = SysAllocString(L"");

                    // -------------
                    // Timezone data
                    // -------------
                    // tz_start
                    if (apptData.tzStart.id != _T(""))
                    {
                        mapDict[L"tz_start_tid"]     = SysAllocString((apptData.tzStart.id).c_str());
                        mapDict[L"tz_start_stdoff"]  = SysAllocString((apptData.tzStart.standardOffset).c_str());
                        mapDict[L"tz_start_dayoff"]  = SysAllocString((apptData.tzStart.daylightOffset).c_str());
                        mapDict[L"tz_start_sweek"]   = SysAllocString((apptData.tzStart.standardStartWeek).c_str());
                        mapDict[L"tz_start_swkday"]  = SysAllocString((apptData.tzStart.standardStartWeekday).c_str());
                        mapDict[L"tz_start_smon"]    = SysAllocString((apptData.tzStart.standardStartMonth).c_str());
                        mapDict[L"tz_start_shour"]   = SysAllocString((apptData.tzStart.standardStartHour).c_str());
                        mapDict[L"tz_start_smin"]    = SysAllocString((apptData.tzStart.standardStartMinute).c_str());
                        mapDict[L"tz_start_ssec"]    = SysAllocString((apptData.tzStart.standardStartSecond).c_str());
                        mapDict[L"tz_start_dweek"]   = SysAllocString((apptData.tzStart.daylightStartWeek).c_str());
                        mapDict[L"tz_start_dwkday"]  = SysAllocString((apptData.tzStart.daylightStartWeekday).c_str());
                        mapDict[L"tz_start_dmon"]    = SysAllocString((apptData.tzStart.daylightStartMonth).c_str());
                        mapDict[L"tz_start_dhour"]   = SysAllocString((apptData.tzStart.daylightStartHour).c_str());
                        mapDict[L"tz_start_dmin"]    = SysAllocString((apptData.tzStart.daylightStartMinute).c_str());
                        mapDict[L"tz_start_dsec"]    = SysAllocString((apptData.tzStart.daylightStartSecond).c_str());
                    }

                    // tz_start_end
                    if (apptData.tzEnd.id != _T(""))
                    {
                        mapDict[L"tz_end_tid"]     = SysAllocString((apptData.tzEnd.id).c_str());
                        mapDict[L"tz_end_stdoff"]  = SysAllocString((apptData.tzEnd.standardOffset).c_str());
                        mapDict[L"tz_end_dayoff"]  = SysAllocString((apptData.tzEnd.daylightOffset).c_str());
                        mapDict[L"tz_end_sweek"]   = SysAllocString((apptData.tzEnd.standardStartWeek).c_str());
                        mapDict[L"tz_end_swkday"]  = SysAllocString((apptData.tzEnd.standardStartWeekday).c_str());
                        mapDict[L"tz_end_smon"]    = SysAllocString((apptData.tzEnd.standardStartMonth).c_str());
                        mapDict[L"tz_end_shour"]   = SysAllocString((apptData.tzEnd.standardStartHour).c_str());
                        mapDict[L"tz_end_smin"]    = SysAllocString((apptData.tzEnd.standardStartMinute).c_str());
                        mapDict[L"tz_end_ssec"]    = SysAllocString((apptData.tzEnd.standardStartSecond).c_str());
                        mapDict[L"tz_end_dweek"]   = SysAllocString((apptData.tzEnd.daylightStartWeek).c_str());
                        mapDict[L"tz_end_dwkday"]  = SysAllocString((apptData.tzEnd.daylightStartWeekday).c_str());
                        mapDict[L"tz_end_dmon"]    = SysAllocString((apptData.tzEnd.daylightStartMonth).c_str());
                        mapDict[L"tz_end_dhour"]   = SysAllocString((apptData.tzEnd.daylightStartHour).c_str());
                        mapDict[L"tz_end_dmin"]    = SysAllocString((apptData.tzEnd.daylightStartMinute).c_str());
                        mapDict[L"tz_end_dsec"]    = SysAllocString((apptData.tzEnd.daylightStartSecond).c_str());
                    }

                    // Legacy timezone data only available on recurrent, and wont be available if the above newstyle available
                    if (apptData.recurPattern.length() > 0)
                    {
                        if (apptData.tzLegacy.id != _T(""))
                        {
                            mapDict[L"tz_legacy_tid"]     = SysAllocString((apptData.tzLegacy.id).c_str());
                            mapDict[L"tz_legacy_stdoff"]  = SysAllocString((apptData.tzLegacy.standardOffset).c_str());
                            mapDict[L"tz_legacy_dayoff"]  = SysAllocString((apptData.tzLegacy.daylightOffset).c_str());
                            mapDict[L"tz_legacy_sweek"]   = SysAllocString((apptData.tzLegacy.standardStartWeek).c_str());
                            mapDict[L"tz_legacy_swkday"]  = SysAllocString((apptData.tzLegacy.standardStartWeekday).c_str());
                            mapDict[L"tz_legacy_smon"]    = SysAllocString((apptData.tzLegacy.standardStartMonth).c_str());
                            mapDict[L"tz_legacy_shour"]   = SysAllocString((apptData.tzLegacy.standardStartHour).c_str());
                            mapDict[L"tz_legacy_smin"]    = SysAllocString((apptData.tzLegacy.standardStartMinute).c_str());
                            mapDict[L"tz_legacy_ssec"]    = SysAllocString((apptData.tzLegacy.standardStartSecond).c_str());
                            mapDict[L"tz_legacy_dweek"]   = SysAllocString((apptData.tzLegacy.daylightStartWeek).c_str());
                            mapDict[L"tz_legacy_dwkday"]  = SysAllocString((apptData.tzLegacy.daylightStartWeekday).c_str());
                            mapDict[L"tz_legacy_dmon"]    = SysAllocString((apptData.tzLegacy.daylightStartMonth).c_str());
                            mapDict[L"tz_legacy_dhour"]   = SysAllocString((apptData.tzLegacy.daylightStartHour).c_str());
                            mapDict[L"tz_legacy_dmin"]    = SysAllocString((apptData.tzLegacy.daylightStartMinute).c_str());
                            mapDict[L"tz_legacy_dsec"]    = SysAllocString((apptData.tzLegacy.daylightStartSecond).c_str());
                        }
                    }

                    // ----------
                    // recurrence
                    // ----------
                    if (apptData.recurPattern.length() > 0)
                    {
                        mapDict[L"freq"]  = SysAllocString((apptData.recurPattern).c_str());
                        mapDict[L"ival"]  = SysAllocString((apptData.recurInterval).c_str());
                        mapDict[L"count"] = SysAllocString((apptData.recurCount).c_str());      // can set this either way

                        if (apptData.recurEndDate.length() > 0)
                            mapDict[L"until"] = SysAllocString((apptData.recurEndDate).c_str());

                        if (apptData.recurWkday.length() > 0)
                            mapDict[L"wkday"] = SysAllocString((apptData.recurWkday).c_str());

                        if (apptData.recurDayOfMonth.length() > 0)
                            mapDict[L"modaylist"] = SysAllocString((apptData.recurDayOfMonth).c_str());

                        if (apptData.recurMonthOfYear.length() > 0)
                            mapDict[L"molist"] = SysAllocString((apptData.recurMonthOfYear).c_str());

                        if (apptData.recurMonthOccurrence.length() > 0)
                            mapDict[L"poslist"] = SysAllocString((apptData.recurMonthOccurrence).c_str());

                        // --------------------------------------------------------
                        // Exception attributes
                        // --------------------------------------------------------
                        int numExceptions = (int)apptData.vExceptions.size();   
                        if (numExceptions > 0)
                        {
                            WCHAR pwszNumExceptions[10];
                            _ltow(numExceptions, pwszNumExceptions, 10);
                            mapDict[L"numExceptions"] = SysAllocString(pwszNumExceptions);

                            for (int i = 0; i < numExceptions; i++)
                            {
                                MAPIAppointment* pExceptAppt = apptData.vExceptions[i];

                                BSTR attrs[NUM_EXCEPTION_ATTRS];
                                CreateExceptionAttrs(attrs, i);

                                // Main
                                mapDict[attrs[0]] =  SysAllocString(pExceptAppt->GetExceptionType()             .c_str());
                                mapDict[attrs[1]]  = SysAllocString(pExceptAppt->GetResponseStatus()            .c_str());
                                mapDict[attrs[2]]  = SysAllocString(pExceptAppt->GetBusyStatus()                .c_str());
                                mapDict[attrs[3]]  = SysAllocString(pExceptAppt->GetAllday()                    .c_str());
                                mapDict[attrs[4]]  = SysAllocString(pExceptAppt->GetSubject()                   .c_str());
                                mapDict[attrs[5]]  = SysAllocString(pExceptAppt->GetSubject()                   .c_str());
                                mapDict[attrs[6]]  = SysAllocString(pExceptAppt->GetLocation()                  .c_str());
                                mapDict[attrs[7]]  = SysAllocString(pExceptAppt->GetReminderMinutes()           .c_str());
                                mapDict[attrs[8]]  = SysAllocString(pExceptAppt->GetStartDate()                 .c_str());
                                mapDict[attrs[9]]  = SysAllocString(pExceptAppt->GetStartDateForRecID()         .c_str());
                                mapDict[attrs[10]] = SysAllocString(pExceptAppt->GetEndDate()                   .c_str());
                                mapDict[attrs[11]] = SysAllocString(pExceptAppt->GetOrganizerAddr()             .c_str());
                                mapDict[attrs[12]] = SysAllocString(pExceptAppt->GetOrganizerName()             .c_str());
                                mapDict[attrs[13]] = SysAllocString(L"text/plain");
                                mapDict[attrs[14]] = SysAllocString(pExceptAppt->GetPlainTextFileAndContent()   .c_str());
                                mapDict[attrs[15]] = SysAllocString(L"text/html");
                                mapDict[attrs[16]] = SysAllocString(pExceptAppt->GetHtmlFileAndContent()        .c_str());

                                // Attendees
                                attendeeData = L"";
                                int numAttendeesInException = (int)pExceptAppt->GetAttendees().size();
                                if (numAttendeesInException > 0)
                                {
                                    for (int j = 0; j < numAttendeesInException; j++)
                                    {         
                                        Attendee* pAttendee = pExceptAppt->GetAttendees()[j];
                                        attendeeData += pAttendee->nam;
                                        attendeeData += L"~";
                                        attendeeData += pAttendee->addr;
                                        attendeeData += L"~";
                                        attendeeData += pAttendee->role;
                                        attendeeData += L"~";
                                        attendeeData += pAttendee->partstat;
                                        if (j < (numAttendeesInException - 1))     // don't write comma after last attendee
                                            attendeeData += L"~";
                                    }
                                    for (int j = (numAttendeesInException - 1); j >= 0; j--)
                                        delete pExceptAppt->GetAttendees()[j];

                                }
                                mapDict[attrs[17]] = SysAllocString(attendeeData.c_str());

                                // Attachments
                                int numAttachmentsInException = (int)pExceptAppt->GetAttachmentInfo().size();
                                WCHAR pwszNumAttachments[10];
                                _ltow(numAttachmentsInException, pwszNumAttachments, 10);
                                mapDict[attrs[18]] = SysAllocString(pwszNumAttachments);
                                if (numAttachmentsInException > 0)
                                {
                                    BSTR attrs[NUM_ATTACHMENT_ATTRS];
                                    for (int j = 0; j < (int)numAttachmentsInException; j++)
                                    {
                                        AttachmentInfo* pAtt = pExceptAppt->GetAttachmentInfo()[j];

                                        CreateAttachmentAttrs(attrs, j, i);

                                        LPTSTR pwszTmp = NULL;

                                        LPSTR pszContentType = pAtt->pszContentType;
                                        AtoW((LPSTR)pszContentType, pwszTmp);
                                        mapDict[attrs[0]] =  SysAllocString(pwszTmp);
                                        delete[] pwszTmp;

                                        LPSTR pszTempFile = pAtt->pszTempFile;
                                        AtoW((LPSTR)pszTempFile, pwszTmp);
                                        mapDict[attrs[1]] =  SysAllocString(pwszTmp);
                                        delete[] pwszTmp;

                                        LPSTR pszRealName = pAtt->pszRealName;
                                        AtoW((LPSTR)pszRealName, pwszTmp);
                                        mapDict[attrs[2]] =  SysAllocString(pwszTmp);
                                        delete[] pwszTmp;

                                        LPSTR pszContentDisposition = pAtt->pszContentDisposition;
                                        AtoW((LPSTR)pszContentDisposition, pwszTmp);
                                        mapDict[attrs[3]] =  SysAllocString(pwszTmp);
                                        delete[] pwszTmp;
                                    }

                                    for (int j = (numAttachmentsInException - 1); j >= 0; j--)
                                    {
                                        AttachmentInfo* pAtt = pExceptAppt->GetAttachmentInfo()[j];
                                        delete pAtt->pszContentType;
                                        delete pAtt->pszTempFile;
                                        delete pAtt->pszRealName;
                                        delete pAtt->pszContentDisposition;
                                        delete pAtt;
                                    }

                                } // if (numAttachmentsInException > 0)

                            } // for (int i = 0; i < numExceptions; i++)

                            // clean up any exceptions
                            for (int i = (numExceptions - 1); i >= 0; i--)
                                delete (apptData.vExceptions[i]);

                        } // if (numExceptions > 0)

                    } // if (apptData.recurPattern.length() > 0)


                } // if (ret == NULL)
            }
            else 
            if (ft == 4)
            {
                // =======================================================================
                // Task
                // =======================================================================
                LOG_TRACE(_T("Task"));

                TaskItemData taskData;
                ret = maapi->GetItem(ItemID, taskData);
                if(ret != NULL)
                {
                    delete ItemID.lpb;
                    hr= S_FALSE;
                    return hr;
                }
                if (ret == NULL)	// 71630
                {
                    mapDict[L"name"]            = SysAllocString((taskData.Subject).c_str());
                    mapDict[L"su"]              = SysAllocString((taskData.Subject).c_str());
                    mapDict[L"priority"]        = SysAllocString((taskData.Importance).c_str());
                    mapDict[L"s"]               = SysAllocString((taskData.TaskStart).c_str());
                    mapDict[L"sFilterDate"]     = SysAllocString((taskData.TaskFilterDate).c_str());   // FBS bug 73982 -- 5/14/12
                    mapDict[L"e"]               = SysAllocString((taskData.TaskDue).c_str());
                    mapDict[L"status"]          = SysAllocString((taskData.Status).c_str());
                    mapDict[L"percentComplete"] = SysAllocString((taskData.PercentComplete).c_str());
                    mapDict[L"xp-TOTAL_WORK"]   = SysAllocString((taskData.TotalWork).c_str());
                    mapDict[L"xp-ACTUAL_WORK"]  = SysAllocString((taskData.ActualWork).c_str());
                    mapDict[L"xp-COMPANIES"]    = SysAllocString((taskData.Companies).c_str());
                    mapDict[L"xp-MILEAGE"]      = SysAllocString((taskData.Mileage).c_str());
                    mapDict[L"xp-BILLING"]      = SysAllocString((taskData.BillingInfo).c_str());

                    if (taskData.TaskFlagDueBy.length() > 0)
                        mapDict[L"taskflagdueby"] = SysAllocString((taskData.TaskFlagDueBy).c_str());

                    mapDict[L"class"]           = SysAllocString((taskData.ApptClass).c_str());
                    mapDict[L"contentType0"]    = SysAllocString((taskData.vMessageParts[0].contentType).c_str());
                    mapDict[L"content0"]        = SysAllocString((taskData.vMessageParts[0].content).c_str());
                    mapDict[L"contentType1"]    = SysAllocString((taskData.vMessageParts[1].contentType).c_str());
                    mapDict[L"content1"]        = SysAllocString((taskData.vMessageParts[1].content).c_str());

                    // attachments
                    int numAttachments = (int)taskData.vAttachments.size();
                    if (numAttachments > 0)
                    {
                        WCHAR pwszNumAttachments[10];
                        BSTR attrs[NUM_ATTACHMENT_ATTRS];

                        _ltow(numAttachments, pwszNumAttachments, 10);
                        mapDict[L"numAttachments"] = SysAllocString(pwszNumAttachments);
                        for (int i = 0; i < numAttachments; i++)
                        {
                            CreateAttachmentAttrs(attrs, i, -1);

                            LPSTR pszContentType = taskData.vAttachments[i]->pszContentType;
                            LPSTR pszTempFile = taskData.vAttachments[i]->pszTempFile;
                            LPSTR pszRealName = taskData.vAttachments[i]->pszRealName;
                            LPSTR pszContentDisposition = taskData.vAttachments[i]->pszContentDisposition;

                            LPTSTR pwDes = NULL;
                            AtoW((LPSTR)pszContentType,pwDes);
                            mapDict[attrs[0]] =  SysAllocString(pwDes);
                            delete[] pwDes;

                            AtoW((LPSTR)pszTempFile,pwDes);
                            mapDict[attrs[1]] =  SysAllocString(pwDes);
                            delete[] pwDes;

                            AtoW((LPSTR)pszRealName,pwDes);
                            mapDict[attrs[2]] =  SysAllocString(pwDes);
                            delete[] pwDes;

                            AtoW((LPSTR)pszContentDisposition,pwDes);
                            mapDict[attrs[3]] =  SysAllocString(pwDes);
                            delete[] pwDes;
                        }

                        // clean up any attachment
                        for (int i = (numAttachments - 1); i >= 0; i--)
                        {
                            delete taskData.vAttachments[i]->pszContentType;
                            delete taskData.vAttachments[i]->pszTempFile;
                            delete taskData.vAttachments[i]->pszRealName;
                            delete taskData.vAttachments[i]->pszContentDisposition;
                            delete taskData.vAttachments[i];
                        }                       
                    }

                    // Tags
                    bool bHasTags = false;
                    if (taskData.vTags)
                    {
                        wstring tagData;
                        int numTags = (int)taskData.vTags->size();
                        if (numTags > 0)
                        {
                            for (int i = 0; i < numTags; i++)
                            {
                                tagData += (*taskData.vTags)[i];
                                if (i < (numTags - 1))
                                    tagData += L",";
                            }
                            mapDict[L"tags"] = SysAllocString(tagData.c_str());
                            delete taskData.vTags;
                            bHasTags = true;
                        }
                    }
                    if (!bHasTags)
                        mapDict[L"tags"] = SysAllocString(L"");

                    // recurrence
                    if (taskData.recurPattern.length() > 0)
                    {
                        mapDict[L"freq"] = SysAllocString((taskData.recurPattern).c_str());
                        mapDict[L"ival"] = SysAllocString((taskData.recurInterval).c_str());
                        mapDict[L"count"] = SysAllocString((taskData.recurCount).c_str());      // can set this either way

                        if (taskData.recurEndDate.length() > 0)
                            mapDict[L"until"] = SysAllocString((taskData.recurEndDate).c_str());

                        if (taskData.recurWkday.length() > 0)
                            mapDict[L"wkday"] = SysAllocString((taskData.recurWkday).c_str());

                        if (taskData.recurDayOfMonth.length() > 0)
                            mapDict[L"modaylist"] = SysAllocString((taskData.recurDayOfMonth).c_str());

                        if (taskData.recurMonthOfYear.length() > 0)
                            mapDict[L"molist"] = SysAllocString((taskData.recurMonthOfYear).c_str());

                        if (taskData.recurMonthOccurrence.length() > 0)
                            mapDict[L"poslist"] = SysAllocString((taskData.recurMonthOccurrence).c_str());

                        /*
                        // timezone
                        mapDict[L"tz_legacy_tid"]      = SysAllocString((apptData.tz.id).c_str());
                        mapDict[L"tz_legacy_stdoff"]   = SysAllocString((apptData.tz.standardOffset).c_str());
                        mapDict[L"tz_legacy_dayoff"]   = SysAllocString((apptData.tz.daylightOffset).c_str());
                        mapDict[L"tz_legacy_sweek"]    = SysAllocString((apptData.tz.standardStartWeek).c_str());
                        mapDict[L"tz_legacy_swkday"]   = SysAllocString((apptData.tz.standardStartWeekday).c_str());
                        mapDict[L"tz_legacy_smon"]     = SysAllocString((apptData.tz.standardStartMonth).c_str());
                        mapDict[L"tz_legacy_shour"]    = SysAllocString((apptData.tz.standardStartHour).c_str());
                        mapDict[L"tz_legacy_smin"]     = SysAllocString((apptData.tz.standardStartMinute).c_str());
                        mapDict[L"tz_legacy_ssec"]     = SysAllocString((apptData.tz.standardStartSecond).c_str());
                        mapDict[L"tz_legacy_dweek"]    = SysAllocString((apptData.tz.daylightStartWeek).c_str());
                        mapDict[L"tz_legacy_dwkday"]   = SysAllocString((apptData.tz.daylightStartWeekday).c_str());
                        mapDict[L"tz_legacy_dmon"]     = SysAllocString((apptData.tz.daylightStartMonth).c_str());
                        mapDict[L"tz_legacy_dhour"]    = SysAllocString((apptData.tz.daylightStartHour).c_str());
                        mapDict[L"tz_legacy_dmin"]     = SysAllocString((apptData.tz.daylightStartMinute).c_str());
                        mapDict[L"tz_legacy_dsec"]     = SysAllocString((apptData.tz.daylightStartSecond).c_str());
                        //
                        */
                    }
                }
            }
        }
        delete ItemID.lpb;
    }

    // ----------------------------------------------------
    // All data now in mapDict -> transfer to pRet VARIANT
    // ----------------------------------------------------
    hr = CppDictToVarArray(mapDict, pRet);

    return hr;
}

STDMETHODIMP CMapiAccessWrap::GetOOOInfo(BSTR *OOOInfo)
{
    LOGFN_TRACE_NO;

    LPCWSTR lpInfo = maapi->GetOOOStateAndMsg();
    *OOOInfo = CComBSTR(lpInfo);
    delete[] lpInfo;

    return S_OK;
}

STDMETHODIMP CMapiAccessWrap::GetRuleList(VARIANT *rules)
{
    // This is the big method for Exchange rules processing.  It reads the PR_RULES_TABLE,
    // gets the info back, and creates a map (pMap) that will be read by the Zimbra API
    // layer (SetModifyFilterRulesRequest, which calls AddFilterRuleToRequest for each rule.)
    // Assuming there are two rules, Foo and Bar, here is an example of pMap.  We'll only show
    // the format of the first rule.  There are 3 entries for each rule, rule, tests and actions.
    // So the first rule will have 0filterRule, 0filterTests, 0filterActions, the second will have
    // 1filterRule, 1filterTests, 1filterActions, etc.  There is a numRules name/value pair that
    // gives the number of rules.  In a given entry, there are certain token delimiters.  The rule
    // entry uses ",", the test entry uses an initial ":" followed by "`~".  The action entries
    // just delimit tokens by "`~".  The string "^^^" is the delimiter for multiple filterTests
    // and multiple filterActions.
    // Here is an example of a map for the rule:
    // "with foo in the subject, forward it to user2 and move it to the Test folder"

    // name            value
    // ----            -----
    // numRules        1
    // 0filterRule     name,TestRule,active,1
    // 0filterTests"   allof:headerTest`~index`~0`~stringComparison`~contains`~header`~Subject`~value`~foo
    // 0filterActions  actionRedirect`~a`~user2^^^actionFileInto`~folderPath`~Test
    
    LOGFN_TRACE_NO;
    HRESULT hr = S_OK;
    std::map<BSTR, BSTR> pMap;
    LPWSTR pwszLine = new WCHAR[20000];
    /*VariantInit(rules);
    rules->vt = VT_ARRAY | VT_DISPATCH;*/

    USES_CONVERSION;
    vector<CRule> vRuleList;

    LPCWSTR status = maapi->GetExchangeRules(vRuleList);
    if(status != NULL)
    {
        LOG_ERROR(_T("GetRuleList failed '%s'"), status);
        return S_FALSE;
    }

    std::vector<CRule>::iterator ruleIndex;
    size_t numRules = vRuleList.size();
    if (numRules == 0)
    {
        LOG_INFO(_T("No rules to migrate"));
        return S_OK;
    }

    WCHAR pwszTemp[5];
    _itow((int)numRules, pwszTemp, 10);
    pMap[L"numRules"] = SysAllocString(pwszTemp);

    CRuleMap* pRuleMap = new CRuleMap();

    // Create the array of attrs (0filterRule, 0filterTests, 0filterActions, 1filterRule, etc.)
    // std:map needs all names allocated separately
    int numAttrs = ((int)numRules) * 3;
    WCHAR tmp[10];
    LPWSTR* ruleMapNames = new LPWSTR[numAttrs];
    for (int i = 0; i < numAttrs; i += 3)
    {
        _ltow(i/3, tmp, 10);
        ruleMapNames[i] = new WCHAR[20];
        lstrcpy(ruleMapNames[i], tmp);
        lstrcat(ruleMapNames[i], L"filterRule");
        ruleMapNames[i + 1] = new WCHAR[20];
        lstrcpy(ruleMapNames[i + 1], tmp);
        lstrcat(ruleMapNames[i + 1], L"filterTests");
        ruleMapNames[i + 2] = new WCHAR[20];
        lstrcpy(ruleMapNames[i + 2], tmp);
        lstrcat(ruleMapNames[i + 2], L"filterActions");
    }
    /////////////////

    int iIndex = 0;
    int iMapIndex = 0;
    for (ruleIndex = vRuleList.begin(); ruleIndex != vRuleList.end(); ruleIndex++)
    {
        std::wstring wstrRuleCondition;
        CRule &rule = *ruleIndex;

        iMapIndex = iIndex * 3;
        pRuleMap->WriteFilterRule(rule, pwszLine);
        pMap[ruleMapNames[iMapIndex]] = SysAllocString(pwszLine);
        pRuleMap->WriteFilterTests(rule, pwszLine);
        pMap[ruleMapNames[iMapIndex + 1]] = SysAllocString(pwszLine);
        pRuleMap->WriteFilterActions(rule, pwszLine);
        pMap[ruleMapNames[iMapIndex + 2]] = SysAllocString(pwszLine);
        iIndex++;
    }

    delete pRuleMap;
    delete pwszLine;

    std::map<BSTR, BSTR>::iterator it;
    VariantInit(rules);

    // Create SafeArray of VARIANT BSTRs
    SAFEARRAY *pSA = NULL;
    SAFEARRAYBOUND aDim[2];                     // two dimensional array
    aDim[0].lLbound = 0;
    aDim[0].cElements = (ULONG)pMap.size();
    aDim[1].lLbound = 0;
    aDim[1].cElements = (ULONG)pMap.size();      // rectangular array
    pSA = SafeArrayCreate(VT_BSTR, 2, aDim);    // again, 2 dimensions

    long aLong[2];
    if (pSA != NULL)
    {
        BSTR temp;
        for (long x = aDim[0].lLbound; x < 2 /*(aDim[0].cElements + aDim[0].lLbound)*/; x++)
        {
            aLong[0] = x;                       // set x index
            it = pMap.begin();
            for (long y = aDim[1].lLbound; y < (long)(aDim[1].cElements + aDim[1].lLbound); y++)
            {
                aLong[1] = y;                   // set y index
                if (aLong[0] > 0)
                    temp = SysAllocString((*it).second);
                else
                    temp = SysAllocString((*it).first);
                hr = SafeArrayPutElement(pSA, aLong, temp);

                it++;
            }
        }
    }
    rules->vt = VT_ARRAY | VT_BSTR;
    rules->parray = pSA;

    // Now that the map is set, delete the rule map names.
    // Don't need to go backwards, but it's a good convention
    for (int i = (numAttrs - 1); i >= 0; i--)
        delete ruleMapNames[i];
    delete ruleMapNames;

    return hr;
}


STDMETHODIMP CMapiAccessWrap::InitializePublicFolders(BSTR * statusMsg)
{
    HRESULT hr = S_OK;
    //init public folders
    maapi->InitializePublicFolders();
    
    std::vector<std::string> pubFldrList;
    
    //enumerate public folder
    maapi->EnumeratePublicFolders(pubFldrList); 
    //print pblic folder
    vector<std::string>::iterator pfenumItr;
    printf("Enumerated Public folders:\n");
    for (pfenumItr = pubFldrList.begin(); pfenumItr != pubFldrList.end(); pfenumItr++)
    {
        printf("- %s \n", (*pfenumItr).c_str());
    }
    
     *statusMsg =  SysAllocString(L"");

    return hr;
}

void CMapiAccessWrap::CreateAttachmentAttrs(BSTR attrs[], int numAtt, int numExcep)
//
// Generates attachment attribute names of the form
//
//   ex<n>_att<n>_attrname e.g. e.g. ex0_att0_attContentType
//
// This method also gets called for non-exceptions in which case numExcep==-1 and "ex<n> is omitted
//
{
    LOGFN_TRACE_NO;

    LPWSTR names[] = {L"attContentType", L"attTempFile", L"attRealName", L"attContentDisposition"};
                     
    WCHAR pwszAttr[40] = {0};

    WCHAR pwszExNum[10];
    _ltow(numExcep, pwszExNum, 10);

    WCHAR pwszAttNum[10];
    _ltow(numAtt, pwszAttNum, 10);

    for (int i = 0; i < NUM_ATTACHMENT_ATTRS; i++)
    {
        if (numExcep>=0)
        {
            // Need to include "ex<n>" prefix
            lstrcpy(pwszAttr, L"ex");
            lstrcat(pwszAttr, pwszExNum);
            lstrcat(pwszAttr, L"_");
            lstrcat(pwszAttr, L"att");
        }
        else
        {
            // Exclude the prefix
            lstrcpy(pwszAttr, L"att");
        }

        lstrcat(pwszAttr, pwszAttNum);
        lstrcat(pwszAttr, L"_");
        lstrcat(pwszAttr, names[i]);
        attrs[i] = SysAllocString(pwszAttr);
    }
}

void CMapiAccessWrap::CreateExceptionAttrs(BSTR attrs[], int num)
{
    LOGFN_TRACE_NO;

    LPWSTR names[] = 
    {
        L"exceptionType", 
        L"ptst", 
        L"fb", 
        L"allDay",
        L"name", 
        L"su", 
        L"loc", 
        L"m", 
        L"s", 
        L"rid", 
        L"e",                      
        L"orAddr", 
        L"orName", 
        L"contentType0", 
        L"content0",
        L"contentType1", 
        L"content1", 
        L"attendees",
        L"numAttachments"
    };
                     
    WCHAR pwszNum[10];
    _ltow(num, pwszNum, 10);

    for (int i = 0; i < NUM_EXCEPTION_ATTRS; i++)
    {
        WCHAR pwszAttr[35];           // ex0_exceptionType etc
        lstrcpy(pwszAttr, L"ex");
        lstrcat(pwszAttr, pwszNum);
        lstrcat(pwszAttr, L"_");
        lstrcat(pwszAttr, names[i]);
        attrs[i] = SysAllocString(pwszAttr);
    }
}

HRESULT CMapiAccessWrap::CppDictToVarArray(std::map<BSTR, BSTR>& mapDict, VARIANT *pVar)
//
// Converts mapDict to a 2xN array of variants for passing back to c# layer
// Assumes each mapDict value is a SysAllocString. Frees this.
//
{
    LOGFN_VERBOSE_NO;
    HRESULT hr = S_OK;

    ULONG ulMapSize = (ULONG)mapDict.size();

    // Array neeeds to be 2 cols (key, value) BY ulMapSize rows

    // Create 2 dimensional rectangular SafeArray of VARIANT BSTRs
    const int nDIMS = 2;
    SAFEARRAYBOUND aDim[nDIMS];
    aDim[0].lLbound = 0;
    aDim[0].cElements = 2;
    aDim[1].lLbound = 0;
    aDim[1].cElements = ulMapSize;

    SAFEARRAY *pSA = SafeArrayCreate(VT_BSTR, nDIMS, aDim); // Effectively BSTR[2, ulMapSize]
    if (pSA)
    {
        // 2 passes x = 0, and x = 1
        // First pass does the key names, second does the values
        for (long x = 0; x < 2; x++)
        {
            long aDest[2];
            aDest[0] = x; 

            std::map<BSTR, BSTR>::iterator itDict = mapDict.begin();
            for (long y = 0; y < (long)ulMapSize; y++)
            {
                BSTR temp;
                if (x == 0)
                    temp = SysAllocString(itDict->first);
                else
                    temp = SysAllocString(itDict->second);

                // Copy the data across
                aDest[1] = y;
                hr = SafeArrayPutElement(pSA, aDest, temp);
                _ASSERT(hr == S_OK);

                SysFreeString(temp);
                itDict++;
            }
        }
    }

    VariantInit(pVar);
    pVar->vt = VT_ARRAY | VT_BSTR;
    pVar->parray = pSA;

    // Free all BSTRs
    std::map<BSTR,BSTR>::iterator itDict = mapDict.begin();
    while(itDict != mapDict.end())
    {
        SysFreeString((*itDict).second);
        itDict++;
    }
    mapDict.clear();

    return hr;
}
