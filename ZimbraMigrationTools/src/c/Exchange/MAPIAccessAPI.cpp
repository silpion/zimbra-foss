/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
#include "common.h"
#include "Exchange.h"
#include "MAPIAccessAPI.h"
#include "Logger.h"
#include "edk/edkmapi.h"
#include "Util.h"

Zimbra::MAPI::MAPISession *MAPIAccessAPI::m_zmmapisession = NULL;
Zimbra::MAPI::MAPIStore *MAPIAccessAPI::m_defaultStore = NULL;
std::wstring MAPIAccessAPI::m_strTargetProfileName = L"";
std::wstring MAPIAccessAPI::m_strExchangeHostName = L"";
bool MAPIAccessAPI::m_bSingleMailBoxMigration = false;
bool MAPIAccessAPI::m_bHasJoinedDomain = false;
MIG_TYPE MAPIAccessAPI::m_MigType = MAILBOX_MIG;

// Initialize with Exchange Sever hostname, Outlook Admin profile name, Exchange mailbox name to be migrated
MAPIAccessAPI::MAPIAccessAPI(wstring strUserName, wstring strUserAccount): 
    m_userStore(NULL), 
    m_rootFolder(NULL),
    m_publicStore(NULL)
{
    LOGFN_TRACE_NO;
    m_bSingleMailBoxMigration=false;
    if (strUserName.empty())
        m_bSingleMailBoxMigration = true;
    else
        m_strUserName = strUserName;

    m_strUserAccount = strUserAccount;

    Zimbra::Mapi::Memory::SetMemAllocRoutines(NULL, MAPIAllocateBuffer, MAPIAllocateMore, MAPIFreeBuffer);
    
    InitFoldersToSkip();
}

MAPIAccessAPI::~MAPIAccessAPI()
{
    LOGFN_TRACE_NO;
    if (m_rootFolder != NULL)
    {
        delete m_rootFolder;
        m_rootFolder = NULL;
    }

    if ((m_userStore) && (!m_bSingleMailBoxMigration))
        delete m_userStore;

    if(m_publicStore)
    {
        delete m_publicStore;
        m_publicStore=NULL;
        MAPIUninitialize();
    }
    m_userStore = NULL;    
    m_bSingleMailBoxMigration = false;
}

void MAPIAccessAPI::InitFoldersToSkip()
{
    m_aFoldersToSkip[TS_JOURNAL]                = JOURNAL;
    m_aFoldersToSkip[TS_OUTBOX]                 = OUTBOX;
    m_aFoldersToSkip[TS_SYNC_CONFLICTS]         = SYNC_CONFLICTS;
    m_aFoldersToSkip[TS_SYNC_ISSUES]            = SYNC_ISSUES;
    m_aFoldersToSkip[TS_SYNC_LOCAL_FAILURES]    = SYNC_LOCAL_FAILURES;
    m_aFoldersToSkip[TS_SYNC_SERVER_FAILURES]   = SYNC_SERVER_FAILURES;
    // m_aFoldersToSkip[TS_JUNK_MAIL]           = JUNK_MAIL;
}

bool MAPIAccessAPI::SkipFolder(ExchangeSpecialFolderId exfid)
{
    //LOGFN_TRACE_NO; // Too verbose
    for (int i = TS_JOURNAL; i < TS_FOLDERS_MAX; i++)
    {
        if (m_aFoldersToSkip[i] == exfid)
            return true;
    }
    return false;
}

LONG WINAPI MAPIAccessAPI::UnhandledExceptionFilter(LPEXCEPTION_POINTERS pExPtrs)
{
    LOGFN_TRACE_NO;
    LPWSTR strOutMessage = NULL;
    LONG lRetVal = Zimbra::Util::MiniDumpGenerator::GenerateCoreDump(pExPtrs,strOutMessage);
    if (strOutMessage)
        LOG_ERROR(strOutMessage);

    WCHAR pwszTempPath[MAX_PATH];
    GetTempPath(MAX_PATH, pwszTempPath);

    wstring strMsg;
    WCHAR strbuf[256];
    wsprintf(strbuf,L"The application has requested the Runtime to terminate it in an unusual way.\nThe core dump would get generated in %s.", pwszTempPath);
    //MessageBox(NULL, strbuf, _T("Runtime Error"), MB_OK);
    LOG_ERROR(strbuf);
    Zimbra::Util::FreeString(strOutMessage);
    return lRetVal;
}

void MAPIAccessAPI::SetOOMRegistry()
// If outlook is one of the mail clients installed on machine,
// make an entry in HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Messaging Subsystem\MSMapiApps
// corresponding to this application. This will cause the MAPI Stub library to route all
// MAPI calls made by this application to Microsoft Outlook, and not the default mail client.
{
    LOGFN_TRACE_NO;

    HKEY hKey = NULL;
    HRESULT lRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"Software\\Clients\\Mail\\Microsoft Outlook", 0, KEY_READ, &hKey);

    // If outlook is one of the mail clients,
    if (ERROR_SUCCESS == lRetCode)
    {
        HKEY hKeyMSMapiApps = NULL;
        LONG lRet = 0;

        // Open the registry key HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Messaging Subsystem\MSMapiApps
        lRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T( "SOFTWARE\\Microsoft\\Windows Messaging Subsystem\\MSMapiApps"), 0, KEY_QUERY_VALUE | KEY_SET_VALUE, &hKeyMSMapiApps);
        if (ERROR_SUCCESS == lRetCode)
        {
            TCHAR exepath[MAX_PATH+1];
            CString strAppName = L"ZimbraMigration.exe";
            if (0 != GetModuleFileName(0, exepath, MAX_PATH+1))
            {
                strAppName = exepath;
                int npos=strAppName.ReverseFind('\\');
                strAppName= strAppName.Right(strAppName.GetLength()-npos-1);
            }

            lRet = RegSetValueEx(hKeyMSMapiApps, strAppName, 0, REG_SZ, (LPBYTE)_T("Microsoft Outlook"), 18 * (sizeof (TCHAR)));
            RegCloseKey(hKeyMSMapiApps);
        }
        else
        {
            LOG_ERROR(_T("Registry-SOFTWARE\\Microsoft\\Windows Messaging Subsystem\\MSMapiApps) error code: %s"), lRet);
        }
        RegCloseKey(hKey);
    }
    else
    {
        LOG_ERROR(_T("Registry-Software\\Clients\\Mail\\Microsoft Outlook) error code: %d"), lRetCode);
    }

    /*************Work around for bug 17687****************/
    hKey = NULL;
    DWORD dwDisposition = 0;
    LONG lRet = RegCreateKeyEx(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Exchange\\Client\\Options"), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, &dwDisposition);
    if (hKey)
    {
        TCHAR szOrgOpt[2] = { 0 };
        DWORD dwSize = DWORD(sizeof (szOrgOpt));

        // Modify the key only if it hasnt been modified previously
        if (RegQueryValueEx(hKey, _T("OrgPickLogonProfile"), NULL, NULL, NULL, NULL))
        {
            if (!RegQueryValueEx(hKey, _T("PickLogonProfile"), NULL, NULL, (LPBYTE)szOrgOpt, &dwSize))
            {
                RegSetValueEx(hKey, _T("OrgPickLogonProfile"), 0, REG_SZ, (LPBYTE)szOrgOpt, DWORD(sizeof (szOrgOpt)));
            }

            lRet = RegSetValueEx(hKey, _T("PickLogonProfile"), 0, REG_SZ, (LPBYTE)_T("0"), DWORD(_tcslen(_T("0")) + 1) * sizeof (TCHAR));
        }
        RegCloseKey(hKey);
    }
    else
    {
        LOG_ERROR(_T("Registry-Software\\Microsoft\\Exchange\\Client\\Options error code: %d"), lRet);
    }

    /*****************************************************/
}

void MAPIAccessAPI::ResetOOMRegistry()
{
    LOGFN_TRACE_NO;

    /******************Bug 17687*********************/
    HKEY hKey = NULL;
    DWORD dwDisposition = 0;
    LONG lRet = RegCreateKeyEx(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Exchange\\Client\\Options"), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, &dwDisposition);
    if (hKey)
    {
        TCHAR szOrgOpt[2] = { 0 };
        DWORD dwSize = DWORD(sizeof (szOrgOpt));
        if (!RegQueryValueEx(hKey, _T("OrgPickLogonProfile"), NULL, NULL, (LPBYTE)szOrgOpt, &dwSize))
        {
            RegSetValueEx(hKey, _T("PickLogonProfile"), 0, REG_SZ, (LPBYTE)szOrgOpt, DWORD(sizeof (szOrgOpt)));
            RegDeleteValue(hKey, _T("OrgPickLogonProfile"));
        }
        RegCloseKey(hKey);
    }
    else
    {
        LOG_ERROR(_T("Registry key cannot be set while UnRegisterReminderHandler for OOM. Error: %d"), lRet);
    }
    /**************************************************/

    HKEY hKeyMSMapiApps = NULL;
    lRet = 0;

    // Open the registry key HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Messaging Subsystem\MSMapiApps
    lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows Messaging Subsystem\\MSMapiApps"), 0, KEY_READ | KEY_SET_VALUE, &hKeyMSMapiApps);
    // Delete the entry in the key corresponding to the application
    if (ERROR_SUCCESS == lRet)
    {
        TCHAR exepath[MAX_PATH+1];
        CString strAppName = L"ZimbraMigration.exe";
        if(0 != GetModuleFileName(0, exepath, MAX_PATH+1))
        {
            strAppName = exepath;
            int npos=strAppName.ReverseFind('\\');
            strAppName= strAppName.Right(strAppName.GetLength()-npos-1);
        }

        lRet = RegDeleteValue(hKeyMSMapiApps, strAppName);
        RegCloseKey(hKeyMSMapiApps);
    }
}

void MAPIAccessAPI::internalInit()
{
    LOGFN_TRACE_NO;

    wstring appdir= Zimbra::Util::GetAppDir();

    //instantiate dump generator
    LPWSTR pwszTempPath = new WCHAR[MAX_PATH];
    wcscpy(pwszTempPath,appdir.c_str());
    Zimbra::Util::AppendString(pwszTempPath,L"dbghelp.dll");

    Zimbra::Util::MiniDumpGenerator::InitMiniDumpGenerator(pwszTempPath);

    SetUnhandledExceptionFilter(UnhandledExceptionFilter);
    delete []pwszTempPath;
}

LPCWSTR MAPIAccessAPI::InitGlobalSessionAndStore(LPCWSTR lpcwstrMigTarget, LPCWSTR userAccount, ULONG flag)
{
    internalInit();

    LPWSTR exceptionmsg = NULL;

    if(wcscmp(userAccount,L"")==0)
        m_bSingleMailBoxMigration = true;

    __try
    {
        return _InitGlobalSessionAndStore(lpcwstrMigTarget,flag);
    }
    __except(Zimbra::Util::MiniDumpGenerator::GenerateCoreDump(GetExceptionInformation(),exceptionmsg))
    {
        LOG_ERROR(exceptionmsg);
        Zimbra::Util::FreeString(exceptionmsg);
    }
    return NULL;
}

LPCWSTR MAPIAccessAPI::_InitGlobalSessionAndStore(LPCWSTR lpcwstrMigTarget, ULONG flag)
{
    LOGFN_TRACE_NO;

    LPWSTR lpwstrStatus = NULL;
    LPWSTR lpwstrRetVal= NULL;
    bool bPublicFolder= false;

    if (m_bSingleMailBoxMigration)
        m_MigType = PROFILE_MIG;
    else
        m_MigType = MAILBOX_MIG;

    // If part of domain, get domain name
    m_bHasJoinedDomain = Zimbra::MAPI::Util::GetDomainName(m_strExchangeHostName);
    LOG_TRACE(_T("HasJoinedDomain: %s"), m_bHasJoinedDomain?_T("Yes"):_T("No"));

    try
    {
        // Log on to MAPI
        m_zmmapisession = new Zimbra::MAPI::MAPISession();

        // Determine if its a profile or PST by extension
        wstring strMigTarget = lpcwstrMigTarget;
        std::transform(strMigTarget.begin(), strMigTarget.end(), strMigTarget.begin(), ::toupper);

        // if pst file, create associated MAPI profile for migration
        if (strMigTarget.find(L".PST") != std::wstring::npos)
        {
            LOG_INFO(_T("PST migration"));
            LPSTR lpstrMigTarget;
            WtoA((LPWSTR)lpcwstrMigTarget, lpstrMigTarget);

            m_MigType = PST_MIG;

            // delete any left over profiles from previous migration
            Zimbra::MAPI::Util::DeleteAlikeProfiles(Zimbra::MAPI::Util::PSTMIG_PROFILE_PREFIX.c_str());
            string strPSTProfileName = Zimbra::MAPI::Util::PSTMIG_PROFILE_PREFIX;

            // Add timestamp to profile to make it unique
            char timeStr[9];
            _strtime(timeStr);
            string strTmpProfile(timeStr);
            replace(strTmpProfile.begin(), strTmpProfile.end(), ':', '_');
            strPSTProfileName += strTmpProfile;

            // create PST profile
            if (!Zimbra::MAPI::Util::CreatePSTProfile((LPSTR)strPSTProfileName.c_str(), lpstrMigTarget))
            {
                SafeDelete(lpstrMigTarget);
                lpwstrStatus = FormatExceptionInfo(E_FAIL, ERR_CREATE_PSTPROFILE, __FILE__, __LINE__);
                LOG_ERROR(lpwstrStatus);
                Zimbra::Util::CopyString(lpwstrRetVal,(LPWSTR)ERR_CREATE_PSTPROFILE);
                goto CLEAN_UP;
            }
            SafeDelete(lpstrMigTarget);

            LPWSTR wstrProfileName;
            AtoW((LPSTR)strPSTProfileName.c_str(), wstrProfileName);
            m_strTargetProfileName = wstrProfileName;
            SafeDelete(wstrProfileName);

            //set registry for PST migration
            SetOOMRegistry();
        }
        else 
        if ((flag & FL_PUBLIC_FOLDER) == FL_PUBLIC_FOLDER)//(strMigTarget.find(L".PUBLIC") != std::wstring::npos)
        {
            bPublicFolder = true;
            m_strTargetProfileName = lpcwstrMigTarget;
        }
        else
        {
            LOG_TRACE(_T("Profile migration"));
            m_strTargetProfileName = lpcwstrMigTarget;
        }
        
        LOG_TRACE(_T("Migration Type: %d"), m_MigType);
        HRESULT hr = m_zmmapisession->Logon((LPWSTR)m_strTargetProfileName.c_str());
        if (hr != S_OK)
            goto CLEAN_UP;

        m_defaultStore = new Zimbra::MAPI::MAPIStore(m_MigType);

        // Open target default store
        hr = m_zmmapisession->OpenDefaultStore(*m_defaultStore);
        if (hr != S_OK)
            goto CLEAN_UP;
    }
    catch (MAPISessionException &msse)
    {
        lpwstrStatus = FormatExceptionInfo(msse.ErrCode(), (LPWSTR)msse.Description().c_str(), (LPSTR)msse.SrcFile().c_str(), msse.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal,msse.ShortDescription().c_str());
    }
    catch (MAPIStoreException &mste)
    {
        lpwstrStatus = FormatExceptionInfo(mste.ErrCode(), (LPWSTR)mste.Description().c_str(), (LPSTR)mste.SrcFile().c_str(), mste.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal,mste.ShortDescription().c_str());
    }
    catch (Util::MapiUtilsException &muex)
    {
        lpwstrStatus = FormatExceptionInfo(muex.ErrCode(), (LPWSTR)muex.Description().c_str(), (LPSTR)muex.SrcFile().c_str(), muex.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal,muex.ShortDescription().c_str());
    }
    
    // Create Temporary dir for temp files
    Zimbra::MAPI::Util::CreateAppTemporaryDirectory();

CLEAN_UP: 
    
    if (lpwstrStatus)
    {
        if (m_zmmapisession)
            delete m_zmmapisession;

        m_zmmapisession = NULL;

        if (m_defaultStore)
            delete m_defaultStore;

        m_defaultStore = NULL;
        Zimbra::Util::FreeString(lpwstrStatus);
    }
    return lpwstrRetVal;
}

void MAPIAccessAPI::UnInitGlobalSessionAndStore()
{
    LOGFN_TRACE_NO;

    Zimbra::Util::MiniDumpGenerator::UninitMiniDumpGenerator();

    // Delete any PST migration profiles
    Zimbra::MAPI::Util::DeleteAlikeProfiles(Zimbra::MAPI::Util::PSTMIG_PROFILE_PREFIX.c_str());

    if (m_defaultStore)
        delete m_defaultStore;
    m_defaultStore = NULL;
    if (m_zmmapisession)
        delete m_zmmapisession;
    m_zmmapisession = NULL;

    if(GetMigrationType() == PST_MIG)
        ResetOOMRegistry();
}

wstring GetCNName(LPWSTR pstrUserDN)
{
    LOGFN_TRACE_NO;

    wstring strUserDN = pstrUserDN;
    size_t npos=strUserDN.find_last_of(L"/");
    if(npos != string::npos)
    {
        strUserDN=strUserDN.substr(npos+1);
        npos=strUserDN.find_first_of(L"cn=");
        if(npos != string::npos)
        {
            strUserDN=strUserDN.substr(3);
        }
    }
    else
        strUserDN=L"";
    return strUserDN;
}

LPCWSTR MAPIAccessAPI::OpenPublicStore()
{
    LOGFN_TRACE_NO;

    LPWSTR lpwstrRetVal=NULL;
    LPWSTR lpwstrStatus = NULL;
    HRESULT hr = S_OK;
    try
    {
        // user store
        m_publicStore = new Zimbra::MAPI::MAPIStore(MAILBOX_MIG);
        hr=m_zmmapisession->OpenPublicStore(*m_publicStore);
        if (hr != S_OK)
        {
            lpwstrStatus = FormatExceptionInfo(hr, L"MAPIAccessAPI::OpenPublicStore() Failed", __FILE__, __LINE__);
            LOG_ERROR(lpwstrStatus);
            Zimbra::Util::CopyString(lpwstrRetVal, lpwstrStatus);
        }
        else
        {
            m_userStore = m_publicStore;
        }
    }
    catch (MAPISessionException &msse)
    {
        lpwstrStatus = FormatExceptionInfo(msse.ErrCode(), (LPWSTR)msse.Description().c_str(), (LPSTR)msse.SrcFile().c_str(), msse.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal, msse.ShortDescription().c_str());
    }
    catch (MAPIStoreException &mste)
    {
        lpwstrStatus = FormatExceptionInfo(mste.ErrCode(), (LPWSTR)mste.Description().c_str(), (LPSTR)mste.SrcFile().c_str(), mste.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal, mste.ShortDescription().c_str());
    }

    if(lpwstrStatus)
        Zimbra::Util::FreeString(lpwstrStatus);
    return lpwstrRetVal;
}

HRESULT MAPIAccessAPI::EnumeratePublicFolders(std::vector<std::string> &pubFldrList)
{
    LOGFN_TRACE_NO;

    HRESULT hr = S_OK;
    LPMAPITABLE pTable = NULL;
    LPSRowSet pRowSet = NULL;
    ULONG ulRows = 0;

    hr = m_publicStore->GetPublicFolderTable(&pTable);
    if (FAILED(hr))
        return E_FAIL;

    hr = pTable->GetRowCount(0, &ulRows);
    if (FAILED(hr))
    {
        pTable->Release();
        return E_FAIL;
    }
    if (ulRows > 0)
    {
        SPropTagArray columns;
        columns.cValues = 1;
        columns.aulPropTag[0] = PR_DISPLAY_NAME;

        hr = pTable->SetColumns(&columns, 0);
        if (FAILED(hr))
            pTable->Release();

        hr = pTable->QueryRows(ulRows, 0, &pRowSet);
        if (FAILED(hr))
            pTable->Release();

        for (unsigned int i = 0; i < pRowSet->cRows; i++)
        {
            std::string strPubFldr;
            bool bwstrConv = false;
            LPWSTR lpwstrPubFldrName = pRowSet->aRow[i].lpProps[0].Value.lpszW;

            int bsz = WideCharToMultiByte(CP_ACP, 0, lpwstrPubFldrName, -1, 0, 0, 0, 0);
            if (bsz > 0)
            {
                char *p = new char[bsz];
                int rc = WideCharToMultiByte(CP_ACP, 0, lpwstrPubFldrName, -1, p, bsz, 0, 0);
                if (rc != 0)
                {
                    p[bsz - 1] = 0;
                    strPubFldr = p;
                    bwstrConv = true;
                }
                delete[] p;
            }
            if (bwstrConv)
                pubFldrList.push_back(strPubFldr);
        }
    }
    
    pTable->Release();
    return hr;
}


LPCWSTR MAPIAccessAPI::InitializePublicFolders()
{
    LOGFN_TRACE_NO;

    LPWSTR lpwstrRetVal=NULL;
    LPWSTR lpwstrStatus = NULL;
    HRESULT hr = S_OK;

    try
    {
        // Open store
        lpwstrStatus = (LPWSTR)OpenPublicStore();

        // Get root folder
        m_rootFolder = new Zimbra::MAPI::MAPIFolder(*m_zmmapisession, *m_publicStore, L"");

        if (FAILED(hr = m_publicStore->GetRootFolder(*m_rootFolder)))
        {
            lpwstrStatus = FormatExceptionInfo(hr, L"MAPIAccessAPI::InitializePublicFolders() Failed", __FILE__, __LINE__);
            Zimbra::Util::CopyString(lpwstrRetVal, L"MAPIAccessAPI::InitializePublicFolders() Failed");
        }
        Zimbra::Mapi::NamedPropsManager::SetNamedProps(m_publicStore->GetInternalMAPIStore());
    }
    catch (GenericException &ge)
    {
        lpwstrStatus = FormatExceptionInfo(ge.ErrCode(), (LPWSTR)ge.Description().c_str(), (LPSTR)ge.SrcFile().c_str(), ge.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal,ge.ShortDescription().c_str());
    }

    if(lpwstrStatus)
        Zimbra::Util::FreeString(lpwstrStatus);

    return lpwstrRetVal;
}


// Open MAPI session and Open Stores
LPCWSTR MAPIAccessAPI::OpenUserStore()
{
    LOGFN_TRACE_NO;

    HRESULT hr = S_OK;
    LPWSTR lpwstrRetVal             = NULL;
    LPWSTR lpwstrStatus             = NULL;

    LPSTR  ExchangeServerDN         = NULL;
    LPSTR  ExchangeUserDN           = NULL;
    LPSTR  ExchangeHostName         = NULL;
    LPWSTR pwstrExchangeServerDN    = NULL;
    LPWSTR pwstrExchangeHostName    = NULL;
    LPWSTR pwstrExchangeUserDN      = NULL;
    
    try
    {
        // user store
        m_userStore = new Zimbra::MAPI::MAPIStore(m_MigType);

        // Get Exchange Server DN
        _ASSERT(m_zmmapisession);
        hr = Zimbra::MAPI::Util::GetUserDnAndServerDnFromProfile(m_zmmapisession->GetMAPISessionObject(), ExchangeServerDN, ExchangeUserDN, ExchangeHostName);
        if (hr != S_OK)
            goto CLEAN_UP;

        AtoW(ExchangeServerDN, pwstrExchangeServerDN);
        AtoW(ExchangeHostName, pwstrExchangeHostName);
        AtoW(ExchangeUserDN, pwstrExchangeUserDN);
        
        // Get CN name from user DN
        wstring wstrCNName = GetCNName(pwstrExchangeUserDN);

        // if logged in user and user to migrate are same. Assume that its a profile migration.
        // No need to get user DN again from LDAP. Most probably, machine is not part of any domain.
        if (wstrCNName == m_strUserName)
        {
            m_userStore = m_defaultStore;
            m_bSingleMailBoxMigration = true;
        }
        else
        {
            //if part of domain, use domain to query LDAP
            //else use Exchange server host name from profile
            //NOTE:If not part of domain and AD is on different host than Exchange Server
            //It will not work. m_strExchangeHostName MUST point to AD.
            //TODO: Find a way to get AD from profile or some other means.
            if(!m_bHasJoinedDomain)
                m_strExchangeHostName = pwstrExchangeHostName;
                
            // Get DN of user to be migrated
            wstring wstruserdn, legacyName;
            Zimbra::MAPI::Util::GetUserDNAndLegacyName(m_strExchangeHostName.c_str(), m_strUserName.c_str(), NULL, wstruserdn, legacyName);
            hr = m_zmmapisession->OpenOtherStore(m_defaultStore->GetInternalMAPIStore(), pwstrExchangeServerDN, (LPWSTR)legacyName.c_str(), *m_userStore);
            if (hr != S_OK)
                goto CLEAN_UP;
        }
        
    }
    catch (MAPISessionException &msse)
    {
        lpwstrStatus = FormatExceptionInfo(msse.ErrCode(), (LPWSTR)msse.Description().c_str(), (LPSTR)msse.SrcFile().c_str(), msse.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal, msse.ShortDescription().c_str());
    }
    catch (MAPIStoreException &mste)
    {
        lpwstrStatus = FormatExceptionInfo(mste.ErrCode(), (LPWSTR)mste.Description().c_str(), (LPSTR)mste.SrcFile().c_str(), mste.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal, mste.ShortDescription().c_str());
    }
    catch (Util::MapiUtilsException &muex)
    {
        lpwstrStatus = FormatExceptionInfo(muex.ErrCode(), (LPWSTR)muex.Description().c_str(), (LPSTR)muex.SrcFile().c_str(), muex.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal, muex.ShortDescription().c_str());
    }

CLEAN_UP: 
    SafeDelete(ExchangeServerDN);
    SafeDelete(ExchangeUserDN);
    SafeDelete(pwstrExchangeServerDN);
    SafeDelete(pwstrExchangeHostName);
    SafeDelete(pwstrExchangeUserDN);

    if ((hr != S_OK)&&(!lpwstrStatus))
    {
        lpwstrStatus = FormatExceptionInfo(hr, L"MAPIAccessAPI::OpenSessionAndStore() Failed", __FILE__, __LINE__);
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal,  L"MAPIAccessAPI::OpenSessionAndStore() Failed");
    }
    if(lpwstrStatus)
        Zimbra::Util::FreeString(lpwstrStatus);
    return lpwstrRetVal;
}

LPCWSTR MAPIAccessAPI::InitializeUser()
{
    LPWSTR exceptionmsg=NULL;
    __try
    {
        return _InitializeUser();
    }
    __except(Zimbra::Util::MiniDumpGenerator::GenerateCoreDump(GetExceptionInformation(),exceptionmsg))
    {
        LOG_ERROR(exceptionmsg);		
    }
    return exceptionmsg;
}

LPCWSTR MAPIAccessAPI::_InitializeUser()
// Log onto store and get root folder
{
    LOGFN_TRACE_NO;

    LPWSTR lpwstrStatus = NULL;
    LPWSTR lpwstrRetVal = NULL;
    HRESULT hr = S_OK;

    try
    {
        // -----------------------------------
        // Open store
        // -----------------------------------
        if (!m_bSingleMailBoxMigration)
        {
            // Following call takes extended time
            lpwstrStatus = (LPWSTR)OpenUserStore();
            if (lpwstrStatus)
                return lpwstrStatus;
        }
        else
        {
            // if profile to be migrated
            m_userStore = m_defaultStore;
        }

        // -----------------------------------
        // Get root folder
        // -----------------------------------
        // Construct empty folder
        m_rootFolder = new Zimbra::MAPI::MAPIFolder(*m_zmmapisession, *m_userStore, L"");

        // Fill it in
        if (FAILED(hr = m_userStore->GetRootFolder(*m_rootFolder)))
        {
            lpwstrStatus = FormatExceptionInfo(hr, L"MAPIAccessAPI::Initialize() Failed", __FILE__, __LINE__);
            Zimbra::Util::CopyString(lpwstrRetVal, L"MAPIAccessAPI::Initialize() Failed");
        }

        // Set named props
        Zimbra::Mapi::NamedPropsManager::SetNamedProps(m_userStore->GetInternalMAPIStore());
    }
    catch (GenericException &ge)
    {
        lpwstrStatus = FormatExceptionInfo(ge.ErrCode(), (LPWSTR)ge.Description().c_str(), (LPSTR)ge.SrcFile().c_str(), ge.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal,ge.ShortDescription().c_str());
    }

    if(lpwstrStatus)
        Zimbra::Util::FreeString(lpwstrStatus);
    return lpwstrRetVal;
}


LPCWSTR MAPIAccessAPI::GetRootFolderHierarchy(vector<Folder_Data> &vfolderlist)
//
// NB Root folder == IPM subtree
//
{
    //LOGFN_TRACE_NO;

    LPWSTR exceptionmsg=NULL;
    __try
    {
        return _GetRootFolderHierarchy(vfolderlist);
    }
    __except(Zimbra::Util::MiniDumpGenerator::GenerateCoreDump(GetExceptionInformation(),exceptionmsg))
    {
        LOG_ERROR(exceptionmsg);		
    }
    return exceptionmsg;
}

LPCWSTR MAPIAccessAPI::_GetRootFolderHierarchy(vector<Folder_Data> &vfolderlist)
//
// NB Root folder == IPM subtree
//
{
    LOGFN_TRACE_NO;

    LPWSTR lpwstrStatus = NULL;
    HRESULT hr = S_OK;

    try
    {
        hr = CacheFolderDataAndIterateChildFoldersDepthFirst(*m_rootFolder, vfolderlist);
    }
    catch (GenericException &ge)
    {
        lpwstrStatus = FormatExceptionInfo(ge.ErrCode(), (LPWSTR)ge.Description().c_str(), (LPSTR)ge.SrcFile().c_str(), ge.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::FreeString(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrStatus, ge.ShortDescription().c_str());
    }
    return lpwstrStatus;
}

void MAPIAccessAPI::MAPIFolder2FolderData(Zimbra::MAPI::MAPIFolder *folder, // In
                                          Folder_Data &flderdata)           // Out
//
// We're building up a vector of Folder_Data items, so fill a Folder_Data for passed-in folder
//
{
    LOGFN_TRACE_NO;

    ULONG itemCount = 0;

    // folder item count
    folder->GetItemCount(itemCount);
    flderdata.itemcount = itemCount;

    // store Folder EntryID
    SBinary sbin = folder->EntryID();
    CopyEntryID(sbin, flderdata.sbin);

    // folder path
    flderdata.folderpath = folder->GetFolderPath();

    // ZimbraSpecialFolderId
    ZimbraSpecialFolderId sfid = folder->GetZimbraFolderId();
    flderdata.zimbraspecialfolderid = (long)sfid;

    // folderName
    if (flderdata.zimbraspecialfolderid == ZM_ROOT)
        flderdata.name = CONST_ROOTFOLDERNAME;
    else
        flderdata.name = folder->Name();
}

HRESULT MAPIAccessAPI::CacheFolderDataAndIterateChildFoldersDepthFirst(Zimbra::MAPI::MAPIFolder &folder, vector<Folder_Data> &fd)
//
// Walks all child folders of "folder", opens them and adds data to fd
//
// Recursive! Depth first.
//
{
    LOG_TRACE(L" ");
    LOG_TRACE(L" ");
    LOG_TRACE(L" ");
    LOG_TRACE(L" ");
    LOG_TRACE(L"-------------------------------------------------------------------------------------------------------");
    LOG_TRACE(L"Iterating child folders of '%s'", folder.GetFolderPath().c_str());
    LOG_TRACE(L"-------------------------------------------------------------------------------------------------------");
    LOGFN_TRACE_NO;

    // ===============================================================
    // Decide if we need to skip
    // ===============================================================
    ExchangeSpecialFolderId exfid = folder.GetExchangeFolderId();

    bool bHidden = folder.HiddenFolder();

    wstring wstrContainerClass;
    folder.ContainerClass(wstrContainerClass);

    // Skip folders in exclusion list, hidden folders and non-standard type folders
    bool bSkipFolder = false;
    if (   SkipFolder(exfid) 
        || bHidden 
        || ((    (wstrContainerClass != L"IPF.Note") 
              && (wstrContainerClass != L"IPF.Contact") 
              && (wstrContainerClass != L"IPF.Appointment") 
              && (wstrContainerClass != L"IPF.Task") 
              && (wstrContainerClass != L"IPF.StickyNote") 
              && (wstrContainerClass != L"IPF.Imap") 
              && (wstrContainerClass != L"")) 
           && (exfid == SPECIAL_FOLDER_ID_NONE)))
        bSkipFolder = true;

    // ===============================================================
    // If not skipping, get it
    // ===============================================================
    if (!bSkipFolder)
    {
        // -----------------------------------------------------
        // Get folder data
        // -----------------------------------------------------
        Folder_Data flderdata;
        flderdata.containerclass = wstrContainerClass;
        MAPIFolder2FolderData(&folder, flderdata);

        // -----------------------------------------------------
        // Add to list
        // -----------------------------------------------------
        // Dont add root folder, if it has no data items
        if((flderdata.zimbraspecialfolderid != ZM_ROOT) || ((flderdata.zimbraspecialfolderid == ZM_ROOT)&&(flderdata.itemcount>0)))
            fd.push_back(flderdata);


        // -----------------------------------------------------------------
        // Get a folder iterator for curr folder so we can walk its folders
        // -----------------------------------------------------------------
        Zimbra::MAPI::FolderIterator *folderIter = new Zimbra::MAPI::FolderIterator;
        HRESULT hr = folder.GetFolderIterator(*folderIter);		
        if (hr == S_OK)
        {
            // -----------------------------------------------------------------
            // Walk its folders - this will be depth first
            // -----------------------------------------------------------------
            BOOL bMore = TRUE;
            while(bMore)
            {
                // Create a MAPIFolder for the child
                Zimbra::MAPI::MAPIFolder *childFolder = new Zimbra::MAPI::MAPIFolder(*m_zmmapisession, *m_userStore, flderdata.folderpath);

                // And initialize it
                bMore = folderIter->GetNext(*childFolder);
                if (bMore)
                    CacheFolderDataAndIterateChildFoldersDepthFirst(*childFolder, fd);

                delete childFolder;
                childFolder = NULL;
            }
        }
        delete folderIter;
        folderIter = NULL;
    }
    return S_OK;
}

HRESULT MAPIAccessAPI::GetInternalFolder(SBinary sbFolderEID, MAPIFolder &folder)
{
    LOGFN_TRACE_NO;

    LPMAPIFOLDER pFolder = NULL;
    HRESULT hr = S_OK;
    ULONG objtype;
    if ((hr = m_userStore->OpenEntry(sbFolderEID.cb, (LPENTRYID)sbFolderEID.lpb, NULL, MAPI_BEST_ACCESS, &objtype, (LPUNKNOWN *)&pFolder)) != S_OK)
        throw GenericException(hr, L"MAPIAccessAPI::GetInternalFolder OpenEntry Failed.", ERR_OPEN_ENTRYID,  __LINE__, __FILE__);

    // Get some props the MAPIFolder needs
    SizedSPropTagArray(5, tags) = {5, { PR_DISPLAY_NAME, PR_CONTAINER_CLASS, PR_ATTR_HIDDEN, PR_SUBFOLDERS, PR_CONTENT_COUNT }};
    ULONG cVals = 0;
    Zimbra::Util::ScopedBuffer<SPropValue> pVals;
    hr = pFolder->GetProps((LPSPropTagArray)& tags, MAPI_UNICODE, &cVals, pVals.getptr());
    if (FAILED(hr))
        throw GenericException(hr, L"MAPIAccessAPI::GetInternalFolder GetProps() Failed.", ERR_OPEN_PROPERTY, __LINE__, __FILE__);

    wstring sDisplayName;
    if (pVals[0].ulPropTag == PR_DISPLAY_NAME)
        sDisplayName = pVals[0].Value.LPSZ;

    wstring sContainerClass;
    if (pVals[1].ulPropTag == PR_CONTAINER_CLASS)
        sContainerClass = pVals[1].Value.LPSZ;
    
    bool bHidden = false;
    if (pVals[2].ulPropTag == PR_ATTR_HIDDEN)
        bHidden = pVals[2].Value.b==1;
    
    bool bSubfolders = true;
    if (pVals[3].ulPropTag == PR_SUBFOLDERS)
        bSubfolders = pVals[3].Value.b==1;
        
    ULONG ulContentCount = 0;
    if (pVals[4].ulPropTag == PR_CONTENT_COUNT)
        ulContentCount = pVals[4].Value.ul;

    folder.InitMAPIFolder(pFolder, sDisplayName.c_str(), &sbFolderEID, sContainerClass.c_str(), bHidden, bSubfolders, ulContentCount);
    return hr;
}

LPCWSTR MAPIAccessAPI::GetFolderItemsList(SBinary sbFolderEID, vector<Item_Data> &ItemList)
{
    //LOGFN_TRACE_NO;

    LPWSTR exceptionmsg=NULL;
    __try
    {
        _GetFolderItemsList(sbFolderEID, ItemList);
    }
    __except(Zimbra::Util::MiniDumpGenerator::GenerateCoreDump(GetExceptionInformation(),exceptionmsg))
    {
        LOG_ERROR(exceptionmsg);		
    }
    return exceptionmsg;
}

LPCWSTR MAPIAccessAPI::_GetFolderItemsList(SBinary sbFolderEID, vector<Item_Data> &ItemList)
{
    LOGFN_TRACE_NO;

    LPWSTR lpwstrStatus = NULL;
    LPWSTR lpwstrRetVal = NULL;
    HRESULT hr = S_OK;

    MAPIFolder folder;
    try
    {
        if (FAILED(hr = GetInternalFolder(sbFolderEID, folder) != S_OK))
        {
            lpwstrStatus = FormatExceptionInfo(hr, L"MAPIAccessAPI::GetFolderItemsList() Failed", __FILE__, __LINE__);
            Zimbra::Util::CopyString(lpwstrRetVal,L"MAPIAccessAPI::GetFolderItemsList() Failed");
            goto ZM_EXIT;
        }

        Zimbra::MAPI::MessageIterator *msgIter = new Zimbra::MAPI::MessageIterator();
        hr = folder.GetMessageIterator(*msgIter);
        if (hr == S_OK)
        {
            // For every msg in the folder, build an itemdata struct and add it to ItemList
            BOOL bContinue = true;
            BOOL skip = false;
            DWORD dwCountMsgs = 0;
            wstring sSubj;
            while (bContinue)
            {
                skip = false;
                {
                    LOG_SUPPRESS_LOGGING_THIS_THREAD(0);
                    Zimbra::MAPI::MAPIMessage *msg = new Zimbra::MAPI::MAPIMessage();

                    try
                    {
                        bContinue = msgIter->GetNext(*msg);
                    }
                    catch (MAPIMessageException &msgex)
                    {
                        lpwstrStatus = FormatExceptionInfo(msgex.ErrCode(), (LPWSTR)msgex.Description().c_str(), (LPSTR)msgex.SrcFile().c_str(), msgex.SrcLine());
                        LOG_ERROR(lpwstrStatus);
                        Zimbra::Util::CopyString(lpwstrRetVal, msgex.ShortDescription().c_str());
                        bContinue = true;
                        skip = true;
                    }

                    if (bContinue && !skip)
                    {
                        // ----------------------------------------------
                        // Fill in Item_Data
                        // ----------------------------------------------
                        Item_Data itemdata;

                        // Item type
                        itemdata.lItemType = msg->ItemType();

                        // EID
                        SBinary sbin = msg->EntryID();
                        CopyEntryID(sbin, itemdata.sbMessageID);

                        // EID in string format
                        itemdata.strMsgEntryId = "";
                        Zimbra::Util::ScopedArray<CHAR> spUid(new CHAR[(sbin.cb * 2) + 1]);
                        if (spUid.get() != NULL)
                        {
                            Zimbra::Util::HexFromBin(sbin.lpb, sbin.cb, spUid.get());
                            itemdata.strMsgEntryId = spUid.getref();
                        }

                        // Date
                        itemdata.i64SubmitDate = msg->SubmitDate();

                        // Subject
                        LPTSTR lpstrSubject;
                        if (msg->Subject(&lpstrSubject))
                        {
                            itemdata.strSubject = lpstrSubject;
                            sSubj = lpstrSubject;
                            SafeDelete(lpstrSubject);
                        }
                        else
                            itemdata.strSubject = L"";

                        // Add to list
                        ItemList.push_back(itemdata);
                    }
                    delete msg;
                }

                if (bContinue && !skip)
                    LOG_VERBOSE(_T("%-5d %s"), ++dwCountMsgs, sSubj.c_str());

            } // while
        }

        delete msgIter;
    }
    catch (MAPISessionException &mssex)
    {
        lpwstrStatus = FormatExceptionInfo(mssex.ErrCode(), (LPWSTR)mssex.Description().c_str(), (LPSTR)mssex.SrcFile().c_str(), mssex.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal, mssex.ShortDescription().c_str());
    }
    catch (MAPIFolderException &mfex)
    {
        lpwstrStatus = FormatExceptionInfo(mfex.ErrCode(), (LPWSTR)mfex.Description().c_str(), (LPSTR)mfex.SrcFile().c_str(), mfex.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal, mfex.ShortDescription().c_str());
    }
    catch (MAPIMessageException &msgex)
    {
        lpwstrStatus = FormatExceptionInfo(msgex.ErrCode(), (LPWSTR)msgex.Description().c_str(), (LPSTR)msgex.SrcFile().c_str(), msgex.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal, msgex.ShortDescription().c_str());
    }
    catch (GenericException &genex)
    {
        lpwstrStatus = FormatExceptionInfo(genex.ErrCode(), (LPWSTR)genex.Description().c_str(), (LPSTR)genex.SrcFile().c_str(), genex.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal, genex.ShortDescription().c_str());
    }

    if(lpwstrStatus)
        Zimbra::Util::FreeString(lpwstrStatus);
    
ZM_EXIT: 
    return lpwstrRetVal;
}

LPCWSTR MAPIAccessAPI::GetItem(SBinary sbItemEID, BaseItemData &itemData)
{
    // LOGFN_TRACE_NO;

    LPWSTR exceptionmsg=NULL;
    __try
    {
        exceptionmsg = (LPWSTR)_GetItem(sbItemEID,itemData);
    }
    __except(Zimbra::Util::MiniDumpGenerator::GenerateCoreDump(GetExceptionInformation(),exceptionmsg))
    {
        LOG_ERROR(exceptionmsg);		
    }

    //g_logger.LogProcessMemoryUsage();

    return exceptionmsg;
}

LPCWSTR MAPIAccessAPI::_GetItem(SBinary sbItemEID, BaseItemData &itemData)
//
// OpenEntry()s item "sbItemEID" and populates itemData from the MAPI obj
//
{
    LOGFN_TRACE_NO;

    LPWSTR lpwstrStatus = NULL;
    LPWSTR lpwstrRetVal = NULL;
    HRESULT hr = S_OK;
    
    LPTSTR pszBin = SBinToStr(sbItemEID);
    LOG_TRACE(L"Item EntryID: %s", pszBin);

    // ---------------------------------------------------------------------
    // OpenEntry() the MAPI item. We'll wrap this with a MAPIMessage below.
    // ---------------------------------------------------------------------
    LPMESSAGE pMessage = NULL;
    ULONG objtype;
    if (FAILED(hr = m_userStore->OpenEntry(sbItemEID.cb, (LPENTRYID)sbItemEID.lpb, NULL, MAPI_BEST_ACCESS, &objtype, (LPUNKNOWN *)&pMessage)))
    {
        lpwstrStatus = FormatExceptionInfo(hr, L"MAPIAccessAPI::GetItem() Failed", __FILE__, __LINE__);		
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal, ERR_OPEN_ENTRYID);
        goto ZM_EXIT;
    }

    /* in case we want some retry logic sometime
    hr = 0x80040115;
    int tries = 10;
    int num = 0;
    while ((hr == 0x80040115) && (num < tries))
    {
        hr = m_userStore->OpenEntry(sbItemEID.cb, (LPENTRYID)sbItemEID.lpb, NULL,
                                    MAPI_BEST_ACCESS, &objtype, (LPUNKNOWN *)&pMessage);
        if (FAILED(hr))
            LOG_INFO(_T("got an 80040115"));
        num++;
    }
    if (FAILED(hr))
    {
        lpwstrStatus = FormatExceptionInfo(hr, L"MAPIAccessAPI::GetItem() Failed", __FILE__, __LINE__);
        LOG_INFO(_T("MAPIAccessAPI -- m_userStore->OpenEntry failed %s"), lpwstrStatus);
        goto ZM_EXIT;
    }
    */

    try
    {
        // ---------------------------------------------------------------------
        // Wrap the opened item
        // ---------------------------------------------------------------------
        MAPIMessage msg;
        msg.InitMAPIMessage(pMessage, *m_zmmapisession); // Does the main GetProps(), GetRecipientTable() etc But note MAPIAppointment does its own GetProps() later too DCB_PERFORMANCE

        // Init keywords
        std::vector<LPWSTR>* pKeywords = msg.SetKeywords();

        Zimbra::MAPI::ZM_ITEM_TYPE nItemType = msg.ItemType();
        if ((nItemType == ZT_MAIL) || (nItemType == ZT_MEETREQ))
        {
           // printf("ITEM TYPE: ZT_MAIL \n");

            MessageItemData *msgdata = (MessageItemData *)&itemData;

            // Subject
            msgdata->Subject = L"";
            LPTSTR lpstrSubject;
            if (msg.Subject(&lpstrSubject))
            {
                msgdata->Subject = lpstrSubject;
                SafeDelete(lpstrSubject);
            }

            msgdata->IsFlagged = msg.IsFlagged();

            msgdata->Urlname = L"";

            LPTSTR lpstrUrlName;
            if (msg.GetURLName(&lpstrUrlName))
            {
                msgdata->Urlname = lpstrUrlName;
                SafeDelete(lpstrUrlName);
            }

            msgdata->IsDraft        = msg.IsDraft();
            msgdata->IsFromMe       = (msg.IsFromMe() == TRUE);
            msgdata->IsUnread       = (msg.IsUnread() == TRUE);
            msgdata->IsForwarded    = (msg.Forwarded() == TRUE);
            msgdata->RepliedTo      = msg.RepliedTo() == TRUE;
            msgdata->HasAttachments = msg.HasAttach();
            msgdata->IsUnsent       = msg.IsUnsent() == TRUE;
            msgdata->HasHtml        = msg.HasHtmlPart();
            msgdata->HasText        = msg.HasTextPart();
            msgdata->Date           = msg.SubmitDate();

            LPWSTR wstrDateString;
            AtoW(msg.SubmitDateString(), wstrDateString);
            msgdata->DateString = wstrDateString;
            SafeDelete(wstrDateString);

            LPWSTR wstrDateUnixString;
            AtoW(msg.SubmitDateUnixString(), wstrDateUnixString);
            msgdata->DateUnixString = wstrDateUnixString;
            SafeDelete(wstrDateUnixString);

            msgdata->deliveryDate = msg.DeliveryDate();

            LPWSTR wstrDelivUnixString;
            AtoW(msg.DeliveryUnixString(), wstrDelivUnixString);
            msgdata->DeliveryUnixString = wstrDelivUnixString;
            SafeDelete(wstrDelivUnixString);

            LPWSTR wstrDelivDateString;
            AtoW(msg.DeliveryDateString(), wstrDelivDateString);
            msgdata->DeliveryDateString = wstrDelivDateString;
            SafeDelete(wstrDelivDateString);

            msgdata->vTags = pKeywords;

            /*
            if (msgdata->HasText)
            {
                LPTSTR textMsgBuffer;
                unsigned int nTextchars;
                msg.TextBody(&textMsgBuffer, nTextchars);
                msgdata->textbody.buffer = textMsgBuffer;
                msgdata->textbody.size = nTextchars;
            }
            if (msgdata->HasHtml)
            {
                LPVOID pHtmlBodyBuffer = NULL;
                unsigned int nHtmlchars;
                msg.HtmlBody(&pHtmlBodyBuffer, nHtmlchars);
                msgdata->htmlbody.buffer = (LPTSTR)pHtmlBodyBuffer;
                msgdata->htmlbody.size = nHtmlchars;
            }
            */

            mimepp::Message mimeMsg;
            try
            {
                msg.ToMimePPMessage(mimeMsg);
            }
            catch(...)
            {
                lpwstrStatus = FormatExceptionInfo(hr, L"ToMimePPMessage Failed", __FILE__, __LINE__);
                LOG_ERROR(_T("MAPIAccessAPI -- exception"));
                LOG_ERROR(lpwstrStatus);
                Zimbra::Util::CopyString(lpwstrRetVal,L"MimePP conversion Failed.");
                goto ZM_EXIT;
            }


            // DCB Unit Test exception handling
            #ifdef _DEBUG
                bool bThrow = false;
                if (bThrow)
                {
                    int* pInt = NULL;
                    *pInt=10;
                }
            #endif

            msgdata->dwMimeSize = (DWORD)(mimeMsg.getString().size());

            // =========================================================
            // Decide whether mime should go to C# in file or buffer
            // =========================================================

            if (msgdata->dwMimeSize > 3 * 1024*1024) // Bigger than 3 meg -> file
            {
                // =========================================================
                // FILE
                // =========================================================

                // Note that file will contain non-unicode

                HRESULT hr = S_OK;
                LPCSTR pszMime = mimeMsg.getString().c_str();

                // ----------------------------------------------------
                // Calc a name for the temp file (in the app temp dir)
                // ----------------------------------------------------
                wstring wstrTempAppDirPath;
                if (!Zimbra::MAPI::Util::GetAppTemporaryDirectory(wstrTempAppDirPath))
                {
                    lpwstrStatus = FormatExceptionInfo(hr, L"GetAppTemporaryDirectory Failed", __FILE__, __LINE__);
                    LOG_ERROR(lpwstrStatus);
                    Zimbra::Util::CopyString(lpwstrRetVal, lpwstrStatus);
                    goto ZM_EXIT;
                }
                char *lpszDirName = NULL;
                WtoA((LPWSTR)wstrTempAppDirPath.c_str(), lpszDirName); // lpszDirName freed below
                string sFQFileName = lpszDirName;

                char *lpszUniqueName = NULL;
                WtoA((LPWSTR)Zimbra::MAPI::Util::GetUniqueName().c_str(), lpszUniqueName); // lpszUniqueName freed below

                sFQFileName += "\\";
                sFQFileName += lpszUniqueName;
                SafeDelete(lpszDirName);
                lpszDirName = NULL;
                SafeDelete(lpszUniqueName);
                lpszUniqueName = NULL;
                // sFQFileName now contains the mime file path

                // ----------------------------------------------------
                // Open a stream on the file
                // ----------------------------------------------------
                Zimbra::Util::ScopedInterface<IStream> pStream;
                if (FAILED(hr = OpenStreamOnFile(MAPIAllocateBuffer, MAPIFreeBuffer, STGM_CREATE | STGM_READWRITE, (LPTSTR)sFQFileName.c_str(), NULL, pStream.getptr())))
                {
                    lpwstrStatus = FormatExceptionInfo(hr, L"Message Error: OpenStreamOnFile Failed.", __FILE__, __LINE__);
                    LOG_ERROR(lpwstrStatus);
                    Zimbra::Util::CopyString(lpwstrRetVal, lpwstrStatus);
                    goto ZM_EXIT;
                }

                // ----------------------------------------------------
                // Write mime to the file
                // ----------------------------------------------------
                ULONG nBytesWritten = 0;
                ULONG nTotalBytesWritten = 0;
                int nBytesToBeWritten = msgdata->dwMimeSize;
                while (!FAILED(hr) && nBytesToBeWritten > 0)
                {
                    hr = pStream->Write(pszMime, nBytesToBeWritten, &nBytesWritten);
                    pszMime += nBytesWritten;
                    nBytesToBeWritten -= nBytesWritten;
                    nTotalBytesWritten += nBytesWritten;
                    nBytesWritten = 0;
                }
                if (FAILED(hr = pStream->Commit(0)))
                {
                    lpwstrStatus = FormatExceptionInfo(hr, L"Commit Failed", __FILE__, __LINE__);
                    LOG_ERROR(lpwstrStatus);
                    Zimbra::Util::CopyString(lpwstrRetVal, lpwstrStatus);
                    goto ZM_EXIT;
                }


                // Write sFQFileName into the field
                WCHAR *lpwstrFQFileName = NULL;
                AtoW((LPSTR)sFQFileName.c_str(), lpwstrFQFileName);
                msgdata->MimeFile = lpwstrFQFileName;
                SafeDelete(lpwstrFQFileName);
                lpwstrFQFileName = NULL;

                // Null out the mime buffer since we're using file instead for this item
                msgdata->pwsMimeBuffer = NULL;

            }
            else
            {
                // =========================================================
                // BUFFER
                // =========================================================

                // Note that buffer gets converted to unicode first

                #ifndef CASE_184490_DIAGNOSTICS
                    // DCB - It seems that we now place the MIME directly in a wstring
                    LPTSTR pwDes = NULL;
                    AtoW((LPSTR)mimeMsg.getString().c_str(),pwDes);   
                    Zimbra::MAPI::Util::PauseAfterLargeMemDelta();

                    msgdata->wsMimeBuffer = pwDes;
                    Zimbra::MAPI::Util::PauseAfterLargeMemDelta();

                    delete[] pwDes;
                    Zimbra::MAPI::Util::PauseAfterLargeMemDelta();
                #else
                    LOG_GEN(_T("Getting mime..."));
                    LPSTR pszMimeMsg = (LPSTR)mimeMsg.getString().c_str();
                    if (pszMimeMsg)
                    {
                        LOG_GEN(_T("mime len: %d"), strlen(pszMimeMsg));
                        LOG_GEN(_T(">AtoW..."));
                        AtoW2(pszMimeMsg, msgdata->pwsMimeBuffer);   
                        Zimbra::MAPI::Util::PauseAfterLargeMemDelta();
                        // no delete[] in this case - done later in CMapiAccessWrap::GetData()
                    }
                    else
                        LOG_ERROR(_T("NULL mime"));
                #endif
            }

        }
        else 
        if (nItemType == ZT_CONTACTS)
        {
           // printf("ITEM TYPE: ZT_CONTACTS \n");
            MAPIContact mapicontact(*m_zmmapisession, msg);

            ContactItemData *cd = (ContactItemData *)&itemData;
            cd->AssistantPhone      = mapicontact.AssistantPhone();
            cd->Birthday            = mapicontact.Birthday();
            cd->CallbackPhone       = mapicontact.CallbackPhone();
            cd->CarPhone            = mapicontact.CarPhone();
            cd->Company             = mapicontact.Company();
            cd->CompanyPhone        = mapicontact.CompanyPhone();
            cd->Department          = mapicontact.Department();
            cd->Email1              = mapicontact.Email();
            cd->Email2              = mapicontact.Email2();
            cd->Email3              = mapicontact.Email3();
            cd->FileAs              = mapicontact.FileAs();
            cd->FirstName           = mapicontact.FirstName();
            cd->HomeCity            = mapicontact.HomeCity();
            cd->HomeCountry         = mapicontact.HomeCountry();
            cd->HomeFax             = mapicontact.HomeFax();
            cd->HomePhone           = mapicontact.HomePhone();
            cd->HomePhone2          = mapicontact.HomePhone2();
            cd->HomePostalCode      = mapicontact.HomePostalCode();
            cd->HomeState           = mapicontact.HomeState();
            cd->HomeStreet          = mapicontact.HomeStreet();
            cd->HomeURL             = mapicontact.HomeURL();
            cd->IMAddress1          = mapicontact.IMAddress1();
            cd->JobTitle            = mapicontact.JobTitle();
            cd->LastName            = mapicontact.LastName();
            cd->MiddleName          = mapicontact.MiddleName();
            cd->MobilePhone         = mapicontact.MobilePhone();
            cd->NamePrefix          = mapicontact.NamePrefix();
            cd->NameSuffix          = mapicontact.NameSuffix();
            cd->NickName            = mapicontact.NickName();
            cd->Notes               = mapicontact.Notes();
            cd->OtherCity           = mapicontact.OtherCity();
            cd->OtherCountry        = mapicontact.OtherCountry();
            cd->OtherFax            = mapicontact.OtherFax();
            cd->OtherPhone          = mapicontact.OtherPhone();
            cd->OtherPostalCode     = mapicontact.OtherPostalCode();
            cd->OtherState          = mapicontact.OtherState();
            cd->OtherStreet         = mapicontact.OtherStreet();
            cd->OtherURL            = mapicontact.OtherURL();
            cd->Pager               = mapicontact.Pager();
            cd->PrimaryPhone        = mapicontact.PrimaryPhone();
            cd->pDList              = mapicontact.DList();
            cd->PictureID           = mapicontact.Picture();
            cd->Type                = mapicontact.Type();
            cd->UserField1          = mapicontact.UserField1();
            cd->UserField2          = mapicontact.UserField2();
            cd->UserField3          = mapicontact.UserField3();
            cd->UserField4          = mapicontact.UserField4();
            cd->WorkCity            = mapicontact.WorkCity();
            cd->WorkCountry         = mapicontact.WorkCountry();
            cd->WorkFax             = mapicontact.WorkFax();
            cd->WorkPhone           = mapicontact.WorkPhone();
            cd->WorkPhone2          = mapicontact.WorkPhone2();
            cd->WorkPostalCode      = mapicontact.WorkPostalCode();
            cd->WorkState           = mapicontact.WorkState();
            cd->WorkStreet          = mapicontact.WorkStreet();
            cd->WorkURL             = mapicontact.WorkURL();
            cd->ContactImagePath    = mapicontact.ContactImagePath();
            cd->ImageContenttype    = mapicontact.ContactImageType();
            cd->ImageContentdisp    = mapicontact.ContactImageDisp();
            cd->vTags               = pKeywords;
            cd->Anniversary         = mapicontact.Anniversary();

            // UDFs
            vector<ContactUDFields>::iterator it;
            for (it= mapicontact.UserDefinedFields()->begin();it != mapicontact.UserDefinedFields()->end();it++)
                cd->UserDefinedFields.push_back(*it);
        }
        else 
        if (nItemType == ZT_APPOINTMENTS)
        {
            // Construct the MAPIAppointment obj
            MAPIAppointment mapiappointment(*m_zmmapisession, *m_userStore ,msg, TOP_LEVEL, NULL);

            // Copy data from the MAPIAppointment to the output struct
            // DCB_PERFORMANCE WASTEFUL! Why don't we put it directly into the output struct to begin with?
            ApptItemData *ad = (ApptItemData *)&itemData;

            // ---------------------------------------------------
            // Subject
            // ---------------------------------------------------
            ad->Subject = mapiappointment.GetSubject();
            ad->Name    = mapiappointment.GetSubject();
            LOG_INFO(L"Subject:   %s",ad->Subject.c_str());

            // ---------------------------------------------------
            // Dates
            // ---------------------------------------------------
            ad->StartDate = mapiappointment.GetStartDate();
            LOG_TRACE(L"StartDate: %s",ad->StartDate.c_str());

            ad->EndDate = mapiappointment.GetEndDate();
            LOG_TRACE(L"EndDate:   %s",ad->EndDate.c_str());

            ad->CalFilterDate = mapiappointment.GetCalFilterDate();

            // ---------------------------------------------------
            // Misc
            // ---------------------------------------------------
            ad->Location = mapiappointment.GetLocation();

            if(!mapiappointment.GetResponseStatus().empty())
                ad->PartStat = mapiappointment.GetResponseStatus();
            else
                ad->PartStat = L"NE";

            ad->CurrStat = mapiappointment.GetCurrentStatus();

            if(!mapiappointment.GetResponseRequested().empty())
                ad->RSVP = mapiappointment.GetResponseRequested();
            else
                ad->RSVP = L"0";

            if(!mapiappointment.GetBusyStatus().empty())
                ad->FreeBusy = mapiappointment.GetBusyStatus();
            else
                ad->FreeBusy = L"F";

            if(!mapiappointment.GetAllday().empty())
                ad->AllDay = mapiappointment.GetAllday();
            else
                ad->AllDay = L"0";

            ad->Transparency = mapiappointment.GetTransparency();
            ad->ApptClass = mapiappointment.GetPrivate();
            if(mapiappointment.GetPrivate().empty())
                LOG_WARNING(_T("Appointment's status (public/private) cannot be fetched. Default status (Public) will be used."));

            ad->AlarmTrigger    = mapiappointment.GetReminderMinutes();
            ad->organizer.nam   = mapiappointment.GetOrganizerName();
            ad->organizer.addr  = mapiappointment.GetOrganizerAddr();
            ad->Uid             = mapiappointment.GetInstanceUID();
            ad->vTags           = pKeywords;
        
            // ---------------------------------------------------
            // Attendees
            // ---------------------------------------------------
            vector<Attendee*> v = mapiappointment.GetAttendees();
            for (size_t i = 0; i < v.size(); i++)
                ad->vAttendees.push_back((Attendee*)v[i]);

            // ---------------------------------------------------
            // Recurrence data
            // ---------------------------------------------------
            if (mapiappointment.IsRecurring())
            {
                ad->recurPattern = mapiappointment.GetRecurPattern();

                if (mapiappointment.GetRecurWkday().length() > 0)
                    ad->recurWkday = mapiappointment.GetRecurWkday();

                ad->recurInterval           = mapiappointment.GetRecurInterval();
                ad->recurCount              = mapiappointment.GetRecurCount();
                ad->recurEndDate            = mapiappointment.GetRecurEndDate();
                ad->recurDayOfMonth         = mapiappointment.GetRecurDayOfMonth();
                ad->recurMonthOccurrence    = mapiappointment.GetRecurMonthOccurrence();
                ad->recurMonthOfYear        = mapiappointment.GetRecurMonthOfYear();

                // if there are exceptions, deal with them
                vector<MAPIAppointment*> ex = mapiappointment.GetExceptions();
                for (size_t i = 0; i < ex.size(); i++)
                {
                    ad->vExceptions.push_back((MAPIAppointment*)ex[i]);
                }
            }

            // ---------------------------------------------------
            // Timezone props
            // ---------------------------------------------------
            ad->tzStart = mapiappointment.GetTimezoneStart();
            ad->tzEnd   = mapiappointment.GetTimezoneEnd();
            if (mapiappointment.IsRecurring())
                ad->tzLegacy = mapiappointment.GetTimezoneLegacy();

            ad->ExceptionType = mapiappointment.GetExceptionType();

            // ---------------------------------------------------
            // Attachments
            // ---------------------------------------------------
            vector<AttachmentInfo*> va = mapiappointment.GetAttachmentInfo();
            for (size_t i = 0; i < va.size(); i++)
                ad->vAttachments.push_back((AttachmentInfo*)va[i]);

            // ---------------------------------------------------
            // Message Part
            // ---------------------------------------------------
            MessagePart mp;
            mp.contentType = L"text/plain";
            mp.content = mapiappointment.GetPlainTextFileAndContent();
            ad->vMessageParts.push_back(mp);
            mp.contentType = L"text/html";
            mp.content = mapiappointment.GetHtmlFileAndContent();
            ad->vMessageParts.push_back(mp);
        }
        else 
        if (nItemType == ZT_TASKS)
        {
            MAPITask mapitask(*m_zmmapisession, msg);
            TaskItemData *td    = (TaskItemData *)&itemData;
            td->Subject         = mapitask.GetSubject();
            td->Importance      = mapitask.GetImportance();
            td->TaskStart       = mapitask.GetTaskStart();
            td->TaskFilterDate  = mapitask.GetTaskFilterDate();
            td->TaskDue         = mapitask.GetTaskDue();
            td->Status          = mapitask.GetTaskStatus();
            td->PercentComplete = mapitask.GetPercentComplete();
            td->TotalWork       = mapitask.GetTotalWork();
            td->ActualWork      = mapitask.GetActualWork();
            td->Companies       = mapitask.GetCompanies();
            td->Mileage         = mapitask.GetMileage();
            td->BillingInfo     = mapitask.GetBillingInfo();
            td->ApptClass       = mapitask.GetPrivate();
            td->vTags           = pKeywords;

            if (mapitask.IsTaskReminderSet())
                td->TaskFlagDueBy = mapitask.GetTaskFlagDueBy();

            // ---------------------------------------------------
            // Recurrence data
            // ---------------------------------------------------
            if (mapitask.IsRecurring())
            {
                //td->tz = mapitask.GetRecurTimezoneLegacy();
                td->recurPattern = mapitask.GetRecurPattern();

                if (mapitask.GetRecurWkday().length() > 0)
                    td->recurWkday = mapitask.GetRecurWkday();

                td->recurInterval           = mapitask.GetRecurInterval();
                td->recurCount              = mapitask.GetRecurCount();
                td->recurEndDate            = mapitask.GetRecurEndDate();
                td->recurDayOfMonth         = mapitask.GetRecurDayOfMonth();
                td->recurMonthOccurrence    = mapitask.GetRecurMonthOccurrence();
                td->recurMonthOfYear        = mapitask.GetRecurMonthOfYear();
            }

            // ---------------------------------------------------
            // Attachments
            // ---------------------------------------------------
            vector<AttachmentInfo*> va = mapitask.GetAttachmentInfo();
            for (size_t i = 0; i < va.size(); i++)
            {
                td->vAttachments.push_back((AttachmentInfo*)va[i]);
            }

            // ---------------------------------------------------
            // Message Part
            // ---------------------------------------------------
            MessagePart mp;
            mp.contentType = L"text/plain";
            mp.content = mapitask.GetPlainTextFileAndContent();
            td->vMessageParts.push_back(mp);
            mp.contentType = L"text/html";
            mp.content = mapitask.GetHtmlFileAndContent();
            td->vMessageParts.push_back(mp);
        }
    }
    catch (MAPIMessageException &mex)
    {
        lpwstrStatus = FormatExceptionInfo(mex.ErrCode(), (LPWSTR)mex.Description().c_str(), (LPSTR)mex.SrcFile().c_str(), mex.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal,mex.ShortDescription().c_str());
    }
    catch (MAPIContactException &cex)
    {
        lpwstrStatus = FormatExceptionInfo(cex.ErrCode(), (LPWSTR)cex.Description().c_str(), (LPSTR)cex.SrcFile().c_str(), cex.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal,cex.ShortDescription().c_str());
    }
    catch (MAPIAppointmentException &aex)
    {
        lpwstrStatus = FormatExceptionInfo(aex.ErrCode(), (LPWSTR)aex.Description().c_str(), (LPSTR)aex.SrcFile().c_str(), aex.SrcLine());
        LOG_ERROR(lpwstrStatus);
        Zimbra::Util::CopyString(lpwstrRetVal,aex.ShortDescription().c_str());
    }

ZM_EXIT: 
    if(lpwstrStatus)
        Zimbra::Util::FreeString(lpwstrStatus);
    return lpwstrRetVal;
}

LPWSTR MAPIAccessAPI::GetOOOStateAndMsg()
{
    LOGFN_TRACE_NO;

    HRESULT hr;
    BOOL bIsOOO = FALSE;
    Zimbra::Util::ScopedInterface<IMAPIFolder> spInbox;
    ULONG objtype;

    LPWSTR lpwstrOOOInfo = new WCHAR[3];
    lstrcpy(lpwstrOOOInfo, L"0:");

    // first get the OOO state -- if TRUE, put a 1: in the return val, else put 0:
    Zimbra::Util::ScopedBuffer<SPropValue> pPropValues;
    hr = HrGetOneProp(m_userStore->GetInternalMAPIStore(), PR_OOF_STATE, pPropValues.getptr());
    if (SUCCEEDED(hr))
    {
        bIsOOO = pPropValues->Value.b;
    }

    Zimbra::Util::ScopedInterface<IMAPITable> pContents;
    SBinaryArray specialFolderIds = m_userStore->GetSpecialFolderIds();
    SBinary sbin = specialFolderIds.lpbin[INBOX];
    hr = m_userStore->OpenEntry(sbin.cb, (LPENTRYID)sbin.lpb, NULL, MAPI_BEST_ACCESS, &objtype, (LPUNKNOWN *)spInbox.getptr());
    if (FAILED(hr))
    {
        return lpwstrOOOInfo;
    }
    
    hr = spInbox->GetContentsTable(MAPI_ASSOCIATED | fMapiUnicode, pContents.getptr());
    if (FAILED(hr))
    {
        //LOG_ERROR(_T("could not get the contents table %x"), hr);
        return lpwstrOOOInfo;
    }
    // set the columns because we are only interested in the body
    SizedSPropTagArray(1, tags) = {1, { PR_BODY }};
 
    pContents->SetColumns((LPSPropTagArray) &tags, 0);

    // restrict the table only to IPM.Note.Rules.OofTemplate.Microsoft
    SPropValue propVal;
    propVal.dwAlignPad = 0;
    propVal.ulPropTag = PR_MESSAGE_CLASS;
    propVal.Value.lpszW = L"IPM.Note.Rules.OofTemplate.Microsoft";

    SRestriction r;
    r.rt = RES_PROPERTY;
    r.res.resProperty.lpProp = &propVal;
    r.res.resProperty.relop = RELOP_EQ;
    r.res.resProperty.ulPropTag = propVal.ulPropTag;

    // lets get the instance key for the store provider
    pContents->Restrict(&r, 0);

    // iterate over the rows looking for the folder in question
    ULONG ulRows = 0;

    pContents->GetRowCount(0, &ulRows);
    if (ulRows == 0)
    {
        return lpwstrOOOInfo;
    }

    Zimbra::Util::ScopedRowSet pRows;

    pContents->QueryRows(ulRows, 0, pRows.getptr());
    for (unsigned int i = 0; i < pRows->cRows; i++)
    {
        if (pRows->aRow[i].lpProps[0].ulPropTag == PR_BODY)
        {
            LPWSTR pwszOOOMsg = pRows->aRow[i].lpProps[0].Value.lpszW;
            int iOOOLen = lstrlen(pwszOOOMsg);
            delete lpwstrOOOInfo;
            lpwstrOOOInfo = new WCHAR[iOOOLen + 3];  // for the 0: or 1: and null terminator
            if (bIsOOO)
            {
                lstrcpy(lpwstrOOOInfo, L"1:");
            }
            else
            {
                lstrcpy(lpwstrOOOInfo, L"0:");
            }           
            lstrcat(lpwstrOOOInfo, pwszOOOMsg);
            break;
        }
    }
    return lpwstrOOOInfo;
}

LPCWSTR MAPIAccessAPI::GetExchangeRules(vector<CRule> &vRuleList)
{
    LOGFN_TRACE_NO;

    LPCWSTR lpwstrStatus = NULL;
    HRESULT hr;
    ULONG objtype;
    Zimbra::Util::ScopedInterface<IMAPIFolder> spInbox;
    LPEXCHANGEMODIFYTABLE lpExchangeTable;
    Zimbra::Util::ScopedInterface<IMAPITable> spMAPIRulesTable;
    ULONG lpulCount = NULL;
    LPSRowSet lppRows = NULL;

    SBinaryArray specialFolderIds = m_userStore->GetSpecialFolderIds();
    SBinary sbin = specialFolderIds.lpbin[INBOX];
    hr = m_userStore->OpenEntry(sbin.cb, (LPENTRYID)sbin.lpb, NULL, MAPI_BEST_ACCESS, &objtype, (LPUNKNOWN *)spInbox.getptr());
    if (FAILED(hr))
    {
        lpwstrStatus = FormatExceptionInfo(hr, L"Unable to get Inbox.", __FILE__, __LINE__);
        return lpwstrStatus;                                  
    }

    hr = spInbox->OpenProperty(PR_RULES_TABLE, (LPGUID)&IID_IExchangeModifyTable, 0, MAPI_DEFERRED_ERRORS, (LPUNKNOWN FAR *)&lpExchangeTable);
    if (FAILED(hr))
    {
        lpwstrStatus = FormatExceptionInfo(hr, L"Unable to get Rules table property.", __FILE__, __LINE__);
        return lpwstrStatus;                                  
    }

    hr = lpExchangeTable->GetTable(0, spMAPIRulesTable.getptr());
    if (FAILED(hr))
    {
        lpExchangeTable->Release();
        lpwstrStatus = FormatExceptionInfo(hr, L"Unable to get Rules table.", __FILE__, __LINE__);
        return lpwstrStatus;                                  
    }

    hr = spMAPIRulesTable->GetRowCount(0, &lpulCount);
    if (FAILED(hr))
    {
        lpExchangeTable->Release();
        lpwstrStatus = FormatExceptionInfo(hr, L"Unable to get Rules table row count.", __FILE__, __LINE__);
        return lpwstrStatus;                                  
    }

    hr = HrQueryAllRows(spMAPIRulesTable.get(), NULL, NULL, NULL, lpulCount, &lppRows);
    if (SUCCEEDED(hr))
    {
        ULONG lPos = 0;
        LPSRestriction pRestriction = 0;
        LPACTIONS pActions = 0;
        ULONG ulWarnings = 0;

        LOG_INFO(L"Begin Rules Migration");

        CRuleProcessor* pRuleProcessor = new CRuleProcessor(m_zmmapisession, m_userStore, m_strUserAccount);

        vRuleList.clear();
        while (lPos < lpulCount)
        {
            CRule rule;

            pRestriction = 0;
            pActions = 0;

            bool bRes = true;
            bool bAct = true;

            rule.Clear();

            std::wstring rulename = CA2W(lppRows->aRow[lPos].lpProps[8].Value.lpszA);

            rule.SetName(rulename.c_str());
            rule.SetActive(1);                      // if it was inactive in Exchange, we'd never see it
            if (rulename.length() == 0)
            {
                LOG_WARNING(_T("Rule %d has no name"), lPos);
                lPos++;
                continue;
            }
 
            LOG_INFO(L"Processing rule '%s'", rulename.c_str());

            pRestriction = (LPSRestriction)lppRows->aRow[lPos].lpProps[5].Value.x;

            // conditions are restrictions - call a recursive method to process them
            // 99 means this is not being called by another restriction
            bRes = pRuleProcessor->ProcessRestrictions(rule, pRestriction, FALSE, 99);

            // check if there were restrictions we couldn't support, possibly causing there to be no conditions
            CListRuleConditions listRuleConditions;

            rule.GetConditions(listRuleConditions);
            if (listRuleConditions.size() == 0)
            {
                LOG_WARNING(_T("Rule %s has no conditions supported by Zimbra -- skipping"), rulename.c_str());
                bRes = false;
            }

            int ulRuleState = lppRows->aRow[lPos].lpProps[3].Value.l;

            if (ulRuleState & ST_EXIT_LEVEL)        // weird looking code to get around compiler warning C4800
                rule.ProcessAdditionalRules(false);
            else
                rule.ProcessAdditionalRules(true);

            pActions = (LPACTIONS)lppRows->aRow[lPos].lpProps[6].Value.x;
            bAct = pRuleProcessor->ProcessActions(rule, pActions);

            // check if there were actions we couldn't support, possibly causing there to be none
            CListRuleActions listRuleActions;

            rule.GetActions(listRuleActions);
            if (listRuleActions.size() == 0)
            {
                LOG_WARNING(_T("Rule %s has no actions supported by Zimbra -- skipping"), rulename.c_str());
                bAct = false;
            }
            if (bRes && bAct)
            {
                vRuleList.push_back(rule);
                LOG_INFO(L"Processed rule '%s'", rulename.c_str());
            }
            else
            {
                LOG_WARNING(L"Unable to import rule %s", rulename.c_str());
                ulWarnings++;
            }
            lPos++;
        }
        delete pRuleProcessor;
        MAPIFreeBuffer(pRestriction);
        MAPIFreeBuffer(pActions);

        // Fix up dict based on vRuleList
    }

    return lpwstrStatus;
}

/* 
// OBSOLETE
// Access MAPI folder items
void MAPIAccessAPI::traverse_folder(Zimbra::MAPI::MAPIFolder &folder)
{
    LOGFN_TRACE_NO;

    Zimbra::MAPI::MessageIterator *msgIter = new Zimbra::MAPI::MessageIterator();

    folder.GetMessageIterator(*msgIter);

    BOOL bContinue = true;
    while (bContinue)
    {
        Zimbra::MAPI::MAPIMessage *msg = new Zimbra::MAPI::MAPIMessage();

        bContinue = msgIter->GetNext(*msg);
        if (bContinue)
        {
            Zimbra::Util::ScopedBuffer<WCHAR> subject;

            if (msg->Subject(subject.getptr()))
                printf("\tsubject--%S\n", subject.get());
        }
        delete msg;
    }
    delete msgIter;
}
*/
