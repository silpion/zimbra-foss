/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
#pragma once

#include "optimize.h"

// use this flag on OpenMsgStore to force cached mode connections
// to read remote data and not local data
#define MDB_ONLINE              ((ULONG)0x00000100)

#define GLOBAL_PROFILE_SECTION_GUID  "\x13\xDB\xB0\xC8\xAA\x05\x10\x1A\x9B\xB0\x00\xAA\x00\x2F\xC4\x5A"
DEFINE_OLEGUID(PSETID_COMMON, MAKELONG(0x2000 + (8), 0x0006), 0, 0);

// A named property which specifies whether the mail is
// completely downloaded or in header only form in case of IMAP
#define DISPID_HEADER_ITEM      0x8578

#define CONST_ROOTFOLDERNAME	  L"RootFolderItems"

namespace Zimbra
{
namespace MAPI
{
class MAPIFolderException: public GenericException
{
public:
    MAPIFolderException(HRESULT hrErrCode, LPCWSTR lpszDescription);
    MAPIFolderException(HRESULT hrErrCode, LPCWSTR lpszDescription, LPCWSTR lpszShortDescription, int nLine, LPCSTR strFile);
    virtual ~MAPIFolderException() {}
};

class MAPIFolder;

// Folder Iterator class
class FolderIterator: public MAPITableIterator
{
public:
    FolderIterator();
    ~FolderIterator();

    virtual LPSPropTagArray GetTableProps();
    virtual LPSSortOrderSet GetSortOrder() { return NULL; }
    virtual LPSRestriction GetRestriction(ULONG TypeMask, FILETIME startDate)
    {
        UNREFERENCED_PARAMETER(TypeMask);
        UNREFERENCED_PARAMETER(startDate);
        return NULL;
    }

    BOOL GetNext(MAPIFolder &folder);

private:
    typedef enum _FolderIterPropTagIdx
    {
        FI_DISPLAY_NAME, 
        FI_ENTRYID, 
        FI_PR_LONGTERM_ENTRYID_FROM_TABLE, 
        FI_FLAGS, 
        FI_CONTAINER_CLASS,
        FI_ATTR_HIDDEN,
        FI_CONTENT_COUNT,
        FI_SUBFOLDERS,

        NFOLDERPROPS
    } FolderIterPropTagIdx;

    typedef struct _FolderIterPropTags
    {
        ULONG cValues;
        ULONG aulPropTags[NFOLDERPROPS];
    } FolderIterPropTags;

protected:
    static FolderIterPropTags m_TableProps;
};

// Exchange System folder enumeration
typedef enum _ExchangeSpecialFolderId
{
    INBOX                       = 0, 
    IPM_SUBTREE                 = 1, 
    CALENDAR                    = 2, 
    CONTACTS                    = 3, 
    DRAFTS                      = 4, 
    JOURNAL                     = 5, 
    NOTE                        = 6,
    TASK                        = 7, 
    OUTBOX                      = 8, 
    SENTMAIL                    = 9, 
    TRASH                       = 10, 
    SYNC_CONFLICTS              = 11,
    SYNC_ISSUES                 = 12, 
    SYNC_LOCAL_FAILURES         = 13, 
    SYNC_SERVER_FAILURES        = 14,
    JUNK_MAIL                   = 15, 
    SUGGESTED_CONTACTS          = 16,
    TOTAL_NUM_SPECIAL_FOLDERS   = 17, 
    SPECIAL_FOLDER_ID_NONE      = 1000

} ExchangeSpecialFolderId;

// Zimbra system folder enumeration
typedef enum _ZimbraSpecialFolderId
{
    ZM_SFID_MIN         = 0, 
    ZM_SFID_NONE        = 0, 
    ZM_ROOT             = 1, 
    ZM_INBOX,           // 2
    ZM_TRASH,           // 3
    ZM_SPAM,            // 4
    ZM_SENT_MAIL,       // 5
    ZM_DRAFTS,          // 6
    ZM_CONTACTS,        // 7
    ZM_TAGS,            // 8
    ZM_CONVERSATIONS,   // 9
    ZM_CALENDAR,        // 10
    ZM_MAILBOX_ROOT,    // 11
    ZM_WIKI,            // 12 
    ZM_EMAILEDCONTACTS, // 13
    ZM_CHATS,           // 14
    ZM_TASKS,           // 15
    ZM_SFID_MAX         // 16
} ZimbraSpecialFolderId;

//IPM folders strings if pst doesn't have IPM EntryIDs in Inbox folder
const int g_MAX_STR_IPM_FOLDERS=7;
const wstring g_strIPM_FOLDERS[g_MAX_STR_IPM_FOLDERS] ={L"Calendar",L"Contacts",L"Drafts",L"Journal",L"Notes",L"Tasks",L"Junk E-Mail"};



// MapiFolder class
class MAPIFolder:public Zimbra::Util::ZimObj
{
public:
    MAPIFolder();
    MAPIFolder(MAPISession &session, MAPIStore &store, const wstring& sParentFolderPath);
    MAPIFolder(const MAPIFolder &folder);
    ~MAPIFolder();

    void InitMAPIFolder(LPMAPIFOLDER pFolder, LPCTSTR displayName, LPSBinary pEntryId, const wstring& sContainerClass, bool bHidden, bool bSubfolders, ULONG ulContentCount);

    HRESULT GetMessageIterator(MessageIterator &msgIterator);
    HRESULT GetFolderIterator(FolderIterator &folderIter);

    HRESULT GetItemCount(ULONG &ulCount);
    ExchangeSpecialFolderId GetExchangeFolderId();
    ZimbraSpecialFolderId GetZimbraFolderId();
    bool HiddenFolder();
    bool HasSubfolders();
    HRESULT ContainerClass(wstring &wstrContainerClass);
    void GetRestrictedContentsTable();

    wstring GetFolderPath() { return m_sFolderpath; }
    wstring Name() { return m_displayname; }
    SBinary EntryID() { return m_EntryID; }

    wstring EscapeInvalidChar(const wstring& s, const char c);

private:
    LPMAPIFOLDER m_pMAPIFolder;
    LPMAPITABLE m_pContentsTable;
    LPMAPITABLE m_pHierarchyTable;

    wstring m_sParentFolderPath;

    wstring m_displayname;
    SBinary m_EntryID;
    MAPISession *m_session;
    MAPIStore *m_store;
    wstring m_sFolderpath;

    void CalcFolderPath();

    wstring m_sContainerClass;
    bool m_bHidden;
    ULONG m_ulContentCount;
    bool m_bSubfolders;
};

// global declaration
static ULONG g_ulIMAPHeaderInfoPropTag = PR_NULL;

} // namespace MAPI
} // namespace Zimbra
