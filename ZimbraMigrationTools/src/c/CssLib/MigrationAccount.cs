﻿/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
using System;
using System.Collections.Generic;

namespace CssLib
{

    // ==================================================================================
    // MigrationAccount
    // ==================================================================================
    // Represents a single account being migrated
    // Instantiated in worker_DoWork() for ZMT and accountToMigrate_DoWork() for ZMC
    public class MigrationAccount
    {
        public MigrationAccount()
        {
            // Dont know the account num yet
            AccountNum = -1;

            // Holds info about the folder currently being migrated
            CurrFolder = new MigrationFolder();

            // Track tags for this account
            TagDict = new Dictionary<string, string>();
        }

        // -----------------------------------------------------------------------------------------
        // Members
        // -----------------------------------------------------------------------------------------
        public string ProfileName;
        public string AccountID;
        public MigrationFolder CurrFolder;
        public Dictionary<string, string> TagDict;

        public event MigrationObjectEventHandler OnChanged;

        // -----------------------------------------------------------------------------------------
        // Misc account properties
        // -----------------------------------------------------------------------------------------
        private string accountName;
        public string AccountName
        {
            get { return this.accountName; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("AccountName", accountName, value));
                accountName = value;
            }
        }

        private int accountNum;
        public int AccountNum
        {
            get { return this.accountNum; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("AccountNum", accountNum, value));
                accountNum = value;
            }
        }


        // -----------------------------------------------------------------------------------------
        // Track various totals for the account
        // -----------------------------------------------------------------------------------------
        private int numItems;
        public int NumItems
        {
            get { return numItems; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("NumItems", numItems, value));
                numItems = value;
            }
        }

        /*
        private int numMails;
        public int NumMails
        {
            get { return numMails; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("NumMails", numMails, value));
                numMails = value;
            }
        }

        private int numContacts;
        public int NumContacts
        {
            get { return numContacts; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("NumContacts", numContacts, value));
                numContacts = value;
            }
        }

        private int numRules;
        public int NumRules
        {
            get { return numRules; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("NumRules", numRules, value));
                numRules = value;
            }
        }

        private int numAppts;
        public int NumAppts
        {
            get { return numAppts; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("TotalAppointments", numAppts, value));
                numAppts = value;
            }
        }

        private int numSent;
        public int NumSent
        {
            get { return numSent; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("NumSent", numSent, value));
                numSent = value;
            }
        }

        private int numTasks;
        public int NumTasks
        {
            get { return numTasks; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("NumTasks", numTasks, value));
                numTasks = value;
            }
        }
        */

        private int numErrs;
        public int NumErrs
        {
            get { return numErrs; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("NumErrs", numErrs, value));
                numErrs = value;
            }
        }

        private int numWarns;
        public int NumWarns
        {
            get { return numWarns; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("NumWarns", numWarns, value));
                numWarns = value;
            }
        }

        Int32 maxErrorCount;
        public Int32 MaxErrorCount
        {
            get { return maxErrorCount; }
            set { maxErrorCount = value; }
        }

        private ProblemInfo lastProblemInfo;
        public ProblemInfo LastProblemInfo
        {
            get { return lastProblemInfo; }
            set { lastProblemInfo = value; }
        }

        private bool IsValidAccount;
        public bool IsValid
        {
            get { return IsValidAccount; }
            set { IsValidAccount = value; }
        }

        private bool IsCompleted;
        public bool IsCompletedMigration
        {
            get { return IsCompleted; }
            set { IsCompleted = value; }
        }
    }


    // ==================================================================================
    // MigrationFolder
    // ==================================================================================
    // Holds info about the folder currently being migrated
    // Instantiated in worker_DoWork()
    public class MigrationFolder
    {
        public event MigrationObjectEventHandler OnChanged;

        public MigrationFolder() { }

        private string accountID;
        public string AccountID
        {
            get { return this.accountID; }
            set { accountID = value; }
        }

        private int accountNum;
        public int AccountNum
        {
            get { return accountNum; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("AccountNum", accountNum, value));
                accountNum = value;
            }
        }

        private string folderName;
        public string FolderName
        {
            get { return folderName; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("FolderName", folderName, value));
                folderName = value;
            }
        }

        private Int64 numItemsToMigrate;
        public Int64 NumItemsToMigrate
        {
            get { return numItemsToMigrate; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("NumItemsToMigrate", numItemsToMigrate, value));
                numItemsToMigrate = value;
            }
        }

        private Int64 numItemsMigrated;
        public Int64 NumItemsMigrated
        {
            get { return numItemsMigrated; }
            set
            {
                if (OnChanged != null)
                    OnChanged(this, new MigrationObjectEventArgs("NumItemsMigrated", numItemsMigrated, value));
                numItemsMigrated = value;
            }
        }

        private string folderView;
        public string FolderView
        {
            get { return folderView; }
            set { folderView = value; }
        }
    }

    // ==================================================================================
    // ProblemInfo
    // ==================================================================================
    public class ProblemInfo
    {
        public const int TYPE_ERR = 1;
        public const int TYPE_WARN = 2;

        public ProblemInfo(MigrationAccount Acct, string objectName, string theMsg, int msgType)
        {
            bool bErrNotWarn = (msgType == ProblemInfo.TYPE_ERR);
            this.objectName = objectName;
            this.msg = theMsg;
            this.msgType = msgType;

            formattedMsg = DateTime.Now.ToString("MM-dd HH:mm:ss.fff") + " ";
            formattedMsg += bErrNotWarn ? "Error" : "Warning";
            formattedMsg += (" in ["+Acct.CurrFolder.FolderName+"\\");
            formattedMsg += objectName + "]";
            formattedMsg += ":     ";
            formattedMsg += msg;

            if (bErrNotWarn)
            {
                Log.err(formattedMsg);
                Acct.NumErrs++;
            }
            else
            {
                Log.warn(formattedMsg);
                Acct.NumWarns++;
            }
        }

        public string ObjectName
        {
            get { return objectName; }
            set { objectName = value; }
        }

        public string Msg
        {
            get { return msg; }
            set { msg = value; }
        }

        public int MsgType
        {
            get { return msgType; }
            set { msgType = value; }
        }

        public string FormattedMsg
        {
            get { return formattedMsg; }
            set { formattedMsg = value; }
        }

        private string formattedMsg;
        private string msg;
        private int msgType;
        private string objectName;
    }

    // ==================================================================================
    // MigrationObjectEventArgs
    // ==================================================================================
    public class MigrationObjectEventArgs : EventArgs
    {
        public MigrationObjectEventArgs(string propertyName, object oldValue, object newValue)
        {
            this.propertyName = propertyName;
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        public string PropertyName
        {
            get { return this.propertyName; }
        }

        public object OldValue
        {
            get { return this.oldValue; }
        }

        public object NewValue
        {
            get { return this.newValue; }
        }

        private object newValue;
        private object oldValue;
        private string propertyName;
    }

    public delegate void MigrationObjectEventHandler(object sender, MigrationObjectEventArgs e);
}
