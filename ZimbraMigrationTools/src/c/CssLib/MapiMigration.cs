﻿/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Win32;

namespace CssLib
{
    public class CompatibilityChk
    {
        public enum MachineType : ushort
        {
            IMAGE_FILE_MACHINE_UNKNOWN = 0x0,
            IMAGE_FILE_MACHINE_AM33 = 0x1d3,
            IMAGE_FILE_MACHINE_AMD64 = 0x8664,
            IMAGE_FILE_MACHINE_ARM = 0x1c0,
            IMAGE_FILE_MACHINE_EBC = 0xebc,
            IMAGE_FILE_MACHINE_I386 = 0x14c,
            IMAGE_FILE_MACHINE_IA64 = 0x200,
            IMAGE_FILE_MACHINE_M32R = 0x9041,
            IMAGE_FILE_MACHINE_MIPS16 = 0x266,
            IMAGE_FILE_MACHINE_MIPSFPU = 0x366,
            IMAGE_FILE_MACHINE_MIPSFPU16 = 0x466,
            IMAGE_FILE_MACHINE_POWERPC = 0x1f0,
            IMAGE_FILE_MACHINE_POWERPCFP = 0x1f1,
            IMAGE_FILE_MACHINE_R4000 = 0x166,
            IMAGE_FILE_MACHINE_SH3 = 0x1a2,
            IMAGE_FILE_MACHINE_SH3DSP = 0x1a3,
            IMAGE_FILE_MACHINE_SH4 = 0x1a6,
            IMAGE_FILE_MACHINE_SH5 = 0x1a8,
            IMAGE_FILE_MACHINE_THUMB = 0x1c2,
            IMAGE_FILE_MACHINE_WCEMIPSV2 = 0x169,
        }

        public static MachineType GetDllMachineType(string dllPath)
        {
            // http://www.microsoft.com/whdc/system/platform/firmware/PECOFF.mspx 

            FileStream fs = new FileStream(dllPath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            fs.Seek(0x3c, SeekOrigin.Begin);
            Int32 peOffset = br.ReadInt32();
            fs.Seek(peOffset, SeekOrigin.Begin);
            UInt32 peHead = br.ReadUInt32();
            if (peHead != 0x00004550) // "PE\0\0", little-endian 
                throw new Exception("Can't find PE header");
            MachineType machineType = (MachineType)br.ReadUInt16();
            br.Close();
            fs.Close();
            return machineType;
        }

        // returns true if the dll is 64-bit, false if 32-bit, and null if unknown 
        public static bool? UnmanagedDllIs64Bit(string dllPath)
        {
            switch (GetDllMachineType(dllPath))
            {
                case MachineType.IMAGE_FILE_MACHINE_AMD64:
                case MachineType.IMAGE_FILE_MACHINE_IA64:
                    return true;
                case MachineType.IMAGE_FILE_MACHINE_I386:
                    return false;
                default:
                    return null;
            }
        }

        public static string CheckCompat(string path)
        {
            string status = "";
            string absolutepath = Path.GetFullPath("Exchange.dll");

            bool retval = UnmanagedDllIs64Bit(absolutepath).Value;

            string Bitness = (string)Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\14.0\Outlook", "Bitness", null);
            if (Bitness != null)
            {
                if ((Bitness == "x64") && (!retval))
                    status = "Outlook is 64 bit and migration is 32 bit";
            }
            return status;
        }
    }

    public class MapiMigration : MailMigration
    {
       // public dynamic MapiWrapper;
        public Exchange.MapiWrapper MapiWrapper;

        static public string  checkPrereqs()
        {
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(/*GetType() + "." + */System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                string str = "";
                string path = Directory.GetDirectoryRoot(Directory.GetCurrentDirectory());
                string absolutepath = Path.GetFullPath("Exchange.dll");

                bool bitness = CompatibilityChk.UnmanagedDllIs64Bit(absolutepath).Value;

                string registryValue = string.Empty;
                RegistryKey localKey = null;
                if (Environment.Is64BitOperatingSystem)
                    localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
                else
                    localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry32);

                try
                {
                    localKey = localKey.OpenSubKey(@"Software\\Microsoft\Office\\");
                    // registryValue = localKey.GetValue("TestKey").ToString();
                    if (localKey.SubKeyCount > 0)
                    {
                        if ((localKey.GetSubKeyNames()).Contains(@"Outlook"))
                        {
                        }
                        else
                        {
                            str = "Outlook is not installed on this system.Please Install Outlook"; // Still need this?
                            return str;
                        }
                    }
                }
                catch (Exception e)
                {
                    str = "Execption in reading regsitry for outlook prereq " + e.Message;
                    return str;
                }



                string InstallPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\14.0\Outlook", "Bitness", null);
                if (InstallPath == null)
                    InstallPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\15.0\Outlook", "Bitness", null);

                if (InstallPath != null)
                {
                    //its 64 bit outlook and 64 bit migration
                    if (bitness == true)
                    {
                        if (InstallPath == "x64")
                        {
                            //64 bit mapi and 64 bit migration perfect
                        }
                        else
                        {
                            if (InstallPath == "x86")
                            {
                                //32 bit mapi and 64 bit migration cannot continue
                                str = "The 64 bit Migration wizard is not compatible with MAPI 32 bit libraries.  Please run the 32 bit Migration wizard.";
                                return str;
                            }
                        }
                    }
                    else
                    {
                        if (InstallPath == "x64")
                        {
                            //64 bit mapi and 32 bit bit migration cannot continue
                            str = "The 32 bit Migration wizard is not compatible with MAPI 64 bit libraries.  Please run the 64 bit Migration wizard.";
                            return str;
                        }
                        else
                        {
                            if (InstallPath == "x86")
                            {
                                //32 bit mapi and 32 bit migration perfect
                            }
                        }
                    }
                }
                else
                {
                    if (bitness == true)
                    {
                        //32 bit mapi and 64 bitmigration
                        str = "Older versions of Outlook are not compatible with the 64 bit Migration wizard.  Please run the 32 bit Migration wizard.";
                        return str;
                    }
                    else
                    {
                        //32 bit mapi and 32 bit migration.
                    }
                }
                return str;
            }
        }

        public MapiMigration()
        {
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock("MapiMigration.MapiMigration"))
            {
                string message = MapiMigration.checkPrereqs();
                if (message == "")
                // try
                {
                    MapiWrapper = new Exchange.MapiWrapper();   // Defined in Exchange.idl See DCB_NOTE_MAPIWRAPPER_DEF
                }
                else
                // catch(Exception )
                {
                    //Log.err("Exception in CSMigrationWrapper construcor", message);
                    // throw new Exception(message);

                    throw new Exception(message);
                }
            }
        }

        public override string GlobalInit(string Target, string AdminUser, string AdminPassword)
        {
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                string s = "";
                try
                {
                    s = MapiWrapper.GlobalInit(Target, AdminUser, AdminPassword);
                }
                catch (Exception e)
                {
                    s = string.Format("Initialization Exception. Make sure to enter the proper credentials: {0}", e.Message);
                }
                return s;
            }
        }

        public override string GlobalUninit()
        {
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                try
                {
                    return MapiWrapper.GlobalUninit();
                }
                catch (Exception e)
                {
                    string msg = string.Format("GlobalUninit Exception: {0}", e.Message);
                    return msg;
                }
            }
        }

        public override bool AvoidInternalErrors(string strErr)
        {
            return (MapiWrapper.AvoidInternalErrors(strErr)!=0);
        }

        public override string GetProfilelist(out object  var)
        {
            return MapiWrapper.GetProfilelist(out var);
        }

        public override string SelectExchangeUsers(out object var)
        {
            return MapiWrapper.SelectExchangeUsers(out var);
        }
    }

    public class MapiUser : MigrationUser
    {
        public Exchange.UserObject UserObj;
        public Exchange.UserObject GetInternalUser()
        {
            return UserObj;
        }

        public MapiUser()
        {
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock("MapiUser.MapiUser"))
            {
                UserObj = new Exchange.UserObject();
            }
        }

        public override string Init(string host, string AccountID, string accountName,int Publicflag)
        {
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                string value = "";
                try
                {
                    // Tip: Following calls unmanaged C++ function CUserObject::Init()
                    // in UserObject.cpp. To step into it make sure unmanaged debugging enabled
                    // Solution Explorer -> Right click ZimbraMigration -> Properties -> Debug -> Enable unmanaged code debugging
                    value = UserObj.Init(host, AccountID, accountName, Publicflag);
                }
                catch (Exception e)
                {
                    Log.err("MAPIMigration: Exception in UserObj.init", e.Message);
                }
                return value;
            }
        }

        public override dynamic[] GetFolders()
        {
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                dynamic[] folders = null;
                try
                {
                    folders = UserObj.GetFolders();
                }
                catch (Exception e)
                {
                    Log.err("MAPIMigration::Getfolders exception", e.Message);
                }
                return folders;
            }
        }

        public override dynamic[] GetItemsForFolder(dynamic folderobject, double Date)
        {
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                dynamic[] Itemsfolder = null;
                try
                {
                    Itemsfolder = UserObj.GetItemsForFolder(folderobject, Date);
                }
                catch (Exception e)
                {
                    Log.err("MAPIMigration::Getfolders exception", e.Message);
                }
                return Itemsfolder;
            }
        }

        public override string[,] GetRules()
        {
            try
            {
                return UserObj.GetRules();
            }
            catch (Exception e)
            {
                Log.err("MAPIMigration: Exception caught in GetRules", e.Message);
                return null;
            }
        }
        public override string GetOOO()
        {
            return UserObj.GetOOO();
        }

        public override void Uninit()
        {
            try
            {
                UserObj.Uninit();
            }
            catch(Exception e)
            {
                 Log.err("MAPIMigration: Exception caught in Uninit", e.Message);
            }
        }
    }

    /*class MapiItem : MigrationItem
    {
        public dynamic ItemObj;
        public dynamic ItemID()
        {
            return ItemObj.ItemID;
        }
        public dynamic Type()
        {
            return ItemObj.Type;
        }
        public MapiItem()
        {
             ItemObj= new Exchange.ItemObject();

        }
        public override string[,]  GetDataForItemID(dynamic userobj, dynamic ItemId, dynamic type)
        {
            return ItemObj.GetDataForItemID( userobj,  ItemId,  type);

        }

    }*/
}
