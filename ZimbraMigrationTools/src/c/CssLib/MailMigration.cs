﻿/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CssLib
{
    public abstract class MailMigration
    {
       public abstract  string GlobalInit(string Target, string AdminUser, string AdminPassword);
       public abstract string GlobalUninit();

       public abstract string GetProfilelist(out object var);
       public abstract string SelectExchangeUsers(out object var);
       public abstract bool AvoidInternalErrors(string strErr);
    }

    public abstract class MigrationUser
    {
       public abstract string Init(string host, string AccountID, string accountName,int Publicflag);

       public abstract dynamic[] GetFolders();

       public abstract dynamic[] GetItemsForFolder(dynamic folderobject, double Date);

       public abstract string[,]  GetRules();
       public abstract string GetOOO();

       public abstract void Uninit();

    }

   /*public  abstract class MigrationItem
    {

       public abstract string[,] GetDataForItemID(dynamic userobj, dynamic ItemId, dynamic type);
    }*/

   
}
