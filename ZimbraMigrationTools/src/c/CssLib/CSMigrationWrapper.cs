/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections;
using System.Xml;

namespace CssLib
{


    [Flags] public enum ItemsAndFoldersOptions
    {
        OOO = 0x100, Junk = 0x0080, DeletedItems = 0x0040, Sent = 0x0020, Rules = 0x0010,
        Tasks = 0x0008, Calendar = 0x0004, Contacts = 0x0002, Mail = 0x0001, None = 0x0000
    }

    public enum ZimbraFolders
    {
        Min = 0, UserRoot = 1, Inbox = 2, Trash = 3, Junk = 4, Sent = 5, Drafts = 6,
        Contacts = 7, Tags = 8, Conversations = 9, Calendar = 10, MailboxRoot = 11, Wiki = 12,
        EmailedContacts = 13, Chats = 14, Tasks = 15, Max = 16
    }

    public class MigrationOptions
    {
        public ItemsAndFoldersOptions ItemsAndFolders;

        public string DateFilter;
        public string MessageSizeFilter;
        public string SkipFolders;
        public bool SkipPrevMigrated;
        public Int32 MaxErrorCnt;
        public string SpecialCharRep;
        public bool IsMaintainenceMode;
        public long LangID;
        public Int32 MaxRetries;
        public string DateFilterItem;
        public bool IsPublicFolders;
        public bool IsZDesktop;
    }

    public class CSMigrationWrapper
    {
        enum ItemType
        {
            Mail = 1, Contacts = 2, Calendar = 3, Task = 4, MeetingReq = 5
        };

        string m_MailClient;
        public string MailClient
        {
            get { return m_MailClient; }
            set { m_MailClient = value; }
        }

        dynamic MailWrapper;

        dynamic m_umUser = null; // used to store user object so Exit can do a user.Uninit
        public dynamic UmUser
        {
            get { return m_umUser; }
            set { m_umUser = value; }
        }

        // TESTPOINT_MIGRATION_INSTANTIATE_MIGRATION_WRAPPER
        public CSMigrationWrapper(string mailClient)
        {
            // try
            {
                Log.info("Initializing MailClient");

                MailClient = mailClient;
                if (MailClient == "MAPI")
                    MailWrapper = new MapiMigration();
            }
            /* catch (Exception e)
            {
                Log.err("Exception in CSMigrationWrapper constructor", e.Message);
            }*/
        }

        public string GlobalInit(string Target, string AdminUser, string AdminPassword)
        {
            using (LogBlock logblock = Log.NotTracing()?null: Log.NotTracing() ? null : new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                string s = "";
                try
                {
                    Log.mem(">GlobalInit");
                    s = MailWrapper.GlobalInit(Target, AdminUser, AdminPassword);
                    Log.mem("<GlobalInit");
                }
                catch (Exception e)
                {
                    s = string.Format("Initialization Exception. Make sure to enter the proper credentials: {0}", e.Message);
                }
                return s;
            }
        }

        public string GlobalUninit()
        {
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                try
                {
                    return MailWrapper.GlobalUninit();
                }
                catch (Exception e)
                {
                    string msg = string.Format("GetListofMapiProfiles Exception: {0}", e.Message);
                    return msg;
                }
            }
        }

        public bool AvoidInternalErrors(string strErr)
        {
            return MailWrapper.AvoidInternalErrors(strErr);
        }

        public string[] GetListofMapiProfiles()
        {
            object var = new object();
            string msg = "";
            string[] s = { "" };

            try
            {
                msg = MailWrapper.GetProfilelist(out var);
                s = (string[])var;
            }
            catch (Exception e)
            {
                // FBS bug 73020 -- 4/18/12
                string tmp = msg;

                msg = string.Format("GetListofMapiProfiles Exception: {0}", e.Message);
                if (tmp.Length > 0)
                {
                    if (tmp.Substring(0, 11) == "No profiles")
                    {
                        msg = "No profiles";
                    }
                }
                s[0] = msg;
            }
            return s;
        }

        public string[] GetListFromObjectPicker()
        {
            // Change this to above signature when I start getting the real ObjectPicker object back
            string[] s = { "" };
            string status = "";
            object var = new object();

            try
            {
                status = MailWrapper.SelectExchangeUsers(out var);
                s = (string[])var;
            }
            catch (Exception e)
            {
                status = string.Format("GetListFromObjectPicker Exception: {0}", e.Message);
                s[0] = status;
            }
            return s;
        }

        private bool SkipFolder(MigrationOptions options, List<string> skipList, dynamic folder)
        {
            //using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                // Note that Rules and OOO do not apply here

                if (folder.ZimbraSpecialFolderId == (int)ZimbraFolders.Junk &&
                    (options.ItemsAndFolders.ToString().Contains("Junk")))
                {
                    return false;
                }


                if ((folder.ZimbraSpecialFolderId == (int)ZimbraFolders.Calendar && !options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Calendar))      ||
                    (folder.ZimbraSpecialFolderId == (int)ZimbraFolders.Contacts && !options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Contacts))      ||

                    (folder.ZimbraSpecialFolderId == (int)ZimbraFolders.Junk     && !options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Junk))          ||
                    (folder.ZimbraSpecialFolderId == (int)ZimbraFolders.Junk     && !options.ItemsAndFolders.ToString().Contains("Junk"))                   ||

                    (folder.ZimbraSpecialFolderId == (int)ZimbraFolders.Sent     && !options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Sent))          ||
                    (folder.ZimbraSpecialFolderId == (int)ZimbraFolders.Tasks    && !options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Tasks))         ||
                    (folder.ZimbraSpecialFolderId == (int)ZimbraFolders.Trash    && !options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.DeletedItems))  ||
                    // FBS NOTE THAT THESE ARE EXCHANGE SPECIFIC and need to be removed
                    (folder.ContainerClass == "IPF.Contact"                      && !options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Contacts))      ||
                    (folder.ContainerClass == "IPF.Appointment"                  && !options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Calendar))      ||
                    (folder.ContainerClass == "IPF.Task"                         && !options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Tasks))         ||
                    (folder.ContainerClass == "IPF.Note"                         && !options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Mail))          ||
                    (folder.ContainerClass == "" /*no class->assume IPF.Note*/   && !options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Mail)))
                {
                    return true;
                }

                for (int i = 0; i < skipList.Count; i++)
                {
                    if (string.Compare(folder.Name, skipList[i], StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        // TODO - this is Exchange specific - should use folder ids
        private string GetFolderViewType(string containerClass)
        {
            string retval = "";                     // if it's a "message", blanks are cool

            if (containerClass == "IPF.Contact")
                retval = "contact";
            else
            if (containerClass == "IPF.Appointment")
                retval = "appointment";
            else
            if (containerClass == "IPF.Task")
                retval = "task";

            return retval;
        }

        
        private string TagNamesToTagIDs(string sTagNames, MigrationAccount acct, ZimbraAPI api)
        //
        // sTagNames is comma-sep list of tag names (returned by C++ layer)
        //
        // if the tag has already been created, just return it; if not, do the req and create it
        //
        {
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                string sTagIDs = "";
                string[] tokens = sTagNames.Split(',');
                for (int i = 0; i < tokens.Length; i++)
                {
                    if (i > 0)
                        sTagIDs += ",";

                    string sTagName = tokens.GetValue(i).ToString();

                    // See if key already exists. Tag names in acct.TagDict are mixed case, but we need case-insensitive
                    // because on ZCS, tag "GREEN" == "green"
                    bool bAlreadyExists = false;
                    string sTagID = "";
                    foreach (var keypair in acct.TagDict)
                    {
                        if (keypair.Key.ToString().ToUpper() == sTagName.ToUpper())
                        {
                            bAlreadyExists = true;
                            sTagID = keypair.Value;
                            break;
                        }
                    }


                    if (bAlreadyExists)
                        sTagIDs += sTagID;
                    else
                    {
                        // Color starts at 0, will keep bumping up by 1 (we don't yet support migrating correct color)
                        string sColour = (acct.TagDict.Count).ToString(); 

                        // Create tag on server - returns TagID
                        sTagID = "";
                        int stat = api.CreateTag(sTagName, sColour, out sTagID);
                        Log.info("Created tag '", sTagName, "' (", sTagID, ")");

                        // add to existing dict; token is Key, tokenNumstr is Val
                        acct.TagDict.Add(sTagName, sTagID);   
                        sTagIDs += sTagID;
                    }
                }
                return sTagIDs;
            }
        }

        private bool ShouldProcessType(MigrationOptions options, ItemType type)
        {
            //using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                bool retval = false;

                switch (type)
                {
                    case ItemType.Mail:
                    case ItemType.MeetingReq:
                        retval = options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Mail);
                        break;

                    case ItemType.Calendar:
                        retval = options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Calendar);
                        break;

                    case ItemType.Contacts:
                        retval = options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Contacts);
                        break;

                    case ItemType.Task:
                        retval = options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Tasks);
                        break;

                    default:
                        break;
                }
                return retval;
            }
        }

        private void VarArrayToCsDict(string[,] data, Dictionary<string,string> dict)
        {
            // C++ data is packaged up as a variant array for passing back to C# layer by CMapiAccessWrap::CppDictToVarArray() 
            // This method takes that array and converts to C# dictionary
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType()+"."+System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                int bound = data.GetUpperBound(1);
                if (bound > 0)
                {
                    for (int i = 0; i <= bound; i++)
                    {
                        string Key = data[0, i];
                        string Value = data[1, i];

                        try
                        {
                            dict.Add(Key, Value);
                            // Console.WriteLine("{0}, {1}", so1, so2);
                        }
                        catch (Exception e)
                        {
                            string s = string.Format("Exception adding {0}/{1}: {2}", Key, Value, e.Message);
                            Log.warn(s);
                            // Console.WriteLine("{0}, {1}", so1, so2);
                        }
                    } // for
                }
            }
        }

        private void ReclaimMemory()
        {
            Log.mem(">Collect");
            GC.Collect();
            GC.WaitForPendingFinalizers();
            Log.mem("<Collect");
            //System.Threading.Thread.Sleep(5000);
        }

        private void PushFolderItems(MigrationAccount Acct, bool isServer, dynamic user, dynamic folder, ZimbraAPI api, string sPathOfFolderBeingMigrated, MigrationOptions options)
        {
            // TESTPOINT_MIGRATION_PUSH_FOLDER_ITEMS Push items for single folder to ZCS
            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType()+"."+System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                int trial = 0;

                do
                {
                    Acct.IsCompletedMigration = true;
                    trial++;

                    Log.summary(" ");
                    Log.summary("===================================================================================================");
                    Log.summary("Processing folder '" + folder.folderPath + "'");
                    Log.summary("===================================================================================================");

                    // ================================================================================
                    // Get the folder's contained items
                    // ================================================================================
                    Log.mem("PushFolderItems > GetItemsForFolder");
                    Log.summary("Getting items list...");
                    dynamic[] itemobjectarray = null;
                    try
                    {
                        DateTime dtUtcNow = DateTime.UtcNow;
                        itemobjectarray = user.GetItemsForFolder(folder, dtUtcNow.ToOADate());
                    }
                    catch (Exception e)
                    {
                        Log.err("exception in PushFolderItems->user.GetItemsFolder", e.Message);
                    }
                    Log.mem("PushFolderItems < GetItemsForFolder");



                    // ================================================================================
                    // Process the contained items
                    // ================================================================================
                    if (itemobjectarray != null)
                    {
                        int iProcessedItems = 0;
                        string historyfile = Path.GetTempPath() + Acct.AccountName.Substring(0, Acct.AccountName.IndexOf('@')) + "history.log";
                        string historyid = "";


                        // Make sure there are some items to process in this folder
                        if (itemobjectarray.GetLength(0) > 0)
                        {
                            // DCB_BUG_97312
                            //
                            // "TotalCountOfItems" is set from the original GetFolderList call and "itemobjectarray.Count()" is obtained by
                            // requesting this folder's items. They should be the same.
                            //
                            // However, it appears from comment on changelist 425346 that they differ in some unknown case for Realogy.
                            // When they differ:
                            // - we loop to an upper bound of itemobjectarray.Count() rathr than Acct.CurrFolder.TotalCountOfItems (probably we should always do this!)
                            // - We add even failed items to the history list.
                            //
                            // I'm not sure why it was done this way - seems questionable to me - but I am simply retaining the behavior when
                            // we had 2 while loops.

                            bool bWriteHistoryEvenIfError = false;
                            long nUpperWhileBound = Acct.CurrFolder.NumItemsToMigrate;

                            if (Acct.CurrFolder.NumItemsToMigrate != Acct.CurrFolder.NumItemsToMigrate)
                            {
                                bWriteHistoryEvenIfError = true;
                                nUpperWhileBound = itemobjectarray.Count();

                                Log.warn(" Itemobjectarray.count is not equal to MigrationFolder.totalcountofitems ", Acct.CurrFolder.NumItemsToMigrate, Acct.CurrFolder.NumItemsToMigrate);
                                string errMesg = "MAPI Could not be returning all the items for the folder - Migration is not complete. Please re-run migration for the user";
                                Acct.NumErrs++;
                                Log.err("CSmigrationwrapper MAPI edgecase", errMesg);

                                Acct.LastProblemInfo = new ProblemInfo(Acct, Acct.AccountName, errMesg, ProblemInfo.TYPE_ERR);
                                Acct.IsCompletedMigration = false;
                                options.SkipPrevMigrated = true;
                            }




                            // ==================================================================================================
                            // ==================================================================================================
                            // Loop until done all items
                            // ==================================================================================================
                            // ==================================================================================================
                            Log.summary("Pushing folder items...");
                            while (iProcessedItems < nUpperWhileBound)
                            {
                                foreach (dynamic itemobject in itemobjectarray)
                                {
                                    // Exit if max permissable error count reached
                                    if (options.MaxErrorCnt > 0)
                                    {
                                        if (Acct.NumErrs > options.MaxErrorCnt)
                                        {
                                            Log.err("Cancelling migration -- error threshold reached");
                                            return;
                                        }
                                    }

                                    Log.trace(" ");
                                    Log.trace(" ");
                                    Log.trace("----------------------------------------------------------------------------------------------------------------------------------------------------");
                                    Log.summary("Processing item " + (iProcessedItems + 1) + " of " +  nUpperWhileBound + " '" + itemobject.Subject +"'");
                                    Log.trace("----------------------------------------------------------------------------------------------------------------------------------------------------");


                                    // ==================================================================================================
                                    // Decide if we should push this item
                                    // ==================================================================================================
                                    // - Type filter
                                    // - Already migrated
                                    // - Size filer
                                    // - Date filter
                                    bool bError = false;

                                    // -------------------------------------------------------
                                    // Type Filter
                                    // -------------------------------------------------------
                                    ItemType nItemType = (ItemType)itemobject.Type;
                                    bool bShouldProcessType = ShouldProcessType(options, nItemType);
                                    if (bShouldProcessType)
                                    {
                                        if (options.IsMaintainenceMode)
                                        {
                                            Log.err("Cancelling migration -- Mailbox is in maintenance mode. Try back later");
                                            return;
                                        }

                                        bool bSkipMessage = false;

                                        // --------------------------------------------------------
                                        // History filter
                                        // --------------------------------------------------------
                                        string sItemType = nItemType.ToString();

                                        try
                                        {
                                            string hex = BitConverter.ToString(itemobject.ItemID);
                                            hex = hex.Replace("-", "");
                                            historyid = sItemType + hex;
                                        }
                                        catch (Exception e)
                                        {
                                            Log.err("exception in Bitconverter converting itemid to a hexstring", e.Message);
                                        }

                                        // Already in history (already pushed in previous session?)
                                        if (options.SkipPrevMigrated)
                                        {
                                            if (historyid != "")
                                            {
                                                Log.trace("Checking if already migrated");
                                                if (CheckifAlreadyMigrated(historyfile, historyid))
                                                {
                                                    bSkipMessage = true;
                                                    iProcessedItems++;
                                                    continue;
                                                }
                                            }
                                        }


                                        // --------------------------------------------------------
                                        // Date and Size filter currently done AFTER fetching full item
                                        // --------------------------------------------------------




                                        // ===========================================================================================================================
                                        // DCB_BUG_100750 Mem leak test: Repeatedly pull large item from C++ layer. Test this with a PST containing a 50Mb message, 
                                        // e.g. with 5 x 10MB attachments, and watch mem trace using process explorer.
                                        // Result: provided garbage is collected, it runs find for hours. Without, it often runs out of mem.
                                        /*
                                            {
                                                Log.mem("BASE");
                                                for (int i = 1; i < 1000000; i++)
                                                {
                                                    {
                                                        Log.mem(">GetDataForItemID " + i);
                                                        string[,] dataTest = itemobject.GetDataForItemID(user.GetInternalUser(), itemobject.ItemID, itemobject.Type);
                                                        Log.mem("<GetDataForItemID " + i);

                                                    
                                                        Dictionary<string, string> dictTest = new Dictionary<string, string>();

                                                        VarArrayToCsDict(dataTest, dictTest);
                                                        string s = dataTest[0, 1];
                                                        Log.summary(s);
                                                    
                                                    }
                                                    //GCSettings.LargeObjectHeapCompactionMode = GCLargeObjectHeapCompactionMode.CompactOnce; // Not available :-(
                                                    GC.Collect();
                                                    Log.mem("Item " + i + " AFTER COLLECT");
                                                }
                                            }
                                        */
                                        // ===========================================================================================================================

                                        
                                        bool bLastItemWasLarge = false;
                                        int nItemsSinceLastFreeMem = 0;
                                        {
                                            // -----------------------------------------------------------
                                            // Grab its full data set into "data" (a 2 dim variant array)
                                            // -----------------------------------------------------------
                                            string[,] data = null;
                                            bLastItemWasLarge = false;

                                            try
                                            {
                                                Log.trace(" ");
                                                Log.trace("--------------------");
                                                Log.trace("Getting item data...");
                                                Log.trace("--------------------");

                                                Log.mem("PushFolderItems > GetDataForItemID");
                                                data = itemobject.GetDataForItemID(user.GetInternalUser(), itemobject.ItemID, itemobject.Type); // data is freed by garbage collector
                                                Log.mem("PushFolderItems < GetDataForItemID");
                                            }
                                            catch (Exception e)
                                            {
                                                Log.err("exception in PushFolderItems->itemobject.GetDataForItemID", e.Message);
                                                iProcessedItems++;
                                                continue;
                                            }
                                            if (data == null)
                                            {
                                                iProcessedItems++;
                                                continue;
                                            }

                                            nItemsSinceLastFreeMem++;

                                            // ---------------------------------------------------------------------------
                                            // Convert "data" from string[,] to dict
                                            // ---------------------------------------------------------------------------
                                            Log.trace(" ");
                                            Log.trace("--------------------");
                                            Log.trace("Populating dict");
                                            Log.trace("--------------------");

                                            // -----------------------------------------
                                            // Create a dict to hold the item's data
                                            // -----------------------------------------
                                            Dictionary<string, string> dict = new Dictionary<string, string>();

                                            //Not significant Log.mem("PushFolderItems > VarArrayToCsDict");
                                            VarArrayToCsDict(data, dict);
                                            //Not significant Log.mem("PushFolderItems < VarArrayToCsDict");

                                            // "dict" now contains this item's data ready for pushing

                                            // ---------------------------------------------------------------------------
                                            // Log the dictionary
                                            // ---------------------------------------------------------------------------
                                            // if (IsLoggingAtVerbose)
                                            var list = dict.Keys.ToList();
                                            list.Sort();
                                            foreach (var key in list)
                                            {
                                                if (key != "wstrmimeBuffer")
                                                    Log.verbose(key.PadRight(40), dict[key]);
                                            }
                                        
                                            int nMimeSize = 0;
                                            if (dict.ContainsKey("MimeSize"))
                                            {
                                                nMimeSize = Int32.Parse(dict["MimeSize"]);
                                                if (nMimeSize > 5*1024*1024 /*5Mb*/)
                                                {
                                                    Log.trace("Large mime (" + dict["MimeSize"] + " bytes)");
                                                    bLastItemWasLarge = true;
                                                }
                                            }

                                            api.AccountID = Acct.AccountID;
                                            api.AccountName = Acct.AccountName;
                                            if (dict.Count > 0)
                                            {
                                                int stat = 0;


                                                // ===========================================================================
                                                // ItemType-dependent code
                                                // ===========================================================================
                                                if ((nItemType == ItemType.Mail) || (nItemType == ItemType.MeetingReq))
                                                {
                                                    // ***************************************************************
                                                    // Mail or MeetingReq
                                                    // ***************************************************************
                                                    Log.trace("Mail/MeetingRqst");

                                                    // Steps:
                                                    // - Check if it should be skipped due to size filter
                                                    // - Check if it should be skipped due to date filter
                                                    // - convert tag names
                                                    // - create item on server
                                                    // - add to history file if success


                                                    // --------------------------------
                                                    // Check if skip due to size filter  <- Note that size filter is based on MIME size, not PR_MESSAGE_SIZE
                                                    // --------------------------------
                                                    int msf = 0;
                                                    if (options.MessageSizeFilter != null)
                                                    {
                                                        if (options.MessageSizeFilter.Length > 0)
                                                        {
                                                            msf = Int32.Parse(options.MessageSizeFilter);
                                                            msf *= 1000000;
                                                            if (nMimeSize > msf)    // FBS bug 74000 -- 5/14/12 
                                                            {
                                                                bSkipMessage = true;
                                                                Log.summary("  Skipping because size ("+ nMimeSize + " bytes) exceeds size filter ("+ options.MessageSizeFilter+" MB)");
                                                            }
                                                        }
                                                    }

                                                    // --------------------------------
                                                    // Check if skip due to date filter
                                                    // --------------------------------
                                                    if (options.DateFilter != null)
                                                    {
                                                        try
                                                        {
                                                            DateTime dtItem = DateTime.Parse(dict["Date"]);
                                                            DateTime dtFilter = Convert.ToDateTime(options.DateFilter);
                                                            if (options.DateFilterItem != null)
                                                            {
                                                                if ((DateTime.Compare(dtItem, dtFilter) < 0) && (options.DateFilterItem.Contains(sItemType)))
                                                                    bSkipMessage = true;
                                                            }
                                                            else
                                                            {
                                                                if ((DateTime.Compare(dtItem, dtFilter) < 0))
                                                                    bSkipMessage = true;
                                                            }
                                                            if (bSkipMessage)
                                                                Log.summary("  Skipping because date (" + dtItem + ") precedes date filter (" + dtFilter + ")");
                                                        }
                                                        catch (Exception)
                                                        {
                                                            Log.info(dict["Subject"], ": unable to parse date");
                                                            bError = true;
                                                        }
                                                    }

                                                    if (!bSkipMessage)
                                                    {
                                                        Log.trace(" ");
                                                        Log.trace("----------------------------");
                                                        Log.trace("Creating destination msg...");
                                                        Log.trace("----------------------------");

                                                        if (dict["tags"].Length > 0)
                                                        {
                                                            // change the tag names into tag numbers for AddMessage
                                                            string tagsNumstrs = TagNamesToTagIDs(dict["tags"], Acct, api);
                                                            bool bRet = dict.Remove("tags");
                                                            dict.Add("tags", tagsNumstrs);
                                                        }





                                                        // =======================================================================
                                                        // Sort out folderid
                                                        // =======================================================================
                                                        //  All type below except AddMsg pass in "sPathOfFolderBeingMigrated". Why
                                                        // is AddMsg different? It seems that it might have been changed to support ZD.
                                                        if (options.IsZDesktop)
                                                        {
                                                            //ZDPST
                                                            string Fpath = folder.FolderPath;
                                                            if (Fpath.Contains("MAPIRoot"))
                                                            {
                                                                //path = Fpath.Replace("MAPIRoot", "PSTData");
                                                                Log.verbose("Zimbrad Desktop file paths");
                                                                string Zdpath = Acct.AccountID.Substring(Acct.AccountID.LastIndexOf('\\') + 1);

                                                                sPathOfFolderBeingMigrated = Fpath.Replace("MAPIRoot", Zdpath);
                                                            }

                                                            string sFolderId = api.FolderPathToFolderId(sPathOfFolderBeingMigrated);
                                                            dict.Add("folderId", sFolderId);

                                                        }
                                                        else
                                                        {
                                                            string sFolderId = api.FolderPathToFolderId(folder.FolderPath);
                                                            if (sFolderId=="")
                                                                Log.warn("folderId is empty, using root (0)");
                                                            dict.Add("folderId", sFolderId);
                                                        }
                                                        // =======================================================================
                                                        // =======================================================================





                                                        // --------------------------------
                                                        // Create new msg on server
                                                        // --------------------------------
                                                        try
                                                        {
                                                            stat = api.AddMessage(dict); // doesnt pass in "sPathOfFolderBeingMigrated" like we do for other types
                                                            if (stat != 0)
                                                            {
                                                                string errMsg = (api.LastError.IndexOf("upload ID: null") != -1)    // FBS bug 75159 -- 6/7/12
                                                                                ? "Unable to upload file. Please check server message size limits."
                                                                                : api.LastError;
                                                                if (errMsg.Contains("maintenance") || errMsg.Contains("not active"))
                                                                {
                                                                    errMsg = errMsg + " Try Back later";
                                                                    options.IsMaintainenceMode = true;
                                                                }

                                                                Acct.LastProblemInfo = new ProblemInfo(Acct, dict["Subject"], errMsg, ProblemInfo.TYPE_ERR);
                                                                bError = true;
                                                            }
                                                        }
                                                        catch (Exception e)
                                                        {
                                                            Acct.NumErrs++;
                                                            Log.err("Exception caught in PushFolderItems->api.AddMessage", e.Message);
                                                            bError = true;
                                                        }
                                                    }
                                                }
                                                else
                                                if (nItemType == ItemType.Contacts)
                                                {
                                                    // ***************************************************************
                                                    // Contact
                                                    // ***************************************************************
                                                    Log.trace("Contact");

                                                    Log.trace(" ");
                                                    Log.trace("-------------------------------");
                                                    Log.trace("Creating destination contact...");
                                                    Log.trace("-------------------------------");

                                                    // Steps:
                                                    // - Check if it should be skipped due to size filter NOT FOR CONTACTS
                                                    // - Check if it should be skipped due to date filter NOT FOR CONTACTS
                                                    // - convert tag names
                                                    // - create item on server
                                                    // - add to history file if success

                                                    if (dict["tags"].Length > 0)
                                                    {
                                                        // change the tag names into tag numbers for CreateContact
                                                        string tagsNumstrs = TagNamesToTagIDs(dict["tags"], Acct, api);
                                                        bool bRet = dict.Remove("tags");
                                                        dict.Add("tags", tagsNumstrs);
                                                    }

                                                    // --------------------------------
                                                    // Create new contact on server
                                                    // --------------------------------
                                                    try
                                                    {
                                                        stat = api.CreateContact(dict, sPathOfFolderBeingMigrated);
                                                        if (stat != 0)
                                                        {
                                                            string errMsg = api.LastError;
                                                            if (errMsg.Contains("maintenance") || errMsg.Contains("not active"))
                                                            {
                                                                errMsg = errMsg + " Try Back later";

                                                                options.IsMaintainenceMode = true;
                                                            }
                                                            Log.err("Error in CreateContact ", errMsg);

                                                            Acct.NumErrs++;
                                                            bError = true;
                                                        }
                                                    }
                                                    catch (Exception e)
                                                    {
                                                        Acct.NumErrs++;
                                                        Log.err("Exception caught in PushFolderItems->api.CreateContact", e.Message);
                                                        bError = true;
                                                    }
                                                }
                                                else
                                                if (nItemType == ItemType.Calendar)
                                                {
                                                    // ***************************************************************
                                                    // Appt
                                                    // ***************************************************************
                                                    Log.trace("Appt");

                                                    // Steps:
                                                    // - Check if it should be skipped due to size filter NOT FOR CALENDAR
                                                    // - Check if it should be skipped due to date filter
                                                    // - convert tag names
                                                    // - create item on server
                                                    // - add to history file if success


                                                    // --------------------------------
                                                    // Check if skip due to date filter
                                                    // --------------------------------
                                                    if (options.DateFilter != null)
                                                    {
                                                        try
                                                        {
                                                            DateTime dtItem = DateTime.Parse(dict["sFilterDate"]);
                                                            DateTime dtFilter = Convert.ToDateTime(options.DateFilter);
                                                            if (options.DateFilterItem != null)
                                                            {
                                                                if ((DateTime.Compare(dtItem, dtFilter) < 0) && (options.DateFilterItem.Contains(sItemType)))
                                                                    bSkipMessage = true;
                                                            }
                                                            else
                                                            {
                                                                if ((DateTime.Compare(dtItem, dtFilter) < 0))
                                                                    bSkipMessage = true;
                                                            }
                                                            if (bSkipMessage)
                                                                Log.summary("  Skipping because appt (" + dtItem + ") date precedes date filter (" + dtFilter + ")");
                                                        }
                                                        catch (Exception)
                                                        {
                                                            Log.info(dict["su"], ": unable to parse date");
                                                            bError = true;
                                                        }
                                                    }

                                                    if (!bSkipMessage)
                                                    {
                                                        Log.trace(" ");
                                                        Log.trace("-------------------------------");
                                                        Log.trace("Creating destination appt...");
                                                        Log.trace("-------------------------------");
                                                        try
                                                        {
                                                            if (dict["tags"].Length > 0)
                                                            {
                                                                // change the tag names into tag numbers for AddAppointment
                                                                string tagsNumstrs = TagNamesToTagIDs(dict["tags"], Acct, api);
                                                                bool bRet = dict.Remove("tags");
                                                                dict.Add("tags", tagsNumstrs);
                                                            }
                                                            dict.Add("accountNum", Acct.AccountNum.ToString());

                                                            stat = api.AddAppointment(dict, sPathOfFolderBeingMigrated);
                                                            if (stat != 0)
                                                            {
                                                                string errMsg = api.LastError;
                                                                if (errMsg.Contains("maintenance") || errMsg.Contains("not active"))
                                                                {
                                                                    errMsg = errMsg + " Try Back later";
                                                                    options.IsMaintainenceMode = true;
                                                                }
                                                                Acct.LastProblemInfo = new ProblemInfo(Acct, dict["su"], errMsg, ProblemInfo.TYPE_ERR);
                                                                bError = true;
                                                            }
                                                        }
                                                        catch (Exception e)
                                                        {
                                                            Acct.LastProblemInfo = new ProblemInfo(Acct, dict["su"], e.Message, ProblemInfo.TYPE_ERR);
                                                            Log.err(dict["su"], "exception caught in PushFolderItems->api.AddAppointment", e.Message);
                                                            bError = true;
                                                        }
                                                    }
                                                }
                                                else
                                                if (nItemType == ItemType.Task)
                                                {
                                                    // ***************************************************************
                                                    // Task
                                                    // ***************************************************************
                                                    Log.trace("Task");

                                                    // Steps:
                                                    // - Check if it should be skipped due to size filter NOT FOR TASKS
                                                    // - Check if it should be skipped due to date filter
                                                    // - convert tag names
                                                    // - create item on server
                                                    // - add to history file if success

                                                    // --------------------------------
                                                    // Check if skip due to date filter
                                                    // --------------------------------
                                                    if (options.DateFilter != null)
                                                    {
                                                        try
                                                        {
                                                            string sReason = "";
                                                            DateTime dtItemFilter = DateTime.Parse(dict["sFilterDate"]);
                                                            DateTime dtItemStart =  DateTime.Parse(dict["s"]);
                                                            DateTime dtItemEnd =    DateTime.Parse(dict["e"]);
                                                            DateTime dtFilter = Convert.ToDateTime(options.DateFilter);

                                                            if (options.DateFilterItem != null)
                                                            {
                                                                if ((dtItemStart != null) && (dtItemEnd != null))
                                                                {
                                                                    if ((DateTime.Compare(dtItemFilter, dtFilter) < 0) && (options.DateFilterItem.Contains(sItemType)))
                                                                    {
                                                                        bSkipMessage = true;
                                                                        sReason = "task older than date filter value";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    bSkipMessage = true;
                                                                    sReason = "task has no start or due date";
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if ((dtItemStart != null) && (dtItemEnd != null))
                                                                {
                                                                    if ((DateTime.Compare(dtItemFilter, dtFilter) < 0))
                                                                    {
                                                                        bSkipMessage = true;
                                                                        sReason = "task older than date filter value";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    bSkipMessage = true;
                                                                    sReason = "task has no start or due date";
                                                                }
                                                            }
                                                            if (bSkipMessage)
                                                                Log.summary("  Skipping because "+ sReason);
                                                        }
                                                        catch (Exception)
                                                        {
                                                            Log.info(dict["su"], ": unable to parse date");
                                                            bError = true;
                                                        }
                                                    }

                                                    if (!bSkipMessage)
                                                    {
                                                        Log.trace(" ");
                                                        Log.trace("-------------------------------");
                                                        Log.trace("Creating destination task...");
                                                        Log.trace("-------------------------------");

                                                        if (dict["tags"].Length > 0)
                                                        {
                                                            // change the tag names into tag numbers for AddTask
                                                            string tagsNumstrs = TagNamesToTagIDs(dict["tags"], Acct, api);
                                                            bool bRet = dict.Remove("tags");
                                                            dict.Add("tags", tagsNumstrs);
                                                        }
                                                        dict.Add("accountNum", Acct.AccountNum.ToString());  // Bug 101136
 
                                                        try
                                                        {
                                                            stat = api.AddTask(dict, sPathOfFolderBeingMigrated);
                                                            if (stat != 0)
                                                            {
                                                                string errMsg = api.LastError;
                                                                if (errMsg.Contains("maintenance") || errMsg.Contains("not active"))
                                                                {
                                                                    errMsg = errMsg + " Try Back later";
                                                                    options.IsMaintainenceMode = true;
                                                                }

                                                                Log.err("error in AddTask ", errMsg);

                                                                Acct.NumErrs++;
                                                                bError = true;
                                                            }
                                                        }
                                                        catch (Exception e)
                                                        {
                                                            Acct.NumErrs++;
                                                            Log.err("exception caught in PushFolderItems->api.AddTask", e.Message);
                                                            bError = true;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Acct.LastProblemInfo = new ProblemInfo(Acct, Acct.AccountName, "Error on message", ProblemInfo.TYPE_ERR);
                                                bError = true;
                                            }

                                            if (!bError)
                                            {
                                                // Note the : statement.  It seems weird to set Acct.CurrFolder.NumItemsMigrated
                                                // to itself, but this is done so the method will be called to increment the progress bar
                                                Acct.CurrFolder.NumItemsMigrated = bSkipMessage
                                                    ? Acct.CurrFolder.NumItemsMigrated
                                                    : Acct.CurrFolder.NumItemsMigrated + 1;
                                            }

                                        }

                                        // Item processed - 
                                        bool bShouldFreeMem = bLastItemWasLarge || (nItemsSinceLastFreeMem > 100);
                                        if (bShouldFreeMem)
                                        {
                                            ReclaimMemory();
                                            nItemsSinceLastFreeMem = 0;
                                            //System.Threading.Thread.Sleep(5000);
                                        }

                                    }
                                    else
                                        Log.trace("Not processing items of this type");


                                    // ========================================== 
                                    // Add to history file to say we've pushed it
                                    // ========================================== 
                                    if ((historyid != "") && (!bError || bWriteHistoryEvenIfError))
                                    {
                                        try
                                        {
                                            File.AppendAllText(historyfile, historyid); //uncomment after more testing
                                            File.AppendAllText(historyfile, "\n");
                                        }
                                        catch (Exception e)
                                        {
                                            Acct.NumErrs++;
                                            Log.err("Exception caught in PushFolderItems writing history to the history file", e.Message);
                                        }
                                    }

                                    // Update processed count
                                    iProcessedItems++;

                                } // foreach (dynamic itemobject in itemobjectarray)

                            } // while (iProcessedItems < Acct.migrationFolder.TotalCountOfItems)
                            Log.summary("Pushing folder items complete");
                            Log.mem("folder items push complete");


                        } // if (itemobjectarray.GetLength(0) > 0)
                        else
                        {
                            // No items to process in this folder
                        }

                    } //if (itemobjectarray != null)
                    else
                    {
                        Log.err("GetItemsForFolder returned null for itemfolderlist");
                        return;
                    }

                    Log.trace("Trial # ", trial);


                } while ((!(Acct.IsCompletedMigration)) && (trial < options.MaxRetries));

                ReclaimMemory();

            } // LogBlock

        } // PushFolderItems

        public string[] GetPublicFolders(MigrationAccount Acct)
        {
            string accountName = "";
            dynamic[] folders = null;
            int idx = Acct.AccountName.IndexOf("@");
            dynamic user = null;
            string value = "";
            string[] s;


            if (MailClient == "MAPI")
                user = new MapiUser();

            if (idx == -1)
            {
                Acct.LastProblemInfo = new ProblemInfo(Acct, Acct.AccountName, "Illegal account name", ProblemInfo.TYPE_ERR);

                String status = string.Format("Error in Getpublicfolders");
                s = new string[1];
                s[0] = status;
                return s;
            }
            else
                accountName = Acct.AccountName.Substring(0, idx);

            try
            {
                value = user.Init("", Acct.ProfileName, accountName, 1);
            }
            catch (Exception e)
            {
                string serr = string.Format("Initialization Exception. {0}", e.Message);

                Acct.LastProblemInfo = new ProblemInfo(Acct, accountName, serr, ProblemInfo.TYPE_ERR);
                s = new string[1];
                s[0] = serr;
                return s;
            }
            Log.info("Account name", accountName);
            Log.info("Account Id", Acct.AccountID);
            Log.info("Account Num", Acct.AccountNum.ToString());

            if (value.Length > 0)
            {
                Acct.IsValid = false;
                if (value.Contains("Outlook"))
                {
                    Log.err("Unable to initialize", accountName, value);
                    Acct.LastProblemInfo = new ProblemInfo(Acct, accountName, value, ProblemInfo.TYPE_ERR);
                }
                else
                {
                    Log.err("Unable to initialize", accountName, value + "or verify if source mailbox exists.");
                    Acct.LastProblemInfo = new ProblemInfo(Acct, accountName, value + " Or Verify if source mailbox exists.", ProblemInfo.TYPE_ERR);
                }
                Acct.NumErrs++;
                //Acct.TotalErrors = Acct.MaxErrorCount;

                string serr = string.Format("Unable to initialize");
                s = new string[1];
                s[0] = serr;
                return s;
            }
            else
            {
                Acct.IsValid = true;
                Acct.IsCompletedMigration = true;
                Log.info("Account '" + accountName + "' initialized");
            }


            Log.trace("Retrieving folders");
            try
            {
                folders = user.GetFolders();
            }
            catch (Exception e)
            {
                Log.err("exception in startmigration->user.GetFolders", e.Message);

            }
            if (folders != null)
            {
                if (folders.Count() == 0)
                {

                    Log.info("No folders for user to migrate");
                    String status = string.Format("No folders for user to migrate");
                    s = new string[1];
                    s[0] = status;
                    return s;
                }
                else
                {
                    s = new string[folders.Count()];
                    int i = 0;
                    foreach (dynamic folder in folders)
                    {
                        s[i] = folder.Name;
                        i++;
                    }
                }
            }
            else
            {
                string serrs = "CSmigrationwrapper get folders returned null for folders";
                Acct.LastProblemInfo = new ProblemInfo(Acct, accountName, serrs, ProblemInfo.TYPE_ERR);

                Acct.IsCompletedMigration = false;
                Log.err(serrs);
                user.Uninit();
                s = new string[1];
                s[0] = serrs;
                return s;
            }

            try
            {
                user.Uninit();
            }
            catch (Exception e)
            {
                Log.err("Exception in user.Uninit ", e.Message);

            }

            return s;
        }

        public void StartMigration(MigrationAccount Acct,
                                   MigrationOptions options,
                                   bool isServer = true,
                                   bool isPreview = false,
                                   bool doRulesAndOOO = true)
        {
            // TESTPOINT_MIGRATION_MAIN_MIGRATION_METHOD Main migration method
            dynamic[] folders = null;
            dynamic user = null;

            options.IsMaintainenceMode = false;

            // =====================================================================================
            // Find the account name
            // =====================================================================================
            string accountName = "";
            int idx = Acct.AccountName.IndexOf("@");
            if (idx != -1)
            {
                accountName = Acct.AccountName.Substring(0, idx);
            }
            else
            {
                Acct.LastProblemInfo = new ProblemInfo(Acct, Acct.AccountName, "Illegal account name", ProblemInfo.TYPE_ERR);
                return;
            }

            using (LogBlock logblock = Log.NotTracing()?null: new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                // =====================================================================================
                // Log on to source mail system PST/EXCH/ZD
                // =====================================================================================
                // TESTPOINT_MIGRATION_LOGON_TO_SOURCE Logon to datasource
                if (MailClient == "MAPI")
                    user = new MapiUser();

                if (!isServer)
                    m_umUser = user;


                bool Zdesktop = options.IsZDesktop;

                Log.summary(" ");
                Log.summary("===================================================================================================================");
                Log.summary("===================================================================================================================");
                Log.summary("Initializing account: " + accountName + " (ID: " + Acct.AccountID + ", Num:" + Acct.AccountNum.ToString()+ ")");
                Log.summary("===================================================================================================================");
                Log.summary("===================================================================================================================");

                string sRetVal = "";

                try
                {
                    bool IsPublic = options.IsPublicFolders;
                    if (IsPublic)
                        sRetVal = user.Init("", Acct.ProfileName, accountName, 1);
                    else
                        sRetVal = user.Init(isServer ? "host" : "", Acct.AccountID, accountName, 0);
                }
                catch (Exception e)
                {
                    string s = string.Format("Initialization Exception. {0}", e.Message);
                    Acct.LastProblemInfo = new ProblemInfo(Acct, accountName, s, ProblemInfo.TYPE_ERR);
                    return;
                }


                if (sRetVal.Length == 0)
                {
                    Acct.IsValid = true;
                    Acct.IsCompletedMigration = true;
                    Log.info("Account '" + accountName + "' initialized successfully");
                }
                else
                {
                    Acct.IsValid = false;
                    if (sRetVal.Contains("Outlook"))
                    {
                        Log.err("Unable to initialize", accountName, sRetVal);
                        Acct.LastProblemInfo = new ProblemInfo(Acct, accountName, sRetVal, ProblemInfo.TYPE_ERR);
                    }
                    else
                    {
                        Log.err("Unable to initialize", accountName, sRetVal + "or verify if source mailbox exists.");
                        Acct.LastProblemInfo = new ProblemInfo(Acct, accountName, sRetVal + " Or Verify if source mailbox exists.", ProblemInfo.TYPE_ERR);
                    }
                    Acct.NumErrs++;
                    //Acct.TotalErrors = Acct.MaxErrorCount;
                    return;
                }

                // ------------------------------------
                // Set up check for skipping folders
                // ------------------------------------
                List<string> skipList = new List<string>();
                if (options.SkipFolders != null && options.SkipFolders.Length > 0)
                {
                    string[] tokens = options.SkipFolders.Split(',');

                    for (int i = 0; i < tokens.Length; i++)
                    {
                        string token = tokens.GetValue(i).ToString();
                        skipList.Add(token.Trim());
                    }
                }

                // =====================================================================================
                // Get all folders
                // =====================================================================================
                // TESTPOINT_MIGRATION_READ_FOLDERS Read folders from source mail system
                // This makes a managed->unmanaged COM call to obtain a vector containing ALL folders
                try
                {
                    Log.mem(">GetFolders");
                    Log.summary("Getting store's folder list...");
                    folders = user.GetFolders();
                    Log.mem("<GetFolders");
                }
                catch (Exception e)
                {
                    Log.err("exception in startmigration->user.GetFolders", e.Message);
                }

                if (folders != null)
                {
                    if (folders.Count() == 0)
                    {
                        Log.info("No folders for user to migrate");
                        return;
                    }
                }
                else
                {
                    string s = "CSmigrationwrapper get folders returned null for folders";
                    Acct.LastProblemInfo = new ProblemInfo(Acct, accountName, s, ProblemInfo.TYPE_ERR);
                    Acct.IsCompletedMigration = false;
                    Log.err(s);
                    user.Uninit();
                    return;
                }

                Acct.CurrFolder.NumItemsMigrated = folders.Count(); // Why? Set to 0 a few lines below.
                Log.trace("Retrieved folders.  Count:", Acct.CurrFolder.NumItemsMigrated.ToString());

                // Update Acct.TotalItems. Note that this will include items which might be later skipped (because already migrated)
                foreach (dynamic folder in folders)
                {
                    if (!SkipFolder(options, skipList, folder))
                        Acct.NumItems += folder.ItemCount;
                }
                Log.info("Total Items: ", Acct.NumItems.ToString());




                // =====================================================================================
                // Create ZimbraAPI (SOAP interface to ZCS)
                // =====================================================================================
                // TESTPOINT_MIGRATION_ZCS_LOGON Log on to ZCS
                ZimbraAPI api;
                if (options.LangID != 0)
                    api = new ZimbraAPI(isServer, options.SpecialCharRep, options.LangID);
                else
                    api = new ZimbraAPI(isServer, options.SpecialCharRep);

                api.AccountID = Acct.AccountID;
                api.AccountName = Acct.AccountName;


                // =====================================================================================
                // Send a GetTagRequest and save results to ZimbraValues
                // =====================================================================================
                Log.trace(" ");
                Log.trace(" ");
                Log.trace("===========================================================================================================");
                Log.info("Getting tags from destination account...");
                Log.trace("===========================================================================================================");
                api.GetTags(Acct);
                Log.trace(" ");
                
                Log.trace("Existing account Tags:");
                var list = Acct.TagDict.Keys.ToList();
                list.Sort();
                foreach (var key in list)
                     Log.trace(key.PadRight(40), Acct.TagDict[key]);


                if (!Zdesktop)
                {
                    // =====================================================================================
                    // Process one folder at a time
                    // =====================================================================================
                    // TESTPOINT_MIGRATION_WALK_FOLDERS Migrate each folder
                    foreach (dynamic folder in folders)
                    {
                        Log.info(" ");
                        Log.info(" ");
                        Log.info("===========================================================================================================");
                        Log.info ("Processing folder '" + folder.FolderPath + "' " + " SFID:" + folder.ZimbraSpecialFolderId);
                        Log.info("===========================================================================================================");

                        if (options.IsMaintainenceMode)
                        {
                            Log.err("Cancelling migration -- Mailbox is in maintainence  mode.try back later");
                            return;
                        }

                        if (options.MaxErrorCnt > 0)
                        {
                            if (Acct.NumErrs > options.MaxErrorCnt)
                            {
                                Log.err("Cancelling migration -- error threshold reached");
                                return;
                            }
                        }

                        if (SkipFolder(options, skipList, folder))
                        {
                            Log.info("Skipping folder '", folder.Name, "'");
                            continue;
                        }

                        // =====================================================================================
                        // First create the (empty) folder on ZCS
                        // =====================================================================================
                        // TESTPOINT_MIGRATION_CREATE_DEST_FOLDER Create destination folder
                        string ViewType = GetFolderViewType(folder.ContainerClass);
                        int stat = 0;
                        try
                        {
                            // Send CreateFolderRequest
                            stat = api.CreateFolder(folder.FolderPath, folder.ZimbraSpecialFolderId, ViewType, "", "", false);
                        }
                        catch (Exception e)
                        {
                            Log.err("Exception in api.CreateFolder in Startmigration ", e.Message);
                        }

                        // =====================================================================================
                        // Skip pushing contents of skipped folder 
                        // =====================================================================================
                        if (stat != 0)
                        {
                            Log.info("Skipping contents of skipped folder '", folder.Name, "'");
                            continue;
                        }

                        // =====================================================================================
                        // Skip pushing contents of empty folder 
                        // =====================================================================================
                        if (folder.ItemCount == 0)
                        {
                            Log.info("Skipping empty folder '", folder.Name, "'");
                            continue;
                        }

                        // =====================================================================================
                        // Initialize Acct.CurrFolder. Event handler on this updates the UI. 
                        // =====================================================================================

                        // Set FolderName at the end, since we trigger results on that, so we need all the values set
                        // All of these result in a call to ScheduleViewModel.Folder_OnChanged()
                        Acct.CurrFolder.NumItemsToMigrate      = folder.ItemCount;
                        Acct.CurrFolder.NumItemsMigrated       = 0;
                        Acct.CurrFolder.FolderView             = folder.ContainerClass;

                        // Causes a call to Folder_OnChanged() which causes new row to be added to results grid for this new folder
                        Acct.CurrFolder.FolderName             = folder.Name;


                        // =====================================================================================
                        // Now populate the folder items on the server
                        // =====================================================================================
                        if (!isPreview)
                            PushFolderItems(Acct, isServer, user, folder, api, folder.FolderPath, options);

                    } // foreach (dynamic folder in folders)

                } // if (!Zdesktop)
                else
                {
                    ////ZD changes
                    //string Upath = "/MAPIRoot/PSTData";
                    string ZDname = "";
                    if (Acct.AccountID.Contains(".pst"))
                    {
                        //ZDname = Acct.AccountID.Substring(0, Acct.AccountID.IndexOf(".pst"));
                        Log.info("CSmigrationwrapper in startmigration Zimbrad Desktop file paths");
                        ZDname = Acct.AccountID.Substring(Acct.AccountID.LastIndexOf('\\') + 1);

                        // int j = ZDname.LastIndexOf('\\');
                        //ZDname = ZDname.Substring(j+1);


                        string Upath = "/MAPIRoot/" + ZDname;
                        int stat01 = api.CreateFolder(Upath, 0/*folder.SpecialFolderId*/, "", "", "", true);
                    }

                    foreach (dynamic folder in folders)
                    {
                        Log.info("Folder Name : ", folder.Name);
                        Log.info("Folder Path : ", folder.FolderPath);
                        Log.info("SFID        : ", folder.ZimbraSpecialFolderId);
                        string path = "";

                        if (options.IsMaintainenceMode)
                        {
                            Log.err("Cancelling migration -- Mailbox is in maintainence  mode.try back later");
                            return;
                        }

                        if (options.MaxErrorCnt > 0)
                        {
                            if (Acct.NumErrs > options.MaxErrorCnt)
                            {
                                Log.err("Cancelling migration -- error threshold reached");
                                return;
                            }
                        }

                        if (SkipFolder(options, skipList, folder))
                        {
                            Log.info("Skipping folder", folder.Name);
                            continue;
                        }

                        string Fpath = folder.FolderPath;
                        if (Fpath.Contains("MAPIRoot"))
                        {
                            //path = Fpath.Replace("MAPIRoot", "PSTData");
                            path = Fpath.Replace("MAPIRoot", ZDname);
                        }

                        Log.info("Processing folder", folder.Name);

                        //if (folder.ZimbraSpecialFolderId == 0)
                        {
                            string ViewType = GetFolderViewType(folder.ContainerClass);
                            try
                            {
                                int stat = api.CreateFolder(path, folder.ZimbraSpecialFolderId, ViewType, "", "", true);
                            }
                            catch (Exception e)
                            {
                                Log.err("Exception in api.CreateFolder in Startmigration ", e.Message);
                            }
                        }

                        if (folder.ItemCount == 0)
                        {
                            Log.info("Skipping empty folder", folder.Name);
                            continue;
                        }

                        // Set FolderName at the end, since we trigger results on that, so we need all the values set
                        Acct.CurrFolder.NumItemsToMigrate   = folder.ItemCount;
                        Acct.CurrFolder.NumItemsMigrated    = 0;
                        Acct.CurrFolder.FolderView          = folder.ContainerClass;
                        Acct.CurrFolder.FolderName          = folder.Name;
                        if (folder.ZimbraSpecialFolderId == (int)ZimbraFolders.Trash)
                        {
                            path = "/MAPIRoot/Deleted Items";   // FBS EXCHANGE SPECIFIC HACK !!!
                        }

                        if (!isPreview)
                        {
                            PushFolderItems(Acct, isServer, user, folder, api, path, options);
                        }
                    }
                }

                // =====================================================================================
                // now do Rules
                // =====================================================================================
                Log.summary(" ");
                Log.summary(" ");
                Log.summary("===========================================================================================================");
                Log.summary("Processing Rules...");
                Log.summary("===========================================================================================================");
                if ((options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.Rules)) && (doRulesAndOOO))
                {
                    string[,] data = null;
                    try
                    {
                        data = user.GetRules();
                    }
                    catch (Exception e)
                    {
                        Log.err("Exception in StartMigration->user.Getrules", e.Message);
                    }

                    if (data != null)
                    {
                        Acct.NumItems++;

                        Acct.CurrFolder.NumItemsToMigrate = 1;
                        Acct.CurrFolder.NumItemsMigrated  = 0;
                        Acct.CurrFolder.FolderView = "All Rules";
                        Acct.CurrFolder.FolderName = "Rules Table";

                        api.AccountID = Acct.AccountID;
                        api.AccountName = Acct.AccountName;
                        if (!isPreview)
                        {
                            Dictionary<string, string> dict = new Dictionary<string, string>();
                            int bound0 = data.GetUpperBound(0);
                            if (bound0 > 0)
                            {
                                for (int i = 0; i <= bound0; i++)
                                {
                                    string Key = data[0, i];
                                    string Value = data[1, i];
                                    try
                                    {
                                        dict.Add(Key, Value);
                                    }
                                    catch (Exception e)
                                    {
                                        string s = string.Format("Exception adding {0}/{1}: {2}", Key, Value, e.Message);
                                        Log.warn(s);
                                        // Console.WriteLine("{0}, {1}", so1, so2);
                                    }
                                }
                            }
                            api.AccountID = Acct.AccountID;
                            api.AccountName = Acct.AccountName;
                            try
                            {
                                Log.summary("Migrating Rules");
                                int stat = api.AddRules(dict);
                            }
                            catch (Exception e)
                            {
                                Acct.NumErrs++;
                                Log.err("CSmigrationWrapper: Exception in AddRules ", e.Message);
                            }
                            Acct.CurrFolder.NumItemsMigrated = 1;
                        }
                    }
                    else
                    {
                        Log.summary("There are no rules to migrate");
                    }
                }

                // =====================================================================================
                // now do OOO
                // =====================================================================================
                Log.summary(" ");
                Log.summary(" ");
                Log.summary("===========================================================================================================");
                Log.summary("Processing OOO...");
                Log.summary("===========================================================================================================");
                if ((options.ItemsAndFolders.HasFlag(ItemsAndFoldersOptions.OOO)) && (doRulesAndOOO))
                {
                    bool isOOO = false;
                    string ooo = "";
                    try
                    {
                        ooo = user.GetOOO();
                    }
                    catch (Exception e)
                    {
                        Log.err("Exception in StartMigration->user.GetOOO", e.Message);
                    }
                    if (ooo.Length > 0)
                    {
                        isOOO = (ooo != "0:");
                    }

                    if (isOOO)
                    {
                        Acct.NumItems++;

                        Acct.CurrFolder.NumItemsToMigrate = 1;
                        Acct.CurrFolder.NumItemsMigrated = 0;
                        Acct.CurrFolder.FolderView = "OOO";
                        Acct.CurrFolder.FolderName = "Out of Office";

                        api.AccountID = Acct.AccountID;
                        api.AccountName = Acct.AccountName;
                        if (!isPreview)
                        {
                            Log.summary("Migrating Out of Office");
                            try
                            {
                                api.AddOOO(ooo, isServer);
                            }
                            catch (Exception e)
                            {
                                Acct.NumErrs++;
                                Log.err("CSmigrationWrapper: Exception in AddOOO ", e.Message);
                            }
                        }
                    }
                    else
                    {
                        Log.summary("Out of Office state is off, and there is no message");
                    }
                }


                Log.summary(" ");
                Log.summary(" ");
                Log.summary("===========================================================================================================");
                Log.summary("Cleaning up...");
                Log.summary("===========================================================================================================");
                try
                {
                    user.Uninit();
                }
                catch (Exception e)
                {
                    Log.err("Exception in user.Uninit ", e.Message);

                }
                if (!isServer)
                {
                    m_umUser = null;
                }

                Log.summary(" ");
                Log.summary(" ");
                Log.summary("===========================================================================================================");
                Log.summary("ACCOUNT MIGRATION COMPLETE");
                Log.summary("===========================================================================================================");

            } //LogBlock

        } // StartMigration

        private bool CheckifAlreadyMigrated(string filename, string itemid)
        {
            List<string> parsedData = new List<string>();

            try
            {
                if (File.Exists(filename))
                {
                    using (StreamReader readFile = new StreamReader(filename))
                    {
                        string line;
                        string row;
                        while ((line = readFile.ReadLine()) != null)
                        {
                            row = line;
                            if (row.CompareTo(itemid) == 0)
                            {
                                Log.trace("Already migrated (according to '", filename, "') -> skip");
                                return true;
                            }
                        }
                        readFile.Close();
                        return false;
                    }

                }
            }
            catch (Exception e)
            {
                Log.err("CSmigrationwrapper  CheckifAlreadyMigrated method errored out", e.Message);
            }

            return false;
        }
    }
}
