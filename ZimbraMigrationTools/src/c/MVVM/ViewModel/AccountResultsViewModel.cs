﻿/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */

using CssLib;
using MVVM.Model;
using Misc;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Input;
using System.Windows;
using System.Windows.Threading;
using System;

namespace MVVM.ViewModel
{
public class AccountResultsViewModel: BaseViewModel
{
    readonly AccountResults m_accountResults = new AccountResults();
    ScheduleViewModel m_scheduleViewModel;
    int m_accountnum;
    int m_AccountOnTab;

    public AccountResultsViewModel(ScheduleViewModel scheduleViewModel, int accountNum, string accountName, bool enableStop)
    {
        using (LogBlock logblock = Log.NotTracing()?null: new LogBlock("AccountResultsViewModel.AccountResultsViewModel"))
        {
            this.m_scheduleViewModel = scheduleViewModel;

            this.AccountName = accountName;
            this.m_accountnum = accountNum;
            this.EnableStop = enableStop;

            this.PBValue = 0;
            this.Status = "Queued";
            this.AcctProgress = "";

            this.NumErrs = 0;
            this.NumWarns = 0;

            this.CurrentItemNum = 0;
            this.TotalItemsToMigrate = 0;
            this.GlobalAcctProgressMsg = "";

            this.SelectedTab = "";
            this.m_AccountOnTab = -1;

            this.OpenLogFileCommand = new ActionCommand(this.OpenLogFile, () => true);
            this.StopCommand = new ActionCommand(this.Stop, () => true);
            this.ExitAppCommand = new ActionCommand(this.ExitApp, () => true);
        }
    }

    public ScheduleViewModel GetScheduleViewModel()
    {
        return m_scheduleViewModel;
    }

    public int GetAccountNum()
    {
        return m_accountnum;
    }

    // Commands
    public ICommand OpenLogFileCommand 
    {
        get;
        private set;
    }
    private void OpenLogFile()
    {
        using (LogBlock logblock = Log.NotTracing() ? null : new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
        {
            string logpath = Path.GetTempPath() + "ZimbraMigration\\Logs\\";

            string logwildcard;
            if (SelectedTab == "Accounts" || SelectedTab == "")
                logwildcard = "*Migration.log";
            else
                logwildcard = "*Migration " + SelectedTab + ".log";

            // Actual log will be prefixed with a datetime....and there might be several
            // so we need to search for most recent
            string[] files = Directory.GetFiles(logpath, logwildcard, SearchOption.TopDirectoryOnly);

            string filenameLatestLog = "";
            DateTime fileLatestCreateDate = new DateTime(1900, 1, 1, 0, 0, 0);
            for (int i = 0; i < files.Length; i++)
            {
                string file = files[i].ToString();

                DateTime fileCreatedDate = File.GetCreationTime(file);
                if (fileCreatedDate >= fileLatestCreateDate)
                {
                    // Found a newer log - use it
                    filenameLatestLog = file;
                    fileLatestCreateDate = fileCreatedDate;
                }
            } // for

            if (filenameLatestLog != "")
            {
                Log.info("Opening log " + filenameLatestLog);
                try
                {
                    Process.Start(filenameLatestLog);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                Log.warn("Can't find logfile");
        }
    }

    public ICommand StopCommand 
    {
        get;
        private set;
    }

    private void Stop()
    {
        using (LogBlock logblock = Log.NotTracing() ? null : new LogBlock(GetType() + "." + System.Reflection.MethodBase.GetCurrentMethod().Name))
        {
            for (int i = 0; i < m_scheduleViewModel.BGWList.Count; i++)
            {
                m_scheduleViewModel.BGWList[i].CancelAsync();
            }
            m_scheduleViewModel.EnableMigrate = true;
            m_scheduleViewModel.EnablePreview = true;

            // Don't uninitialize -- this should eventually set the static stop in csslib.
            // Go through each thread and stop each user?

            // CSMigrationwrapper mw = ((IntroViewModel)ViewModelPtrs[(int)ViewType.INTRO]).mw;
            // string ret = mw.UninitializeMailClient();

            EnableStop = !m_scheduleViewModel.EnableMigrate;
        }
    }

    public ICommand ExitAppCommand 
    {
        get;
        private set;
    }
    private void ExitApp()
    {
        Application.Current.Shutdown();
    }

    //

    private ObservableCollection<AccountResultsViewModel> accountResultsList = new ObservableCollection<AccountResultsViewModel>();
    public ObservableCollection<AccountResultsViewModel> AccountResultsList 
    {
        get { return accountResultsList; }
    }

    private ObservableCollection<ProblemInfo> accountProblemsList = new ObservableCollection<ProblemInfo>();
    public ObservableCollection<ProblemInfo> AccountProblemsList 
    {
        get { return accountProblemsList; }
    }

    private ObservableCollection<FolderResultsViewModel> m_folderResultsList = new ObservableCollection<FolderResultsViewModel>();
    public ObservableCollection<FolderResultsViewModel> FolderResultsList
    {
        get { return m_folderResultsList; }
    }

    public int PBValue 
    {
        get { return m_accountResults.PBValue; }
        set
        {
            if (value == m_accountResults.PBValue)
                return;
            m_accountResults.PBValue = value;
            OnPropertyChanged(new PropertyChangedEventArgs("PBValue"));
        }
    }

    public string Status 
    {
        get { return m_accountResults.Status; }
        set
        {
            if (value == m_accountResults.Status)
                return;
            m_accountResults.Status = value;
            OnPropertyChanged(new PropertyChangedEventArgs("Status"));
        }
    }

    public string AccountName 
    {
        get { return m_accountResults.AccountName; }
        set
        {
            if (value == m_accountResults.AccountName)
                return;
            m_accountResults.AccountName = value;
            OnPropertyChanged(new PropertyChangedEventArgs("AccountName"));
        }
    }

    public int CurrentItemNum 
    {
        get { return m_accountResults.CurrentItemNum; }
        set
        {
            if (value == m_accountResults.CurrentItemNum)
                return;
            m_accountResults.CurrentItemNum = value;
            OnPropertyChanged(new PropertyChangedEventArgs("CurrentItemNum"));
        }
    }

    public int TotalItemsToMigrate 
    {
        get { return m_accountResults.TotalItemsToMigrate; }
        set
        {
            if (value == m_accountResults.TotalItemsToMigrate)
                return;
            m_accountResults.TotalItemsToMigrate = value;
            OnPropertyChanged(new PropertyChangedEventArgs("TotalItemsToMigrate"));
        }
    }

    public string AcctProgress 
    {
        get { return m_accountResults.AcctProgress; }
        set
        {
            if (value == m_accountResults.AcctProgress)
                return;
            m_accountResults.AcctProgress = value;
            OnPropertyChanged(new PropertyChangedEventArgs("AcctProgress"));
        }
    }

    public string GlobalAcctProgressMsg
    {
        get { return m_accountResults.GlobalAcctProgressMsg; }
        set
        {
            if (value == m_accountResults.GlobalAcctProgressMsg)
                return;
            m_accountResults.GlobalAcctProgressMsg = value;
            OnPropertyChanged(new PropertyChangedEventArgs("GlobalAcctProgressMsg"));
        }
    }

    public int NumErrs 
    {
        get { return m_accountResults.NumErrs; }
        set
        {
            if (value == m_accountResults.NumErrs)
                return;
            m_accountResults.NumErrs = value;
            OnPropertyChanged(new PropertyChangedEventArgs("NumErrs"));
        }
    }

    public int NumWarns 
    {
        get { return m_accountResults.NumWarns; }
        set
        {
            if (value == m_accountResults.NumWarns)
                return;
            m_accountResults.NumWarns = value;
            OnPropertyChanged(new PropertyChangedEventArgs("NumWarns"));
        }
    }

    public int CurrentAccountSelection 
    {
        get { return m_accountResults.CurrentAccountSelection; }
        set
        {
            if (value == m_accountResults.CurrentAccountSelection)
                return;
            m_accountResults.CurrentAccountSelection = value;
            OnPropertyChanged(new PropertyChangedEventArgs("CurrentAccountSelection"));
        }
    }

    private bool openLogFileEnabled;
    public bool OpenLogFileEnabled 
    {
        get { return openLogFileEnabled; }
        set
        {
            openLogFileEnabled = value;
            OnPropertyChanged(new PropertyChangedEventArgs("OpenLogFileEnabled"));
        }
    }

    private bool enableStop;
    public bool EnableStop 
    {
        get { return enableStop; }
        set
        {
            enableStop = value;
            OnPropertyChanged(new PropertyChangedEventArgs("EnableStop"));
        }
    }

    public string SelectedTab 
    {
        get { return m_accountResults.SelectedTab; }
        set
        {
            if (value == m_accountResults.SelectedTab)
                return;
            m_accountResults.SelectedTab = value;
            OnPropertyChanged(new PropertyChangedEventArgs("SelectedTab"));
        }
    }

    public int AccountOnTab 
    {
        get { return m_AccountOnTab; }
        set { m_AccountOnTab = value; }
    }
}
}
