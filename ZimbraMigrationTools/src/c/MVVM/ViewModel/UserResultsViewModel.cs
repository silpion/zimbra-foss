﻿/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */

using MVVM.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Input;
using System.Windows;
using System;
using CssLib;

namespace MVVM.ViewModel
{
public class FolderResultsViewModel: BaseViewModel
{
    readonly FolderResults m_folderResults = new FolderResults("", "", "");

    public FolderResultsViewModel(string folderName, string typeName, string progressMsg)
    {
        using (LogBlock logblock = Log.NotTracing()?null: new LogBlock("FolderResultsViewModel.FolderResultsViewModel"))
        {
            this.FolderName = folderName;
            this.TypeName = typeName;
            this.FolderProgress = progressMsg;
        }
    }

    public string FolderName 
    {
        get { return m_folderResults.FolderName; }
        set
        {
            if (value == m_folderResults.FolderName)
                return;

            m_folderResults.FolderName = value;
            OnPropertyChanged(new PropertyChangedEventArgs("FolderName"));
        }
    }

    public string TypeName 
    {
        get { return m_folderResults.TypeName; }
        set
        {
            if (value == m_folderResults.TypeName)
                return;

            m_folderResults.TypeName = value;
            OnPropertyChanged(new PropertyChangedEventArgs("TypeName"));
        }
    }

    public string FolderProgress 
    {
        get { return m_folderResults.FolderProgress; }
        set
        {
            if (value == m_folderResults.FolderProgress)
                return;

            m_folderResults.FolderProgress = value;
            OnPropertyChanged(new PropertyChangedEventArgs("FolderProgress"));
        }
    }
}
}
