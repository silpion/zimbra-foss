﻿/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2011, 2012, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */

namespace MVVM.Model
{
using System;

public class AccountResults
{
    internal AccountResults()
    {
        this.AccountName = "";
        this.Status = "";

        this.PBValue = 0;
        this.AcctProgress = "";
        this.NumErrs = 0;
        this.NumWarns = 0;
        this.EnableStop = false;
        this.CurrentItemNum = 0;
        this.TotalItemsToMigrate = 0;
    }

    public int PBValue 
    {
        get;
        set;
    }

    public string Status 
    {
        get;
        set;
    }

    public string AccountName 
    {
        get;
        set;
    }

    public int CurrentItemNum 
    {
        get;
        set;
    }

    public int TotalItemsToMigrate 
    {
        get;
        set;
    }

    public string AcctProgress 
    {
        get;
        set;
    }

    public string GlobalAcctProgressMsg
    {
        get;
        set;
    }

    public int NumErrs 
    {
        get;
        set;
    }

    public int NumWarns 
    {
        get;
        set;
    }

    public int CurrentAccountSelection 
    {
        get;
        set;
    }

    public bool OpenLogFileEnabled 
    {
        get;
        set;
    }

    public bool EnableStop 
    {
        get;
        set;
    }

    public string SelectedTab 
    {
        get;
        set;
    }
}

public class FolderResults
{
    internal FolderResults(string folderName, string typeName, string sProgress)
    {
        this.FolderName = folderName;
        this.TypeName = typeName;
        this.FolderProgress = sProgress;
    }

    public string FolderName 
    {
        get;
        set;
    }

    public string TypeName 
    {
        get;
        set;
    }

    public string FolderProgress 
    {
        get;
        set;
    }
}
}
